<?php include('include/header.php'); ?>

<section class="single-product-section">
	<div class="container-fluid">
		<div class="row">
			<div class="col-lg-12">
				<div class="single-zoom-wrapper">
					<div class="img-zoom-div">
						<div class="left-zoom-img">
							<div class="zoom-img-carousel">  
								<div class="zoom-img-main">
									<img class="xzoom" id="xzoom-default" src="img/single-product/preview/01_b_car.jpg" xoriginal="img/single-product/original/01_b_car.jpg" />
								</div>
								<div class="middle-zoom-img">
									<div class="xzoom-thumbs">
										<a href="img/single-product/original/01_b_car.jpg">
											<img class="xzoom-gallery" width="80" src="img/single-product/thumbs/01_b_car.jpg"  xpreview="img/single-product/preview/01_b_car.jpg">
										</a>
										<a href="img/single-product/original/02_o_car.jpg">
											<img class="xzoom-gallery" width="80" src="img/single-product/preview/02_o_car.jpg">
										</a>
										<a href="img/single-product/original/03_r_car.jpg">
											<img class="xzoom-gallery" width="80" src="img/single-product/preview/03_r_car.jpg">
										</a>
										<a href="img/single-product/original/04_g_car.jpg">
											<img class="xzoom-gallery" width="80" src="img/single-product/preview/04_g_car.jpg">
										</a>
										<a href="img/single-product/original/02_o_car.jpg">
											<img class="xzoom-gallery" width="80" src="img/single-product/preview/02_o_car.jpg">
										</a>
										<a href="img/single-product/original/03_r_car.jpg">
											<img class="xzoom-gallery" width="80" src="img/single-product/preview/03_r_car.jpg">
										</a>
										<a href="img/single-product/original/04_g_car.jpg">
											<img class="xzoom-gallery" width="80" src="img/single-product/preview/04_g_car.jpg">
										</a>
									</div>
								</div> 
							</div> 
						</div> 
						<div class="btn-buy-wrapper">
							<button type="button" class="btn btn-primary btn-buy-single" data-toggle="modal" data-target="#buySingleModal">
								Buy Now
							</button> 
							<button type="button" class="btn btn-secondary info-buy-btn" id="example" data-toggle="tooltip" data-placement="bottom" title="Tooltip on bottom">
							  	<i class="fas fa-info"></i>
							</button> 
						</div> 
					</div> 
					<div class="right-zoom-img">
						<div class="single-product-info">
							<h1 class="single-product-title">Redmi Note 7S (Sapphire Blue, 64 GB) (4 GB RAM) Black</h1>
							<div class="rating-review">
								<span class="rating-span">4.2 <i class="far fa-star"></i></span>
								<span class="rating-span-text">1250 Ratings</span>
								<span class="review-span">120 Reviews</span>
								<span class="verified-span">
									<img src="img/single-product/verified.png" alt="">
								</span>
								<span class="wishlist-icon">
									<i class="far fa-heart"></i>
								</span>
							</div>
							<div class="price-div">
								<p class="main-price">RS. 1,40,000</p>
								<p class="discounted-price">RS. 1400</p>
								<p class="off-price">10% OFF
									<button type="button" class="" data-toggle="tooltip" data-placement="bottom" title="Tooltip on bottom">
										<i class="fas fa-info"></i>
									</button>
								</p>
							</div>
							<div class="product-desc">
								<p class="desc-p">
									Lorem ipsum dolor sit amet, consectetur adipisicing elit. Sint quibusdam, excepturi odit veritatis aliquid sed nulla atque, tempore totam voluptate placeat harum repellendus ipsa natus quaerat nam corrupti autem maxime!
								</p>
								<ul class="product-desc-ul">
									<li>
										Product Stock : <span>500</span> <a href="" data-toggle="modal" data-target="#viewStock"> View Stock Detail <i class="fas fa-chevron-right"></i> </a>
										<div class="modal fade bd-example-modal-lg" id="viewStock" tabindex="-1" role="dialog" aria-labelledby="viewStockTitle" aria-hidden="true">
											<div class="modal-dialog modal-dialog-centered modal-lg" role="document">
												<div class="modal-content">
													<div class="modal-header">
														<h5 class="modal-title" id="exampleModalCenterTitle">Stock List</h5>
														<button type="button" class="close" data-dismiss="modal" aria-label="Close">
														<span aria-hidden="true">&times;</span>
														</button>
													</div>
													<div class="modal-body">
														<table class="table table-bordered">
															<thead>
																<tr>
																	<th scope="col">S.N.</th>
																	<th scope="col">Attribute</th>
																	<th scope="col">Quantity</th>
																</tr>
															</thead>
															<tbody>
																<tr>
																	<td>1</td>
																	<td>RED</td>
																	<td>25</td>
																</tr>
																<tr>
																	<td>2</td>
																	<td>RED</td>
																	<td>25</td>
																</tr>
																<tr>
																	<td>3</td>
																	<td>RED</td>
																	<td>25</td>
																</tr>
															</tbody>
														</table>
													</div>
												</div>
											</div>
										</div>
									</li>
									<li>
										Status: <a href="" data-toggle="modal" data-target="#viewStatus"> View Closing Time Detail <i class="fas fa-chevron-right"></i></a>
										<div class="modal fade bd-example-modal-lg" id="viewStatus" tabindex="-1" role="dialog" aria-labelledby="viewStatusTitle" aria-hidden="true">
											<div class="modal-dialog modal-dialog-centered modal-lg" role="document">
												<div class="modal-content">
													<div class="modal-header">
														<h5 class="modal-title" id="exampleModalCenterTitle">Closing Time List</h5>
														<button type="button" class="close" data-dismiss="modal" aria-label="Close">
														<span aria-hidden="true">&times;</span>
														</button>
													</div>
													<div class="modal-body">
														<table class="table table-bordered status-table">
															<thead>
																<tr>
																	<th scope="col">S.N.</th>
																	<th scope="col">AREA</th>
																	<th scope="col">CLOSING TIME</th>
																	<th scope="col">Status</th>
																</tr>
															</thead>
															<tbody>
																<tr>
																	<td>1</td>
																	<td>Provience 1</td>
																	<td>2076/06/13</td>
																	<td><span class="closed">Closed</span></td>
																</tr>
																<tr>
																	<td>2</td>
																	<td>Provience 2</td>
																	<td>2076/06/15</td>
																	<td><span class="open">Open</span></td>
																</tr>
															</tbody>
														</table>
													</div>
												</div>
											</div>
										</div>
									</li>
								</ul>
								<div class="location-form">
									<form action="">
										<div class="row">
											<div class="col-lg-3">
												<div class="form-group">
													<label for="selectArea" class="area-lbl-single-prt">Area</label>
													<select name="" class="form-control form-control-lg" id="selectArea">
														<option value="0">Select Area</option>
														<option value="1">kathmandu</option>
														<option value="2">Lalitpur</option>
													</select>
												</div>
											</div>
											<div class="col-lg-3">
												<div class="form-group">
													<label for="selectAttribute" class="area-lbl-single-prt">Attribute</label>
													<select name="" class="form-control form-control-lg" id="selectAttribute">
														<option value="0">Select Area</option>
														<option value="1">Black</option>
														<option value="2">XL</option>
													</select>
												</div>
											</div>
											<div class="col-lg-3">
												<div class="form-group">
													<label for="textQuantity" class="area-lbl-single-prt">Quantity</label>
													<input type="number" class="form-control form-control-lg" id="textQuantity" placeholder="eg: 1">
												</div>
											</div>
											<div class="col-lg-3">
												<button type="button" class="btn btn-primary btn-demand-single" data-toggle="modal" data-target="#demandSingleModal">
												Demand
												</button>
											</div>
										</div>
									</form>
								</div>
								<div class="alert alert-primary alert-demand" role="alert">
									You will need to sign in or register as demander or bidder to demand/bid the product.
								</div>
							</div>
						</div>
						<div class="modal fade" id="demandSingleModal" tabindex="-1" role="dialog" aria-labelledby="demandSingleModalTitle" aria-hidden="true">
							<div class="modal-dialog modal-dialog-centered modal-lg" role="document">
								<div class="modal-content">
									<div class="modal-header">
										<h5 class="modal-title" id="demandSingleModalTitle">Demand Request List</h5>
										<button type="button" class="close" data-dismiss="modal" aria-label="Close">
										<span aria-hidden="true">&times;</span>
										</button>
									</div>
									<div class="modal-body">
										<div class="demand-div-wrapper">
											<div class="demand-div-first">
												<h3>Your Demanding Product Description</h3>
												<div class="demand-list-div">
													<p>Product Name: <span>Redmi Note 7S (Sapphire Blue, 64 GB) (4 GB RAM) Black</span></p>
													<p>Area: <span>Kathmandu</span></p>
													<p>Attribute: <span>Black</span></p>
													<p>Quantity: <span>50 </span></p>
												</div>
											</div>
											<div class="demand-div-second">
												<div class="alert alert-info" role="alert">
													Are you sure you want to continue ?
												</div>
											</div>
										</div>
									</div>
									<div class="modal-footer"> 
										<button type="button" class="btn close-btn-modal" data-dismiss="modal">Close</button>
										<button type="button" class="btn save-btn-modal">Save changes</button>
									</div>
								</div>
							</div>
						</div>
						<div class="product-desc-tabs">
							<ul class="nav nav-tabs" id="myTab" role="tablist">
								<li class="nav-item">
									<a class="nav-link active" id="description-tab" data-toggle="tab" href="#description" role="tab" aria-controls="description" aria-selected="true">Descriptions</a>
								</li>
								<li class="nav-item">
									<a class="nav-link" id="feature-tab" data-toggle="tab" href="#feature" role="tab" aria-controls="feature" aria-selected="false">Features</a>
								</li>
								<li class="nav-item">
									<a class="nav-link" id="demand-tab" data-toggle="tab" href="#demand" role="tab" aria-controls="demand" aria-selected="false">Active Demand</a>
								</li>
								<li class="nav-item">
									<a class="nav-link" id="history-tab" data-toggle="tab" href="#history" role="tab" aria-controls="history" aria-selected="false">History Log</a>
								</li>
							</ul>
							<div class="tab-content" id="myTabContent">
								<div class="tab-pane fade show active" id="description" role="tabpanel" aria-labelledby="description-tab">
									<p>
										Vivo presents Vivo Y17 mobile sporting 6.35 Inches display in Mineral Blue colour. The device functions on Android v9.0 (Pie) Operating System, and for powerful performance, Vivo Y17 comes with combination of 2.3 GHz Octa Core processors.
									</p>
									<p>
										Vivo Y17 128 GB Camera, Configuration, Battery & More
										The Vivo Y17 Mineral Blue comprises of an internal storage of 128 GB. This smartphone has a RAM of 4 GB to provide lag-free experience. When it comes to its camera, the Vivo Y17 Mineral Blue boasts a good primary camera set up of 13 MP + 8 MP + 2 MP and 20 MP Secondary Camera for clicking stunning pictures. Battery being one of crucial feature of any smartphone, has been taken care of by Vivo. The Vivo Y17 Mineral Blue is backed with a powerful battery of 5000 mAh Capacity that work for long hours. Weighing, 190.5 gm, Vivo Y17 128 GB (4 GB RAM) in Mineral Blue also features 4G VoLTE Network Support and Bluetooth, and a lot more as network connectivity options. The dimensions of the device is 159.43 x 76.77 x 8.92 mm. Undoubtedly, the Vivo Y17 128 GB (Mineral Blue) offers an exceptional design and a dedicated microSD. So, buy Vivo Y17 128 GB (Mineral Blue, 4 GB) at the best price in India here and update to the latest smartphone today!
										Vivo presents Vivo Y17 mobile sporting 6.35 Inches display in Mineral Blue colour. The device functions on Android v9.0 (Pie) Operating System, and for powerful performance, Vivo Y17 comes with combination of 2.3 GHz Octa Core processors.
									</p>
									<p>
										The Vivo Y17 Mineral Blue comprises of an internal storage of 128 GB. This smartphone has a RAM of 4 GB to provide lag-free experience. When it comes to its camera, the Vivo Y17 Mineral Blue boasts a good primary camera set up of 13 MP + 8 MP + 2 MP and 20 MP Secondary Camera for clicking stunning pictures. Battery being one of crucial feature of any smartphone, has been taken care of by Vivo. The Vivo Y17 Mineral Blue is backed with a powerful battery of 5000 mAh Capacity that work for long hours. Weighing, 190.5 gm, Vivo Y17 128 GB (4 GB RAM) in Mineral Blue also features 4G VoLTE Network Support and Bluetooth, and a lot more as network connectivity options. The dimensions of the device is 159.43 x 76.77 x 8.92 mm. Undoubtedly, the Vivo Y17 128 GB (Mineral Blue) offers an exceptional design and a dedicated microSD. So, buy Vivo Y17 128 GB (Mineral Blue, 4 GB) at the best price in India here and update to the latest smartphone today!
										Vivo presents Vivo Y17 mobile sporting 6.35 Inches display in Mineral Blue colour. The device functions on Android v9.0 (Pie) Operating System, and for powerful performance, Vivo Y17 comes with combination of 2.3 GHz Octa Core processors.
									</p>
									<p>
										Vivo Y17 128 GB Camera, Configuration, Battery & More
										The Vivo Y17 Mineral Blue comprises of an internal storage of 128 GB. This smartphone has a RAM of 4 GB to provide lag-free experience. When it comes to its camera, the Vivo Y17 Mineral Blue boasts a good primary camera set up of 13 MP + 8 MP + 2 MP and 20 MP Secondary Camera for clicking stunning pictures. Battery being one of crucial feature of any smartphone, has been taken care of by Vivo. The Vivo Y17 Mineral Blue is backed with a powerful battery of 5000 mAh Capacity that work for long hours. Weighing, 190.5 gm, Vivo Y17 128 GB (4 GB RAM) in Mineral Blue also features 4G VoLTE Network Support and Bluetooth, and a lot more as network connectivity options. The dimensions of the device is 159.43 x 76.77 x 8.92 mm. Undoubtedly, the Vivo Y17 128 GB (Mineral Blue) offers an exceptional design and a dedicated microSD. So, buy Vivo Y17 128 GB (Mineral Blue, 4 GB) at the best price in India here and update to the latest smartphone today!
										Vivo presents Vivo Y17 mobile sporting 6.35 Inches display in Mineral Blue colour. The device functions on Android v9.0 (Pie) Operating System, and for powerful performance, Vivo Y17 comes with combination of 2.3 GHz Octa Core processors.
									</p>
								</div>
								<div class="tab-pane fade" id="feature" role="tabpanel" aria-labelledby="feature-tab">
									<ul class="feature-single-user-ul">
										<li>Touch Screen: <span> Yes</span></li>
										<li>Screen Size: <span> 6.35 Inches</span></li>
										<li>Screen Resolution: <span> 720 x 1544 Pixels</span></li>
										<li>RAM: <span> 4 GB</span></li>
										<li>ROM: <span> 128 GB</span></li>
										<li>Card Slot: <span> Yes</span></li>
										<li>Operating System: <span> Android v9.0 (Pie)</span></li>
										<li>Processor Speed: <span> 2.3 GHz</span></li>
										<li>RAM: <span> 4 GB</span></li>
										<li>ROM: <span> 128 GB</span></li>
										<li>Card Slot: <span> Yes</span></li>
									</ul>
								</div>
								<div class="tab-pane fade" id="demand" role="tabpanel" aria-labelledby="demand-tab">
									<div class="filter-div">
										<ul>
											<li>
												<div class="form-group">
													<select class="form-control-lg" id="exampleFormControlSelect1">
														<option>Select Area</option>
														<option>Provience 1</option>
														<option>Provience 2</option>
														<option>Provience 3</option>
														<option>Provience 4</option>
														<option>Provience 5</option>
														<option>Provience 6</option>
														<option>Provience 7</option>
													</select>
												</div>
											</li>
											<li>
												<div class="form-group">
													<select class="form-control-lg" id="exampleFormControlSelect1">
														<option>Attribute</option>
														<option>Red</option>
														<option>Green</option>
														<option>Blue</option>
														<option>S</option>
														<option>M</option>
														<option>L</option>
														<option>XL</option>
													</select>
												</div>
											</li>
										</ul>
									</div>
									<div class="table-responsive demand-table-single-user">
										<table class="table table-bordered">
											<tr>
												<th>SN</th>
												<th>Area</th>
												<th>Attribute</th>
												<th>Quantity</th>
												<th>Price</th>
												<th>Last Demand At</th>
												<th>View Demand Pattern</th>
											</tr>
											<tr>
												<td>1.</td>
												<td>Butwal</td>
												<td>XXL</td>
												<td>
													<form action="" class="quantity-form">
														<div class="input-group input-group-lg">
															<div class="input-group-prepend">
																<span class="input-group-text" id="addon-wrapping">100</span>
															</div>
															<input type="number" class="form-control input-width" aria-label="number" aria-describedby="addon-wrapping">
															<div class="input-group-append">
																<button class="btn btn-outline-secondary" type="button" id="button-addon2">Submit</button>
															</div>
														</div>
													</form>
												</td>
												<td>RS. 1,00,000</td>
												<td>2076-10-28 (30 min ago)</td>
												<td>
													<a href="#" class="view-a" data-toggle="modal" data-target="#viewDemandPattern">View
														<i class="fas fa-chevron-right"></i>
													</a>
													<div class="modal fade bd-example-modal-lg" id="viewDemandPattern" tabindex="-1" role="dialog" aria-labelledby="viewStockTitle" aria-hidden="true">
														<div class="modal-dialog modal-dialog-centered modal-lg" role="document">
															<div class="modal-content">
																<div class="modal-header">
																	<h5 class="modal-title" id="exampleModalCenterTitle">Stock List</h5>
																	<button type="button" class="close" data-dismiss="modal" aria-label="Close">
																	<span aria-hidden="true">&times;</span>
																	</button>
																</div>
																<div class="modal-body">
																	<table class="table table-bordered">
																		<thead>
																			<tr>
																				<th scope="col">S.N.</th>
																				<th scope="col">Demand Number</th>
																				<th scope="col">Quantity</th>
																				<th scope="col">Demand At</th>
																				<th scope="col">Actions</th>
																			</tr>
																		</thead>
																		<tbody>
																			<tr>
																				<td>1</td>
																				<td>D100</td>
																				<td>25</td>
																				<td>2076-01-08 (1 hour ago)</td>
																				<td><a href="#" class="btn btn-primary">Request</a></td>
																			</tr>
																			<tr>
																				<td>2</td>
																				<td>D100</td>
																				<td>25</td>
																				<td>2076-01-08 (1 hour ago)</td>
																				<td><a href="#" class="btn btn-primary">Request</a></td>
																			</tr>
																			<tr>
																				<td>3</td>
																				<td>D100</td>
																				<td>25</td>
																				<td>2076-01-08 (1 hour ago)</td>
																				<td><a href="#" class="btn btn-primary">Request</a></td>
																			</tr>
																		</tbody>
																	</table>
																</div>
															</div>
														</div>
													</div>
												</td>
											</tr>
											<tr>
												<td>2.</td>
												<td>Birtamode</td>
												<td>XXXL</td>
												<td>
													<form action="" class="quantity-form">
														<div class="input-group input-group-lg">
															<div class="input-group-prepend">
																<span class="input-group-text" id="addon-wrapping">200</span>
															</div>
															<input type="number" class="form-control input-width" aria-label="number" aria-describedby="addon-wrapping">
															<div class="input-group-append">
																<button class="btn btn-outline-secondary" type="button" id="button-addon2">Submit</button>
															</div>
														</div>
													</form>
												</td>
												<td>RS. 1,00,000</td>
												<td>2076-01-08 (1 hour ago)</td>
												<td>
													<a href="#" class="view-a" data-toggle="modal" data-target="#viewDemandPattern">View
														<i class="fas fa-chevron-right"></i>
													</a>
													<div class="modal fade bd-example-modal-lg" id="viewDemandPattern" tabindex="-1" role="dialog" aria-labelledby="viewStockTitle" aria-hidden="true">
														<div class="modal-dialog modal-dialog-centered modal-lg" role="document">
															<div class="modal-content">
																<div class="modal-header">
																	<h5 class="modal-title" id="exampleModalCenterTitle">Stock List</h5>
																	<button type="button" class="close" data-dismiss="modal" aria-label="Close">
																	<span aria-hidden="true">&times;</span>
																	</button>
																</div>
																<div class="modal-body">
																	<table class="table table-bordered">
																		<thead>
																			<tr>
																				<th scope="col">S.N.</th>
																				<th scope="col">Demand Number</th>
																				<th scope="col">Quantity</th>
																				<th scope="col">Demand At</th>
																				<th scope="col">Actions</th>
																			</tr>
																		</thead>
																		<tbody>
																			<tr>
																				<td>1</td>
																				<td>D100</td>
																				<td>25</td>
																				<td>2076-01-08 (1 hour ago)</td>
																				<td><a href="#" class="btn btn-primary">Request</a></td>
																			</tr>
																			<tr>
																				<td>2</td>
																				<td>D100</td>
																				<td>25</td>
																				<td>2076-01-08 (1 hour ago)</td>
																				<td><a href="#" class="btn btn-primary">Request</a></td>
																			</tr>
																			<tr>
																				<td>3</td>
																				<td>D100</td>
																				<td>25</td>
																				<td>2076-01-08 (1 hour ago)</td>
																				<td><a href="#" class="btn btn-primary">Request</a></td>
																			</tr>
																		</tbody>
																	</table>
																</div>
															</div>
														</div>
													</div>
												</td>
											</tr>
											<tr>
												<td>3.</td>
												<td>Pokhara</td>
												<td>XXXL</td>
												<td>
													<form action="" class="quantity-form">
														<div class="input-group input-group-lg">
															<div class="input-group-prepend">
																<span class="input-group-text" id="addon-wrapping">10</span>
															</div>
															<input type="number" class="form-control input-width" aria-label="number" aria-describedby="addon-wrapping">
															<div class="input-group-append">
																<button class="btn btn-outline-secondary" type="button" id="button-addon2">Submit</button>
															</div>
														</div>
													</form>
												</td>
												<td>RS. 5,000</td>
												<td>2076-09-20 (30 min ago)</td>
												<td>
													<a href="#" class="view-a" data-toggle="modal" data-target="#viewDemandPattern">View
														<i class="fas fa-chevron-right"></i>
													</a>
													<div class="modal fade bd-example-modal-lg" id="viewDemandPattern" tabindex="-1" role="dialog" aria-labelledby="viewStockTitle" aria-hidden="true">
														<div class="modal-dialog modal-dialog-centered modal-lg" role="document">
															<div class="modal-content">
																<div class="modal-header">
																	<h5 class="modal-title" id="exampleModalCenterTitle">Stock List</h5>
																	<button type="button" class="close" data-dismiss="modal" aria-label="Close">
																	<span aria-hidden="true">&times;</span>
																	</button>
																</div>
																<div class="modal-body">
																	<table class="table table-bordered">
																		<thead>
																			<tr>
																				<th scope="col">S.N.</th>
																				<th scope="col">Demand Number</th>
																				<th scope="col">Quantity</th>
																				<th scope="col">Demand At</th>
																				<th scope="col">Actions</th>
																			</tr>
																		</thead>
																		<tbody>
																			<tr>
																				<td>1</td>
																				<td>D100</td>
																				<td>25</td>
																				<td>2076-01-08 (1 hour ago)</td>
																				<td><a href="#" class="btn btn-primary">Request</a></td>
																			</tr>
																			<tr>
																				<td>2</td>
																				<td>D100</td>
																				<td>25</td>
																				<td>2076-01-08 (1 hour ago)</td>
																				<td><a href="#" class="btn btn-primary">Request</a></td>
																			</tr>
																			<tr>
																				<td>3</td>
																				<td>D100</td>
																				<td>25</td>
																				<td>2076-01-08 (1 hour ago)</td>
																				<td><a href="#" class="btn btn-primary">Request</a></td>
																			</tr>
																		</tbody>
																	</table>
																</div>
															</div>
														</div>
													</div>
												</td>
											</tr>
										</table>
									</div>
								</div>
								<div class="tab-pane fade" id="history" role="tabpanel" aria-labelledby="history-tab">
									<div class="table-responsive demand-table-single-user">
										<table class="table table-bordered">
											<tr>
												<th>SN</th>
												<th>Area</th>
												<th>Quantity</th>
												<th>Size / Color</th>
												<th>Demand on</th>
											</tr>
											<tr>
												<td>1.</td>
												<td>Butwal</td>
												<td>120</td>
												<td>XXL</td>
												<td>2076-10-28</td>
											</tr>
											<tr>
												<td>2.</td>
												<td>Birtamode</td>
												<td>40</td>
												<td>XXXL</td>
												<td>2076-01-08</td>
											</tr>
											<tr>
												<td>3.</td>
												<td>Pokhara</td>
												<td>20</td>
												<td>M</td>
												<td>2076-09-20</td>
											</tr>
										</table>
									</div>
								</div>
							</div>
						</div>
						<div class="app-logo-wrap p-b-20"> 
                            <div class="inner-review-wrapper">
                            	<div class="app-review">
                            		<div class="row">
                            			<div class="col-lg-5">
                            				<p class="topic">Ratings &amp; Reviews</p>
                            			</div>
                            			<div class="col-lg-7">
                            				<div class="rate-review">
				                            	<label for="">Rate product:</label>
				                            	<ul class="list-unstyled stars">
				                                    <li><i class="fas fa-star"></i></li>
				                                    <li><i class="fas fa-star"></i></li>
				                                    <li><i class="fas fa-star"></i></li>
				                                    <li><i class="fas fa-star"></i></li>
				                                    <li><i class="fas fa-star"></i></li>
				                                </ul>
				                            </div>
                            			</div>
                            		</div> 
                                </div>
                            	<div class="row">
                            		<div class="col-lg-5">  
                            			<div class="row">  
		                            		<div class="col-lg-3 col-md-3">
		                            			<p class="app-rate"><span>4.7</span>out of 5</p>
		                            		</div>
		                            		<div class="col-lg-4 col-md-3">
		                            			<div class="stars-group float-left mr-3">
			                                        <ul class="list-unstyled stars">
			                                            <li class="active"><i class="fas fa-star"></i></li>
			                                            <li class="active"><i class="fas fa-star"></i></li>
			                                            <li class="active"><i class="fas fa-star"></i></li>
			                                            <li class="active"><i class="fas fa-star"></i></li>
			                                            <li class="active"><i class="fas fa-star"></i></li>
			                                        </ul>
			                                        <ul class="list-unstyled stars">
			                                            <li class="active"><i class="fas fa-star"></i></li>
			                                            <li class="active"><i class="fas fa-star"></i></li>
			                                            <li class="active"><i class="fas fa-star"></i></li>
			                                            <li class="active"><i class="fas fa-star"></i></li>
			                                            <li><i class="fas fa-star"></i></li>
			                                        </ul>
			                                        <ul class="list-unstyled stars">
			                                            <li class="active"><i class="fas fa-star"></i></li>
			                                            <li class="active"><i class="fas fa-star"></i></li>
			                                            <li class="active"><i class="fas fa-star"></i></li>
			                                            <li><i class="fas fa-star"></i></li>
			                                            <li><i class="fas fa-star"></i></li>
			                                        </ul>
			                                        <ul class="list-unstyled stars">
			                                            <li class="active"><i class="fas fa-star"></i></li>
			                                            <li class="active"><i class="fas fa-star"></i></li>
			                                            <li><i class="fas fa-star"></i></li>
			                                            <li><i class="fas fa-star"></i></li>
			                                            <li><i class="fas fa-star"></i></li>
			                                        </ul>
			                                        <ul class="list-unstyled stars">
			                                            <li class="active"><i class="fas fa-star"></i></li>
			                                            <li><i class="fas fa-star"></i></li>
			                                            <li><i class="fas fa-star"></i></li>
			                                            <li><i class="fas fa-star"></i></li>
			                                            <li><i class="fas fa-star"></i></li>
			                                        </ul>
			                                    </div>
		                            		</div>
		                            		<div class="col-lg-5 col-md-6"> 
			                                    <div class="progress-bar-group float-right"> 
			                                        <div class="progress">
			                                            <div class="progress-bar" role="progressbar" style="width: 75%" aria-valuenow="75" aria-valuemin="0" aria-valuemax="100"></div>
			                                        </div>
			                                        <div class="progress">
			                                            <div class="progress-bar" role="progressbar" style="width: 25%" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100"></div>
			                                        </div>
			                                        <div class="progress">
			                                            <div class="progress-bar" role="progressbar" style="width: 50%" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100"></div>
			                                        </div>
			                                        <div class="progress">
			                                            <div class="progress-bar" role="progressbar" style="width: 30%" aria-valuenow="30" aria-valuemin="0" aria-valuemax="100"></div>
			                                        </div>
			                                        <div class="progress">
			                                            <div class="progress-bar" role="progressbar" style="width: 15%" aria-valuenow="15" aria-valuemin="0" aria-valuemax="100"></div>
			                                        </div>
			                                    </div> 
		                            		</div>
	                            		</div> 
	                            	</div>
	                            	<div class="col-lg-7">
	                            		
			                            <div class="write-review">
			                            	<form action="">
			                            		<div class="form-group">
			                            			<textarea name="" id="" class="form-control" cols="10" placeholder="Write review for product" rows="3"></textarea> 
			                            			<button class="btn btn-review btn-success">Submit</button>
			                            		</div>
			                            	</form>
			                            </div>
	                            	</div>
                            	</div> 
                            </div> 
                        </div>
                        <div class="single-product-review"> 
	                        <div class="single-product-review-item">
	                        	<div class="single-review-wrapper">
	                        		<div class="review-title">
	                        			<div class="review-title-first">
	                        				<span>4.5 <i class="far fa-star"></i></span>
	                        				<h5>Gopal pratap singh bahadur basnet</h5>
	                        			</div> 
	                        			<p>2019 Nov 05</p>
	                        		</div>
	                        		<div class="review-info">
	                        			Lorem ipsum dolor sit amet, consectetur adipisicing elit. Deleniti, sed. Pariatur quia quos porro eligendi molestiae unde aspernatur impedit cum modi voluptate quod maiores, vitae, quas neque nulla beatae rerum!
	                        			<div class="response-review"> 
	                        				<div class="response-div"> 
	                        					<span class="title"><i class="fas fa-reply-all fa-rotate-180"></i> Response From Vendor: Gopal Basnet</span>
	                        					<span class="date">22 days ago</span>
	                        				</div> 
	                        				<p class="info">
	                        					Lorem ipsum dolor sit amet, consectetur adipisicing elit. Sit, tempore animi similique impedit doloribus placeat ipsum voluptates unde maiores minus dolor voluptatum ducimus accusamus cum earum autem. Voluptates, saepe, tempora.
	                        				</p>
	                        				
	                        			</div>
	                        		</div>
	                        	</div>
	                        </div>

	                        <div class="single-product-review-item">
	                        	<div class="single-review-wrapper">
	                        		<div class="review-title">
	                        			<div class="review-title-first">
	                        				<span>4.5 <i class="far fa-star"></i></span>
	                        				<h5>Gopal pratap singh bahadur basnet</h5>
	                        			</div> 
	                        			<p>2019 Nov 05</p>
	                        		</div>
	                        		<div class="review-info">
	                        			Lorem ipsum dolor sit amet, consectetur adipisicing elit. Deleniti, sed. Pariatur quia quos porro eligendi molestiae unde aspernatur impedit cum modi voluptate quod maiores, vitae, quas neque nulla beatae rerum!
	                        			<div class="response-review"> 
	                        				<div class="response-div"> 
	                        					<span class="title"><i class="fas fa-reply-all fa-rotate-180"></i> Response From Vendor: Gopal Basnet</span>
	                        					<span class="date">22 days ago</span>
	                        				</div> 
	                        				<p class="info">
	                        					Lorem ipsum dolor sit amet, consectetur adipisicing elit. Sit, tempore animi similique impedit doloribus placeat ipsum voluptates unde maiores minus dolor voluptatum ducimus accusamus cum earum autem. Voluptates, saepe, tempora.
	                        				</p>
	                        				
	                        			</div>
	                        		</div>
	                        	</div>
	                        </div>
	                    </div>
					</div>
				</div> 
			</div>
		</div>
	</div>
	<div class="modal fade" id="buySingleModal" tabindex="-1" role="dialog" aria-labelledby="buySingleModalTitle" aria-hidden="true">
		<div class="modal-dialog modal-dialog-centered modal-lg modal-dialog-scrollable" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<h5 class="modal-title" id="buySingleModalTitle">Instant Buy</h5>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true" class="color-fff">&times;</span>
					</button>
				</div>
				<div class="modal-body">
					<div class="buy-div-wrapper">
						<div class="buy-div-first">
							<h3>You are buying</h3>
							<div class="buy-list-div">
								<p>Product Name: <span>Redmi Note 7S (Sapphire Blue, 64 GB) (4 GB RAM) Black</span></p>
								<form action="">
									<div class="row">
										<div class="col-lg-6">
											<div class="form-group">
												<label for="selectArea" class="area-lbl-single-prt">Area</label>
												<select name="" class="form-control form-control-lg" id="selectArea">
													<option value="0">Select Area</option>
													<option value="1">kathmandu</option>
													<option value="2">Lalitpur</option>
												</select>
											</div>
										</div>
										<div class="col-lg-6">
											<div class="form-group">
												<label for="selectAttribute" class="area-lbl-single-prt">Attribute</label>
												<select name="" class="form-control form-control-lg" id="selectAttribute">
													<option value="0">Select Attribute</option>
													<option value="1">Black</option>
													<option value="2">XL</option>
												</select>
											</div>
										</div>
									</div>
								</form>
							</div>
						</div>
						<div class="buy-div-second">
							<div class="alert alert-info" role="alert">
								Shipping Address: New Baneshwor, Kathmandu, NEPAL.
								<button type="button" class="btn save-btn-modal m-l-10">Change</button> 
							</div> 
						</div>
						<div class="buy-div-third">
							<div class="map-search-div"> 
								<form action="" class="search-map-input">
									<div class="input-group mb-3">
										<input type="text" class="form-control" placeholder="eg: koteshwor" aria-label="eg: koteshwor" aria-describedby="eg: koteshwor">
										<div class="input-group-append">
											<button class="btn save-btn-modal" type="button">Change</button>
										</div>
									</div> 
								</form> 
								<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d7065.462890258502!2d85.33544097677283!3d27.69469403091117!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x39eb199a06c2eaf9%3A0xc5670a9173e161de!2sNew%20Baneshwor%2C%20Kathmandu%2044600!5e0!3m2!1sen!2snp!4v1571905456884!5m2!1sen!2snp" width="100%" height="250" frameborder="0" style="border:0;" allowfullscreen=""> 
								</iframe>
							</div>
						</div>
					</div> 
				</div>
				<div class="modal-footer"> 
					<button type="button" class="btn close-btn-modal" data-dismiss="modal">Close</button>
					<button type="button" class="btn save-btn-modal">Save changes</button>
				</div>
			</div>
		</div>
	</div>
	<div class="modal fade bd-example-modal-lg modal-lg-address" id="exampleModalCenter" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
		<div class="modal-dialog modal-dialog-centered modal-lg" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<h5 class="modal-title" id="exampleModalLongTitle">Modal title</h5>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
					</button>
				</div>
				<div class="modal-body">
					<form action="">
						<div class="form-group">
							<label for="">Choose Address</label>
							<select name="" id="" class="form-control">
								<option value="">select Address</option>
								<option value="">Provience-1</option>
								<option value="">Provience-2</option>
								<option value="">Provience-3</option>
								<option value="">Provience-4</option>
								<option value="">Provience-5</option>
								<option value="">Provience-6</option>
								<option value="">Provience-7</option>
							</select>
						</div>
					</form>
				</div>
				<div class="modal-footer">
					<i class="fas fas-door"></i>
					<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
					<button type="button" class="btn btn-primary">Save changes</button>
				</div>
			</div>
		</div>
	</div>
</section>

<section class="related-product common-padding">
	<div class="container-fluid">
		<div class="white-card">
			<h3 class="related-h3">Related Product</h3>
			<div class="owl-carousel owl-theme owl-nav-top">
				<div class="item">
					<div class="product-card">
						<div class="card-image">
							<a href="javascript:">
								<img data-src="img/products/product1.jpg" alt="Product-Image" class="lazyload">
							</a>
						</div>
						<div class="card-detail">
							<div class="rating">
								<ul class="list-unstyled side-listing">
									<li><i class="fas fa-star active"></i></li>
									<li><i class="fas fa-star active"></i></li>
									<li><i class="fas fa-star active"></i></li>
									<li><i class="fas fa-star"></i></li>
									<li><i class="fas fa-star"></i></li>
								</ul>
							</div>
							<p class="product-title"><a href="javascript:">Butterflies Hand-held Bag</a></p>
							<p class="product-price">RS. 1,10,000</p>
						</div>
					</div>
				</div>
				<div class="item">
					<div class="product-card">
						<div class="card-image">
							<a href="javascript:">
								<img data-src="img/products/product2.jpg" alt="Product-Image" class="lazyload">
							</a>
						</div>
						<div class="card-detail">
							<div class="rating">
								<ul class="list-unstyled side-listing">
									<li><i class="fas fa-star active"></i></li>
									<li><i class="fas fa-star active"></i></li>
									<li><i class="fas fa-star active"></i></li>
									<li><i class="fas fa-star"></i></li>
									<li><i class="fas fa-star"></i></li>
								</ul>
							</div>
							<p class="product-title"><a href="javascript:">Butterflies Hand-held Bag</a></p>
							<p class="product-price">RS. 1,10,000</p>
						</div>
					</div>
				</div>
				<div class="item">
					<div class="product-card">
						<div class="card-image">
							<a href="javascript:">
								<img data-src="img/products/product3.jpg" alt="Product-Image" class="lazyload">
							</a>
						</div>
						<div class="card-detail">
							<div class="rating">
								<ul class="list-unstyled side-listing">
									<li><i class="fas fa-star active"></i></li>
									<li><i class="fas fa-star active"></i></li>
									<li><i class="fas fa-star active"></i></li>
									<li><i class="fas fa-star"></i></li>
									<li><i class="fas fa-star"></i></li>
								</ul>
							</div>
							<p class="product-title"><a href="javascript:">Butterflies Hand-held Bag</a></p>
							<p class="product-price">RS. 1,10,000</p>
						</div>
					</div>
				</div>
				<div class="item">
					<div class="product-card">
						<div class="card-image">
							<a href="javascript:">
								<img data-src="img/products/product4.jpg" alt="Product-Image" class="lazyload">
							</a>
						</div>
						<div class="card-detail">
							<div class="rating">
								<ul class="list-unstyled side-listing">
									<li><i class="fas fa-star active"></i></li>
									<li><i class="fas fa-star active"></i></li>
									<li><i class="fas fa-star active"></i></li>
									<li><i class="fas fa-star"></i></li>
									<li><i class="fas fa-star"></i></li>
								</ul>
							</div>
							<p class="product-title"><a href="javascript:">Butterflies Hand-held Bag</a></p>
							<p class="product-price">RS. 1,10,000</p>
						</div>
					</div>
				</div>
				<div class="item">
					<div class="product-card">
						<div class="card-image">
							<a href="javascript:">
								<img data-src="img/products/product5.jpg" alt="Product-Image" class="lazyload">
							</a>
						</div>
						<div class="card-detail">
							<div class="rating">
								<ul class="list-unstyled side-listing">
									<li><i class="fas fa-star active"></i></li>
									<li><i class="fas fa-star active"></i></li>
									<li><i class="fas fa-star active"></i></li>
									<li><i class="fas fa-star"></i></li>
									<li><i class="fas fa-star"></i></li>
								</ul>
							</div>
							<p class="product-title"><a href="javascript:">Butterflies Hand-held Bag</a></p>
							<p class="product-price">RS. 1,10,000</p>
						</div>
					</div>
				</div>
				<div class="item">
					<div class="product-card">
						<div class="card-image">
							<a href="javascript:">
								<img data-src="img/products/product3.jpg" alt="Product-Image" class="lazyload">
							</a>
						</div>
						<div class="card-detail">
							<div class="rating">
								<ul class="list-unstyled side-listing">
									<li><i class="fas fa-star active"></i></li>
									<li><i class="fas fa-star active"></i></li>
									<li><i class="fas fa-star active"></i></li>
									<li><i class="fas fa-star"></i></li>
									<li><i class="fas fa-star"></i></li>
								</ul>
							</div>
							<p class="product-title"><a href="javascript:">Butterflies Hand-held Bag</a></p>
							<p class="product-price">RS. 1,10,000</p>
						</div>
					</div>
				</div>
				<div class="item">
					<div class="product-card">
						<div class="card-image">
							<a href="javascript:">
								<img data-src="img/products/product4.jpg" alt="Product-Image" class="lazyload">
							</a>
						</div>
						<div class="card-detail">
							<div class="rating">
								<ul class="list-unstyled side-listing">
									<li><i class="fas fa-star active"></i></li>
									<li><i class="fas fa-star active"></i></li>
									<li><i class="fas fa-star active"></i></li>
									<li><i class="fas fa-star"></i></li>
									<li><i class="fas fa-star"></i></li>
								</ul>
							</div>
							<p class="product-title"><a href="javascript:">Butterflies Hand-held Bag</a></p>
							<p class="product-price">RS. 1,10,000</p>
						</div>
					</div>
				</div>
				<div class="item">
					<div class="product-card">
						<div class="card-image">
							<a href="javascript:">
								<img data-src="img/products/product5.jpg" alt="Product-Image" class="lazyload">
							</a>
						</div>
						<div class="card-detail">
							<div class="rating">
								<ul class="list-unstyled side-listing">
									<li><i class="fas fa-star active"></i></li>
									<li><i class="fas fa-star active"></i></li>
									<li><i class="fas fa-star active"></i></li>
									<li><i class="fas fa-star"></i></li>
									<li><i class="fas fa-star"></i></li>
								</ul>
							</div>
							<p class="product-title"><a href="javascript:">Butterflies Hand-held Bag</a></p>
							<p class="product-price">RS. 1,10,000</p>
						</div>
					</div>
				</div>
				<div class="item">
					<div class="product-card">
						<div class="card-image">
							<a href="javascript:">
								<img data-src="img/products/product3.jpg" alt="Product-Image" class="lazyload">
							</a>
						</div>
						<div class="card-detail">
							<div class="rating">
								<ul class="list-unstyled side-listing">
									<li><i class="fas fa-star active"></i></li>
									<li><i class="fas fa-star active"></i></li>
									<li><i class="fas fa-star active"></i></li>
									<li><i class="fas fa-star"></i></li>
									<li><i class="fas fa-star"></i></li>
								</ul>
							</div>
							<p class="product-title"><a href="javascript:">Butterflies Hand-held Bag</a></p>
							<p class="product-price">RS. 1,10,000</p>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
<?php include('include/footer.php'); ?>