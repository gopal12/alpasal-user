$(document).ready(function(){

    $('.category-lvl').select2();
    $('.area').select2();
    $('.select2-area').select2(); 

    $('#myTable').DataTable();

    // Register Form Trigger

    function toggleRecover(e){
    e.preventDefault();
      $('#recover').toggle();
    }
  
    function toggleLogin(e){
    e.preventDefault();
      $('#login').toggle(); 
    }

    function toggleUser(e){
    e.preventDefault();
      $('#user-signup').show();
      $('#vendor-signup').hide();
      $('#user-bttn').addClass("active");
      $('#vendor-bttn').removeClass("active");
    }
  
    function toggleVendor(e){
    e.preventDefault();
      $('#vendor-signup').show(); 
      $('#user-signup').hide();
      $('#user-bttn').removeClass("active");
      $('#vendor-bttn').addClass("active");
    }
  
  $(()=>{
  
    $('#recover-btn').click(toggleLogin);
    $('#recover-btn').click(toggleRecover);
  
    $('#login-btn').click(toggleRecover);
    $('#login-btn').click(toggleLogin);

    $('#vendor-bttn').click(toggleVendor);
  
    $('#user-bttn').click(toggleUser);
  
  });


  // Login Page Msg Close
  
  $('.close-icon').on('click', function(){
      $('.login-msg').hide();
  });


  // Register Multiselect

  $('.signup-select').select2();

  $('.business-area').select2({
    placeholder: 'Select Business Area',
  });

  $('.select-categories').select2({
    placeholder: 'Select Categories',
  });

    // Index Slider

    $('.index-slider-section .owl-carousel').owlCarousel({
        items: 1,
        margin: 16,
        loop: true, 
        nav: false,
        dots: true,
        dotsEach: true,
        lazyLoad: false,
        autoplay: true,
        autoplaySpeed: 1500,
        navSpeed: 1500,
        autoplayTimeout: 4000,
        autoplayHoverPause: true, 
        responsive: {
            0: {
                items: 1,
            },
            750: {
                items: 1,
            },
            1000: {
                items: 1,
            }
        }
    });
 


    // Top Category Slider

    $('.top-category .owl-carousel').owlCarousel({
        items: 6,
        margin: 16,
        loop: true, 
        nav: false,
        dots: false,
        dotsEach: false,
        lazyLoad: false,
        autoplay: false,
        // autoplaySpeed: 1500,
        // navSpeed: 1500,
        autoplayTimeout: 4000,
        autoplayHoverPause: true, 
        responsive: {
            0: {
                items: 2,
            },
            750: {
                items: 3,
            },
            1000: {
                items: 6,
            },
            1370:{
                items: 8
            }
        }
    });

    //  relatred product

     $('.related-product .owl-carousel').owlCarousel({
        items: 6,
        margin: 16,
        loop: true, 
        nav: false,
        dots: false,
        dotsEach: false,
        lazyLoad: false,
        autoplay: true,
        autoplaySpeed: 1500,
        navSpeed: 1500,
        autoplayTimeout: 4000,
        autoplayHoverPause: true, 
        responsive: {
            0: {
                items: 2,
            },
            750: {
                items: 3,
            },
            1000: {
                items: 6,
            },
            1370:{
                items: 8
            }
        }
    });

    // Flash Sale Slider

    $('.flash-list .owl-carousel').owlCarousel({
        items: 4,
        margin: 16,
        loop: true, 
        nav: true,
        dots: false,
        dotsEach: false,
        lazyLoad: false,
        autoplay: true,
        autoplaySpeed: 2000,
        navSpeed: 2000,
        autoplayTimeout: 4000,
        autoplayHoverPause: true, 
        responsive: {
            0: {
                items: 2,
            },
            750: {
                items: 3,
            },
            1000: {
                items: 6,
            },
            1370: {
                items: 8,
            }
        }
    });
 
    $('.xzoom, .xzoom-gallery').xzoom({zoomWidth: 400, title: true, tint: '#333', Xoffset: 15});

    $('.our-team .owl-carousel').owlCarousel({
    items: 4,
    margin: 30,
    loop: true,
    nav: true,
    dots: false,
    dotsEach: true,
    lazyLoad: false,
    autoplay: true,
    autoplaySpeed: 1500,
    navSpeed: 1500,
    autoplayTimeout: 4000,
    autoplayHoverPause: true,
    navText: [
        "<i class='fas fa-angle-left'></i>",
        "<i class='fas fa-angle-right'></i>"
      ],
    responsive: {
        0: {
            items: 1,
        },
        750: {
            items: 2,
        },
        1000: {
            items: 4,
        }
    }
  });

    $(".btn-group, .hover-li-nav").hover(
    function () {
        $('>.hover-dis-a-nav', this).stop(true, true).fadeIn("fast");
        $(this).addClass('open');
    },
    function () {
        $('>.hover-dis-a-nav', this).stop(true, true).fadeOut("fast");
        $(this).removeClass('open');
    });

    $('.faq-question').on('click',function(){
    var pannel=$(this).attr('data-pannel');
    $('#'+pannel).slideToggle('medium');
    $(".faq-answer-wrap").not($(this).next()).slideUp('medium');
  });

    $('.app-review-mains .owl-carousel').owlCarousel({
    items: 1,
    margin: 16,
    loop: true,
    // stagePadding: 64,
    nav: true,
    dots: false,
    dotsEach: true,
    lazyLoad: false,
    autoplay: true,
    autoplaySpeed: 1500,
    navSpeed: 1500,
    autoplayTimeout: 4000,
    autoplayHoverPause: true,
    navText: [
        "<i class='fas fa-angle-left'></i>",
        "<i class='fas fa-angle-right'></i>"
      ],
    responsive: {
        0: {
            items: 1,
        },
        750: {
            items: 1,
        },
        1000: {
            items: 1,
        }
    }
  });
 

});