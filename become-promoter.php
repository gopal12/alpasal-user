<?php include('include/header.php'); ?>

<section class="index-vendor-section">
    <div class="container-fluid p-0">
        <div class="vendor-wrapper">
            <a href="javascript:">
                <img class="lazyload" data-src="img/vendor-banner1.png" alt="Landing-Banner">
            </a>
            <div class="vender-hero-content">
                <div class="row">
                    <div class="col-lg-6 offset-lg-6">
                        <div class="hero-main-content card-form">
                            <p class="hero-title">Become a Promoter on Nepal's LEADING ecommerce platform.</p>
                            <div class="row"> 
                                <div class="col-md-6"> 
                                    <div class="input-group mb-3">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text" id="basic-addon1"><i class="fas fa-mobile-alt"></i></span>
                                        </div>
                                        <input type="text" class="form-control" placeholder="Enter 10 digits phone number here" aria-label="Phone Number" aria-describedby="basic-addon2" required>
                                    </div>
                                </div> 
                                <div class="col-md-6"> 
                                    <a href="javascript:" class="hero-button">Register Now &rarr;</a>
                                </div> 
                            </div> 
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="vendor-testimonial">
    <div class="container-fluid">
        <div class="owl-carousel owl-theme owl-def"> 
            <div class="item"> 
                <div class="testimonial-container d-flex align-items-center">
                    <div class="image">
                        <img data-src="img/team/team4.jpg" alt="Testimonial-image" class="lazyload">
                    </div>
                    <div class="content">
                        <p class="testimonial">"Becoming a Promoter on Alpasal was one of the best decisions of my life."</p>
                        <p class="name">- Gopal Basnet, Heaven Maker</p>
                    </div>
                </div>
            </div> 
            <div class="item"> 
                <div class="testimonial-container d-flex align-items-center">
                    <div class="image">
                        <img data-src="img/team/team1.jpg" alt="Testimonial-image" class="lazyload">
                    </div>
                    <div class="content">
                        <p class="testimonial">"From being a homemaker to owning a business, I have come a long way with Alpasal."</p>
                        <p class="name">- Gopal Basnet, Heaven Maker</p>
                    </div>
                </div>
            </div> 
            <div class="item"> 
                <div class="testimonial-container d-flex align-items-center">
                    <div class="image">
                        <img data-src="img/team/team3.jpg" alt="Testimonial-image" class="lazyload">
                    </div>
                    <div class="content">
                        <p class="testimonial">"My business was about to shut down when I joined Alpasal, and there has been no looking back."</p>
                        <p class="name">- Gopal Basnet, Heaven Maker</p>
                    </div>
                </div>
            </div>  
            <div class="item"> 
                <div class="testimonial-container d-flex align-items-center">
                    <div class="image">
                        <img data-src="img/team/team11.jpg" alt="Testimonial-image" class="lazyload">
                    </div>
                    <div class="content">
                        <p class="testimonial">"From being a homemaker to owning a business, I have come a long way with Alpasal."</p>
                        <p class="name">- Gopal Basnet, Heaven Maker</p>
                    </div>
                </div>
            </div> 

            <div class="item"> 
                <div class="testimonial-container d-flex align-items-center">
                    <div class="image">
                        <img data-src="img/team/team12.jpg" alt="Testimonial-image" class="lazyload">
                    </div>
                    <div class="content">
                        <p class="testimonial">"My business was about to shut down when I joined Alpasal, and there has been no looking back."</p>
                        <p class="name">- Gopal Basnet, Heaven Maker</p>
                    </div>
                </div>
            </div>  
        </div> 
    </div>
</section>

<section class="vendor-introduction common-padding bg-grey">
    <div class="container-fluid"> 
        <ul class="nav nav-tabs" id="myTab" role="tablist">
            <li class="nav-item">
                <a class="nav-link active" id="why-tab" data-toggle="tab" href="#why" role="tab" aria-controls="why" aria-selected="true">Why Sell with Alpasal</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" id="how-tab" data-toggle="tab" href="#how" role="tab" aria-controls="how" aria-selected="false">How to Register</a>
            </li>
        </ul>  
        <div class="tab-content white-card" id="myTabContent">
            <div class="tab-pane fade show active" id="why" role="tabpanel" aria-labelledby="why-tab">
                <div class="row"> 
                    <div class="col-md-5">  
                        <div class="vendor-video-wrapper">
                            <h3 class="topic-title">#LifeIsGoodWithMoreFood</h3>
                            <iframe src="https://www.youtube.com/embed/Vf9E07v-3Nc" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                        </div> 
                    </div> 
                    <div class="col-md-7">  
                        <h3 class="topic-title text-center">Advantages of selling on Alpasal</h3> 
                        <div class="row no-gutters">  
                            <div class="col-md-6"> 
                                <a href="javascript:">
                                <div class="points d-flex align-items-center" data-mh="benifits">
                                    <div class="image">
                                        <img src="img/icons/growth.png" alt="ICON">
                                    </div>
                                    <div class="content">
                                        <h4 class="title">Growth</h4>
                                        <p>Widen your reach to a customer base of 1 million and grow your business further. Lorem ipsum dolor sit amet.</p>
                                    </div>
                                </div>
                                </a>
                            </div>  
                            <div class="col-md-6"> 
                                <a href="javascript:">
                                <div class="points d-flex align-items-center" data-mh="benifits">
                                    <div class="image">
                                        <img src="img/icons/point4.png" alt="ICON">
                                    </div>
                                    <div class="content">
                                        <h4 class="title">Lowest cost of doing Business</h4>
                                        <p>Lorem ipsum dolor sit, amet consectetur adipisicing elit. Vel quod error nostrum.</p>
                                    </div>
                                </div>
                                </a>
                            </div>  
                            <div class="col-md-6"> 
                                <a href="javascript:">
                                <div class="points d-flex align-items-center" data-mh="benifits">
                                    <div class="image">
                                        <img src="img/icons/point3.png" alt="ICON">
                                    </div>
                                    <div class="content">
                                        <h4 class="title">Ease</h4>
                                        <p>You just need 1 product and 2 documents to start selling on Alpasal.</p>
                                    </div>
                                </div>
                                </a>
                            </div>  
                            <div class="col-md-6"> 
                                <a href="javascript:">
                                <div class="points d-flex align-items-center" data-mh="benifits">
                                    <div class="image">
                                        <img src="img/icons//point2.png" alt="ICON">
                                    </div>
                                    <div class="content">
                                        <h4 class="title">Transparency</h4>
                                        <p>Equal opportunities for all the Promoters to grow.</p>
                                    </div>
                                </div>
                                </a>
                            </div>  
                        </div>  
                        <div class="text-center">
                            <a href="javascript:" class="hero-button">Register Now &rarr;</a>
                        </div> 
                    </div> 
                </div>  
            </div> 
            <div class="tab-pane fade" id="how" role="tabpanel" aria-labelledby="how-tab"> 
                <div class="row"> 
                    <div class="col-md-7">  
                        <h3 class="topic-title text-center">You need just 3 things to become a Alpasal Promoter.</h3> 
                        <div class="row no-gutters">  
                            <div class="col-md-6"> 
                                <a href="javascript:">
                                <div class="points d-flex align-items-center" data-mh="benifits">
                                    <div class="image">
                                        <img src="img/icons/step1.png" alt="ICON">
                                    </div>
                                    <div class="content">
                                        <h4 class="title">At least 1 product to sell</h4>
                                        <p>Widen your reach to a customer base of 1 million and grow your business further. Lorem ipsum dolor sit amet.</p>
                                    </div>
                                </div>
                                </a>
                            </div>  
                            <div class="col-md-6"> 
                                <a href="javascript:">
                                <div class="points d-flex align-items-center" data-mh="benifits">
                                    <div class="image">
                                        <img src="img/icons/step2.png" alt="ICON">
                                    </div>
                                    <div class="content">
                                        <h4 class="title">GSTIN Details</h4>
                                        <p>Lorem ipsum dolor sit, amet consectetur adipisicing elit. Vel quod error nostrum.</p>
                                    </div>
                                </div>
                                </a>
                            </div>  
                            <div class="col-md-6 offset-md-3"> 
                                <a href="javascript:">
                                <div class="points d-flex align-items-center" data-mh="benifits">
                                    <div class="image">
                                        <img src="img/icons/step3.png" alt="ICON">
                                    </div>
                                    <div class="content">
                                        <h4 class="title">Cancelled Cheque</h4>
                                        <p>You just need 1 product and 2 documents to start selling on Alpasal.</p>
                                    </div>
                                </div>
                                </a>
                            </div>  
                        </div>  
                        <div class="text-center">
                            <a href="javascript:" class="hero-button">Register Now &rarr;</a>
                        </div> 
                    </div>  
                    <div class="col-md-5">  
                        <div class="vendor-video-wrapper">
                            <h3 class="topic-title">3 steps to register as a Alpasal Promoter</h3> 
                            <iframe src="https://www.youtube.com/embed/tnjzYZN0oG0" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                        </div> 
                    </div> 
                </div>  
            </div> 
        </div> 
    </div>  
</section>




<?php include('include/footer.php'); ?>