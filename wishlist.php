<?php include('include/header.php'); ?>

<nav aria-label="breadcrumb" class="breadcrumb-main bg-para" style="background: linear-gradient(rgba(0,0,0,0.4),rgba(0,0,0,0.4)), url('img/bread.jpg');">
    <div class="container clearfix"> <!-- Container .// -->
        <h3 class="float-left">Wishlist</h3>
        <ol class="breadcrumb float-right">
            <li class="breadcrumb-item"><a href="index.php">Home</a></li>
            <!-- <li class="breadcrumb-item"><a href="#">Library</a></li> -->
            <li class="breadcrumb-item active" aria-current="page">Wishlist</li>
        </ol>
    </div> <!-- Container .// -->
</nav>

<section class="user-dashboard">
    <div class="container-fluid"> 
        <div class="row"> 
            <div class="col-lg-3">
                <div class="left-side-user-dashboard"> 
                    <div class="header">
                        <div class="prof_who">
                            <div class="img_holder">
                                <img src="img/user/user-profile.jpg" class="img-fluid" alt="">
                            </div>
                            <div class="title_holder"> 
                                <span>Welcome</span>
                                <h3>Gopal Basnet</h3> 
                            </div>
                        </div>
                    </div>
                    <div class="content nav flex-column nav-pills" role="tablist" aria-orientation="vertical">
                        <ul class="nav"> 
                            <li class="label">Update User Profile</li>
                            <li class="left-nav"><a class="nav-link " href="update-info.php">Update Info</a></li>
                            <li class="left-nav"><a class="nav-link" href="request-area.php">Request Area</a></li>
                            <li class="left-nav"><a class= "nav-link" href="change-password.php">Change Password</a></li>
                            <li class="left-nav"><a class= "nav-link" href="shipping-address.php">Shipping Address</a></li>
                            <li class="left-nav"><a class= "nav-link active" href="wishlist.php">WishList</a></li>
                            <li class="left-nav"><a class= "nav-link" href="notification.php">Notifications</a></li>
                            <li class="left-nav"><a class= "nav-link" href="#">Log Out</a></li>
                        </ul>
                    </div>
                </div> 
            </div>
            <div class="col-lg-9">
                <div class="right-user-dashboard">
                    <div class="tab-content">   
                        <div class="tab-pane fade fade show active">
                            <div class="add-product-right-side"> 
                                <div class="alert alert-vendor alert-dismissible fade show" role="alert">
                                    Item Added to Wishlist.
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                </div> 
                                <h3 class="add-product-title">Wishlist</h3>
                                <table class="table table-responsive-sm table-hover wishlist-main">
                                    <thead>
                                        <tr>
                                            <th scope="col">S.No.</th>
                                            <th scope="col">Product</th>
                                            <th scope="col">Description</th>
                                            <th scope="col">Availability</th>
                                            <th scope="col">Price</th> 
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td>1.</td>
                                            <td>
                                                <div class="wishlist-img-wrap">
                                                    <img src="img/products/1.jpg" alt="Product">
                                                </div>
                                            </td>
                                            <td>
                                                Bluetooth Headphone
                                                <span>Product Id: 98761</span>
                                            </td>
                                            <td>
                                                <div class="availability-div">
                                                    <p class="bid-on-p"> <i class="far fa-check-square"></i> Bid On</p>
                                                </div>
                                            </td>
                                            <td>
                                                Rs. 2,500
                                            </td> 
                                        </tr>
                                        <tr>
                                            <td>2.</td>
                                            <td>
                                                <div class="wishlist-img-wrap">
                                                    <img src="img/products/2.jpg" alt="Product">
                                                </div>
                                            </td>
                                            <td>
                                                Leather Bag
                                                <span>Product Id: 98762</span>
                                            </td>
                                            <td>
                                                <div class="availability-div">
                                                    <p class="bid-closed"> <i class="far fa-window-close"></i> Bid Closed</p>
                                                </div>
                                            </td>
                                            <td>
                                                Rs. 1,700
                                            </td> 
                                        </tr>
                                        <tr>
                                            <td>3.</td>
                                            <td>
                                                <div class="wishlist-img-wrap">
                                                    <img src="img/products/6.jpg" alt="Product">
                                                </div>
                                            </td>
                                            <td>
                                                Nikon DSLR
                                                <span>Product Id: 98763</span>
                                            </td>
                                            <td>
                                                <div class="availability-div">
                                                    <p class="bid-on-p"> <i class="far fa-check-square"></i> Bid On</p>
                                                </div>
                                            </td>
                                            <td>
                                                Rs. 80,000
                                            </td> 
                                        </tr>
                                    </tbody>
                                </table> 
                                <button class="btn btn-load-more-wishlist">Load More</button> 
                            </div>
                        </div> 
                    </div>
                </div>
            </div>
        </div>
    </div> 
</section>

<?php include('include/footer.php'); ?>