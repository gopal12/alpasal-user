<?php include('include/header.php'); ?>

<nav aria-label="breadcrumb" class="breadcrumb-main bg-para" style="background: linear-gradient(rgba(0,0,0,0.4),rgba(0,0,0,0.4)), url('img/bread.jpg');">
    <div class="container clearfix"> <!-- Container .// -->
        <h3 class="float-left">Category List</h3>
        <ol class="breadcrumb float-right">
            <li class="breadcrumb-item"><a href="index.php">Home</a></li>
            <!-- <li class="breadcrumb-item"><a href="#">Library</a></li> -->
            <li class="breadcrumb-item active" aria-current="page">Category List</li>
        </ol>
    </div> <!-- Container .// -->
</nav>


<section class="section-list-cat"> 
	<div class="container-fluid">
		<div class="row">
			<div class="col-lg-2"> 
				<div class="user-category-left-wrapper">
					<div class="user-category-title">
						<h3>Area</h3>
					</div>
					<div id="sidebar_area_section" class="user-category-info">
						<div class="custom-control custom-checkbox">
							<input type="checkbox" value="ALL-NEPAL" class="custom-control-input" id="check_area_ALL-NEPAL" name="areas_selected[]">
							<label class="custom-control-label" for="check_area_ALL-NEPAL">All Nepal</label>
						</div>
						<div class="custom-control custom-checkbox">
							<input type="checkbox" value="KTM" class="custom-control-input" id="check_area_KTM" name="areas_selected[]">
							<label class="custom-control-label" for="check_area_KTM">Kathmandu</label>
						</div>
						<div class="custom-control custom-checkbox">
							<input type="checkbox" value="BKT" class="custom-control-input" id="check_area_BKT" name="areas_selected[]">
							<label class="custom-control-label" for="check_area_BKT">Bhaktapur</label>
						</div>
						<div class="custom-control custom-checkbox">
							<input type="checkbox" value="LTP" class="custom-control-input" id="check_area_LTP" name="areas_selected[]">
							<label class="custom-control-label" for="check_area_LTP">Lalitpur</label>
						</div>
						<div class="custom-control custom-checkbox">
							<input type="checkbox" value="PROVINCE 1" class="custom-control-input" id="check_area_PROVINCE 1" name="areas_selected[]">
							<label class="custom-control-label" for="check_area_PROVINCE 1">Province 1</label>
						</div>
						<div class="custom-control custom-checkbox">
							<input type="checkbox" value="PROVINCE 2" class="custom-control-input" id="check_area_PROVINCE 2" name="areas_selected[]">
							<label class="custom-control-label" for="check_area_PROVINCE 2">Province 2</label>
						</div>
						<div class="custom-control custom-checkbox">
							<input type="checkbox" value="PROVINCE 3" class="custom-control-input" id="check_area_PROVINCE 3" name="areas_selected[]">
							<label class="custom-control-label" for="check_area_PROVINCE 3">Province 3</label>
						</div>
						<div class="custom-control custom-checkbox">
							<input type="checkbox" value="PROVINCE 4" class="custom-control-input" id="check_area_PROVINCE 4" name="areas_selected[]">
							<label class="custom-control-label" for="check_area_PROVINCE 4">Province 4</label>
						</div>
						<div class="custom-control custom-checkbox">
							<input type="checkbox" value="PROVINCE 6" class="custom-control-input" id="check_area_PROVINCE 6" name="areas_selected[]">
							<label class="custom-control-label" for="check_area_PROVINCE 6">Province 6</label>
						</div>
						<div class="custom-control custom-checkbox">
							<input type="checkbox" value="PROVINCE 7" class="custom-control-input" id="check_area_PROVINCE 7" name="areas_selected[]">
							<label class="custom-control-label" for="check_area_PROVINCE 7">Province 7</label>
						</div>
						<div class="custom-control custom-checkbox">
							<input type="checkbox" value="PROVINCE 5" class="custom-control-input" id="check_area_PROVINCE 5" name="areas_selected[]">
							<label class="custom-control-label" for="check_area_PROVINCE 5">Province 5</label>
						</div>
					</div>
				</div>
				<div id="sidebar_category_section" class="user-category-left-wrapper">
					<div class="user-category-title">
						<h3>
						Home Appliance
						</h3>
					</div>
					<div class="user-cat-size-color-div">
						<input type="hidden" id="searching_cat_level" value="2">
						<div class="custom-control custom-checkbox">
							<input type="checkbox" data-cat_level="1" value="large-appliances" class="custom-control-input" id="check_cat_large-appliances" name="cats_selected[]">
							<label class="custom-control-label" for="check_cat_large-appliances">Large  Appliances</label>
						</div>
					</div>
				</div>
				<div class="user-category-left-wrapper">
					<button class="btn btn-success btn-block" id="filter_results" type="submit">Filter</button>
				</div>
			</div>
			<div class="col-lg-10">
				<div class="custom-grid cat-grid"> 
	                <div class="custom-grid-item">
	                    <div class="product-card" data-mh="product">
	                        <div class="card-image">
	                            <a href="javascript:">
	                                <img data-src="img/products/product1.jpg" alt="Product-Image" class="lazyload">
	                            </a>
	                        </div>
	                        <div class="card-detail">
	                            <div class="rating">
	                                <ul class="list-unstyled side-listing">
	                                    <li><i class="fas fa-star active"></i></li>
	                                    <li><i class="fas fa-star active"></i></li>
	                                    <li><i class="fas fa-star active"></i></li>
	                                    <li><i class="fas fa-star"></i></li>
	                                    <li><i class="fas fa-star"></i></li>
	                                </ul>
	                            </div>
	                            <p class="product-title"><a href="javascript:">Butterflies Hand-held Bag</a></p>
	                            <p class="stock-product">Product Stock : <span>500</span></p>
	                            <p class="price-product">Product Price : <span>RS. 10,00,000</span></p>
	                            <p class="status">Status : <span class="closed">Closed</span></p> 
	                        </div>
	                    </div>
	                </div> 
	                <div class="custom-grid-item">
	                    <div class="product-card" data-mh="product">
	                        <div class="card-image">
	                            <a href="javascript:">
	                                <img data-src="img/products/product2.jpg" alt="Product-Image" class="lazyload">
	                            </a>
	                        </div>
	                        <div class="card-detail">
	                            <div class="rating">
	                                <ul class="list-unstyled side-listing">
	                                    <li><i class="fas fa-star active"></i></li>
	                                    <li><i class="fas fa-star active"></i></li>
	                                    <li><i class="fas fa-star active"></i></li>
	                                    <li><i class="fas fa-star"></i></li>
	                                    <li><i class="fas fa-star"></i></li>
	                                </ul>
	                            </div>
	                            <p class="product-title"><a href="javascript:">Butterflies Hand-held Bag</a></p> 
	                            <p class="stock-product">Product Stock : <span>500</span></p>
	                            <p class="price-product">Product Price : <span>RS. 10,00,000</span></p>
	                            <p class="status">Status : <span class="open">open</span></p> 
	                        </div>
	                    </div>
	                </div> 
	                <div class="custom-grid-item">
	                    <div class="product-card" data-mh="product">
	                        <div class="card-image">
	                            <a href="javascript:">
	                                <img data-src="img/products/product5.jpg" alt="Product-Image" class="lazyload">
	                            </a>
	                        </div>
	                        <div class="card-detail">
	                            <div class="rating">
	                                <ul class="list-unstyled side-listing">
	                                    <li><i class="fas fa-star active"></i></li>
	                                    <li><i class="fas fa-star active"></i></li>
	                                    <li><i class="fas fa-star active"></i></li>
	                                    <li><i class="fas fa-star"></i></li>
	                                    <li><i class="fas fa-star"></i></li>
	                                </ul>
	                            </div>
	                            <p class="product-title"><a href="javascript:">Butterflies Hand-held Bag</a></p>
	                            <p class="stock-product">Product Stock : <span>500</span></p>
	                            <p class="price-product">Product Price : <span>RS. 10,00,000</span></p>
	                            <p class="status">Status : <span class="closed">Closed</span></p> 
	                        </div>
	                    </div>
	                </div> 
	                <div class="custom-grid-item">
	                    <div class="product-card" data-mh="product">
	                        <div class="card-image">
	                            <a href="javascript:">
	                                <img data-src="img/products/product9.webp" alt="Product-Image" class="lazyload">
	                            </a>
	                        </div>
	                        <div class="card-detail">
	                            <div class="rating">
	                                <ul class="list-unstyled side-listing">
	                                    <li><i class="fas fa-star active"></i></li>
	                                    <li><i class="fas fa-star active"></i></li>
	                                    <li><i class="fas fa-star active"></i></li>
	                                    <li><i class="fas fa-star"></i></li>
	                                    <li><i class="fas fa-star"></i></li>
	                                </ul>
	                            </div>
	                            <p class="product-title"><a href="javascript:">Butterflies Hand-held Bag</a></p>
	                            <p class="stock-product">Product Stock : <span>500</span></p>
	                            <p class="price-product">Product Price : <span>RS. 10,00,000</span></p>
	                            <p class="status">Status : <span class="open">open</span></p> 
	                        </div>
	                    </div>
	                </div> 
	                <div class="custom-grid-item">
	                    <div class="product-card" data-mh="product">
	                        <div class="card-image">
	                            <a href="javascript:">
	                                <img data-src="img/products/product3.jpg" alt="Product-Image" class="lazyload">
	                            </a>
	                        </div>
	                        <div class="card-detail">
	                            <div class="rating">
	                                <ul class="list-unstyled side-listing">
	                                    <li><i class="fas fa-star active"></i></li>
	                                    <li><i class="fas fa-star active"></i></li>
	                                    <li><i class="fas fa-star active"></i></li>
	                                    <li><i class="fas fa-star"></i></li>
	                                    <li><i class="fas fa-star"></i></li>
	                                </ul>
	                            </div>
	                            <p class="product-title"><a href="javascript:">Butterflies Hand-held Bag</a></p>
	                            <p class="stock-product">Product Stock : <span>500</span></p>
	                            <p class="price-product">Product Price : <span>RS. 10,00,000</span></p>
	                            <p class="status">Status : <span class="open">open</span></p> 
	                        </div>
	                    </div>
	                </div> 
	                <div class="custom-grid-item">
	                    <div class="product-card" data-mh="product">
	                        <div class="card-image">
	                            <a href="javascript:">
	                                <img data-src="img/products/product4.jpg" alt="Product-Image" class="lazyload">
	                            </a>
	                        </div>
	                        <div class="card-detail">
	                            <div class="rating">
	                                <ul class="list-unstyled side-listing">
	                                    <li><i class="fas fa-star active"></i></li>
	                                    <li><i class="fas fa-star active"></i></li>
	                                    <li><i class="fas fa-star active"></i></li>
	                                    <li><i class="fas fa-star"></i></li>
	                                    <li><i class="fas fa-star"></i></li>
	                                </ul>
	                            </div>
	                            <p class="product-title"><a href="javascript:">Butterflies Hand-held Bag</a></p>
	                            <p class="stock-product">Product Stock : <span>500</span></p>
	                            <p class="price-product">Product Price : <span>RS. 10,00,000</span></p>
	                            <p class="status">Status : <span class="closed">Closed</span></p> 
	                        </div>
	                    </div>
	                </div> 
	                <div class="custom-grid-item">
	                    <div class="product-card" data-mh="product">
	                        <div class="card-image">
	                            <a href="javascript:">
	                                <img data-src="img/products/product6.jpg" alt="Product-Image" class="lazyload">
	                            </a>
	                        </div>
	                        <div class="card-detail">
	                            <div class="rating">
	                                <ul class="list-unstyled side-listing">
	                                    <li><i class="fas fa-star active"></i></li>
	                                    <li><i class="fas fa-star active"></i></li>
	                                    <li><i class="fas fa-star active"></i></li>
	                                    <li><i class="fas fa-star"></i></li>
	                                    <li><i class="fas fa-star"></i></li>
	                                </ul>
	                            </div>
	                            <p class="product-title"><a href="javascript:">Butterflies Hand-held Bag</a></p>
	                            <p class="stock-product">Product Stock : <span>500</span></p>
	                            <p class="price-product">Product Price : <span>RS. 10,00,000</span></p>
	                            <p class="status">Status : <span class="open">Open</span></p> 
	                        </div>
	                    </div>
	                </div> 
	                <div class="custom-grid-item">
	                    <div class="product-card" data-mh="product">
	                        <div class="card-image">
	                            <a href="javascript:">
	                                <img data-src="img/products/product9.webp" alt="Product-Image" class="lazyload">
	                            </a>
	                        </div>
	                        <div class="card-detail">
	                            <div class="rating">
	                                <ul class="list-unstyled side-listing">
	                                    <li><i class="fas fa-star active"></i></li>
	                                    <li><i class="fas fa-star active"></i></li>
	                                    <li><i class="fas fa-star active"></i></li>
	                                    <li><i class="fas fa-star"></i></li>
	                                    <li><i class="fas fa-star"></i></li>
	                                </ul>
	                            </div>
	                            <p class="product-title"><a href="javascript:">Butterflies Hand-held Bag</a></p>
	                            <p class="stock-product">Product Stock : <span>500</span></p>
	                            <p class="price-product">Product Price : <span>RS. 10,00,000</span></p>
	                            <p class="status">Status : <span class="closed">Closed</span></p> 
	                        </div>
	                    </div>
	                </div>
				</div>
			</div>
		</div>
	</div>
</section>
<?php include('include/footer.php'); ?>