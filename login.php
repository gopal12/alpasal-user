<?php include('include/header.php'); ?>

<!----------------------------
-------Breadcrumb-------
----------------------------->

<nav aria-label="breadcrumb" class="breadcrumb-main bg-para" style="background: linear-gradient(rgba(0,0,0,0.4),rgba(0,0,0,0.4)), url('img/bread.jpg');">
    <div class="container clearfix"> <!-- Container .// -->
        <h3 class="float-left">LOGIN</h3>
        <ol class="breadcrumb float-right">
            <li class="breadcrumb-item"><a href="index.php">Home</a></li>
            <!-- <li class="breadcrumb-item"><a href="#">Library</a></li> -->
            <li class="breadcrumb-item active" aria-current="page">Login</li>
        </ol>
    </div> <!-- Container .// -->
</nav>

<!----------------------------
-------Breadcrumb-------
----------------------------->

<!----------------------------
-------Login Section-------
----------------------------->

<section class="login-main">
    <div class="container"> <!-- Container .// -->
        <div class="row"> <!-- Row .// -->
            <div class="col-md-6 offset-md-3"> <!-- Col .// -->

                <!-- <h3 class="big-heading text-center mb-5">Already Registered?</h3> -->

                <div class="login-msg">
                    <div class="clearfix">
                        <p class="float-left">Please Login First to Start the Journey</p>
                        <div class="close-icon float-right">
                            <i class="fas fa-times"></i>
                        </div>
                    </div>
                    
                </div>

                <div class="login-form" id="login"> <!-- Login-Form .// -->
                    <h4 class="page-title text-center">Login</h4>
                    <form>
                        <div class="form-group">
                            <label for="email">Email address *</label>
                            <small id="emailHelp" class="form-text text-muted float-right">* Required Fields</small>
                            <input type="email" id="email" aria-describedby="emailHelp" placeholder="Email Address" required>
                            <!-- <small id="emailHelp" class="form-text text-muted">We'll never share your email with anyone else.</small> -->
                        </div>
                        <div class="form-group">
                            <label for="exampleInputPassword1">Password *</label>
                            <input type="password" id="exampleInputPassword1" placeholder="Password" required>
                        </div>
                        <button type="submit" class="form-button">Login</button>
                    </form>
                    <div class="text-center">
                        <button class="lost-password" id="recover-btn">Forgot your password?</button>
                    </div>
                </div> <!-- Login-Form .// -->

                <div class="login-form" id="recover"> <!-- Login-Form .// -->
                    <h4 class="page-title text-center">Recover Account</h4>
                    <form>
                        <div class="form-group">
                            <label for="email1">Email address *</label>
                            <small id="emailHelp1" class="form-text text-muted float-right">* Required Fields</small>
                            <input type="email" id="email1" aria-describedby="emailHelp" placeholder="Email Address" required>
                            <!-- <small id="emailHelp" class="form-text text-muted">We'll never share your email with anyone else.</small> -->
                        </div>
                        <button type="submit" class="form-button">Recover Password</button>
                    </form>
                    <div class="text-center">
                        <button class="lost-password" id="login-btn">Back to Login</button>
                    </div>
                </div> <!-- Login-Form .// -->

                <h4 class="page-title text-center mt-5 mb-4">New Customer</h4>

                <p class="normal-content">By creating an account with our store, you will be able to move through the checkout process faster, store multiple shipping addresses, view and track your orders in your account and more.</p>

                <a href="register.php" class="form-button">Create An Account</a>

            </div> <!-- Col .// -->
        </div> <!-- Row .// -->
    </div> <!-- Container .// -->
</section>

<!----------------------------
-------Login Section-------
----------------------------->

<!-- <div class="extra-space"></div> -->

<?php include('include/footer.php'); ?>