<?php include('include/header.php'); ?>

<!----------------------------
-------Breadcrumb-------
----------------------------->

<nav aria-label="breadcrumb" class="breadcrumb-main bg-para" style="background: linear-gradient(rgba(0,0,0,0.4),rgba(0,0,0,0.4)), url('img/bread.jpg');">
    <div class="container clearfix"> <!-- Container .// -->
        <h3 class="float-left">FAQ</h3>
        <ol class="breadcrumb float-right">
            <li class="breadcrumb-item"><a href="index.php">Home</a></li>
            <!-- <li class="breadcrumb-item"><a href="#">Library</a></li> -->
            <li class="breadcrumb-item active" aria-current="page">FAQ</li>
        </ol>
    </div> <!-- Container .// -->
</nav>

<!----------------------------
-------Breadcrumb-------
----------------------------->

<!----------------------------
-------FAQ-Main-------
----------------------------->

<section class="faq-main common-padding">
    <div class="container"> <!-- Container .// -->
        <div class="row"> <!-- Row .// -->
            <div class="col-lg-6"> <!-- Col .// -->

            <div class="faq-group-wrapper">
                <h4 class="page-title">Frequently Asked Questions</h4>

                <div class="faq-group">

                    <div class="faq-box-wrap"> <!--- FAQ-Box .// -->
                        <p class="faq-question" data-pannel="pannel1">What does your Company do? <span class="float-right"><i class="fas fa-chevron-down"></i></span></p>
                        <div class="faq-answer-wrap" id="pannel1">
                            <p class="faq-answer">Lorem ipsum dolor sit, amet consectetur adipisicing elit. Exercitationem nisi necessitatibus omnis laborum quibusdam error fugit autem, eligendi hic beatae vitae expedita maxime nihil itaque non optio dolorem fugiat, praesentium quos recusandae asperiores mollitia in?</p>
                        </div>
                    </div> <!--- FAQ-Box .// -->

                    <div class="faq-box-wrap"> <!--- FAQ-Box .// -->
                        <p class="faq-question" data-pannel="pannel2">What industries do you specialize in? <span class="float-right"><i class="fas fa-chevron-down"></i></span></p>
                        <div class="faq-answer-wrap" id="pannel2">
                            <p class="faq-answer">Lorem ipsum dolor sit, amet consectetur adipisicing elit. Exercitationem nisi necessitatibus omnis laborum quibusdam error fugit autem, eligendi hic beatae vitae expedita maxime nihil itaque non optio dolorem fugiat, praesentium quos recusandae asperiores mollitia in?</p>
                        </div>
                    </div> <!--- FAQ-Box .// -->

                    <div class="faq-box-wrap"> <!--- FAQ-Box .// -->
                        <p class="faq-question" data-pannel="pannel3">Can you guarantee that our plan will raise capital? <span class="float-right"><i class="fas fa-chevron-down"></i></span></p>
                        <div class="faq-answer-wrap" id="pannel3">
                            <p class="faq-answer">Lorem ipsum dolor sit, amet consectetur adipisicing elit. Exercitationem nisi necessitatibus omnis laborum quibusdam error fugit autem, eligendi hic beatae vitae expedita maxime nihil itaque non optio dolorem fugiat, praesentium quos recusandae asperiores mollitia in?</p>
                        </div>
                    </div> <!--- FAQ-Box .// -->

                    <div class="faq-box-wrap"> <!--- FAQ-Box .// -->
                        <p class="faq-question" data-pannel="pannel4">What does your Company do? <span class="float-right"><i class="fas fa-chevron-down"></i></span></p>
                        <div class="faq-answer-wrap" id="pannel4">
                            <p class="faq-answer">Lorem ipsum dolor sit, amet consectetur adipisicing elit. Exercitationem nisi necessitatibus omnis laborum quibusdam error fugit autem, eligendi hic beatae vitae expedita maxime nihil itaque non optio dolorem fugiat, praesentium quos recusandae asperiores mollitia in?</p>
                        </div>
                    </div> <!--- FAQ-Box .// -->

                    <div class="faq-box-wrap"> <!--- FAQ-Box .// -->
                        <p class="faq-question" data-pannel="pannel5">What industries do you specialize in? <span class="float-right"><i class="fas fa-chevron-down"></i></span></p>
                        <div class="faq-answer-wrap" id="pannel5">
                            <p class="faq-answer">Lorem ipsum dolor sit, amet consectetur adipisicing elit. Exercitationem nisi necessitatibus omnis laborum quibusdam error fugit autem, eligendi hic beatae vitae expedita maxime nihil itaque non optio dolorem fugiat, praesentium quos recusandae asperiores mollitia in?</p>
                        </div>
                    </div> <!--- FAQ-Box .// -->

                    <div class="faq-box-wrap"> <!--- FAQ-Box .// -->
                        <p class="faq-question" data-pannel="pannel6">Can you guarantee that our plan will raise capital? <span class="float-right"><i class="fas fa-chevron-down"></i></span></p>
                        <div class="faq-answer-wrap" id="pannel6">
                            <p class="faq-answer">Lorem ipsum dolor sit, amet consectetur adipisicing elit. Exercitationem nisi necessitatibus omnis laborum quibusdam error fugit autem, eligendi hic beatae vitae expedita maxime nihil itaque non optio dolorem fugiat, praesentium quos recusandae asperiores mollitia in?</p>
                        </div>
                    </div> <!--- FAQ-Box .// -->

                </div>

            </div>
                
            </div> <!-- Col .// -->
            <div class="col-lg-6"> <!-- Col .// -->
                <div class="register-box">
                    <h4 class="page-title">Ask Any Question</h4>
                    <!-- <form action="faq"> -->
                        <div class="login-form faq-form">
                            <input type="text" placeholder="Full Name">
                            <input type="email" placeholder="Email Address">
                            <input type="text" placeholder="Contact Number">
                            <textarea name="faq-message" id="faq-desc" placeholder="Your Question..."></textarea>

                            <div class="row"> <!-- Inner Row .// -->
                                <div class="col-sm-6 col-xs-6"> <!-- Inner Col .// -->
                                    <button type="submit" class="form-button" data-toggle="modal" data-target="#exampleModal">Submit</button>
                                </div> <!-- Inner Col .// -->
                            </div> <!-- Inner Row .// -->

                        </div>
                    <!-- </form> -->
                </div>
            </div> <!-- Col .// -->
        </div> <!-- Row .// -->
    </div> <!-- Container .// -->

    <!----------------------------
    -------FAQ-Modal-------
    ----------------------------->

    <!-- Modal -->
    <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">

        <div class="modal-body text-center"> <!-- Modal Body .// -->
            <div class="success-icon">
                <i class="far fa-check-circle"></i>
            </div>
            <h4 class="page-title">Submitted Successfully !</h4>
            <p class="success-desc">Thank You ! We are glad to hear your opinion. Please take 2 minutes for rate app in <a href="#">App Store</a> or Know more <a href="#">About Us</a>. </p>
            <div class="text-center">
                <button type="button" class="form-button mt-4" data-dismiss="modal">Done !</button>
            </div>
        </div> <!-- Modal Body .// -->
        </div>
    </div>
    </div>

    <!----------------------------
    -------FAQ-Modal-------
    ----------------------------->

</section>

<!----------------------------
-------FAQ-Main-------
----------------------------->

<?php include('include/footer.php'); ?>