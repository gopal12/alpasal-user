<?php include('include/header.php'); ?>

<!----------------------------
-------Breadcrumb-------
----------------------------->

<nav aria-label="breadcrumb" class="breadcrumb-main bg-para" style="background: linear-gradient(rgba(0,0,0,0.4),rgba(0,0,0,0.4)), url('img/bread.jpg');">
    <div class="container clearfix"> <!-- Container .// -->
        <h3 class="float-left">Privacy Policy</h3>
        <ol class="breadcrumb float-right">
            <li class="breadcrumb-item"><a href="index.php">Home</a></li>
            <!-- <li class="breadcrumb-item"><a href="#">Library</a></li> -->
            <li class="breadcrumb-item active" aria-current="page">Privacy Policy</li>
        </ol>
    </div> <!-- Container .// -->
</nav>

<!----------------------------
-------Breadcrumb-------
----------------------------->

<!----------------------------
------Terms-&-Conditions------
----------------------------->

<section class="terms privacy common-padding">
    <div class="container"> <!-- Container .// -->
        <h3 class="section-title">Privacy Policy <span>(Last Updated On: 14/07/2019)</span></h3>

        <div class="box-modal"> <!-- Box-Modal .// -->

            <div class="term-group"> <!-- Term-Group .// -->
                <p class="normal-content"><strong>Lorem ipsum, dolor sit amet consectetur adipisicing elit. Aspernatur ipsam atque quaerat hic officiis temporibus mollitia reprehenderit laboriosam nobis? Et sequi voluptatum,</strong> dicta totam non tenetur velit ratione voluptatem modi omnis numquam vel repellendus maxime quam asperiores incidunt similique a facere magnam ducimus aspernatur nemo. Nobis libero accusantium, aliquam impedit, corrupti officia labore illo quod, similique dolorum consectetur architecto quisquam error. Illum, provident voluptatem voluptate, enim magnam explicabo itaque excepturi consequatur, adipisci maiores neque natus. Lorem ipsum dolor, ex fuga nesciunt qui, unde sit neque dignissimos sunt eos sint? Porro consequatur totam inventore deserunt magni eligendi debitis atque, itaque ipsum! Recusandae numquam est odit hic magni voluptatem temporibus? Tempore quis temporibus debitis dolorem corrupti. Earum, praesentium? Culpa velit corporis pariatur consectetur aspernatur enim error. Harum accusamus commodi vel aperiam atque adipisci, corrupti, obcaecati possimus itaque mollitia fugiat doloribus enim aspernatur placeat ea facilis magni voluptates? Debitis, fugiat porro, accusantium nihil molestiae nulla deserunt sit alias facilis tenetur, natus id harum praesentium.</p>
            </div> <!-- Term-Group .// -->

            <div class="term-group"> <!-- Term-Group .// -->
                <h4 class="page-title">AUTHORIZATION:</h4>
                <p class="normal-content">Lorem ipsum dolor sit amet consectetur adipisicing elit. Delectus, sed quis! Quae minima distinctio est reiciendis qui, debitis impedit assumenda ea recusandae illum pariatur autem odio et accusantium facilis dolore porro laboriosam consequuntur dolorum nihil?</p>
                <p class="normal-content">Lorem ipsum dolor sit amet consectetur adipisicing elit. Incidunt quibusdam maxime molestiae veniam neque quo laborum laboriosam natus fugit. Ab ut deserunt, quo quam cum perferendis maiores qui vitae nam sed veniam officia repellendus quis tempora asperiores repudiandae exercitationem distinctio omnis accusamus id ullam incidunt quas a! Non unde quisquam tempore est numquam, ullam minima.</p>
            </div> <!-- Term-Group .// -->

            <div class="term-group"> <!-- Term-Group .// -->
                <h4 class="page-title">PRIVACY / DATA PROTECTION RIGHTS</h4>
                <p class="normal-content">Lorem, ipsum dolor sit amet consectetur adipisicing elit. Perferendis earum velit mollitia. Doloremque, eum fugit! Lorem ipsum, dolor sit amet consectetur adipisicing elit. Corporis asperiores error nesciunt praesentium ipsam quibusdam?</p>
                <ul class="privacy-list">
                    <li class="normal-content">Lorem ipsum dolor sit, amet consectetur adipisicing elit. Vero impedit exercitationem sint laborum explicabo deleniti facere. Quia praesentium numquam necessitatibus quos laborum. Voluptate, aliquid quo!</li>
                    <li class="normal-content">Lorem ipsum dolor sit amet consectetur adipisicing elit. Iste ea commodi cumque dolor quia ullam totam repellat assumenda. Veniam, laboriosam.</li>
                    <li class="normal-content">Lorem ipsum dolor sit amet consectetur adipisicing elit. Quo minima hic itaque, ratione amet maiores est maxime qui dolor sint deserunt fuga ex voluptates. Distinctio velit explicabo optio aut inventore laudantium, minus ea impedit sed!</li>
                </ul>
            </div> <!-- Term-Group .// -->

        </div> <!-- Box-Modal .// -->
    </div> <!-- Container .// -->
</section>

<!----------------------------
------Terms-&-Conditions------
----------------------------->

<?php include('include/footer.php'); ?>