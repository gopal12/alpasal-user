<?php include('include/header.php'); ?>

<!----------------------------
-------Breadcrumb-------
----------------------------->

<nav aria-label="breadcrumb" class="breadcrumb-main bg-para" style="background: linear-gradient(rgba(0,0,0,0.4),rgba(0,0,0,0.4)), url('img/bread.jpg');">
    <div class="container clearfix"> <!-- Container .// -->
        <h3 class="float-left">Register</h3>
        <ol class="breadcrumb float-right">
            <li class="breadcrumb-item"><a href="index.php">Home</a></li>
            <!-- <li class="breadcrumb-item"><a href="#">Library</a></li> -->
            <li class="breadcrumb-item active" aria-current="page">Register</li>
        </ol>
    </div> <!-- Container .// -->
</nav>

<!----------------------------
-------Breadcrumb-------
----------------------------->

<!----------------------------
-------Register-------
----------------------------->

<section class="register-main">
    <div class="container"> <!-- Container .// -->
        <div class="row"> <!-- Row .// -->

            <div class="col-lg-6"> <!-- Col .// -->
                <div class="h-100">
                    <div class="register-phone-wrap sticky-top">
                        <img src="img/phone.png" alt="Phone-Image">
                    </div>
                </div>
            </div> <!-- Col .// -->

            <div class="col-lg-6"> <!-- Col .// -->
                <div class="register-box">
                    <h4 class="page-title text-center">Create Account</h4>
                    <p class="normal-content text-center my-3">Sign up to view and bid different Products.</p>

                    <div class="tog-btn row"> <!-- Inner-Row .// -->
                        <div class="col-sm-6 my-3"> <!-- Inner-Col .// -->
                            <button class="form-button active" id="user-bttn">User Signup</button>
                        </div> <!-- Inner-Col .// -->
                        <div class="col-sm-6 my-3"> <!-- Inner-Col .// -->
                            <button class="form-button" id="vendor-bttn">Vendor Signup</button>
                        </div> <!-- Inner-Col .// -->
                    </div> <!-- Inner-Row .// -->

                    <div class="sign-up-icon text-center">
                        <img src="img/icons/signup.png" alt="ICON">
                    </div>

                    <div class="login-form user-signup" id="user-signup"> <!-- User-Signup .// -->
                        <p class="normal-content text-center text-uppercase color-theme">User Signup</p>

                        <form action="user-signup">
                            <input type="text" placeholder="Full Name *" required>
                            <input type="email" placeholder="Email Address *" required>
                            <input type="password" placeholder="Password *" required>
                            <input type="password" placeholder="Confirm Password *" required>

                            <select class="signup-select" name="address[]">
                                <option value="0">Select Address</option>
                                <option value="1">Kathmandu</option>
                                <option value="2">Pokhara</option>
                                <option value="3">Galchi</option>
                                <option value="4">Sukute</option>
                                <option value="5">Bandipur</option>
                            </select>

                            <input type="text" placeholder="Telephone Number">
                            <input type="text" placeholder="Mobile Number *" requried>

                            <div class="mn-7">
                            <select class="signup-select business-area" name="states[]" multiple="multiple">
                                <option value="1">Mechi</option>
                                <option value="2">Koshi</option>
                                <option value="3">Sagarmatha</option>
                                <option value="4">Janakpur</option>
                                <option value="5">Narayani</option>
                                <option value="6">Bagmati</option>
                                <option value="7">Gandaki</option>
                                <option value="8">Lumbini</option>
                                <option value="9">Dhaulagiri</option>
                                <option value="10">Rapti</option>
                                <option value="11">Karnali</option>
                                <option value="12">Bheri</option>
                                <option value="13">Seti</option>
                                <option value="14">Mahakali</option>
                            </select>
                            </div>

                            <div class="form-group">
                                <label for="user-image">User Image *</label>
                                <input type="file" class="form-control" name="userImage" id="user-image">
                            </div>

                            <div class="form-group">
                                <label for="comp-logo">Logo *</label>
                                <input type="file" class="form-control" name="userImage" id="comp-logo">
                            </div>

                            <div class="form-group">
                                <label for="coverImage">Cover Image *</label>
                                <input type="file" class="form-control" name="userImage" id="coverImage">
                            </div>

                            <div class="form-check">
                                <input type="checkbox" class="valid-check form-check-input" id="exampleCheck1">
                                <label class="form-check-label" for="exampleCheck1">I agree to all the terms & conditions.</label>
                            </div>

                            <button type="submit" class="valid-btn form-button">Register</button>

                        </form>

                    </div> <!-- User-Signup .// -->

                    <div class="login-form user-signup" id="vendor-signup"> <!-- Vendor-Signup .// -->
                        <p class="normal-content text-center text-uppercase color-theme">Vendor Signup</p>

                        <form action="user-signup">
                            <input type="text" placeholder="Shop Name *" required>
                            <input type="email" placeholder="Email Address *" required>
                            <input type="password" placeholder="Password *" required>
                            <input type="password" placeholder="Confirm Password *" required>
                            <input type="text" placeholder="Telephone Number">
                            <input type="text" placeholder="Mobile Number *" requried>

                            <div class="form-group">
                                <label for="comp-logo1">Logo *</label>
                                <input type="file" class="form-control" name="userImage" id="comp-logo1">
                            </div>

                            <div class="form-group">
                                <label for="user-image1">Image *</label>
                                <input type="file" class="form-control" name="userImage" id="user-image1">
                            </div>

                            <div class="form-group">
                                <label for="coverImage1">Cover Image *</label>
                                <input type="file" class="form-control" name="userImage" id="coverImage1">
                            </div>

                            <select class="signup-select" name="address[]">
                                <option value="0">Select Address</option>
                                <option value="1">Kathmandu</option>
                                <option value="2">Pokhara</option>
                                <option value="3">Galchi</option>
                                <option value="4">Sukute</option>
                                <option value="5">Bandipur</option>
                            </select>

                            <input type="number" placeholder="PAN / VAT number *" requried>

                            <div class="mn-7">
                            <select class="signup-select business-area" name="states[]" multiple="multiple">
                                <option value="1">Mechi</option>
                                <option value="2">Koshi</option>
                                <option value="3">Sagarmatha</option>
                                <option value="4">Janakpur</option>
                                <option value="5">Narayani</option>
                                <option value="6">Bagmati</option>
                                <option value="7">Gandaki</option>
                                <option value="8">Lumbini</option>
                                <option value="9">Dhaulagiri</option>
                                <option value="10">Rapti</option>
                                <option value="11">Karnali</option>
                                <option value="12">Bheri</option>
                                <option value="13">Seti</option>
                                <option value="14">Mahakali</option>
                            </select>
                            </div>

                            <div class="mn-7">
                            <select class="signup-select select-categories" name="states[]" multiple="multiple">
                                <option value="1">Hospitality</option>
                                <option value="2">Information Technology</option>
                                <option value="3">Cafe / Restaurant</option>
                                <option value="4">Category 1</option>
                                <option value="5">Category 2</option>
                            </select>
                            </div>

                            <textarea name="info" id="about-info" placeholder="Description"></textarea>

                            <div class="form-check">
                                <input type="checkbox" class="valid-check form-check-input" id="exampleCheck11">
                                <label class="form-check-label" for="exampleCheck11">I agree to all the terms & conditions.</label>
                            </div>

                            <button type="submit" class="form-button">Register</button>

                        </form>

                    </div> <!-- Vendor-Signup .// -->

                </div>
            </div> <!-- Col .// -->

        </div> <!-- Row .// -->
    </div> <!-- Container .// -->
</section>

<!----------------------------
-------Register-------
----------------------------->

<?php include('include/footer.php'); ?>