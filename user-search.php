<?php include('include/header.php'); ?>

<section class="bg-grey p-t-b-20">
	<div class="container-fluid"> 
	    <div class="row">
	        <div class="col-md-12">
	            <div class="card m-b-0"> 
	                <div class="card-body p-20">
	                    <h3 class="search-crumb-h3">Search Result For "Angular Js"</h3> 
	                    <h5 class="search-crumb-h5">700 result Founds.</h5>
	                </div>
	            </div> 
	        </div> 
	    </div> 
	</div>
</section>

<section class="main-products all-products bg-grey" id="infinite-products">
    <div class="container-fluid">
        <div class="white-card"> 
            <div class="custom-grid"> <!-- Parent .// -->

                <div class="custom-grid-item"> <!-- Card .// -->
                    <div class="product-card" data-mh="product">
                        <div class="card-image">
                            <a href="javascript:">
                                <img data-src="img/products/product1.jpg" alt="Product-Image" class="lazyload">
                            </a>
                            <div class="card-buttons">
                                <a href="javascript:">Add to cart</a>
                                <a href="javascript:"><i class="far fa-eye"></i></a>
                                <a href="javascript:"><i class="fas fa-heart"></i></a>
                                <a href="javascript:"><i class="fas fa-copy"></i></a>
                            </div>
                        </div>
                        <div class="card-detail">
                            <div class="rating">
                                <ul class="list-unstyled side-listing">
                                    <li><i class="fas fa-star active"></i></li>
                                    <li><i class="fas fa-star active"></i></li>
                                    <li><i class="fas fa-star active"></i></li>
                                    <li><i class="fas fa-star"></i></li>
                                    <li><i class="fas fa-star"></i></li>
                                </ul>
                            </div>
                            <p class="product-title"><a href="javascript:">Butterflies Hand-held Bag</a></p>
                            <p class="product-price">RS. 1,00,100</p>
                        </div>
                    </div>
                </div> <!-- Card .// -->

                <div class="custom-grid-item"> <!-- Card .// -->
                    <div class="product-card" data-mh="product">
                        <div class="card-image">
                            <a href="javascript:">
                                <img data-src="img/products/product2.jpg" alt="Product-Image" class="lazyload">
                            </a>
                            <div class="card-buttons">
                                <a href="javascript:">Add to cart</a>
                                <a href="javascript:"><i class="far fa-eye"></i></a>
                                <a href="javascript:"><i class="fas fa-heart"></i></a>
                                <a href="javascript:"><i class="fas fa-copy"></i></a>
                            </div>
                        </div>
                        <div class="card-detail">
                            <div class="rating">
                                <ul class="list-unstyled side-listing">
                                    <li><i class="fas fa-star active"></i></li>
                                    <li><i class="fas fa-star active"></i></li>
                                    <li><i class="fas fa-star active"></i></li>
                                    <li><i class="fas fa-star"></i></li>
                                    <li><i class="fas fa-star"></i></li>
                                </ul>
                            </div>
                            <p class="product-title"><a href="javascript:">Butterflies Hand-held Bag</a></p>
                            <p class="product-price">RS. 1,00,100</p>
                        </div>
                    </div>
                </div> <!-- Card .// -->

                <div class="custom-grid-item"> <!-- Card .// -->
                    <div class="product-card" data-mh="product">
                        <div class="card-image">
                            <a href="javascript:">
                                <img data-src="img/products/product5.jpg" alt="Product-Image" class="lazyload">
                            </a>
                            <div class="card-buttons">
                                <a href="javascript:">Add to cart</a>
                                <a href="javascript:"><i class="far fa-eye"></i></a>
                                <a href="javascript:"><i class="fas fa-heart"></i></a>
                                <a href="javascript:"><i class="fas fa-copy"></i></a>
                            </div>
                        </div>
                        <div class="card-detail">
                            <div class="rating">
                                <ul class="list-unstyled side-listing">
                                    <li><i class="fas fa-star active"></i></li>
                                    <li><i class="fas fa-star active"></i></li>
                                    <li><i class="fas fa-star active"></i></li>
                                    <li><i class="fas fa-star"></i></li>
                                    <li><i class="fas fa-star"></i></li>
                                </ul>
                            </div>
                            <p class="product-title"><a href="javascript:">Butterflies Hand-held Bag</a></p>
                            <p class="product-price">RS. 1,00,100</p>
                        </div>
                    </div>
                </div> <!-- Card .// -->

                <div class="custom-grid-item"> <!-- Card .// -->
                    <div class="product-card" data-mh="product">
                        <div class="card-image">
                            <a href="javascript:">
                                <img data-src="img/products/product9.webp" alt="Product-Image" class="lazyload">
                            </a>
                            <div class="card-buttons">
                                <a href="javascript:">Add to cart</a>
                                <a href="javascript:"><i class="far fa-eye"></i></a>
                                <a href="javascript:"><i class="fas fa-heart"></i></a>
                                <a href="javascript:"><i class="fas fa-copy"></i></a>
                            </div>
                        </div>
                        <div class="card-detail">
                            <div class="rating">
                                <ul class="list-unstyled side-listing">
                                    <li><i class="fas fa-star active"></i></li>
                                    <li><i class="fas fa-star active"></i></li>
                                    <li><i class="fas fa-star active"></i></li>
                                    <li><i class="fas fa-star"></i></li>
                                    <li><i class="fas fa-star"></i></li>
                                </ul>
                            </div>
                            <p class="product-title"><a href="javascript:">Butterflies Hand-held Bag</a></p>
                            <p class="product-price">RS. 1,00,100</p>
                        </div>
                    </div>
                </div> <!-- Card .// -->

                <div class="custom-grid-item"> <!-- Card .// -->
                    <div class="product-card" data-mh="product">
                        <div class="card-image">
                            <a href="javascript:">
                                <img data-src="img/products/product3.jpg" alt="Product-Image" class="lazyload">
                            </a>
                            <div class="card-buttons">
                                <a href="javascript:">Add to cart</a>
                                <a href="javascript:"><i class="far fa-eye"></i></a>
                                <a href="javascript:"><i class="fas fa-heart"></i></a>
                                <a href="javascript:"><i class="fas fa-copy"></i></a>
                            </div>
                        </div>
                        <div class="card-detail">
                            <div class="rating">
                                <ul class="list-unstyled side-listing">
                                    <li><i class="fas fa-star active"></i></li>
                                    <li><i class="fas fa-star active"></i></li>
                                    <li><i class="fas fa-star active"></i></li>
                                    <li><i class="fas fa-star"></i></li>
                                    <li><i class="fas fa-star"></i></li>
                                </ul>
                            </div>
                            <p class="product-title"><a href="javascript:">Butterflies Hand-held Bag</a></p>
                            <p class="product-price">RS. 1,00,100</p>
                        </div>
                    </div>
                </div> <!-- Card .// -->

                <div class="custom-grid-item"> <!-- Card .// -->
                    <div class="product-card" data-mh="product">
                        <div class="card-image">
                            <a href="javascript:">
                                <img data-src="img/products/product4.jpg" alt="Product-Image" class="lazyload">
                            </a>
                            <div class="card-buttons">
                                <a href="javascript:">Add to cart</a>
                                <a href="javascript:"><i class="far fa-eye"></i></a>
                                <a href="javascript:"><i class="fas fa-heart"></i></a>
                                <a href="javascript:"><i class="fas fa-copy"></i></a>
                            </div>
                        </div>
                        <div class="card-detail">
                            <div class="rating">
                                <ul class="list-unstyled side-listing">
                                    <li><i class="fas fa-star active"></i></li>
                                    <li><i class="fas fa-star active"></i></li>
                                    <li><i class="fas fa-star active"></i></li>
                                    <li><i class="fas fa-star"></i></li>
                                    <li><i class="fas fa-star"></i></li>
                                </ul>
                            </div>
                            <p class="product-title"><a href="javascript:">Butterflies Hand-held Bag</a></p>
                            <p class="product-price">RS. 1,00,100</p>
                        </div>
                    </div>
                </div> <!-- Card .// -->

                <div class="custom-grid-item"> <!-- Card .// -->
                    <div class="product-card" data-mh="product">
                        <div class="card-image">
                            <a href="javascript:">
                                <img data-src="img/products/product6.jpg" alt="Product-Image" class="lazyload">
                            </a>
                            <div class="card-buttons">
                                <a href="javascript:">Add to cart</a>
                                <a href="javascript:"><i class="far fa-eye"></i></a>
                                <a href="javascript:"><i class="fas fa-heart"></i></a>
                                <a href="javascript:"><i class="fas fa-copy"></i></a>
                            </div>
                        </div>
                        <div class="card-detail">
                            <div class="rating">
                                <ul class="list-unstyled side-listing">
                                    <li><i class="fas fa-star active"></i></li>
                                    <li><i class="fas fa-star active"></i></li>
                                    <li><i class="fas fa-star active"></i></li>
                                    <li><i class="fas fa-star"></i></li>
                                    <li><i class="fas fa-star"></i></li>
                                </ul>
                            </div>
                            <p class="product-title"><a href="javascript:">Butterflies Hand-held Bag</a></p>
                            <p class="product-price">RS. 1,00,100</p>
                        </div>
                    </div>
                </div> <!-- Card .// -->

                <div class="custom-grid-item"> <!-- Card .// -->
                    <div class="product-card" data-mh="product">
                        <div class="card-image">
                            <a href="javascript:">
                                <img data-src="img/products/product9.webp" alt="Product-Image" class="lazyload">
                            </a>
                            <div class="card-buttons">
                                <a href="javascript:">Add to cart</a>
                                <a href="javascript:"><i class="far fa-eye"></i></a>
                                <a href="javascript:"><i class="fas fa-heart"></i></a>
                                <a href="javascript:"><i class="fas fa-copy"></i></a>
                            </div>
                        </div>
                        <div class="card-detail">
                            <div class="rating">
                                <ul class="list-unstyled side-listing">
                                    <li><i class="fas fa-star active"></i></li>
                                    <li><i class="fas fa-star active"></i></li>
                                    <li><i class="fas fa-star active"></i></li>
                                    <li><i class="fas fa-star"></i></li>
                                    <li><i class="fas fa-star"></i></li>
                                </ul>
                            </div>
                            <p class="product-title"><a href="javascript:">Butterflies Hand-held Bag</a></p>
                            <p class="product-price">RS. 1,00,100</p>
                        </div>
                    </div>
                </div> <!-- Card .// -->

                <div class="custom-grid-item"> <!-- Card .// -->
                    <div class="product-card" data-mh="product">
                        <div class="card-image">
                            <a href="javascript:">
                                <img data-src="img/products/product1.jpg" alt="Product-Image" class="lazyload">
                            </a>
                            <div class="card-buttons">
                                <a href="javascript:">Add to cart</a>
                                <a href="javascript:"><i class="far fa-eye"></i></a>
                                <a href="javascript:"><i class="fas fa-heart"></i></a>
                                <a href="javascript:"><i class="fas fa-copy"></i></a>
                            </div>
                        </div>
                        <div class="card-detail">
                            <div class="rating">
                                <ul class="list-unstyled side-listing">
                                    <li><i class="fas fa-star active"></i></li>
                                    <li><i class="fas fa-star active"></i></li>
                                    <li><i class="fas fa-star active"></i></li>
                                    <li><i class="fas fa-star"></i></li>
                                    <li><i class="fas fa-star"></i></li>
                                </ul>
                            </div>
                            <p class="product-title"><a href="javascript:">Butterflies Hand-held Bag</a></p>
                            <p class="product-price">RS. 1,00,100</p>
                        </div>
                    </div>
                </div> <!-- Card .// -->

                <div class="custom-grid-item"> <!-- Card .// -->
                    <div class="product-card" data-mh="product">
                        <div class="card-image">
                            <a href="javascript:">
                                <img data-src="img/products/product2.jpg" alt="Product-Image" class="lazyload">
                            </a>
                            <div class="card-buttons">
                                <a href="javascript:">Add to cart</a>
                                <a href="javascript:"><i class="far fa-eye"></i></a>
                                <a href="javascript:"><i class="fas fa-heart"></i></a>
                                <a href="javascript:"><i class="fas fa-copy"></i></a>
                            </div>
                        </div>
                        <div class="card-detail">
                            <div class="rating">
                                <ul class="list-unstyled side-listing">
                                    <li><i class="fas fa-star active"></i></li>
                                    <li><i class="fas fa-star active"></i></li>
                                    <li><i class="fas fa-star active"></i></li>
                                    <li><i class="fas fa-star"></i></li>
                                    <li><i class="fas fa-star"></i></li>
                                </ul>
                            </div>
                            <p class="product-title"><a href="javascript:">Butterflies Hand-held Bag</a></p>
                            <p class="product-price">RS. 1,00,100</p>
                        </div>
                    </div>
                </div> <!-- Card .// -->

                <div class="custom-grid-item"> <!-- Card .// -->
                    <div class="product-card" data-mh="product">
                        <div class="card-image">
                            <a href="javascript:">
                                <img data-src="img/products/product5.jpg" alt="Product-Image" class="lazyload">
                            </a>
                            <div class="card-buttons">
                                <a href="javascript:">Add to cart</a>
                                <a href="javascript:"><i class="far fa-eye"></i></a>
                                <a href="javascript:"><i class="fas fa-heart"></i></a>
                                <a href="javascript:"><i class="fas fa-copy"></i></a>
                            </div>
                        </div>
                        <div class="card-detail">
                            <div class="rating">
                                <ul class="list-unstyled side-listing">
                                    <li><i class="fas fa-star active"></i></li>
                                    <li><i class="fas fa-star active"></i></li>
                                    <li><i class="fas fa-star active"></i></li>
                                    <li><i class="fas fa-star"></i></li>
                                    <li><i class="fas fa-star"></i></li>
                                </ul>
                            </div>
                            <p class="product-title"><a href="javascript:">Butterflies Hand-held Bag</a></p>
                            <p class="product-price">RS. 1,00,100</p>
                        </div>
                    </div>
                </div> <!-- Card .// -->

                <div class="custom-grid-item"> <!-- Card .// -->
                    <div class="product-card" data-mh="product">
                        <div class="card-image">
                            <a href="javascript:">
                                <img data-src="img/products/product9.webp" alt="Product-Image" class="lazyload">
                            </a>
                            <div class="card-buttons">
                                <a href="javascript:">Add to cart</a>
                                <a href="javascript:"><i class="far fa-eye"></i></a>
                                <a href="javascript:"><i class="fas fa-heart"></i></a>
                                <a href="javascript:"><i class="fas fa-copy"></i></a>
                            </div>
                        </div>
                        <div class="card-detail">
                            <div class="rating">
                                <ul class="list-unstyled side-listing">
                                    <li><i class="fas fa-star active"></i></li>
                                    <li><i class="fas fa-star active"></i></li>
                                    <li><i class="fas fa-star active"></i></li>
                                    <li><i class="fas fa-star"></i></li>
                                    <li><i class="fas fa-star"></i></li>
                                </ul>
                            </div>
                            <p class="product-title"><a href="javascript:">Butterflies Hand-held Bag</a></p>
                            <p class="product-price">RS. 1,00,100</p>
                        </div>
                    </div>
                </div> <!-- Card .// -->

                <div class="custom-grid-item"> <!-- Card .// -->
                    <div class="product-card" data-mh="product">
                        <div class="card-image">
                            <a href="javascript:">
                                <img data-src="img/products/product3.jpg" alt="Product-Image" class="lazyload">
                            </a>
                            <div class="card-buttons">
                                <a href="javascript:">Add to cart</a>
                                <a href="javascript:"><i class="far fa-eye"></i></a>
                                <a href="javascript:"><i class="fas fa-heart"></i></a>
                                <a href="javascript:"><i class="fas fa-copy"></i></a>
                            </div>
                        </div>
                        <div class="card-detail">
                            <div class="rating">
                                <ul class="list-unstyled side-listing">
                                    <li><i class="fas fa-star active"></i></li>
                                    <li><i class="fas fa-star active"></i></li>
                                    <li><i class="fas fa-star active"></i></li>
                                    <li><i class="fas fa-star"></i></li>
                                    <li><i class="fas fa-star"></i></li>
                                </ul>
                            </div>
                            <p class="product-title"><a href="javascript:">Butterflies Hand-held Bag</a></p>
                            <p class="product-price">RS. 1,00,100</p>
                        </div>
                    </div>
                </div> <!-- Card .// -->

                <div class="custom-grid-item"> <!-- Card .// -->
                    <div class="product-card" data-mh="product">
                        <div class="card-image">
                            <a href="javascript:">
                                <img data-src="img/products/product4.jpg" alt="Product-Image" class="lazyload">
                            </a>
                            <div class="card-buttons">
                                <a href="javascript:">Add to cart</a>
                                <a href="javascript:"><i class="far fa-eye"></i></a>
                                <a href="javascript:"><i class="fas fa-heart"></i></a>
                                <a href="javascript:"><i class="fas fa-copy"></i></a>
                            </div>
                        </div>
                        <div class="card-detail">
                            <div class="rating">
                                <ul class="list-unstyled side-listing">
                                    <li><i class="fas fa-star active"></i></li>
                                    <li><i class="fas fa-star active"></i></li>
                                    <li><i class="fas fa-star active"></i></li>
                                    <li><i class="fas fa-star"></i></li>
                                    <li><i class="fas fa-star"></i></li>
                                </ul>
                            </div>
                            <p class="product-title"><a href="javascript:">Butterflies Hand-held Bag</a></p>
                            <p class="product-price">RS. 1,00,100</p>
                        </div>
                    </div>
                </div> <!-- Card .// -->

                <div class="custom-grid-item"> <!-- Card .// -->
                    <div class="product-card" data-mh="product">
                        <div class="card-image">
                            <a href="javascript:">
                                <img data-src="img/products/product6.jpg" alt="Product-Image" class="lazyload">
                            </a>
                            <div class="card-buttons">
                                <a href="javascript:">Add to cart</a>
                                <a href="javascript:"><i class="far fa-eye"></i></a>
                                <a href="javascript:"><i class="fas fa-heart"></i></a>
                                <a href="javascript:"><i class="fas fa-copy"></i></a>
                            </div>
                        </div>
                        <div class="card-detail">
                            <div class="rating">
                                <ul class="list-unstyled side-listing">
                                    <li><i class="fas fa-star active"></i></li>
                                    <li><i class="fas fa-star active"></i></li>
                                    <li><i class="fas fa-star active"></i></li>
                                    <li><i class="fas fa-star"></i></li>
                                    <li><i class="fas fa-star"></i></li>
                                </ul>
                            </div>
                            <p class="product-title"><a href="javascript:">Butterflies Hand-held Bag</a></p>
                            <p class="product-price">RS. 1,00,100</p>
                        </div>
                    </div>
                </div> <!-- Card .// -->

                <div class="custom-grid-item"> <!-- Card .// -->
                    <div class="product-card" data-mh="product">
                        <div class="card-image">
                            <a href="javascript:">
                                <img data-src="img/products/product9.webp" alt="Product-Image" class="lazyload">
                            </a>
                            <div class="card-buttons">
                                <a href="javascript:">Add to cart</a>
                                <a href="javascript:"><i class="far fa-eye"></i></a>
                                <a href="javascript:"><i class="fas fa-heart"></i></a>
                                <a href="javascript:"><i class="fas fa-copy"></i></a>
                            </div>
                        </div>
                        <div class="card-detail">
                            <div class="rating">
                                <ul class="list-unstyled side-listing">
                                    <li><i class="fas fa-star active"></i></li>
                                    <li><i class="fas fa-star active"></i></li>
                                    <li><i class="fas fa-star active"></i></li>
                                    <li><i class="fas fa-star"></i></li>
                                    <li><i class="fas fa-star"></i></li>
                                </ul>
                            </div>
                            <p class="product-title"><a href="javascript:">Butterflies Hand-held Bag</a></p>
                            <p class="product-price">RS. 1,00,100</p>
                        </div>
                    </div>
                </div> <!-- Card .// -->

                <div class="custom-grid-item"> <!-- Card .// -->
                    <div class="product-card" data-mh="product">
                        <div class="card-image">
                            <a href="javascript:">
                                <img data-src="img/products/product1.jpg" alt="Product-Image" class="lazyload">
                            </a>
                            <div class="card-buttons">
                                <a href="javascript:">Add to cart</a>
                                <a href="javascript:"><i class="far fa-eye"></i></a>
                                <a href="javascript:"><i class="fas fa-heart"></i></a>
                                <a href="javascript:"><i class="fas fa-copy"></i></a>
                            </div>
                        </div>
                        <div class="card-detail">
                            <div class="rating">
                                <ul class="list-unstyled side-listing">
                                    <li><i class="fas fa-star active"></i></li>
                                    <li><i class="fas fa-star active"></i></li>
                                    <li><i class="fas fa-star active"></i></li>
                                    <li><i class="fas fa-star"></i></li>
                                    <li><i class="fas fa-star"></i></li>
                                </ul>
                            </div>
                            <p class="product-title"><a href="javascript:">Butterflies Hand-held Bag</a></p>
                            <p class="product-price">RS. 1,00,100</p>
                        </div>
                    </div>
                </div> <!-- Card .// -->

                <div class="custom-grid-item"> <!-- Card .// -->
                    <div class="product-card" data-mh="product">
                        <div class="card-image">
                            <a href="javascript:">
                                <img data-src="img/products/product2.jpg" alt="Product-Image" class="lazyload">
                            </a>
                            <div class="card-buttons">
                                <a href="javascript:">Add to cart</a>
                                <a href="javascript:"><i class="far fa-eye"></i></a>
                                <a href="javascript:"><i class="fas fa-heart"></i></a>
                                <a href="javascript:"><i class="fas fa-copy"></i></a>
                            </div>
                        </div>
                        <div class="card-detail">
                            <div class="rating">
                                <ul class="list-unstyled side-listing">
                                    <li><i class="fas fa-star active"></i></li>
                                    <li><i class="fas fa-star active"></i></li>
                                    <li><i class="fas fa-star active"></i></li>
                                    <li><i class="fas fa-star"></i></li>
                                    <li><i class="fas fa-star"></i></li>
                                </ul>
                            </div>
                            <p class="product-title"><a href="javascript:">Butterflies Hand-held Bag</a></p>
                            <p class="product-price">RS. 1,00,100</p>
                        </div>
                    </div>
                </div> <!-- Card .// -->

                <div class="custom-grid-item"> <!-- Card .// -->
                    <div class="product-card" data-mh="product">
                        <div class="card-image">
                            <a href="javascript:">
                                <img data-src="img/products/product5.jpg" alt="Product-Image" class="lazyload">
                            </a>
                            <div class="card-buttons">
                                <a href="javascript:">Add to cart</a>
                                <a href="javascript:"><i class="far fa-eye"></i></a>
                                <a href="javascript:"><i class="fas fa-heart"></i></a>
                                <a href="javascript:"><i class="fas fa-copy"></i></a>
                            </div>
                        </div>
                        <div class="card-detail">
                            <div class="rating">
                                <ul class="list-unstyled side-listing">
                                    <li><i class="fas fa-star active"></i></li>
                                    <li><i class="fas fa-star active"></i></li>
                                    <li><i class="fas fa-star active"></i></li>
                                    <li><i class="fas fa-star"></i></li>
                                    <li><i class="fas fa-star"></i></li>
                                </ul>
                            </div>
                            <p class="product-title"><a href="javascript:">Butterflies Hand-held Bag</a></p>
                            <p class="product-price">RS. 1,00,100</p>
                        </div>
                    </div>
                </div> <!-- Card .// -->

                <div class="custom-grid-item"> <!-- Card .// -->
                    <div class="product-card" data-mh="product">
                        <div class="card-image">
                            <a href="javascript:">
                                <img data-src="img/products/product9.webp" alt="Product-Image" class="lazyload">
                            </a>
                            <div class="card-buttons">
                                <a href="javascript:">Add to cart</a>
                                <a href="javascript:"><i class="far fa-eye"></i></a>
                                <a href="javascript:"><i class="fas fa-heart"></i></a>
                                <a href="javascript:"><i class="fas fa-copy"></i></a>
                            </div>
                        </div>
                        <div class="card-detail">
                            <div class="rating">
                                <ul class="list-unstyled side-listing">
                                    <li><i class="fas fa-star active"></i></li>
                                    <li><i class="fas fa-star active"></i></li>
                                    <li><i class="fas fa-star active"></i></li>
                                    <li><i class="fas fa-star"></i></li>
                                    <li><i class="fas fa-star"></i></li>
                                </ul>
                            </div>
                            <p class="product-title"><a href="javascript:">Butterflies Hand-held Bag</a></p>
                            <p class="product-price">RS. 1,00,100</p>
                        </div>
                    </div>
                </div> <!-- Card .// -->

                <div class="custom-grid-item"> <!-- Card .// -->
                    <div class="product-card" data-mh="product">
                        <div class="card-image">
                            <a href="javascript:">
                                <img data-src="img/products/product3.jpg" alt="Product-Image" class="lazyload">
                            </a>
                            <div class="card-buttons">
                                <a href="javascript:">Add to cart</a>
                                <a href="javascript:"><i class="far fa-eye"></i></a>
                                <a href="javascript:"><i class="fas fa-heart"></i></a>
                                <a href="javascript:"><i class="fas fa-copy"></i></a>
                            </div>
                        </div>
                        <div class="card-detail">
                            <div class="rating">
                                <ul class="list-unstyled side-listing">
                                    <li><i class="fas fa-star active"></i></li>
                                    <li><i class="fas fa-star active"></i></li>
                                    <li><i class="fas fa-star active"></i></li>
                                    <li><i class="fas fa-star"></i></li>
                                    <li><i class="fas fa-star"></i></li>
                                </ul>
                            </div>
                            <p class="product-title"><a href="javascript:">Butterflies Hand-held Bag</a></p>
                            <p class="product-price">RS. 1,00,100</p>
                        </div>
                    </div>
                </div> <!-- Card .// -->

                <div class="custom-grid-item"> <!-- Card .// -->
                    <div class="product-card" data-mh="product">
                        <div class="card-image">
                            <a href="javascript:">
                                <img data-src="img/products/product4.jpg" alt="Product-Image" class="lazyload">
                            </a>
                            <div class="card-buttons">
                                <a href="javascript:">Add to cart</a>
                                <a href="javascript:"><i class="far fa-eye"></i></a>
                                <a href="javascript:"><i class="fas fa-heart"></i></a>
                                <a href="javascript:"><i class="fas fa-copy"></i></a>
                            </div>
                        </div>
                        <div class="card-detail">
                            <div class="rating">
                                <ul class="list-unstyled side-listing">
                                    <li><i class="fas fa-star active"></i></li>
                                    <li><i class="fas fa-star active"></i></li>
                                    <li><i class="fas fa-star active"></i></li>
                                    <li><i class="fas fa-star"></i></li>
                                    <li><i class="fas fa-star"></i></li>
                                </ul>
                            </div>
                            <p class="product-title"><a href="javascript:">Butterflies Hand-held Bag</a></p>
                            <p class="product-price">RS. 1,00,100</p>
                        </div>
                    </div>
                </div> <!-- Card .// -->

                <div class="custom-grid-item"> <!-- Card .// -->
                    <div class="product-card" data-mh="product">
                        <div class="card-image">
                            <a href="javascript:">
                                <img data-src="img/products/product6.jpg" alt="Product-Image" class="lazyload">
                            </a>
                            <div class="card-buttons">
                                <a href="javascript:">Add to cart</a>
                                <a href="javascript:"><i class="far fa-eye"></i></a>
                                <a href="javascript:"><i class="fas fa-heart"></i></a>
                                <a href="javascript:"><i class="fas fa-copy"></i></a>
                            </div>
                        </div>
                        <div class="card-detail">
                            <div class="rating">
                                <ul class="list-unstyled side-listing">
                                    <li><i class="fas fa-star active"></i></li>
                                    <li><i class="fas fa-star active"></i></li>
                                    <li><i class="fas fa-star active"></i></li>
                                    <li><i class="fas fa-star"></i></li>
                                    <li><i class="fas fa-star"></i></li>
                                </ul>
                            </div>
                            <p class="product-title"><a href="javascript:">Butterflies Hand-held Bag</a></p>
                            <p class="product-price">RS. 1,00,100</p>
                        </div>
                    </div>
                </div> <!-- Card .// -->

                <div class="custom-grid-item"> <!-- Card .// -->
                    <div class="product-card" data-mh="product">
                        <div class="card-image">
                            <a href="javascript:">
                                <img data-src="img/products/product9.webp" alt="Product-Image" class="lazyload">
                            </a>
                            <div class="card-buttons">
                                <a href="javascript:">Add to cart</a>
                                <a href="javascript:"><i class="far fa-eye"></i></a>
                                <a href="javascript:"><i class="fas fa-heart"></i></a>
                                <a href="javascript:"><i class="fas fa-copy"></i></a>
                            </div>
                        </div>
                        <div class="card-detail">
                            <div class="rating">
                                <ul class="list-unstyled side-listing">
                                    <li><i class="fas fa-star active"></i></li>
                                    <li><i class="fas fa-star active"></i></li>
                                    <li><i class="fas fa-star active"></i></li>
                                    <li><i class="fas fa-star"></i></li>
                                    <li><i class="fas fa-star"></i></li>
                                </ul>
                            </div>
                            <p class="product-title"><a href="javascript:">Butterflies Hand-held Bag</a></p>
                            <p class="product-price">RS. 1,00,100</p>
                        </div>
                    </div>
                </div> <!-- Card .// -->

                <div class="custom-grid-item"> <!-- Card .// -->
                    <div class="product-card" data-mh="product">
                        <div class="card-image">
                            <a href="javascript:">
                                <img data-src="img/products/product1.jpg" alt="Product-Image" class="lazyload">
                            </a>
                            <div class="card-buttons">
                                <a href="javascript:">Add to cart</a>
                                <a href="javascript:"><i class="far fa-eye"></i></a>
                                <a href="javascript:"><i class="fas fa-heart"></i></a>
                                <a href="javascript:"><i class="fas fa-copy"></i></a>
                            </div>
                        </div>
                        <div class="card-detail">
                            <div class="rating">
                                <ul class="list-unstyled side-listing">
                                    <li><i class="fas fa-star active"></i></li>
                                    <li><i class="fas fa-star active"></i></li>
                                    <li><i class="fas fa-star active"></i></li>
                                    <li><i class="fas fa-star"></i></li>
                                    <li><i class="fas fa-star"></i></li>
                                </ul>
                            </div>
                            <p class="product-title"><a href="javascript:">Butterflies Hand-held Bag</a></p>
                            <p class="product-price">RS. 1,00,100</p>
                        </div>
                    </div>
                </div> <!-- Card .// -->

                <div class="custom-grid-item"> <!-- Card .// -->
                    <div class="product-card" data-mh="product">
                        <div class="card-image">
                            <a href="javascript:">
                                <img data-src="img/products/product2.jpg" alt="Product-Image" class="lazyload">
                            </a>
                            <div class="card-buttons">
                                <a href="javascript:">Add to cart</a>
                                <a href="javascript:"><i class="far fa-eye"></i></a>
                                <a href="javascript:"><i class="fas fa-heart"></i></a>
                                <a href="javascript:"><i class="fas fa-copy"></i></a>
                            </div>
                        </div>
                        <div class="card-detail">
                            <div class="rating">
                                <ul class="list-unstyled side-listing">
                                    <li><i class="fas fa-star active"></i></li>
                                    <li><i class="fas fa-star active"></i></li>
                                    <li><i class="fas fa-star active"></i></li>
                                    <li><i class="fas fa-star"></i></li>
                                    <li><i class="fas fa-star"></i></li>
                                </ul>
                            </div>
                            <p class="product-title"><a href="javascript:">Butterflies Hand-held Bag</a></p>
                            <p class="product-price">RS. 1,00,100</p>
                        </div>
                    </div>
                </div> <!-- Card .// -->

                <div class="custom-grid-item"> <!-- Card .// -->
                    <div class="product-card" data-mh="product">
                        <div class="card-image">
                            <a href="javascript:">
                                <img data-src="img/products/product5.jpg" alt="Product-Image" class="lazyload">
                            </a>
                            <div class="card-buttons">
                                <a href="javascript:">Add to cart</a>
                                <a href="javascript:"><i class="far fa-eye"></i></a>
                                <a href="javascript:"><i class="fas fa-heart"></i></a>
                                <a href="javascript:"><i class="fas fa-copy"></i></a>
                            </div>
                        </div>
                        <div class="card-detail">
                            <div class="rating">
                                <ul class="list-unstyled side-listing">
                                    <li><i class="fas fa-star active"></i></li>
                                    <li><i class="fas fa-star active"></i></li>
                                    <li><i class="fas fa-star active"></i></li>
                                    <li><i class="fas fa-star"></i></li>
                                    <li><i class="fas fa-star"></i></li>
                                </ul>
                            </div>
                            <p class="product-title"><a href="javascript:">Butterflies Hand-held Bag</a></p>
                            <p class="product-price">RS. 1,00,100</p>
                        </div>
                    </div>
                </div> <!-- Card .// -->

                <div class="custom-grid-item"> <!-- Card .// -->
                    <div class="product-card" data-mh="product">
                        <div class="card-image">
                            <a href="javascript:">
                                <img data-src="img/products/product9.webp" alt="Product-Image" class="lazyload">
                            </a>
                            <div class="card-buttons">
                                <a href="javascript:">Add to cart</a>
                                <a href="javascript:"><i class="far fa-eye"></i></a>
                                <a href="javascript:"><i class="fas fa-heart"></i></a>
                                <a href="javascript:"><i class="fas fa-copy"></i></a>
                            </div>
                        </div>
                        <div class="card-detail">
                            <div class="rating">
                                <ul class="list-unstyled side-listing">
                                    <li><i class="fas fa-star active"></i></li>
                                    <li><i class="fas fa-star active"></i></li>
                                    <li><i class="fas fa-star active"></i></li>
                                    <li><i class="fas fa-star"></i></li>
                                    <li><i class="fas fa-star"></i></li>
                                </ul>
                            </div>
                            <p class="product-title"><a href="javascript:">Butterflies Hand-held Bag</a></p>
                            <p class="product-price">RS. 1,00,100</p>
                        </div>
                    </div>
                </div> <!-- Card .// -->

                <div class="custom-grid-item"> <!-- Card .// -->
                    <div class="product-card" data-mh="product">
                        <div class="card-image">
                            <a href="javascript:">
                                <img data-src="img/products/product3.jpg" alt="Product-Image" class="lazyload">
                            </a>
                            <div class="card-buttons">
                                <a href="javascript:">Add to cart</a>
                                <a href="javascript:"><i class="far fa-eye"></i></a>
                                <a href="javascript:"><i class="fas fa-heart"></i></a>
                                <a href="javascript:"><i class="fas fa-copy"></i></a>
                            </div>
                        </div>
                        <div class="card-detail">
                            <div class="rating">
                                <ul class="list-unstyled side-listing">
                                    <li><i class="fas fa-star active"></i></li>
                                    <li><i class="fas fa-star active"></i></li>
                                    <li><i class="fas fa-star active"></i></li>
                                    <li><i class="fas fa-star"></i></li>
                                    <li><i class="fas fa-star"></i></li>
                                </ul>
                            </div>
                            <p class="product-title"><a href="javascript:">Butterflies Hand-held Bag</a></p>
                            <p class="product-price">RS. 1,00,100</p>
                        </div>
                    </div>
                </div> <!-- Card .// -->

                <div class="custom-grid-item"> <!-- Card .// -->
                    <div class="product-card" data-mh="product">
                        <div class="card-image">
                            <a href="javascript:">
                                <img data-src="img/products/product4.jpg" alt="Product-Image" class="lazyload">
                            </a>
                            <div class="card-buttons">
                                <a href="javascript:">Add to cart</a>
                                <a href="javascript:"><i class="far fa-eye"></i></a>
                                <a href="javascript:"><i class="fas fa-heart"></i></a>
                                <a href="javascript:"><i class="fas fa-copy"></i></a>
                            </div>
                        </div>
                        <div class="card-detail">
                            <div class="rating">
                                <ul class="list-unstyled side-listing">
                                    <li><i class="fas fa-star active"></i></li>
                                    <li><i class="fas fa-star active"></i></li>
                                    <li><i class="fas fa-star active"></i></li>
                                    <li><i class="fas fa-star"></i></li>
                                    <li><i class="fas fa-star"></i></li>
                                </ul>
                            </div>
                            <p class="product-title"><a href="javascript:">Butterflies Hand-held Bag</a></p>
                            <p class="product-price">RS. 1,00,100</p>
                        </div>
                    </div>
                </div> <!-- Card .// -->

                <div class="custom-grid-item"> <!-- Card .// -->
                    <div class="product-card" data-mh="product">
                        <div class="card-image">
                            <a href="javascript:">
                                <img data-src="img/products/product6.jpg" alt="Product-Image" class="lazyload">
                            </a>
                            <div class="card-buttons">
                                <a href="javascript:">Add to cart</a>
                                <a href="javascript:"><i class="far fa-eye"></i></a>
                                <a href="javascript:"><i class="fas fa-heart"></i></a>
                                <a href="javascript:"><i class="fas fa-copy"></i></a>
                            </div>
                        </div>
                        <div class="card-detail">
                            <div class="rating">
                                <ul class="list-unstyled side-listing">
                                    <li><i class="fas fa-star active"></i></li>
                                    <li><i class="fas fa-star active"></i></li>
                                    <li><i class="fas fa-star active"></i></li>
                                    <li><i class="fas fa-star"></i></li>
                                    <li><i class="fas fa-star"></i></li>
                                </ul>
                            </div>
                            <p class="product-title"><a href="javascript:">Butterflies Hand-held Bag</a></p>
                            <p class="product-price">RS. 1,00,100</p>
                        </div>
                    </div>
                </div> <!-- Card .// -->

                <div class="custom-grid-item"> <!-- Card .// -->
                    <div class="product-card" data-mh="product">
                        <div class="card-image">
                            <a href="javascript:">
                                <img data-src="img/products/product9.webp" alt="Product-Image" class="lazyload">
                            </a>
                            <div class="card-buttons">
                                <a href="javascript:">Add to cart</a>
                                <a href="javascript:"><i class="far fa-eye"></i></a>
                                <a href="javascript:"><i class="fas fa-heart"></i></a>
                                <a href="javascript:"><i class="fas fa-copy"></i></a>
                            </div>
                        </div>
                        <div class="card-detail">
                            <div class="rating">
                                <ul class="list-unstyled side-listing">
                                    <li><i class="fas fa-star active"></i></li>
                                    <li><i class="fas fa-star active"></i></li>
                                    <li><i class="fas fa-star active"></i></li>
                                    <li><i class="fas fa-star"></i></li>
                                    <li><i class="fas fa-star"></i></li>
                                </ul>
                            </div>
                            <p class="product-title"><a href="javascript:">Butterflies Hand-held Bag</a></p>
                            <p class="product-price">RS. 1,00,100</p>
                        </div>
                    </div>
                </div> <!-- Card .// -->
            </div> <!-- Parent .// -->
            <div class="grid-btn-div">
                <a href="javascript:" class="grid-btn"><i class="fas fa-shopping-cart"></i> Load More</a>
            </div> 
        </div>
    </div>
</section>




<?php include('include/footer.php'); ?>