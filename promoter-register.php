<?php include('include/header.php'); ?>
 
<nav aria-label="breadcrumb" class="breadcrumb-main bg-para" style="background: linear-gradient(rgba(0,0,0,0.4),rgba(0,0,0,0.4)), url('img/bread.jpg');">
    <div class="container-fluid clearfix"> 
        <h3 class="float-left">Register</h3>
        <ol class="breadcrumb float-right">
            <li class="breadcrumb-item"><a href="index.php">Home</a></li> ->
            <li class="breadcrumb-item active" aria-current="page">Register</li>
        </ol>
    </div> 
</nav>



<section class="promoter-register-wrapper">
    <div  class="promoter-register-padding">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-12">
                    <div class="flex-content">
                        <div class="register-left">
                            <h3>Registration</h3> 
                            <form>
                                <div class="form-group row">
                                    <label for="promoter-re-name" class="col-lg-3 col-form-label"> Full Name: </label>
                                    <div class="col-lg-9">
                                        <input type="text" class="form-control" id="promoter-re-name" placeholder="Full Name">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="promoter-re-email" class="col-lg-3 col-form-label">Email: </label>
                                    <div class="col-lg-9">
                                        <input type="email" class="form-control" id="promoter-re-email" placeholder="Email Address">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="promoter-re-address" class="col-lg-3 col-form-label">Address: </label>
                                    <div class="col-lg-9">
                                        <input type="text" class="form-control" id="promoter-re-address" placeholder="Address">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="promoter-re-number1" class="col-lg-3 col-form-label">Contact Number: </label>
                                    <div class="col-lg-9">
                                        <input type="email" class="form-control" id="promoter-re-number1" placeholder="Contact Number - 1">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="promoter-re-number2" class="col-lg-3 col-form-label">Another Number: </label>
                                    <div class="col-lg-9">
                                        <input type="email" class="form-control" id="promoter-re-number2" placeholder="Contact Number - 2">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="promoter-re-username" class="col-lg-3 col-form-label">User Name: </label>
                                    <div class="col-lg-9">
                                        <input type="text" class="form-control" id="promoter-re-username" placeholder="User Name">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="promoter-re-username" class="col-lg-3 col-form-label">User Picture: </label>
                                    <div class="col-lg-9">
                                        <input type="file" class="form-control" id="promoter-re-pic">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="promoter-re-username" class="col-lg-3 col-form-label">Citizenship: </label>
                                    <div class="col-lg-9">
                                        <input type="file" class="form-control" id="promoter-re-citizen">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="promoter-re-password1" class="col-lg-3 col-form-label">Password: </label>
                                    <div class="col-lg-9">
                                        <input type="password" class="form-control" id="promoter-re-password1" placeholder="**********">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="promoter-re-password2" class="col-lg-3 col-form-label">Re-Password: </label>
                                    <div class="col-lg-9">
                                        <input type="password" class="form-control" id="promoter-re-password2" placeholder="**********">
                                    </div>
                                </div>  
                                 
                                <div class="promoter-reg-btn">   
                                    <button class="btn btn-promoter-register">Register</button>
                                </div>
                                
                                
                            </form>
                            
                            <div class="other-links">
                                <p class="link-line">Already Have An Account ? <a href="#">Login Here</a></p>
                            </div>
                        </div> 
                        <div class="register-right">
                            <div class="promoter-right-img-wrapper">  
                                <img src="img/promotion-registration.jpg" class="img-fluid" alt="registration-img">
                                <div class="promoter-right-shadow"> 
                                    <figure>
                                        <img src="img/logo.png" class="img-fluid" alt="">
                                    </figure>
                                    <h1>What is Alpasal promoter ?</h1>
                                    <p> 
                                        Alpasal Promoter is Nepal’s leading platform for selling online. Be it a manufacturer, vendor or supplier, simply sell your products online on Alpasal and become a top e-commerce player with minimum investment. Through a team of experts offering exclusive seller workshops, training, and seller support, Alpasal focuses on empowering sellers across Nepal.</br></br>

                                        Selling on Alpasal.com is easy and absolutely free. All you need is to register, list your catalog and start selling your products.</br></br>

                                        What's more? We have third party ‘Ecommerce Service Providers’ who provide logistics, cataloging support, product photoshoot and packaging materials. We have a program called Seller Protection Fund to safeguard sellers from losses via compensations. We provide Alpasal Fulfilment services through which you can ensure faster delivery of your items, quality check by our experts and a delightful packaging. Combine these with the fastest payments in the industry and you get an excellent seller portal. No wonder Alpasal is Nepal’s favourite place to sell online.
                                    </p> 
                                </div>
                            </div> 
                        </div>
                    </div> 
                </div>
            </div>
        </div>
    </div>
</section>
 

<?php include('include/footer.php'); ?>