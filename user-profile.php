<?php include('include/header.php'); ?>

<nav aria-label="breadcrumb" class="breadcrumb-main bg-para" style="background: linear-gradient(rgba(0,0,0,0.4),rgba(0,0,0,0.4)), url('img/bread.jpg');">
    <div class="container clearfix"> <!-- Container .// -->
        <h3 class="float-left">User Profile</h3>
        <ol class="breadcrumb float-right">
            <li class="breadcrumb-item"><a href="index.php">Home</a></li> 
            <li class="breadcrumb-item active" aria-current="page">User Profile</li>
        </ol>
    </div> <!-- Container .// -->
</nav>

<section class="user-dashboard">
    <div class="container-fluid"> 
        <div class="row"> 
            <div class="col-lg-3">
                <div class="left-side-user-dashboard"> 
                    <div class="header">
                        <div class="prof_who">
                            <div class="img_holder">
                                <img src="img/user/user-profile.jpg" class="img-fluid" alt="">
                            </div>
                            <div class="title_holder"> 
                                <span>Welcome</span>
                                <h3>Gopal Basnet</h3> 
                            </div>
                        </div>
                    </div>
                    <div class="content nav flex-column nav-pills" role="tablist" aria-orientation="vertical">
                        <ul class="nav"> 
                            <li class="label">Update User Profile</li>
                            <li class="left-nav"><a class="nav-link" href="update-info.php">Update Info</a></li>
                            <li class="left-nav"><a class="nav-link" href="request-area.php">Request Area</a></li>
                            <li class="left-nav"><a class= "nav-link" href="change-password.php">Change Password</a></li>
                            <li class="left-nav"><a class= "nav-link" href="shipping-address.php">Shipping Address</a></li>
                            <li class="left-nav"><a class= "nav-link" href="wishlist.php">WishList</a></li>
                            <li class="left-nav"><a class= "nav-link" href="notification.php">Notifications</a></li>
                            <li class="left-nav"><a class= "nav-link" href="#">Log Out</a></li>
                        </ul>
                    </div>
                </div> 
            </div>
            <div class="col-lg-9">
                <div class="right-user-dashboard">
                    <div class="tab-content">   
                        <div class="tab-pane fade show active">
                            <form action="" class="right-user-dash-edit">
                                <div class="row">
                                    <div class="col-lg-3">
                                        <div class="user-dash-profile-img">
                                            <img src="img/user/user-profile.jpg" class="img-fluid" alt="">
                                            <div class="changer">
                                                <input type="file" name="" id="file-1" class="inputfile" accept="image/*">
                                                <label for="file-1">
                                                    <span>Change Photo</span>
                                                </label>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-6">
                                        <div class="user-dash-profile-img cover-user-dashbaord">
                                            <img src="img/user/shop-name.png" class="img-fluid" alt="">
                                            <div class="changer">
                                                <input type="file" name="" id="file-1" class="inputfile" accept="image/*">
                                                <label for="file-1">
                                                    <span>Change Cover Photo</span>
                                                </label>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-3">
                                        <div class="user-dash-profile-img user-dashboard-logo">
                                            <img src="img/logo.png" class="img-fluid" alt="">
                                            <div class="changer">
                                                <input type="file" name="" id="file-1" class="inputfile" accept="image/*">
                                                <label for="file-1">
                                                    <span>Change Logo</span>
                                                </label>
                                            </div>
                                        </div>
                                    </div> 
                                    <div class="col-lg-6">
                                        <div class="form-group right-user-dash-edit-group">
                                            <label for="">Full Name</label>
                                            <input type="text" class="form-control" placeholder="Gopal Basnet">
                                        </div>
                                    </div>
                                    <div class="col-lg-6">
                                        <div class="form-group right-user-dash-edit-group">
                                            <label for="">Address</label>
                                            <input type="text" class="form-control" placeholder="Koteshwor, Kathmandu, NEPAL">
                                        </div>
                                    </div>
                                    <div class="col-lg-6">
                                        <div class="form-group right-user-dash-edit-group">
                                            <label for="">Contact Number</label>
                                            <input type="number" class="form-control" placeholder="+977-98665654587">
                                        </div>
                                    </div>
                                    <div class="col-lg-6">
                                        <div class="form-group right-user-dash-edit-group">
                                            <label for="">Email</label>
                                            <input type="email" class="form-control" placeholder="info@gmail.com">
                                        </div>
                                    </div>
                                    <div class="col-lg-12">
                                        <button class="btn btn-change-password">Update Profile</button>
                                    </div>
                                </div>
                            </form>
                        </div> 
                    </div>
                </div>
            </div>
        </div>
    </div> 
</section>

<?php include('include/footer.php'); ?>