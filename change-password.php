<?php include('include/header.php'); ?>

<nav aria-label="breadcrumb" class="breadcrumb-main bg-para" style="background: linear-gradient(rgba(0,0,0,0.4),rgba(0,0,0,0.4)), url('img/bread.jpg');">
    <div class="container clearfix"> <!-- Container .// -->
        <h3 class="float-left">Change Password</h3>
        <ol class="breadcrumb float-right">
            <li class="breadcrumb-item"><a href="index.php">Home</a></li>
            <!-- <li class="breadcrumb-item"><a href="#">Library</a></li> -->
            <li class="breadcrumb-item active" aria-current="page">Change Password</li>
        </ol>
    </div> <!-- Container .// -->
</nav>

<section class="user-dashboard">
    <div class="container-fluid"> 
        <div class="row"> 
            <div class="col-lg-3">
                <div class="left-side-user-dashboard"> 
                    <div class="header">
                        <div class="prof_who">
                            <div class="img_holder">
                                <img src="img/user/user-profile.jpg" class="img-fluid" alt="">
                            </div>
                            <div class="title_holder"> 
                                <span>Welcome</span>
                                <h3>Gopal Basnet</h3> 
                            </div>
                        </div>
                    </div>
                    <div class="content nav flex-column nav-pills" role="tablist" aria-orientation="vertical">
                        <ul class="nav"> 
                            <li class="label">Update User Profile</li>
                            <li class="left-nav"><a class="nav-link " href="update-info.php">Update Info</a></li>
                            <li class="left-nav"><a class="nav-link" href="request-area.php">Request Area</a></li>
                            <li class="left-nav"><a class= "nav-link active" href="change-password.php">Change Password</a></li>
                            <li class="left-nav"><a class= "nav-link" href="shipping-address.php">Shipping Address</a></li>
                            <li class="left-nav"><a class= "nav-link " href="wishlist.php">WishList</a></li>
                            <li class="left-nav"><a class= "nav-link " href="notification.php">Notifications</a></li>
                            <li class="left-nav"><a class= "nav-link" href="#">Log Out</a></li>
                        </ul>
                    </div>
                </div> 
            </div>
            <div class="col-lg-9">
                <div class="right-user-dashboard">
                    <div class="tab-content">   
                        <div class="tab-pane fade fade show active">
                            <div class="alert alert-vendor alert-dismissible fade show" role="alert">
                                Password Changed Successfully
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            <form action="" class="right-user-dash-edit"> 
                                <div class="row">
                                    <div class="col-lg-6">
                                        <div class="form-group right-user-dash-edit-group">
                                            <label for="">Old Password</label>
                                            <input type="text" class="form-control" placeholder="*************">
                                        </div>
                                    </div>
                                    <div class="col-lg-6">
                                        <div class="form-group right-user-dash-edit-group">
                                            <label for="">New Password</label>
                                            <input type="text" class="form-control" placeholder="">
                                        </div>
                                    </div>
                                    <div class="col-lg-6">
                                        <div class="form-group right-user-dash-edit-group">
                                            <label for="">Confirm Password</label>
                                            <input type="text" class="form-control" placeholder="">
                                        </div>
                                    </div>
                                    <div class="col-lg-12">
                                        <button class="btn btn-change-password">Change password</button>
                                    </div>
                                     
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div> 
</section>

<?php include('include/footer.php'); ?>