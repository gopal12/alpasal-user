<?php include('include/header.php'); ?>

    <section id="wrapper" class="error-page">
        <div class="error-box">
            <div class="error-body text-center">
                <h1 class="">503</h1>
                <h3 class="text-uppercase">This site is getting up in few minutes. </h3>
                <p class="text-muted m-t-30 m-b-30">Please try after some time</p>
                <a href="index.php" class="btn btn-back-home">Back to home</a> 
            </div> 
        </div>
    </section>
    
<?php include('include/footer.php'); ?>