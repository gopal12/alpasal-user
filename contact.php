<?php include('include/header.php'); ?>

<!----------------------------
-------Breadcrumb-------
----------------------------->

<nav aria-label="breadcrumb" class="breadcrumb-main bg-para" style="background: linear-gradient(rgba(0,0,0,0.4),rgba(0,0,0,0.4)), url('img/bread.jpg');">
    <div class="container clearfix"> <!-- Container .// -->
        <h3 class="float-left">Contact</h3>
        <ol class="breadcrumb float-right">
            <li class="breadcrumb-item"><a href="index.php">Home</a></li>
            <!-- <li class="breadcrumb-item"><a href="#">Library</a></li> -->
            <li class="breadcrumb-item active" aria-current="page">Contact</li>
        </ol>
    </div> <!-- Container .// -->
</nav>

<!----------------------------
-------Breadcrumb-------
----------------------------->

<!----------------------------
-------Contact-Main-------
----------------------------->

<section class="contact-main common-padding">
    <div class="container"> <!-- Container .// -->
        <div class="row"> <!-- Row .// -->
            <div class="col-lg-8 my-3"> <!-- Col .// -->
                <form action="contact-form">
                    <div class="login-form">

                        <div class="form-group">
                            <label for="contactName">Name:</label>
                            <input type="text" id="contactName" placeholder="Enter Full Name">
                        </div>

                        <div class="row"> <!-- Inner Row .// -->
                            <div class="col-sm-6 col-xs-6"> <!-- Inner Col .// -->
                                <div class="form-group">
                                    <label for="contactEmail">Email:</label>
                                    <input type="email" id="contactEmail" placeholder="abc@xyz.com">
                                </div>
                            </div> <!-- Inner Col .// -->
                            <div class="col-sm-6 col-xs-6"> <!-- Inner Col .// -->
                                <div class="form-group">
                                    <label for="contactNumber">Phone Number:</label>
                                    <input type="text" id="contactNumber" placeholder="+977 - XXXX XX XX XX">
                                </div>
                            </div> <!-- Inner Col .// -->
                        </div> <!-- Inner Row .// -->

                        <div class="form-group">
                            <label for="contactMessage">Message:</label>
                            <textarea name="contactMessage" id="contactMessage" placeholder="Enter a Message"></textarea>
                        </div>

                        <button type="submit" class="short-button">Send Message</button>

                    </div>
                </form>
            </div> <!-- Col .// -->
            <div class="col-lg-4 my-3"> <!-- Col .// -->

                <aside class="contact-logo">
                    <a href="index.php"><img src="img/logo.png" alt="LOGO"></a>
                </aside>

                <h5 class="mini-title">Connect With Us</h5>
                <p class="normal-content">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt.</p>

                <ul class="list-unstyled contact-details">
                    <li class="normal-content"><span><i class="fas fa-map-marker-alt"></i></span> New Baneshwor, Kathmandu, Nepal</li>
                    <li class="normal-content"><span><i class="fas fa-phone"></i></span> +977 - 9808236689, 9863692273</li>
                    <li class="normal-content"><span><i class="fas fa-envelope"></i></span> support@somedomain.com</li>
                </ul>

                <ul class="mini-comp-logo list-unstyled">
                    <li><a href="#" class="facebook"><i class="fab fa-facebook-f"></i></a></li>
                    <li><a href="#" class="twitter"><i class="fab fa-twitter"></i></a></li>
                    <li><a href="#" class="instagram"><i class="fab fa-instagram"></i></a></li>
                    <li><a href="#" class="linkedin"><i class="fab fa-linkedin-in"></i></a></li>
                </ul>

            </div> <!-- Col .// -->
        </div> <!-- Row .// -->
    </div> <!-- Container .// -->
</section>

<!----------------------------
-------Contact-Main-------
----------------------------->

<?php include('include/footer.php'); ?>