<?php include('include/header.php'); ?>
 

<nav aria-label="breadcrumb" class="breadcrumb-main bg-para" style="background: linear-gradient(rgba(0,0,0,0.4),rgba(0,0,0,0.4)), url('img/bread.jpg');">
    <div class="container-fluid clearfix"> 
        <h3 class="float-left">Track Order</h3>
        <ol class="breadcrumb float-right">
            <li class="breadcrumb-item"><a href="index.php">Home</a></li> 
            <li class="breadcrumb-item active" aria-current="page">Track Order</li>
        </ol>
    </div> 
</nav>
<section class="message-ceo common-padding">
    <div class="container-fluid"> 
        <div class="row"> 
            <div class="col-lg-12">
                <div class="tracking-div-wrapper">
                    <div class="tracking-img"> 
                        <img src="img/omlotExpress-logo.png" class="img-fluid" alt=""> 
                    </div>
                    <div class="text-tracking-info">
                        <h1>Digital Tracking</h1>
                        <h3>Comming Soon !!!</h3>
                    </div> 
                </div> 
            </div> 
        </div> 
    </div> 
</section>


 

<?php include('include/footer.php'); ?>