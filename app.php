<?php include('include/header.php'); ?>

<!----------------------------
-------App Section-------
----------------------------->

<section class="get-page get-app-common-pad bg-img" style="background: linear-gradient(rgba(0,0,0,0.2), rgba(0,0,0,0.2)), url('img/app-img.jpg');">
    <div class="container">
        <div class="app-wrapper"> 
            <h1 class="color-white app-heading">Take your business on the go <span>with the Alpasal.com app</span></h1>
            <ul class="list-unstyled play-link">
                <li><a href="#" target="_blank"><img src="img/appstore.png" alt="APP_STORE"></a></li>
                <li><a href="#" target="_blank"><img src="img/playstore.png" alt="PLAY_STORE"></a></li>
            </ul>
            <p>Get download link</p>
            <form action="app-link">
                <div class="login-form">
                    <input type="email" placeholder="Email Address" required>
                    <button class="form-button">Submit</button>
                </div>
            </form>
        </div>
    </div>
</section>

<!----------------------------
-------App Section-------
----------------------------->

<!----------------------------
-------App Review-------
----------------------------->

<section class="app-review-mains">
    <div class="container-fluid"> 
        <div class="row">  
            <div class="col-lg-7">   
                <div class="app-logo-wraps">
                    <div class="app-logos">
                        <img src="img/app-logo.png" class="img-fluid" alt="APP-LOGO">
                    </div>
                    <div class="inner-review-wrappers">
                        <div class="app-reviews">
                            <p class="topics">Ratings & Reviews</p>
                            <p class="app-rates"><span>4.7</span>out of 5</p>
                        </div>

                        <div class="rating-stars-wrappers clearfix"> <!-- Stars and Bar Wrapper .// -->
                            <div class="stars-groups"> <!-- Stars-Group .// -->
                                <ul class="list-unstyled starss">
                                    <li class="active"><i class="fas fa-star"></i></li>
                                    <li class="active"><i class="fas fa-star"></i></li>
                                    <li class="active"><i class="fas fa-star"></i></li>
                                    <li class="active"><i class="fas fa-star"></i></li>
                                    <li class="active"><i class="fas fa-star"></i></li>
                                </ul>
                                <ul class="list-unstyled starss">
                                    <li class="active"><i class="fas fa-star"></i></li>
                                    <li class="active"><i class="fas fa-star"></i></li>
                                    <li class="active"><i class="fas fa-star"></i></li>
                                    <li class="active"><i class="fas fa-star"></i></li>
                                    <li><i class="fas fa-star"></i></li>
                                </ul>
                                <ul class="list-unstyled starss">
                                    <li class="active"><i class="fas fa-star"></i></li>
                                    <li class="active"><i class="fas fa-star"></i></li>
                                    <li class="active"><i class="fas fa-star"></i></li>
                                    <li><i class="fas fa-star"></i></li>
                                    <li><i class="fas fa-star"></i></li>
                                </ul>
                                <ul class="list-unstyled starss">
                                    <li class="active"><i class="fas fa-star"></i></li>
                                    <li class="active"><i class="fas fa-star"></i></li>
                                    <li><i class="fas fa-star"></i></li>
                                    <li><i class="fas fa-star"></i></li>
                                    <li><i class="fas fa-star"></i></li>
                                </ul>
                                <ul class="list-unstyled starss">
                                    <li class="active"><i class="fas fa-star"></i></li>
                                    <li><i class="fas fa-star"></i></li>
                                    <li><i class="fas fa-star"></i></li>
                                    <li><i class="fas fa-star"></i></li>
                                    <li><i class="fas fa-star"></i></li>
                                </ul>
                            </div> <!-- Stars-Group .// -->
                            <div class="progress-bar-groupss">
                            
                                <div class="progress">
                                    <div class="progress-bar" role="progressbar" style="width: 75%" aria-valuenow="75" aria-valuemin="0" aria-valuemax="100"></div>
                                </div>
                                <div class="progress">
                                    <div class="progress-bar" role="progressbar" style="width: 25%" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100"></div>
                                </div>
                                <div class="progress">
                                    <div class="progress-bar" role="progressbar" style="width: 50%" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100"></div>
                                </div>
                                <div class="progress">
                                    <div class="progress-bar" role="progressbar" style="width: 30%" aria-valuenow="30" aria-valuemin="0" aria-valuemax="100"></div>
                                </div>
                                <div class="progress">
                                    <div class="progress-bar" role="progressbar" style="width: 15%" aria-valuenow="15" aria-valuemin="0" aria-valuemax="100"></div>
                                </div> 
                            </div>
                        </div> <!-- Stars and Bar Wrapper .// -->
                    </div>
                </div>  
            </div> 

            <div class="col-lg-5"> 
                <div class="register-boxs">
                    <div class="owl-carousel"> <!-- Owl-Carousel .// -->

                        <div class="item"> <!-- Owl-Item .// -->
                            <div class="app-testimonial-wraps">
                                <p class="date">7 June 2019</p>
                                <p class="testimonialss normal-contents">Alpasal gives complete knowledge and guidance to buy products from all over the country. Excellent App.</p>
                                <div class="name-reviews clearfix">
                                    <p class="float-left">- Sushan Paudyal</p>
                                    <ul class="list-unstyled starss float-left">
                                        <li class="active"><i class="fas fa-star"></i></li>
                                        <li class="active"><i class="fas fa-star"></i></li>
                                        <li class="active"><i class="fas fa-star"></i></li>
                                        <li class="active"><i class="fas fa-star"></i></li>
                                        <li><i class="fas fa-star"></i></li>
                                    </ul>
                                </div>
                            </div>
                        </div> <!-- Owl-Item .// -->

                        <div class="item"> <!-- Owl-Item .// -->
                            <div class="app-testimonial-wraps">
                                <p class="dates">20 July 2019</p>
                                <p class="testimonialss normal-contents">The whole world is in my hand with this app. It's a very good app.</p>
                                <div class="name-reviews clearfix">
                                    <p class="float-left">- Rohan Yogi</p>
                                    <ul class="list-unstyled starss float-left">
                                        <li class="active"><i class="fas fa-star"></i></li>
                                        <li class="active"><i class="fas fa-star"></i></li>
                                        <li class="active"><i class="fas fa-star"></i></li>
                                        <li class="active"><i class="fas fa-star"></i></li>
                                        <li class="active"><i class="fas fa-star"></i></li>
                                    </ul>
                                </div>
                            </div>
                        </div> <!-- Owl-Item .// -->

                        <div class="item"> <!-- Owl-Item .// -->
                            <div class="app-testimonial-wraps">
                                <p class="dates">12 Feburary 2019</p>
                                <p class="testimonials normal-contents">Trust worthy, the trade assurance refunded my money.</p>
                                <div class="name-reviews clearfix">
                                    <p class="float-left">- Eric Adams</p>
                                    <ul class="list-unstyled starss float-left">
                                        <li class="active"><i class="fas fa-star"></i></li>
                                        <li class="active"><i class="fas fa-star"></i></li>
                                        <li class="active"><i class="fas fa-star"></i></li>
                                        <li><i class="fas fa-star"></i></li>
                                        <li><i class="fas fa-star"></i></li>
                                    </ul>
                                </div>
                            </div>
                        </div> <!-- Owl-Item .// -->

                    </div> <!-- Owl-Carousel .// -->
                </div>

            </div> <!-- COl .// -->

        </div> <!-- Row .// -->
    </div> <!-- Container .// -->
</section>

<!----------------------------
-------App Review-------
----------------------------->

<?php include('include/footer.php'); ?>