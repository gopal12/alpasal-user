<?php include('include/header.php'); ?>
<section class="single-product-section">
	<div class="">
		<div class="container-fluid">
			<div class="row div-flex">
				<div class="col-xl-3 col-lg-4 col-md-4 left-zoom-img">
					<div class="zoom-img-wrapper">
						<div class="zoom-img-main">
							<img class="xzoom" id="xzoom-default" src="img/single-product/preview/01_b_car.jpg" xoriginal="img/single-product/original/01_b_car.jpg" />
						</div>
						
					</div> 
				</div>
				<div class="col-xl-1 col-lg-1 col-md-1 right-zoom-img">
					<div class="zoom-img-thumb">
							<div class="xzoom-thumbs">
								<a href="img/single-product/original/01_b_car.jpg">
									<img class="xzoom-gallery" width="80" src="img/single-product/thumbs/01_b_car.jpg"  xpreview="img/single-product/preview/01_b_car.jpg">
								</a>
								<a href="img/single-product/original/02_o_car.jpg">
									<img class="xzoom-gallery" width="80" src="img/single-product/preview/02_o_car.jpg">
								</a>
								<a href="img/single-product/original/03_r_car.jpg">
									<img class="xzoom-gallery" width="80" src="img/single-product/preview/03_r_car.jpg">
								</a>
								<a href="img/single-product/original/04_g_car.jpg">
									<img class="xzoom-gallery" width="80" src="img/single-product/preview/04_g_car.jpg">
								</a>
							</div> 
						</div>
				</div>
				<div class="col-xl-8 col-lg-7 col-md-7 another">
					<h1>Samsung Galaxy A70 (White, 128 GB)  (6 GB RAM)</h1>
					Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quas ea eligendi illo, tenetur, sit, similique maxime natus veniam, assumenda doloremque sapiente. Dolores iure quis, accusamus harum nesciunt mollitia nobis obcaecati.
					<h1>Samsung Galaxy A70 (White, 128 GB)  (6 GB RAM)</h1>
					Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quas ea eligendi illo, tenetur, sit, similique maxime natus veniam, assumenda doloremque sapiente. Dolores iure quis, accusamus harum nesciunt mollitia nobis obcaecati.

					]Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
					tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
					quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
					consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
					cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
					proident, sunt in culpa qui officia deserunt mollit anim id est laborum.

					loremLorem ipsum dolor sit amet, consectetur adipisicing elit. Sapiente iusto iste at dolor fugiat, nisi nihil ea dicta nostrum, voluptates ducimus, consequatur iure praesentium dolorem nesciunt eaque culpa ut tempora.Lorem ipsum dolor sit amet, consectetur adipisicing elit. Inventore ipsum dignissimos non, esse doloremque qui consequatur eius, fuga aspernatur, facere corporis porro sit distinctio voluptatibus praesentium pariatur aliquam. Quae, voluptate.
					<h1>Samsung Galaxy A70 (White, 128 GB)  (6 GB RAM)</h1>
					Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quas ea eligendi illo, tenetur, sit, similique maxime natus veniam, assumenda doloremque sapiente. Dolores iure quis, accusamus harum nesciunt mollitia nobis obcaecati.
					<h1>Samsung Galaxy A70 (White, 128 GB)  (6 GB RAM)</h1>
					Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quas ea eligendi illo, tenetur, sit, similique maxime natus veniam, assumenda doloremque sapiente. Dolores iure quis, accusamus harum nesciunt mollitia nobis obcaecati.

					]Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
					tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
					quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
					consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
					cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
					proident, sunt in culpa qui officia deserunt mollit anim id est laborum.

					loremLorem ipsum dolor sit amet, consectetur adipisicing elit. Sapiente iusto iste at dolor fugiat, nisi nihil ea dicta nostrum, voluptates ducimus, consequatur iure praesentium dolorem nesciunt eaque culpa ut tempora.Lorem ipsum dolor sit amet, consectetur adipisicing elit. Inventore ipsum dignissimos non, esse doloremque qui consequatur eius, fuga aspernatur, facere corporis porro sit distinctio voluptatibus praesentium pariatur aliquam. Quae, voluptate.
					<h1>Samsung Galaxy A70 (White, 128 GB)  (6 GB RAM)</h1>
					Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quas ea eligendi illo, tenetur, sit, similique maxime natus veniam, assumenda doloremque sapiente. Dolores iure quis, accusamus harum nesciunt mollitia nobis obcaecati.
					<h1>Samsung Galaxy A70 (White, 128 GB)  (6 GB RAM)</h1>
					Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quas ea eligendi illo, tenetur, sit, similique maxime natus veniam, assumenda doloremque sapiente. Dolores iure quis, accusamus harum nesciunt mollitia nobis obcaecati.

					]Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
					tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
					quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
					consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
					cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
					proident, sunt in culpa qui officia deserunt mollit anim id est laborum.

					loremLorem ipsum dolor sit amet, consectetur adipisicing elit. Sapiente iusto iste at dolor fugiat, nisi nihil ea dicta nostrum, voluptates ducimus, consequatur iure praesentium dolorem nesciunt eaque culpa ut tempora.Lorem ipsum dolor sit amet, consectetur adipisicing elit. Inventore ipsum dignissimos non, esse doloremque qui consequatur eius, fuga aspernatur, facere corporis porro sit distinctio voluptatibus praesentium pariatur aliquam. Quae, voluptate.
					<h1>Samsung Galaxy A70 (White, 128 GB)  (6 GB RAM)</h1>
					Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quas ea eligendi illo, tenetur, sit, similique maxime natus veniam, assumenda doloremque sapiente. Dolores iure quis, accusamus harum nesciunt mollitia nobis obcaecati.
					<h1>Samsung Galaxy A70 (White, 128 GB)  (6 GB RAM)</h1>
					Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quas ea eligendi illo, tenetur, sit, similique maxime natus veniam, assumenda doloremque sapiente. Dolores iure quis, accusamus harum nesciunt mollitia nobis obcaecati.

					]Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
					tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
					quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
					consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
					cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
					proident, sunt in culpa qui officia deserunt mollit anim id est laborum.

					loremLorem ipsum dolor sit amet, consectetur adipisicing elit. Sapiente iusto iste at dolor fugiat, nisi nihil ea dicta nostrum, voluptates ducimus, consequatur iure praesentium dolorem nesciunt eaque culpa ut tempora.Lorem ipsum dolor sit amet, consectetur adipisicing elit. Inventore ipsum dignissimos non, esse doloremque qui consequatur eius, fuga aspernatur, facere corporis porro sit distinctio voluptatibus praesentium pariatur aliquam. Quae, voluptate.
					<h1>Samsung Galaxy A70 (White, 128 GB)  (6 GB RAM)</h1>
					Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quas ea eligendi illo, tenetur, sit, similique maxime natus veniam, assumenda doloremque sapiente. Dolores iure quis, accusamus harum nesciunt mollitia nobis obcaecati.
					<h1>Samsung Galaxy A70 (White, 128 GB)  (6 GB RAM)</h1>
					Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quas ea eligendi illo, tenetur, sit, similique maxime natus veniam, assumenda doloremque sapiente. Dolores iure quis, accusamus harum nesciunt mollitia nobis obcaecati.

					]Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
					tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
					quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
					consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
					cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
					proident, sunt in culpa qui officia deserunt mollit anim id est laborum.

					loremLorem ipsum dolor sit amet, consectetur adipisicing elit. Sapiente iusto iste at dolor fugiat, nisi nihil ea dicta nostrum, voluptates ducimus, consequatur iure praesentium dolorem nesciunt eaque culpa ut tempora.Lorem ipsum dolor sit amet, consectetur adipisicing elit. Inventore ipsum dignissimos non, esse doloremque qui consequatur eius, fuga aspernatur, facere corporis porro sit distinctio voluptatibus praesentium pariatur aliquam. Quae, voluptate.
				</div>
			</div>
		</div>
	</div>
</section>
<?php include('include/footer.php'); ?>