<?php include('include/header.php'); ?>

<nav aria-label="breadcrumb" class="breadcrumb-main bg-para" style="background: linear-gradient(rgba(0,0,0,0.4),rgba(0,0,0,0.4)), url('img/bread.jpg');">
    <div class="container clearfix"> <!-- Container .// -->
        <h3 class="float-left">Request Area</h3>
        <ol class="breadcrumb float-right">
            <li class="breadcrumb-item"><a href="index.php">Home</a></li>
            <!-- <li class="breadcrumb-item"><a href="#">Library</a></li> -->
            <li class="breadcrumb-item active" aria-current="page">Request Area</li>
        </ol>
    </div> <!-- Container .// -->
</nav>

<section class="user-dashboard">
    <div class="container-fluid"> 
        <div class="row"> 
            <div class="col-lg-3">
                <div class="left-side-user-dashboard"> 
                    <div class="header">
                        <div class="prof_who">
                            <div class="img_holder">
                                <img src="img/user/user-profile.jpg" class="img-fluid" alt="">
                            </div>
                            <div class="title_holder"> 
                                <span>Welcome</span>
                                <h3>Gopal Basnet</h3> 
                            </div>
                        </div>
                    </div>
                    <div class="content nav flex-column nav-pills" role="tablist" aria-orientation="vertical">
                        <ul class="nav"> 
                            <li class="label">Update User Profile</li>
                            <li class="left-nav"><a class="nav-link " href="update-info.php">Update Info</a></li>
                            <li class="left-nav"><a class="nav-link active" href="request-area.php">Request Area</a></li>
                            <li class="left-nav"><a class= "nav-link " href="change-password.php">Change Password</a></li>
                            <li class="left-nav"><a class= "nav-link " href="shipping-address.php">Shipping Address</a></li>
                            <li class="left-nav"><a class= "nav-link " href="wishlist.php">WishList</a></li>
                            <li class="left-nav"><a class= "nav-link " href="notification.php">Notifications</a></li>
                            <li class="left-nav"><a class= "nav-link" href="#">Log Out</a></li>
                        </ul>
                    </div>
                </div> 
            </div>
            <div class="col-lg-9">
                <div class="right-user-dashboard">
                    <div class="tab-content">   
                        <div class="tab-pane fade fade show active">
                            <div class="add-product-right-side"> 
                                <div class="alert alert-vendor alert-dismissible fade show" role="alert">
                                    Area Requested Successfully
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                </div> 
                                <form action="" class="">  
                                    <div class="form-group area-request-div">
                                        <label for="">Select Areas</label>
                                        <select class="area-request form-control select2-area" name="" multiple="multiple">
                                            <option value="1">Mechi</option>
                                            <option value="2">Koshi</option>
                                            <option value="3">Sagarmatha</option>
                                            <option value="4">Janakpur</option>
                                            <option value="5">Narayani</option>
                                            <option value="6">Bagmati</option>
                                            <option value="7">Gandaki</option>
                                            <option value="8">Lumbini</option>
                                            <option value="9">Dhaulagiri</option>
                                            <option value="10">Rapti</option>
                                            <option value="11">Karnali</option>
                                            <option value="12">Bheri</option>
                                            <option value="13">Seti</option>
                                            <option value="14">Mahakali</option>
                                        </select>
                                    </div>
                                    <div class="btn btn-add-product">
                                        Request Area
                                    </div> 
                                </form>
                                <div class="table-responsive user-dashboard-tbl">
                                    <table class="table table-bordered">
                                        <thead>
                                            <tr>
                                                <th scope="col">S.N</th>
                                                <th scope="col">My Area Request</th>  
                                                <th scope="col">Actions</th> 
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <td>1.</td> 
                                                <td class="area-request-list-ul">
                                                    <ul>
                                                        <li>Kathmandu</li>
                                                        <li>Butwal</li>
                                                        <li>Birtamode</li>
                                                    </ul>
                                                </td> 
                                                <td class="add-img-td-action"> 
                                                    <a href="#" class="btn btn-delete-product" title="Delete"><i class="far fa-trash-alt"></i></a> 
                                                </td> 
                                            </tr> 
                                            <tr>
                                                <td>2.</td>
                                                <td class="area-request-list-ul">
                                                    <ul>
                                                        <li>Kathmandu</li>
                                                        <li>Butwal</li>
                                                        <li>Birtamode</li>
                                                    </ul>
                                                </td>  
                                                <td class="add-img-td-action"> 
                                                    <a href="#" class="btn btn-delete-product" title="Delete"><i class="far fa-trash-alt"></i></a> 
                                                </td>
                                            </tr>  
                                        </tbody> 
                                    </table>
                                </div>
                            </div> 
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div> 
</section>

<?php include('include/footer.php'); ?>