<?php include('include/header.php'); ?>

<!-- Landing Page -->

<section class="index-slider-section">
	<div class="container-fluid p-0">
        <div class="owl-carousel owl-theme">
            <div class="item">
                <div class="slider-wrapper">
                    <a href="javascript:">
                        <img class="lazyload animate-default" data-src="img/slider/Image1.jpg" alt="Landing-Banner">
                    </a>
                </div>
            </div>
            <div class="item">
                <div class="slider-wrapper">
                    <a href="javascript:">
                        <img class="lazyload animate-default" data-src="img/banner-winter-another-02.jpg" alt="Landing-Banner">
                    </a>
                </div>
            </div>
            <div class="item">
                <div class="slider-wrapper">
                    <a href="javascript:">
                        <img class="lazyload animate-default" data-src="img/banner-winter-another-01.jpg" alt="Landing-Banner1">
                    </a>
                </div>
            </div>
        </div>
	</div>
</section>

<!-- Landing Page -->

<!-- two-section -->

<section class="two-section bg-grey ">
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-12">
                <section class="white-card flash-list">
                    <div class="section-title-wrapper title-tab clearfix"> 
                        <h3 class="float-left">Flash Sale</h3>
                    </div>  
                    <div class="owl-carousel owl-theme owl-nav-top">
                        <div class="item">                            
                            <div class="product-card">
                                <div class="card-image effect-hover-zoom">
                                    <a href="javascript:">
                                        <img data-src="img/1.jpg" alt="Product-Image" class="lazyload animate-default">
                                    </a>
                                    <div class="discount">
                                        <p>35% <span>OFF</span></p>
                                    </div>
                                </div>
                                <div class="card-detail">
                                    <div class="rating">
                                        <ul class="list-unstyled side-listing">
                                            <li><i class="fas fa-star active"></i></li>
                                            <li><i class="fas fa-star active"></i></li>
                                            <li><i class="fas fa-star active"></i></li>
                                            <li><i class="fas fa-star"></i></li>
                                            <li><i class="fas fa-star"></i></li>
                                        </ul>
                                    </div>
                                    <p class="product-title"><a href="javascript:">Butterflies Hand-held Bag</a></p>
                                    <p class="product-price">RS. 10,000 <span>RS. 2000</span> </p>
                                </div>
                            </div>
                            <div class="product-card">
                                <div class="card-image effect-hover-zoom">
                                    <a href="javascript:">
                                        <img data-src="img/products/product5.jpg" alt="Product-Image" class="lazyload animate-default">
                                    </a>

                                    <div class="discount">
                                        <p>35% <span>OFF</span></p>
                                    </div>
                                </div>
                                <div class="card-detail">
                                    <div class="rating">
                                        <ul class="list-unstyled side-listing">
                                            <li><i class="fas fa-star active"></i></li>
                                            <li><i class="fas fa-star active"></i></li>
                                            <li><i class="fas fa-star active"></i></li>
                                            <li><i class="fas fa-star"></i></li>
                                            <li><i class="fas fa-star"></i></li>
                                        </ul>
                                    </div>
                                    <p class="product-title"><a href="javascript:">Butterflies Hand-held Bag</a></p>
                                    <p class="product-price">RS. 10,000 <span>RS. 2000</span></p>
                                </div>
                            </div>
                        </div>
                        <div class="item">                            
                            <div class="product-card">
                                <div class="card-image effect-hover-zoom">
                                    <a href="javascript:">
                                        <img data-src="img/products/product2.jpg" alt="Product-Image" class="lazyload animate-default">
                                    </a> 
                                    <div class="discount">
                                        <p>35% <span>OFF</span></p>
                                    </div>
                                </div>
                                <div class="card-detail">
                                    <div class="rating">
                                        <ul class="list-unstyled side-listing">
                                            <li><i class="fas fa-star active"></i></li>
                                            <li><i class="fas fa-star active"></i></li>
                                            <li><i class="fas fa-star active"></i></li>
                                            <li><i class="fas fa-star"></i></li>
                                            <li><i class="fas fa-star"></i></li>
                                        </ul>
                                    </div>
                                    <p class="product-title"><a href="javascript:">Butterflies Hand-held Bag</a></p>
                                    <p class="product-price">RS. 10,000 <span>RS. 2000</span></p>
                                </div>
                            </div> 
                            <div class="product-card">
                                <div class="card-image effect-hover-zoom">
                                    <a href="javascript:">
                                        <img data-src="img/2.png" alt="Product-Image" class="lazyload animate-default">
                                    </a> 
                                    <div class="discount">
                                        <p>35% <span>OFF</span></p>
                                    </div>
                                </div>
                                <div class="card-detail">
                                    <div class="rating">
                                        <ul class="list-unstyled side-listing">
                                            <li><i class="fas fa-star active"></i></li>
                                            <li><i class="fas fa-star active"></i></li>
                                            <li><i class="fas fa-star active"></i></li>
                                            <li><i class="fas fa-star"></i></li>
                                            <li><i class="fas fa-star"></i></li>
                                        </ul>
                                    </div>
                                    <p class="product-title"><a href="javascript:">Butterflies Hand-held Bag</a></p>
                                    <p class="product-price">RS. 10,000 <span>RS. 2000</span></p>
                                </div>
                            </div>
                        </div>
                        <div class="item">                            
                            <div class="product-card">
                                <div class="card-image effect-hover-zoom">
                                    <a href="javascript:">
                                        <img data-src="img/products/product3.jpg" alt="Product-Image" class="lazyload animate-default">
                                    </a>
                                    <div class="discount">
                                        <p>35% <span>OFF</span></p>
                                    </div>
                                </div>
                                <div class="card-detail">
                                    <div class="rating">
                                        <ul class="list-unstyled side-listing">
                                            <li><i class="fas fa-star active"></i></li>
                                            <li><i class="fas fa-star active"></i></li>
                                            <li><i class="fas fa-star active"></i></li>
                                            <li><i class="fas fa-star"></i></li>
                                            <li><i class="fas fa-star"></i></li>
                                        </ul>
                                    </div>
                                    <p class="product-title"><a href="javascript:">Butterflies Hand-held Bag</a></p>
                                    <p class="product-price">RS. 10,000 <span>RS. 2000</span></p>
                                </div>
                            </div>

                            <div class="product-card">
                                <div class="card-image effect-hover-zoom">
                                    <a href="javascript:">
                                        <img data-src="img/3.jpg" alt="Product-Image" class="lazyload animate-default">
                                    </a>
                                    <div class="discount">
                                        <p>35% <span>OFF</span></p>
                                    </div>
                                </div>
                                <div class="card-detail">
                                    <div class="rating">
                                        <ul class="list-unstyled side-listing">
                                            <li><i class="fas fa-star active"></i></li>
                                            <li><i class="fas fa-star active"></i></li>
                                            <li><i class="fas fa-star active"></i></li>
                                            <li><i class="fas fa-star"></i></li>
                                            <li><i class="fas fa-star"></i></li>
                                        </ul>
                                    </div>
                                    <p class="product-title"><a href="javascript:">Butterflies Hand-held Bag</a></p>
                                    <p class="product-price">RS. 1,10,000 <span>RS. 200</span></p>
                                </div>
                            </div>
                        </div>
                        <div class="item">                            
                            <div class="product-card">
                                <div class="card-image effect-hover-zoom">
                                    <a href="javascript:">
                                        <img data-src="img/products/product4.jpg" alt="Product-Image" class="lazyload animate-default">
                                    </a>
                                    <div class="discount">
                                        <p>35% <span>OFF</span></p>
                                    </div>
                                </div>
                                <div class="card-detail">
                                    <div class="rating">
                                        <ul class="list-unstyled side-listing">
                                            <li><i class="fas fa-star active"></i></li>
                                            <li><i class="fas fa-star active"></i></li>
                                            <li><i class="fas fa-star active"></i></li>
                                            <li><i class="fas fa-star"></i></li>
                                            <li><i class="fas fa-star"></i></li>
                                        </ul>
                                    </div>
                                    <p class="product-title"><a href="javascript:">Butterflies Hand-held Bag</a></p>
                                    <p class="product-price">RS. 1,10,000 <span>RS. 200</span></p>
                                </div>
                            </div>

                            <div class="product-card">
                                <div class="card-image effect-hover-zoom">
                                    <a href="javascript:">
                                        <img data-src="img/products/product8.jpg" alt="Product-Image" class="lazyload animate-default">
                                    </a>
                                    <div class="discount">
                                        <p>35% <span>OFF</span></p>
                                    </div>
                                </div>
                                <div class="card-detail">
                                    <div class="rating">
                                        <ul class="list-unstyled side-listing">
                                            <li><i class="fas fa-star active"></i></li>
                                            <li><i class="fas fa-star active"></i></li>
                                            <li><i class="fas fa-star active"></i></li>
                                            <li><i class="fas fa-star"></i></li>
                                            <li><i class="fas fa-star"></i></li>
                                        </ul>
                                    </div>
                                    <p class="product-title"><a href="javascript:">Butterflies Hand-held Bag</a></p>
                                    <p class="product-price">RS. 1,10,000 <span>RS. 200</span></p>
                                </div>
                            </div>
                        </div>
                        <div class="item">                           
                            <div class="product-card">
                                <div class="card-image effect-hover-zoom">
                                    <a href="javascript:">
                                        <img data-src="img/products/product5.jpg" alt="Product-Image" class="lazyload animate-default">
                                    </a>
                                    <div class="discount">
                                        <p>35% <span>OFF</span></p>
                                    </div>
                                </div>
                                <div class="card-detail">
                                    <div class="rating">
                                        <ul class="list-unstyled side-listing">
                                            <li><i class="fas fa-star active"></i></li>
                                            <li><i class="fas fa-star active"></i></li>
                                            <li><i class="fas fa-star active"></i></li>
                                            <li><i class="fas fa-star"></i></li>
                                            <li><i class="fas fa-star"></i></li>
                                        </ul>
                                    </div>
                                    <p class="product-title"><a href="javascript:">Butterflies Hand-held Bag</a></p>
                                    <p class="product-price">RS. 1,10,000 <span>RS. 200</span></p>
                                </div>
                            </div>
                            <div class="product-card">
                                <div class="card-image effect-hover-zoom">
                                    <a href="javascript:">
                                        <img data-src="img/products/product1.jpg" alt="Product-Image" class="lazyload animate-default">
                                    </a>
                                    <div class="discount">
                                        <p>35% <span>OFF</span></p>
                                    </div>
                                </div>
                                <div class="card-detail">
                                    <div class="rating">
                                        <ul class="list-unstyled side-listing">
                                            <li><i class="fas fa-star active"></i></li>
                                            <li><i class="fas fa-star active"></i></li>
                                            <li><i class="fas fa-star active"></i></li>
                                            <li><i class="fas fa-star"></i></li>
                                            <li><i class="fas fa-star"></i></li>
                                        </ul>
                                    </div>
                                    <p class="product-title"><a href="javascript:">Butterflies Hand-held Bag</a></p>
                                    <p class="product-price">RS. 1,10,000 <span>RS. 200</span></p>
                                </div>
                            </div>
                        </div>
                        <div class="item">                            
                            <div class="product-card">
                                <div class="card-image effect-hover-zoom">
                                    <a href="javascript:">
                                        <img data-src="img/products/product3.jpg" alt="Product-Image" class="lazyload animate-default">
                                    </a>
                                    <div class="discount">
                                        <p>35% <span>OFF</span></p>
                                    </div>
                                </div>
                                <div class="card-detail">
                                    <div class="rating">
                                        <ul class="list-unstyled side-listing">
                                            <li><i class="fas fa-star active"></i></li>
                                            <li><i class="fas fa-star active"></i></li>
                                            <li><i class="fas fa-star active"></i></li>
                                            <li><i class="fas fa-star"></i></li>
                                            <li><i class="fas fa-star"></i></li>
                                        </ul>
                                    </div>
                                    <p class="product-title"><a href="javascript:">Butterflies Hand-held Bag</a></p>
                                    <p class="product-price">RS. 1,10,000 <span>RS. 200</span></p>
                                </div>
                            </div>
                            <div class="product-card">
                                <div class="card-image effect-hover-zoom">
                                    <a href="javascript:">
                                        <img data-src="img/products/product2.jpg" alt="Product-Image" class="lazyload animate-default">
                                    </a>
                                    <div class="discount">
                                        <p>35% <span>OFF</span></p>
                                    </div>
                                </div>
                                <div class="card-detail">
                                    <div class="rating">
                                        <ul class="list-unstyled side-listing">
                                            <li><i class="fas fa-star active"></i></li>
                                            <li><i class="fas fa-star active"></i></li>
                                            <li><i class="fas fa-star active"></i></li>
                                            <li><i class="fas fa-star"></i></li>
                                            <li><i class="fas fa-star"></i></li>
                                        </ul>
                                    </div>
                                    <p class="product-title"><a href="javascript:">Butterflies Hand-held Bag</a></p>
                                    <p class="product-price">RS. 1,10,000 <span>RS. 200</span></p>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
                <section class="advo">
                    <a href="javascript:">
                        <img src="img/advo/advo-long1.jpg" alt="Image" class="lazyload animate-default img-fluid">
                        <div class="advo-long-content">
                            <p>NEW MENS FASHION <span>SAVE UP TO 40% OFF</span></p>
                        </div>
                    </a>
                </section>
            </div>
        </div>
    </div>
</section>

<!-- ----------- -->
<!-- two-section -->
<!-- ----------- -->

<!-- ----------- -->
<!-- Category -->
<!-- ----------- -->

<section class="category-listing">
    <div class="container-fluid">
        <div class="card-cat-wrapper">
            <div class="row no-gutters"> 
                <div class="col-lg-2 col-sm-3 col-6">
                    <div class="category-box">
                        <a href="javascript:">
                            <div class="d-flex align-items-center">
                                <div class="content">
                                    <h4>Cells & Tablets</h4>
                                    <p>12 Products</p>
                                </div>
                                <div class="image">
                                    <img src="img/category/category1.jpg" alt="Category-Image" class="lazyload">
                                </div>
                            </div>
                        </a>
                    </div> 
                </div>
                <div class="col-lg-2 col-sm-3 col-6">
                    <div class="category-box">
                        <a href="javascript:">
                            <div class="d-flex align-items-center">
                                <div class="content">
                                    <h4>Computers</h4>
                                    <p>12 Products</p>
                                </div>
                                <div class="image">
                                    <img src="img/category/category2.jpg" alt="Category-Image" class="lazyload">
                                </div>
                            </div>
                        </a>
                    </div> 
                </div>
                <div class="col-lg-2 col-sm-3 col-6">
                    <div class="category-box">
                        <a href="javascript:">
                            <div class="d-flex align-items-center">
                                <div class="content">
                                    <h4>Fashion</h4>
                                    <p>12 Products</p>
                                </div>
                                <div class="image">
                                    <img src="img/category/category3.jpg" alt="Category-Image" class="lazyload">
                                </div>
                            </div>
                        </a>
                    </div> 
                </div>
                <div class="col-lg-2 col-sm-3 col-6">
                    <div class="category-box">
                        <a href="javascript:">
                            <div class="d-flex align-items-center">
                                <div class="content">
                                    <h4>Sunglasses</h4>
                                    <p>12 Products</p>
                                </div>
                                <div class="image">
                                    <img src="img/category/category4.jpg" alt="Category-Image" class="lazyload">
                                </div>
                            </div>
                        </a>
                    </div> 
                </div>
                <div class="col-lg-2 col-sm-3 col-6">
                    <div class="category-box">
                        <a href="javascript:">
                            <div class="d-flex align-items-center">
                                <div class="content">
                                    <h4>Baby & Kids</h4>
                                    <p>12 Products</p>
                                </div>
                                <div class="image">
                                    <img src="img/category/category5.jpg" alt="Category-Image" class="lazyload">
                                </div>
                            </div>
                        </a>
                    </div> 
                </div>
                <div class="col-lg-2 col-sm-3 col-6">
                    <div class="category-box">
                        <a href="javascript:">
                            <div class="d-flex align-items-center">
                                <div class="content">
                                    <h4>Accessories</h4>
                                    <p>12 Products</p>
                                </div>
                                <div class="image">
                                    <img src="img/category/category6.jpg" alt="Category-Image" class="lazyload">
                                </div>
                            </div>
                        </a>
                    </div> 
                </div>
                <div class="col-lg-2 col-sm-3 col-6">
                    <div class="category-box">
                        <a href="javascript:">
                            <div class="d-flex align-items-center">
                                <div class="content">
                                    <h4>Furniture</h4>
                                    <p>12 Products</p>
                                </div>
                                <div class="image">
                                    <img src="img/category/category7.jpg" alt="Category-Image" class="lazyload">
                                </div>
                            </div>
                        </a>
                    </div> 
                </div>
                <div class="col-lg-2 col-sm-3 col-6">
                    <div class="category-box">
                        <a href="javascript:">
                            <div class="d-flex align-items-center">
                                <div class="content">
                                    <h4>Electronics</h4>
                                    <p>12 Products</p>
                                </div>
                                <div class="image">
                                    <img src="img/category/category8.jpg" alt="Category-Image" class="lazyload">
                                </div>
                            </div>
                        </a>
                    </div> 
                </div>
                <div class="col-lg-2 col-sm-3 col-6">
                    <div class="category-box">
                        <a href="javascript:">
                            <div class="d-flex align-items-center">
                                <div class="content">
                                    <h4>Fashion</h4>
                                    <p>12 Products</p>
                                </div>
                                <div class="image">
                                    <img src="img/category/category9.jpg" alt="Category-Image" class="lazyload">
                                </div>
                            </div>
                        </a>
                    </div> 
                </div>
                <div class="col-lg-2 col-sm-3 col-6">
                    <div class="category-box">
                        <a href="javascript:">
                            <div class="d-flex align-items-center">
                                <div class="content">
                                    <h4>Automobiles</h4>
                                    <p>12 Products</p>
                                </div>
                                <div class="image">
                                    <img src="img/category/category10.jpg" alt="Category-Image" class="lazyload">
                                </div>
                            </div>
                        </a>
                    </div> 
                </div>
                <div class="col-lg-2 col-sm-3 col-6">
                    <div class="category-box">
                        <a href="javascript:">
                            <div class="d-flex align-items-center">
                                <div class="content">
                                    <h4>Cells & Tablets</h4>
                                    <p>12 Products</p>
                                </div>
                                <div class="image">
                                    <img src="img/category/category1.jpg" alt="Category-Image" class="lazyload">
                                </div>
                            </div>
                        </a>
                    </div> 
                </div>
                <div class="col-lg-2 col-sm-3 col-6">
                    <div class="category-box">
                        <a href="javascript:">
                            <div class="d-flex align-items-center">
                                <div class="content">
                                    <h4>Computers</h4>
                                    <p>12 Products</p>
                                </div>
                                <div class="image">
                                    <img src="img/category/category2.jpg" alt="Category-Image" class="lazyload">
                                </div>
                            </div>
                        </a>
                    </div> 
                </div> 
            </div>
        </div>
    </div>
</section>

<!-- ----------- -->
<!-- Category -->
<!-- ----------- -->


<!-- Fashion-category -->

<section class="top-category pad-t-20 bg-grey">
    <div class="container-fluid">
        <div class="white-card">
        <div class="section-title-wrapper title-tab clearfix">
            <!-- <h3 class="float-left">Top Category</h3> -->
            <ul class="nav nav-tabs cat-nav" id="myTab" role="tablist">
                <li class="nav-item">
                    <a class="nav-link active" id="women-tab" data-toggle="tab" href="#women" role="tab" aria-controls="women" aria-selected="true">Womens Fashion</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" id="mens-tab" data-toggle="tab" href="#mens" role="tab" aria-controls="mens" aria-selected="false">Mens Fashion</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" id="kids-tab" data-toggle="tab" href="#kids" role="tab" aria-controls="kids" aria-selected="false">Kids Fashion</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" id="jewelery-tab" data-toggle="tab" href="#jewelery" role="tab" aria-controls="jewelery" aria-selected="false">Jewelery</a>
                </li>
                <li class="view-more-btn">
                    <a href="javascript:" class="float-right">View More</a>
                </li>
            </ul>
        </div>

        <div class="tab-content" id="myTabContent"> 
            <div class="tab-pane fade show active" id="women" role="tabpanel" aria-labelledby="women-tab"> 
                <div class="owl-carousel owl-theme owl-nav-top"> 
                    <div class="item"> 
                        <div class="product-card"> 
                            <div class="card-image effect-hover-zoom">
                                <a href="javascript:">
                                    <img data-src="img/products/product1.jpg" alt="Product-Image" class="lazyload animate-default">
                                </a>
                            </div>
                            <div class="card-detail">
                                <div class="rating">
                                    <ul class="list-unstyled side-listing">
                                        <li><i class="fas fa-star active"></i></li>
                                        <li><i class="fas fa-star active"></i></li>
                                        <li><i class="fas fa-star active"></i></li>
                                        <li><i class="fas fa-star"></i></li>
                                        <li><i class="fas fa-star"></i></li>
                                    </ul>
                                </div>
                                <p class="product-title"><a href="javascript:">Butterflies Hand-held Bag</a></p>
                                <p class="product-price">RS. 1,10,000</p>
                            </div>
                        </div>

                        <div class="product-card">
                            <div class="card-image effect-hover-zoom">
                                <a href="javascript:">
                                    <img data-src="img/products/product6.jpg" alt="Product-Image" class="lazyload animate-default">
                                </a>
                            </div>
                            <div class="card-detail">
                                <div class="rating">
                                    <ul class="list-unstyled side-listing">
                                        <li><i class="fas fa-star active"></i></li>
                                        <li><i class="fas fa-star active"></i></li>
                                        <li><i class="fas fa-star active"></i></li>
                                        <li><i class="fas fa-star"></i></li>
                                        <li><i class="fas fa-star"></i></li>
                                    </ul>
                                </div>
                                <p class="product-title"><a href="javascript:">Butterflies Hand-held Bag</a></p>
                                <p class="product-price">RS. 1,10,000</p>
                            </div>
                        </div>

                    </div>
                    <div class="item">
                        <div class="product-card">
                            <div class="card-image effect-hover-zoom">
                                <a href="javascript:">
                                    <img data-src="img/products/product2.jpg" alt="Product-Image" class="lazyload animate-default">
                                </a>
                            </div>
                            <div class="card-detail">
                                <div class="rating">
                                    <ul class="list-unstyled side-listing">
                                        <li><i class="fas fa-star active"></i></li>
                                        <li><i class="fas fa-star active"></i></li>
                                        <li><i class="fas fa-star active"></i></li>
                                        <li><i class="fas fa-star"></i></li>
                                        <li><i class="fas fa-star"></i></li>
                                    </ul>
                                </div>
                                <p class="product-title"><a href="javascript:">Butterflies Hand-held Bag</a></p>
                                <p class="product-price">RS. 1,10,000</p>
                            </div>
                        </div>

                        <div class="product-card">
                            <div class="card-image effect-hover-zoom">
                                <a href="javascript:">
                                    <img data-src="img/products/product8.jpg" alt="Product-Image" class="lazyload animate-default">
                                </a>
                            </div>
                            <div class="card-detail">
                                <div class="rating">
                                    <ul class="list-unstyled side-listing">
                                        <li><i class="fas fa-star active"></i></li>
                                        <li><i class="fas fa-star active"></i></li>
                                        <li><i class="fas fa-star active"></i></li>
                                        <li><i class="fas fa-star"></i></li>
                                        <li><i class="fas fa-star"></i></li>
                                    </ul>
                                </div>
                                <p class="product-title"><a href="javascript:">Butterflies Hand-held Bag</a></p>
                                <p class="product-price">RS. 1,10,000</p>
                            </div>
                        </div>

                    </div>
                    <div class="item">
                        <div class="product-card">
                            <div class="card-image effect-hover-zoom">
                                <a href="javascript:">
                                    <img data-src="img/products/product3.jpg" alt="Product-Image" class="lazyload animate-default">
                                </a>
                            </div>
                            <div class="card-detail">
                                <div class="rating">
                                    <ul class="list-unstyled side-listing">
                                        <li><i class="fas fa-star active"></i></li>
                                        <li><i class="fas fa-star active"></i></li>
                                        <li><i class="fas fa-star active"></i></li>
                                        <li><i class="fas fa-star"></i></li>
                                        <li><i class="fas fa-star"></i></li>
                                    </ul>
                                </div>
                                <p class="product-title"><a href="javascript:">Butterflies Hand-held Bag</a></p>
                                <p class="product-price">RS. 1,10,000</p>
                            </div>
                        </div>

                        <div class="product-card">
                            <div class="card-image effect-hover-zoom">
                                <a href="javascript:">
                                    <img data-src="img/products/product9.webp" alt="Product-Image" class="lazyload animate-default">
                                </a>
                            </div>
                            <div class="card-detail">
                                <div class="rating">
                                    <ul class="list-unstyled side-listing">
                                        <li><i class="fas fa-star active"></i></li>
                                        <li><i class="fas fa-star active"></i></li>
                                        <li><i class="fas fa-star active"></i></li>
                                        <li><i class="fas fa-star"></i></li>
                                        <li><i class="fas fa-star"></i></li>
                                    </ul>
                                </div>
                                <p class="product-title"><a href="javascript:">Butterflies Hand-held Bag</a></p>
                                <p class="product-price">RS. 1,10,000</p>
                            </div>
                        </div>

                    </div>
                    <div class="item">
                        <div class="product-card">
                            <div class="card-image effect-hover-zoom">
                                <a href="javascript:">
                                    <img data-src="img/products/product4.jpg" alt="Product-Image" class="lazyload animate-default">
                                </a>
                            </div>
                            <div class="card-detail">
                                <div class="rating">
                                    <ul class="list-unstyled side-listing">
                                        <li><i class="fas fa-star active"></i></li>
                                        <li><i class="fas fa-star active"></i></li>
                                        <li><i class="fas fa-star active"></i></li>
                                        <li><i class="fas fa-star"></i></li>
                                        <li><i class="fas fa-star"></i></li>
                                    </ul>
                                </div>
                                <p class="product-title"><a href="javascript:">Butterflies Hand-held Bag</a></p>
                                <p class="product-price">RS. 1,10,000</p>
                            </div>
                        </div>

                        <div class="product-card">
                            <div class="card-image effect-hover-zoom">
                                <a href="javascript:">
                                    <img data-src="img/products/product1.jpg" alt="Product-Image" class="lazyload animate-default">
                                </a>
                            </div>
                            <div class="card-detail">
                                <div class="rating">
                                    <ul class="list-unstyled side-listing">
                                        <li><i class="fas fa-star active"></i></li>
                                        <li><i class="fas fa-star active"></i></li>
                                        <li><i class="fas fa-star active"></i></li>
                                        <li><i class="fas fa-star"></i></li>
                                        <li><i class="fas fa-star"></i></li>
                                    </ul>
                                </div>
                                <p class="product-title"><a href="javascript:">Butterflies Hand-held Bag</a></p>
                                <p class="product-price">RS. 1,10,000</p>
                            </div>
                        </div>

                    </div>
                    <div class="item">
                        <div class="product-card">
                            <div class="card-image effect-hover-zoom">
                                <a href="javascript:">
                                    <img data-src="img/products/product5.jpg" alt="Product-Image" class="lazyload animate-default">
                                </a>
                            </div>
                            <div class="card-detail">
                                <div class="rating">
                                    <ul class="list-unstyled side-listing">
                                        <li><i class="fas fa-star active"></i></li>
                                        <li><i class="fas fa-star active"></i></li>
                                        <li><i class="fas fa-star active"></i></li>
                                        <li><i class="fas fa-star"></i></li>
                                        <li><i class="fas fa-star"></i></li>
                                    </ul>
                                </div>
                                <p class="product-title"><a href="javascript:">Butterflies Hand-held Bag</a></p>
                                <p class="product-price">RS. 1,10,000</p>
                            </div>
                        </div>

                        <div class="product-card">
                            <div class="card-image effect-hover-zoom">
                                <a href="javascript:">
                                    <img data-src="img/products/product2.jpg" alt="Product-Image" class="lazyload animate-default">
                                </a>
                            </div>
                            <div class="card-detail">
                                <div class="rating">
                                    <ul class="list-unstyled side-listing">
                                        <li><i class="fas fa-star active"></i></li>
                                        <li><i class="fas fa-star active"></i></li>
                                        <li><i class="fas fa-star active"></i></li>
                                        <li><i class="fas fa-star"></i></li>
                                        <li><i class="fas fa-star"></i></li>
                                    </ul>
                                </div>
                                <p class="product-title"><a href="javascript:">Butterflies Hand-held Bag</a></p>
                                <p class="product-price">RS. 1,10,000</p>
                            </div>
                        </div>

                    </div>
                    <div class="item">
                        <div class="product-card">
                            <div class="card-image effect-hover-zoom">
                                <a href="javascript:">
                                    <img data-src="img/products/product3.jpg" alt="Product-Image" class="lazyload animate-default">
                                </a>
                            </div>
                            <div class="card-detail">
                                <div class="rating">
                                    <ul class="list-unstyled side-listing">
                                        <li><i class="fas fa-star active"></i></li>
                                        <li><i class="fas fa-star active"></i></li>
                                        <li><i class="fas fa-star active"></i></li>
                                        <li><i class="fas fa-star"></i></li>
                                        <li><i class="fas fa-star"></i></li>
                                    </ul>
                                </div>
                                <p class="product-title"><a href="javascript:">Butterflies Hand-held Bag</a></p>
                                <p class="product-price">RS. 1,10,000</p>
                            </div>
                        </div>

                        <div class="product-card">
                            <div class="card-image effect-hover-zoom">
                                <a href="javascript:">
                                    <img data-src="img/products/product7.jpg" alt="Product-Image" class="lazyload animate-default">
                                </a>
                            </div>
                            <div class="card-detail">
                                <div class="rating">
                                    <ul class="list-unstyled side-listing">
                                        <li><i class="fas fa-star active"></i></li>
                                        <li><i class="fas fa-star active"></i></li>
                                        <li><i class="fas fa-star active"></i></li>
                                        <li><i class="fas fa-star"></i></li>
                                        <li><i class="fas fa-star"></i></li>
                                    </ul>
                                </div>
                                <p class="product-title"><a href="javascript:">Butterflies Hand-held Bag</a></p>
                                <p class="product-price">RS. 1,10,000</p>
                            </div>
                        </div>

                    </div>
                    <div class="item">
                        <div class="product-card">
                            <div class="card-image effect-hover-zoom">
                                <a href="javascript:">
                                    <img data-src="img/products/product4.jpg" alt="Product-Image" class="lazyload animate-default">
                                </a>
                            </div>
                            <div class="card-detail">
                                <div class="rating">
                                    <ul class="list-unstyled side-listing">
                                        <li><i class="fas fa-star active"></i></li>
                                        <li><i class="fas fa-star active"></i></li>
                                        <li><i class="fas fa-star active"></i></li>
                                        <li><i class="fas fa-star"></i></li>
                                        <li><i class="fas fa-star"></i></li>
                                    </ul>
                                </div>
                                <p class="product-title"><a href="javascript:">Butterflies Hand-held Bag</a></p>
                                <p class="product-price">RS. 1,10,000</p>
                            </div>
                        </div>

                        <div class="product-card">
                            <div class="card-image effect-hover-zoom">
                                <a href="javascript:">
                                    <img data-src="img/products/product1.jpg" alt="Product-Image" class="lazyload animate-default">
                                </a>
                            </div>
                            <div class="card-detail">
                                <div class="rating">
                                    <ul class="list-unstyled side-listing">
                                        <li><i class="fas fa-star active"></i></li>
                                        <li><i class="fas fa-star active"></i></li>
                                        <li><i class="fas fa-star active"></i></li>
                                        <li><i class="fas fa-star"></i></li>
                                        <li><i class="fas fa-star"></i></li>
                                    </ul>
                                </div>
                                <p class="product-title"><a href="javascript:">Butterflies Hand-held Bag</a></p>
                                <p class="product-price">RS. 1,10,000</p>
                            </div>
                        </div>

                    </div>
                    <div class="item">
                        <div class="product-card">
                            <div class="card-image effect-hover-zoom">
                                <a href="javascript:">
                                    <img data-src="img/products/product5.jpg" alt="Product-Image" class="lazyload animate-default">
                                </a>
                            </div>
                            <div class="card-detail">
                                <div class="rating">
                                    <ul class="list-unstyled side-listing">
                                        <li><i class="fas fa-star active"></i></li>
                                        <li><i class="fas fa-star active"></i></li>
                                        <li><i class="fas fa-star active"></i></li>
                                        <li><i class="fas fa-star"></i></li>
                                        <li><i class="fas fa-star"></i></li>
                                    </ul>
                                </div>
                                <p class="product-title"><a href="javascript:">Butterflies Hand-held Bag</a></p>
                                <p class="product-price">RS. 1,10,000</p>
                            </div>
                        </div>

                        <div class="product-card">
                            <div class="card-image effect-hover-zoom">
                                <a href="javascript:">
                                    <img data-src="img/products/product2.jpg" alt="Product-Image" class="lazyload animate-default">
                                </a>
                            </div>
                            <div class="card-detail">
                                <div class="rating">
                                    <ul class="list-unstyled side-listing">
                                        <li><i class="fas fa-star active"></i></li>
                                        <li><i class="fas fa-star active"></i></li>
                                        <li><i class="fas fa-star active"></i></li>
                                        <li><i class="fas fa-star"></i></li>
                                        <li><i class="fas fa-star"></i></li>
                                    </ul>
                                </div>
                                <p class="product-title"><a href="javascript:">Butterflies Hand-held Bag</a></p>
                                <p class="product-price">RS. 1,10,000</p>
                            </div>
                        </div>

                    </div>
                    <div class="item">
                        <div class="product-card">
                            <div class="card-image effect-hover-zoom">
                                <a href="javascript:">
                                    <img data-src="img/products/product3.jpg" alt="Product-Image" class="lazyload animate-default">
                                </a>
                            </div>
                            <div class="card-detail">
                                <div class="rating">
                                    <ul class="list-unstyled side-listing">
                                        <li><i class="fas fa-star active"></i></li>
                                        <li><i class="fas fa-star active"></i></li>
                                        <li><i class="fas fa-star active"></i></li>
                                        <li><i class="fas fa-star"></i></li>
                                        <li><i class="fas fa-star"></i></li>
                                    </ul>
                                </div>
                                <p class="product-title"><a href="javascript:">Butterflies Hand-held Bag</a></p>
                                <p class="product-price">RS. 1,10,000</p>
                            </div>
                        </div>

                        <div class="product-card">
                            <div class="card-image effect-hover-zoom">
                                <a href="javascript:">
                                    <img data-src="img/products/product7.jpg" alt="Product-Image" class="lazyload animate-default">
                                </a>
                            </div>
                            <div class="card-detail">
                                <div class="rating">
                                    <ul class="list-unstyled side-listing">
                                        <li><i class="fas fa-star active"></i></li>
                                        <li><i class="fas fa-star active"></i></li>
                                        <li><i class="fas fa-star active"></i></li>
                                        <li><i class="fas fa-star"></i></li>
                                        <li><i class="fas fa-star"></i></li>
                                    </ul>
                                </div>
                                <p class="product-title"><a href="javascript:">Butterflies Hand-held Bag</a></p>
                                <p class="product-price">RS. 1,10,000</p>
                            </div>
                        </div>

                    </div>
                </div>
            </div>

            <div class="tab-pane fade" id="mens" role="tabpanel" aria-labelledby="mens-tab">
            
                <div class="owl-carousel owl-theme owl-nav-top">

                    <div class="item">
                        <div class="product-card">
                            <div class="card-image effect-hover-zoom">
                                <a href="javascript:">
                                    <img data-src="img/products/product1.jpg" alt="Product-Image" class="lazyload animate-default">
                                </a>
                            </div>
                            <div class="card-detail">
                                <div class="rating">
                                    <ul class="list-unstyled side-listing">
                                        <li><i class="fas fa-star active"></i></li>
                                        <li><i class="fas fa-star active"></i></li>
                                        <li><i class="fas fa-star active"></i></li>
                                        <li><i class="fas fa-star"></i></li>
                                        <li><i class="fas fa-star"></i></li>
                                    </ul>
                                </div>
                                <p class="product-title"><a href="javascript:">Butterflies Hand-held Bag</a></p>
                                <p class="product-price">RS. 1,10,000</p>
                            </div>
                        </div>

                        <div class="product-card">
                            <div class="card-image effect-hover-zoom">
                                <a href="javascript:">
                                    <img data-src="img/products/product6.jpg" alt="Product-Image" class="lazyload animate-default">
                                </a>
                            </div>
                            <div class="card-detail">
                                <div class="rating">
                                    <ul class="list-unstyled side-listing">
                                        <li><i class="fas fa-star active"></i></li>
                                        <li><i class="fas fa-star active"></i></li>
                                        <li><i class="fas fa-star active"></i></li>
                                        <li><i class="fas fa-star"></i></li>
                                        <li><i class="fas fa-star"></i></li>
                                    </ul>
                                </div>
                                <p class="product-title"><a href="javascript:">Butterflies Hand-held Bag</a></p>
                                <p class="product-price">RS. 1,10,000</p>
                            </div>
                        </div>

                    </div>
                    <div class="item">
                        <div class="product-card">
                            <div class="card-image effect-hover-zoom">
                                <a href="javascript:">
                                    <img data-src="img/products/product2.jpg" alt="Product-Image" class="lazyload animate-default">
                                </a>
                            </div>
                            <div class="card-detail">
                                <div class="rating">
                                    <ul class="list-unstyled side-listing">
                                        <li><i class="fas fa-star active"></i></li>
                                        <li><i class="fas fa-star active"></i></li>
                                        <li><i class="fas fa-star active"></i></li>
                                        <li><i class="fas fa-star"></i></li>
                                        <li><i class="fas fa-star"></i></li>
                                    </ul>
                                </div>
                                <p class="product-title"><a href="javascript:">Butterflies Hand-held Bag</a></p>
                                <p class="product-price">RS. 1,10,000</p>
                            </div>
                        </div>

                        <div class="product-card">
                            <div class="card-image effect-hover-zoom">
                                <a href="javascript:">
                                    <img data-src="img/products/product8.jpg" alt="Product-Image" class="lazyload animate-default">
                                </a>
                            </div>
                            <div class="card-detail">
                                <div class="rating">
                                    <ul class="list-unstyled side-listing">
                                        <li><i class="fas fa-star active"></i></li>
                                        <li><i class="fas fa-star active"></i></li>
                                        <li><i class="fas fa-star active"></i></li>
                                        <li><i class="fas fa-star"></i></li>
                                        <li><i class="fas fa-star"></i></li>
                                    </ul>
                                </div>
                                <p class="product-title"><a href="javascript:">Butterflies Hand-held Bag</a></p>
                                <p class="product-price">RS. 1,10,000</p>
                            </div>
                        </div>

                    </div>
                    <div class="item">
                        <div class="product-card">
                            <div class="card-image effect-hover-zoom">
                                <a href="javascript:">
                                    <img data-src="img/products/product3.jpg" alt="Product-Image" class="lazyload animate-default">
                                </a>
                            </div>
                            <div class="card-detail">
                                <div class="rating">
                                    <ul class="list-unstyled side-listing">
                                        <li><i class="fas fa-star active"></i></li>
                                        <li><i class="fas fa-star active"></i></li>
                                        <li><i class="fas fa-star active"></i></li>
                                        <li><i class="fas fa-star"></i></li>
                                        <li><i class="fas fa-star"></i></li>
                                    </ul>
                                </div>
                                <p class="product-title"><a href="javascript:">Butterflies Hand-held Bag</a></p>
                                <p class="product-price">RS. 1,10,000</p>
                            </div>
                        </div>

                        <div class="product-card">
                            <div class="card-image effect-hover-zoom">
                                <a href="javascript:">
                                    <img data-src="img/products/product9.webp" alt="Product-Image" class="lazyload animate-default">
                                </a>
                            </div>
                            <div class="card-detail">
                                <div class="rating">
                                    <ul class="list-unstyled side-listing">
                                        <li><i class="fas fa-star active"></i></li>
                                        <li><i class="fas fa-star active"></i></li>
                                        <li><i class="fas fa-star active"></i></li>
                                        <li><i class="fas fa-star"></i></li>
                                        <li><i class="fas fa-star"></i></li>
                                    </ul>
                                </div>
                                <p class="product-title"><a href="javascript:">Butterflies Hand-held Bag</a></p>
                                <p class="product-price">RS. 1,10,000</p>
                            </div>
                        </div>

                    </div>
                    <div class="item">
                        <div class="product-card">
                            <div class="card-image effect-hover-zoom">
                                <a href="javascript:">
                                    <img data-src="img/products/product4.jpg" alt="Product-Image" class="lazyload animate-default">
                                </a>
                            </div>
                            <div class="card-detail">
                                <div class="rating">
                                    <ul class="list-unstyled side-listing">
                                        <li><i class="fas fa-star active"></i></li>
                                        <li><i class="fas fa-star active"></i></li>
                                        <li><i class="fas fa-star active"></i></li>
                                        <li><i class="fas fa-star"></i></li>
                                        <li><i class="fas fa-star"></i></li>
                                    </ul>
                                </div>
                                <p class="product-title"><a href="javascript:">Butterflies Hand-held Bag</a></p>
                                <p class="product-price">RS. 1,10,000</p>
                            </div>
                        </div>

                        <div class="product-card">
                            <div class="card-image effect-hover-zoom">
                                <a href="javascript:">
                                    <img data-src="img/products/product1.jpg" alt="Product-Image" class="lazyload animate-default">
                                </a>
                            </div>
                            <div class="card-detail">
                                <div class="rating">
                                    <ul class="list-unstyled side-listing">
                                        <li><i class="fas fa-star active"></i></li>
                                        <li><i class="fas fa-star active"></i></li>
                                        <li><i class="fas fa-star active"></i></li>
                                        <li><i class="fas fa-star"></i></li>
                                        <li><i class="fas fa-star"></i></li>
                                    </ul>
                                </div>
                                <p class="product-title"><a href="javascript:">Butterflies Hand-held Bag</a></p>
                                <p class="product-price">RS. 1,10,000</p>
                            </div>
                        </div>

                    </div>
                    <div class="item">
                        <div class="product-card">
                            <div class="card-image effect-hover-zoom">
                                <a href="javascript:">
                                    <img data-src="img/products/product5.jpg" alt="Product-Image" class="lazyload animate-default">
                                </a>
                            </div>
                            <div class="card-detail">
                                <div class="rating">
                                    <ul class="list-unstyled side-listing">
                                        <li><i class="fas fa-star active"></i></li>
                                        <li><i class="fas fa-star active"></i></li>
                                        <li><i class="fas fa-star active"></i></li>
                                        <li><i class="fas fa-star"></i></li>
                                        <li><i class="fas fa-star"></i></li>
                                    </ul>
                                </div>
                                <p class="product-title"><a href="javascript:">Butterflies Hand-held Bag</a></p>
                                <p class="product-price">RS. 1,10,000</p>
                            </div>
                        </div>

                        <div class="product-card">
                            <div class="card-image effect-hover-zoom">
                                <a href="javascript:">
                                    <img data-src="img/products/product2.jpg" alt="Product-Image" class="lazyload animate-default">
                                </a>
                            </div>
                            <div class="card-detail">
                                <div class="rating">
                                    <ul class="list-unstyled side-listing">
                                        <li><i class="fas fa-star active"></i></li>
                                        <li><i class="fas fa-star active"></i></li>
                                        <li><i class="fas fa-star active"></i></li>
                                        <li><i class="fas fa-star"></i></li>
                                        <li><i class="fas fa-star"></i></li>
                                    </ul>
                                </div>
                                <p class="product-title"><a href="javascript:">Butterflies Hand-held Bag</a></p>
                                <p class="product-price">RS. 1,10,000</p>
                            </div>
                        </div>

                    </div>
                    <div class="item">
                        <div class="product-card">
                            <div class="card-image effect-hover-zoom">
                                <a href="javascript:">
                                    <img data-src="img/products/product3.jpg" alt="Product-Image" class="lazyload animate-default">
                                </a>
                            </div>
                            <div class="card-detail">
                                <div class="rating">
                                    <ul class="list-unstyled side-listing">
                                        <li><i class="fas fa-star active"></i></li>
                                        <li><i class="fas fa-star active"></i></li>
                                        <li><i class="fas fa-star active"></i></li>
                                        <li><i class="fas fa-star"></i></li>
                                        <li><i class="fas fa-star"></i></li>
                                    </ul>
                                </div>
                                <p class="product-title"><a href="javascript:">Butterflies Hand-held Bag</a></p>
                                <p class="product-price">RS. 1,10,000</p>
                            </div>
                        </div>

                        <div class="product-card">
                            <div class="card-image effect-hover-zoom">
                                <a href="javascript:">
                                    <img data-src="img/products/product7.jpg" alt="Product-Image" class="lazyload animate-default">
                                </a>
                            </div>
                            <div class="card-detail">
                                <div class="rating">
                                    <ul class="list-unstyled side-listing">
                                        <li><i class="fas fa-star active"></i></li>
                                        <li><i class="fas fa-star active"></i></li>
                                        <li><i class="fas fa-star active"></i></li>
                                        <li><i class="fas fa-star"></i></li>
                                        <li><i class="fas fa-star"></i></li>
                                    </ul>
                                </div>
                                <p class="product-title"><a href="javascript:">Butterflies Hand-held Bag</a></p>
                                <p class="product-price">RS. 1,10,000</p>
                            </div>
                        </div>

                    </div>
                    <div class="item">
                        <div class="product-card">
                            <div class="card-image effect-hover-zoom">
                                <a href="javascript:">
                                    <img data-src="img/products/product4.jpg" alt="Product-Image" class="lazyload animate-default">
                                </a>
                            </div>
                            <div class="card-detail">
                                <div class="rating">
                                    <ul class="list-unstyled side-listing">
                                        <li><i class="fas fa-star active"></i></li>
                                        <li><i class="fas fa-star active"></i></li>
                                        <li><i class="fas fa-star active"></i></li>
                                        <li><i class="fas fa-star"></i></li>
                                        <li><i class="fas fa-star"></i></li>
                                    </ul>
                                </div>
                                <p class="product-title"><a href="javascript:">Butterflies Hand-held Bag</a></p>
                                <p class="product-price">RS. 1,10,000</p>
                            </div>
                        </div>

                        <div class="product-card">
                            <div class="card-image effect-hover-zoom">
                                <a href="javascript:">
                                    <img data-src="img/products/product1.jpg" alt="Product-Image" class="lazyload animate-default">
                                </a>
                            </div>
                            <div class="card-detail">
                                <div class="rating">
                                    <ul class="list-unstyled side-listing">
                                        <li><i class="fas fa-star active"></i></li>
                                        <li><i class="fas fa-star active"></i></li>
                                        <li><i class="fas fa-star active"></i></li>
                                        <li><i class="fas fa-star"></i></li>
                                        <li><i class="fas fa-star"></i></li>
                                    </ul>
                                </div>
                                <p class="product-title"><a href="javascript:">Butterflies Hand-held Bag</a></p>
                                <p class="product-price">RS. 1,10,000</p>
                            </div>
                        </div>

                    </div>
                    <div class="item">
                        <div class="product-card">
                            <div class="card-image effect-hover-zoom">
                                <a href="javascript:">
                                    <img data-src="img/products/product5.jpg" alt="Product-Image" class="lazyload animate-default">
                                </a>
                            </div>
                            <div class="card-detail">
                                <div class="rating">
                                    <ul class="list-unstyled side-listing">
                                        <li><i class="fas fa-star active"></i></li>
                                        <li><i class="fas fa-star active"></i></li>
                                        <li><i class="fas fa-star active"></i></li>
                                        <li><i class="fas fa-star"></i></li>
                                        <li><i class="fas fa-star"></i></li>
                                    </ul>
                                </div>
                                <p class="product-title"><a href="javascript:">Butterflies Hand-held Bag</a></p>
                                <p class="product-price">RS. 1,10,000</p>
                            </div>
                        </div>

                        <div class="product-card">
                            <div class="card-image effect-hover-zoom">
                                <a href="javascript:">
                                    <img data-src="img/products/product2.jpg" alt="Product-Image" class="lazyload animate-default">
                                </a>
                            </div>
                            <div class="card-detail">
                                <div class="rating">
                                    <ul class="list-unstyled side-listing">
                                        <li><i class="fas fa-star active"></i></li>
                                        <li><i class="fas fa-star active"></i></li>
                                        <li><i class="fas fa-star active"></i></li>
                                        <li><i class="fas fa-star"></i></li>
                                        <li><i class="fas fa-star"></i></li>
                                    </ul>
                                </div>
                                <p class="product-title"><a href="javascript:">Butterflies Hand-held Bag</a></p>
                                <p class="product-price">RS. 1,10,000</p>
                            </div>
                        </div>

                    </div>
                    <div class="item">
                        <div class="product-card">
                            <div class="card-image effect-hover-zoom">
                                <a href="javascript:">
                                    <img data-src="img/products/product3.jpg" alt="Product-Image" class="lazyload animate-default">
                                </a>
                            </div>
                            <div class="card-detail">
                                <div class="rating">
                                    <ul class="list-unstyled side-listing">
                                        <li><i class="fas fa-star active"></i></li>
                                        <li><i class="fas fa-star active"></i></li>
                                        <li><i class="fas fa-star active"></i></li>
                                        <li><i class="fas fa-star"></i></li>
                                        <li><i class="fas fa-star"></i></li>
                                    </ul>
                                </div>
                                <p class="product-title"><a href="javascript:">Butterflies Hand-held Bag</a></p>
                                <p class="product-price">RS. 1,10,000</p>
                            </div>
                        </div>

                        <div class="product-card">
                            <div class="card-image effect-hover-zoom">
                                <a href="javascript:">
                                    <img data-src="img/products/product7.jpg" alt="Product-Image" class="lazyload animate-default">
                                </a>
                            </div>
                            <div class="card-detail">
                                <div class="rating">
                                    <ul class="list-unstyled side-listing">
                                        <li><i class="fas fa-star active"></i></li>
                                        <li><i class="fas fa-star active"></i></li>
                                        <li><i class="fas fa-star active"></i></li>
                                        <li><i class="fas fa-star"></i></li>
                                        <li><i class="fas fa-star"></i></li>
                                    </ul>
                                </div>
                                <p class="product-title"><a href="javascript:">Butterflies Hand-held Bag</a></p>
                                <p class="product-price">RS. 1,10,000</p>
                            </div>
                        </div>

                    </div>
                </div>
            </div>

            <div class="tab-pane fade" id="kids" role="tabpanel" aria-labelledby="kids-tab">
            
                <div class="owl-carousel owl-theme owl-nav-top">

                    <div class="item">
                        <div class="product-card">
                            <div class="card-image effect-hover-zoom">
                                <a href="javascript:">
                                    <img data-src="img/products/product1.jpg" alt="Product-Image" class="lazyload animate-default">
                                </a>
                            </div>
                            <div class="card-detail">
                                <div class="rating">
                                    <ul class="list-unstyled side-listing">
                                        <li><i class="fas fa-star active"></i></li>
                                        <li><i class="fas fa-star active"></i></li>
                                        <li><i class="fas fa-star active"></i></li>
                                        <li><i class="fas fa-star"></i></li>
                                        <li><i class="fas fa-star"></i></li>
                                    </ul>
                                </div>
                                <p class="product-title"><a href="javascript:">Butterflies Hand-held Bag</a></p>
                                <p class="product-price">RS. 1,10,000</p>
                            </div>
                        </div>

                        <div class="product-card">
                            <div class="card-image effect-hover-zoom">
                                <a href="javascript:">
                                    <img data-src="img/products/product6.jpg" alt="Product-Image" class="lazyload animate-default">
                                </a>
                            </div>
                            <div class="card-detail">
                                <div class="rating">
                                    <ul class="list-unstyled side-listing">
                                        <li><i class="fas fa-star active"></i></li>
                                        <li><i class="fas fa-star active"></i></li>
                                        <li><i class="fas fa-star active"></i></li>
                                        <li><i class="fas fa-star"></i></li>
                                        <li><i class="fas fa-star"></i></li>
                                    </ul>
                                </div>
                                <p class="product-title"><a href="javascript:">Butterflies Hand-held Bag</a></p>
                                <p class="product-price">RS. 1,10,000</p>
                            </div>
                        </div>

                    </div>
                    <div class="item">
                        <div class="product-card">
                            <div class="card-image effect-hover-zoom">
                                <a href="javascript:">
                                    <img data-src="img/products/product2.jpg" alt="Product-Image" class="lazyload animate-default">
                                </a>
                            </div>
                            <div class="card-detail">
                                <div class="rating">
                                    <ul class="list-unstyled side-listing">
                                        <li><i class="fas fa-star active"></i></li>
                                        <li><i class="fas fa-star active"></i></li>
                                        <li><i class="fas fa-star active"></i></li>
                                        <li><i class="fas fa-star"></i></li>
                                        <li><i class="fas fa-star"></i></li>
                                    </ul>
                                </div>
                                <p class="product-title"><a href="javascript:">Butterflies Hand-held Bag</a></p>
                                <p class="product-price">RS. 1,10,000</p>
                            </div>
                        </div>

                        <div class="product-card">
                            <div class="card-image effect-hover-zoom">
                                <a href="javascript:">
                                    <img data-src="img/products/product8.jpg" alt="Product-Image" class="lazyload animate-default">
                                </a>
                            </div>
                            <div class="card-detail">
                                <div class="rating">
                                    <ul class="list-unstyled side-listing">
                                        <li><i class="fas fa-star active"></i></li>
                                        <li><i class="fas fa-star active"></i></li>
                                        <li><i class="fas fa-star active"></i></li>
                                        <li><i class="fas fa-star"></i></li>
                                        <li><i class="fas fa-star"></i></li>
                                    </ul>
                                </div>
                                <p class="product-title"><a href="javascript:">Butterflies Hand-held Bag</a></p>
                                <p class="product-price">RS. 1,10,000</p>
                            </div>
                        </div>

                    </div>
                    <div class="item">
                        <div class="product-card">
                            <div class="card-image effect-hover-zoom">
                                <a href="javascript:">
                                    <img data-src="img/products/product3.jpg" alt="Product-Image" class="lazyload animate-default">
                                </a>
                            </div>
                            <div class="card-detail">
                                <div class="rating">
                                    <ul class="list-unstyled side-listing">
                                        <li><i class="fas fa-star active"></i></li>
                                        <li><i class="fas fa-star active"></i></li>
                                        <li><i class="fas fa-star active"></i></li>
                                        <li><i class="fas fa-star"></i></li>
                                        <li><i class="fas fa-star"></i></li>
                                    </ul>
                                </div>
                                <p class="product-title"><a href="javascript:">Butterflies Hand-held Bag</a></p>
                                <p class="product-price">RS. 1,10,000</p>
                            </div>
                        </div>

                        <div class="product-card">
                            <div class="card-image effect-hover-zoom">
                                <a href="javascript:">
                                    <img data-src="img/products/product9.webp" alt="Product-Image" class="lazyload animate-default">
                                </a>
                            </div>
                            <div class="card-detail">
                                <div class="rating">
                                    <ul class="list-unstyled side-listing">
                                        <li><i class="fas fa-star active"></i></li>
                                        <li><i class="fas fa-star active"></i></li>
                                        <li><i class="fas fa-star active"></i></li>
                                        <li><i class="fas fa-star"></i></li>
                                        <li><i class="fas fa-star"></i></li>
                                    </ul>
                                </div>
                                <p class="product-title"><a href="javascript:">Butterflies Hand-held Bag</a></p>
                                <p class="product-price">RS. 1,10,000</p>
                            </div>
                        </div>

                    </div>
                    <div class="item">
                        <div class="product-card">
                            <div class="card-image effect-hover-zoom">
                                <a href="javascript:">
                                    <img data-src="img/products/product4.jpg" alt="Product-Image" class="lazyload animate-default">
                                </a>
                            </div>
                            <div class="card-detail">
                                <div class="rating">
                                    <ul class="list-unstyled side-listing">
                                        <li><i class="fas fa-star active"></i></li>
                                        <li><i class="fas fa-star active"></i></li>
                                        <li><i class="fas fa-star active"></i></li>
                                        <li><i class="fas fa-star"></i></li>
                                        <li><i class="fas fa-star"></i></li>
                                    </ul>
                                </div>
                                <p class="product-title"><a href="javascript:">Butterflies Hand-held Bag</a></p>
                                <p class="product-price">RS. 1,10,000</p>
                            </div>
                        </div>

                        <div class="product-card">
                            <div class="card-image effect-hover-zoom">
                                <a href="javascript:">
                                    <img data-src="img/products/product1.jpg" alt="Product-Image" class="lazyload animate-default">
                                </a>
                            </div>
                            <div class="card-detail">
                                <div class="rating">
                                    <ul class="list-unstyled side-listing">
                                        <li><i class="fas fa-star active"></i></li>
                                        <li><i class="fas fa-star active"></i></li>
                                        <li><i class="fas fa-star active"></i></li>
                                        <li><i class="fas fa-star"></i></li>
                                        <li><i class="fas fa-star"></i></li>
                                    </ul>
                                </div>
                                <p class="product-title"><a href="javascript:">Butterflies Hand-held Bag</a></p>
                                <p class="product-price">RS. 1,10,000</p>
                            </div>
                        </div>

                    </div>
                    <div class="item">
                        <div class="product-card">
                            <div class="card-image effect-hover-zoom">
                                <a href="javascript:">
                                    <img data-src="img/products/product5.jpg" alt="Product-Image" class="lazyload animate-default">
                                </a>
                            </div>
                            <div class="card-detail">
                                <div class="rating">
                                    <ul class="list-unstyled side-listing">
                                        <li><i class="fas fa-star active"></i></li>
                                        <li><i class="fas fa-star active"></i></li>
                                        <li><i class="fas fa-star active"></i></li>
                                        <li><i class="fas fa-star"></i></li>
                                        <li><i class="fas fa-star"></i></li>
                                    </ul>
                                </div>
                                <p class="product-title"><a href="javascript:">Butterflies Hand-held Bag</a></p>
                                <p class="product-price">RS. 1,10,000</p>
                            </div>
                        </div>

                        <div class="product-card">
                            <div class="card-image effect-hover-zoom">
                                <a href="javascript:">
                                    <img data-src="img/products/product2.jpg" alt="Product-Image" class="lazyload animate-default">
                                </a>
                            </div>
                            <div class="card-detail">
                                <div class="rating">
                                    <ul class="list-unstyled side-listing">
                                        <li><i class="fas fa-star active"></i></li>
                                        <li><i class="fas fa-star active"></i></li>
                                        <li><i class="fas fa-star active"></i></li>
                                        <li><i class="fas fa-star"></i></li>
                                        <li><i class="fas fa-star"></i></li>
                                    </ul>
                                </div>
                                <p class="product-title"><a href="javascript:">Butterflies Hand-held Bag</a></p>
                                <p class="product-price">RS. 1,10,000</p>
                            </div>
                        </div>

                    </div>
                    <div class="item">
                        <div class="product-card">
                            <div class="card-image effect-hover-zoom">
                                <a href="javascript:">
                                    <img data-src="img/products/product3.jpg" alt="Product-Image" class="lazyload animate-default">
                                </a>
                            </div>
                            <div class="card-detail">
                                <div class="rating">
                                    <ul class="list-unstyled side-listing">
                                        <li><i class="fas fa-star active"></i></li>
                                        <li><i class="fas fa-star active"></i></li>
                                        <li><i class="fas fa-star active"></i></li>
                                        <li><i class="fas fa-star"></i></li>
                                        <li><i class="fas fa-star"></i></li>
                                    </ul>
                                </div>
                                <p class="product-title"><a href="javascript:">Butterflies Hand-held Bag</a></p>
                                <p class="product-price">RS. 1,10,000</p>
                            </div>
                        </div>

                        <div class="product-card">
                            <div class="card-image effect-hover-zoom">
                                <a href="javascript:">
                                    <img data-src="img/products/product7.jpg" alt="Product-Image" class="lazyload animate-default">
                                </a>
                            </div>
                            <div class="card-detail">
                                <div class="rating">
                                    <ul class="list-unstyled side-listing">
                                        <li><i class="fas fa-star active"></i></li>
                                        <li><i class="fas fa-star active"></i></li>
                                        <li><i class="fas fa-star active"></i></li>
                                        <li><i class="fas fa-star"></i></li>
                                        <li><i class="fas fa-star"></i></li>
                                    </ul>
                                </div>
                                <p class="product-title"><a href="javascript:">Butterflies Hand-held Bag</a></p>
                                <p class="product-price">RS. 1,10,000</p>
                            </div>
                        </div>

                    </div>
                    <div class="item">
                        <div class="product-card">
                            <div class="card-image effect-hover-zoom">
                                <a href="javascript:">
                                    <img data-src="img/products/product4.jpg" alt="Product-Image" class="lazyload animate-default">
                                </a>
                            </div>
                            <div class="card-detail">
                                <div class="rating">
                                    <ul class="list-unstyled side-listing">
                                        <li><i class="fas fa-star active"></i></li>
                                        <li><i class="fas fa-star active"></i></li>
                                        <li><i class="fas fa-star active"></i></li>
                                        <li><i class="fas fa-star"></i></li>
                                        <li><i class="fas fa-star"></i></li>
                                    </ul>
                                </div>
                                <p class="product-title"><a href="javascript:">Butterflies Hand-held Bag</a></p>
                                <p class="product-price">RS. 1,10,000</p>
                            </div>
                        </div>

                        <div class="product-card">
                            <div class="card-image effect-hover-zoom">
                                <a href="javascript:">
                                    <img data-src="img/products/product1.jpg" alt="Product-Image" class="lazyload animate-default">
                                </a>
                            </div>
                            <div class="card-detail">
                                <div class="rating">
                                    <ul class="list-unstyled side-listing">
                                        <li><i class="fas fa-star active"></i></li>
                                        <li><i class="fas fa-star active"></i></li>
                                        <li><i class="fas fa-star active"></i></li>
                                        <li><i class="fas fa-star"></i></li>
                                        <li><i class="fas fa-star"></i></li>
                                    </ul>
                                </div>
                                <p class="product-title"><a href="javascript:">Butterflies Hand-held Bag</a></p>
                                <p class="product-price">RS. 1,10,000</p>
                            </div>
                        </div>

                    </div>
                    <div class="item">
                        <div class="product-card">
                            <div class="card-image effect-hover-zoom">
                                <a href="javascript:">
                                    <img data-src="img/products/product5.jpg" alt="Product-Image" class="lazyload animate-default">
                                </a>
                            </div>
                            <div class="card-detail">
                                <div class="rating">
                                    <ul class="list-unstyled side-listing">
                                        <li><i class="fas fa-star active"></i></li>
                                        <li><i class="fas fa-star active"></i></li>
                                        <li><i class="fas fa-star active"></i></li>
                                        <li><i class="fas fa-star"></i></li>
                                        <li><i class="fas fa-star"></i></li>
                                    </ul>
                                </div>
                                <p class="product-title"><a href="javascript:">Butterflies Hand-held Bag</a></p>
                                <p class="product-price">RS. 1,10,000</p>
                            </div>
                        </div>

                        <div class="product-card">
                            <div class="card-image effect-hover-zoom">
                                <a href="javascript:">
                                    <img data-src="img/products/product2.jpg" alt="Product-Image" class="lazyload animate-default">
                                </a>
                            </div>
                            <div class="card-detail">
                                <div class="rating">
                                    <ul class="list-unstyled side-listing">
                                        <li><i class="fas fa-star active"></i></li>
                                        <li><i class="fas fa-star active"></i></li>
                                        <li><i class="fas fa-star active"></i></li>
                                        <li><i class="fas fa-star"></i></li>
                                        <li><i class="fas fa-star"></i></li>
                                    </ul>
                                </div>
                                <p class="product-title"><a href="javascript:">Butterflies Hand-held Bag</a></p>
                                <p class="product-price">RS. 1,10,000</p>
                            </div>
                        </div> 
                    </div>
                    <div class="item">
                        <div class="product-card">
                            <div class="card-image effect-hover-zoom">
                                <a href="javascript:">
                                    <img data-src="img/products/product3.jpg" alt="Product-Image" class="lazyload animate-default">
                                </a>
                            </div>
                            <div class="card-detail">
                                <div class="rating">
                                    <ul class="list-unstyled side-listing">
                                        <li><i class="fas fa-star active"></i></li>
                                        <li><i class="fas fa-star active"></i></li>
                                        <li><i class="fas fa-star active"></i></li>
                                        <li><i class="fas fa-star"></i></li>
                                        <li><i class="fas fa-star"></i></li>
                                    </ul>
                                </div>
                                <p class="product-title"><a href="javascript:">Butterflies Hand-held Bag</a></p>
                                <p class="product-price">RS. 1,10,000</p>
                            </div>
                        </div>

                        <div class="product-card">
                            <div class="card-image effect-hover-zoom">
                                <a href="javascript:">
                                    <img data-src="img/products/product7.jpg" alt="Product-Image" class="lazyload animate-default">
                                </a>
                            </div>
                            <div class="card-detail">
                                <div class="rating">
                                    <ul class="list-unstyled side-listing">
                                        <li><i class="fas fa-star active"></i></li>
                                        <li><i class="fas fa-star active"></i></li>
                                        <li><i class="fas fa-star active"></i></li>
                                        <li><i class="fas fa-star"></i></li>
                                        <li><i class="fas fa-star"></i></li>
                                    </ul>
                                </div>
                                <p class="product-title"><a href="javascript:">Butterflies Hand-held Bag</a></p>
                                <p class="product-price">RS. 1,10,000</p>
                            </div>
                        </div>

                    </div>
                </div>
            </div>

            <div class="tab-pane fade" id="jewelery" role="tabpanel" aria-labelledby="jewelery-tab">
            
                <div class="owl-carousel owl-theme owl-nav-top">

                    <div class="item">
                        <div class="product-card">
                            <div class="card-image effect-hover-zoom">
                                <a href="javascript:">
                                    <img data-src="img/products/product1.jpg" alt="Product-Image" class="lazyload animate-default">
                                </a>
                            </div>
                            <div class="card-detail">
                                <div class="rating">
                                    <ul class="list-unstyled side-listing">
                                        <li><i class="fas fa-star active"></i></li>
                                        <li><i class="fas fa-star active"></i></li>
                                        <li><i class="fas fa-star active"></i></li>
                                        <li><i class="fas fa-star"></i></li>
                                        <li><i class="fas fa-star"></i></li>
                                    </ul>
                                </div>
                                <p class="product-title"><a href="javascript:">Butterflies Hand-held Bag</a></p>
                                <p class="product-price">RS. 1,10,000</p>
                            </div>
                        </div>

                        <div class="product-card">
                            <div class="card-image effect-hover-zoom">
                                <a href="javascript:">
                                    <img data-src="img/products/product6.jpg" alt="Product-Image" class="lazyload animate-default">
                                </a>
                            </div>
                            <div class="card-detail">
                                <div class="rating">
                                    <ul class="list-unstyled side-listing">
                                        <li><i class="fas fa-star active"></i></li>
                                        <li><i class="fas fa-star active"></i></li>
                                        <li><i class="fas fa-star active"></i></li>
                                        <li><i class="fas fa-star"></i></li>
                                        <li><i class="fas fa-star"></i></li>
                                    </ul>
                                </div>
                                <p class="product-title"><a href="javascript:">Butterflies Hand-held Bag</a></p>
                                <p class="product-price">RS. 1,10,000</p>
                            </div>
                        </div>

                    </div>
                    <div class="item">
                        <div class="product-card">
                            <div class="card-image effect-hover-zoom">
                                <a href="javascript:">
                                    <img data-src="img/products/product2.jpg" alt="Product-Image" class="lazyload animate-default">
                                </a>
                            </div>
                            <div class="card-detail">
                                <div class="rating">
                                    <ul class="list-unstyled side-listing">
                                        <li><i class="fas fa-star active"></i></li>
                                        <li><i class="fas fa-star active"></i></li>
                                        <li><i class="fas fa-star active"></i></li>
                                        <li><i class="fas fa-star"></i></li>
                                        <li><i class="fas fa-star"></i></li>
                                    </ul>
                                </div>
                                <p class="product-title"><a href="javascript:">Butterflies Hand-held Bag</a></p>
                                <p class="product-price">RS. 1,10,000</p>
                            </div>
                        </div>

                        <div class="product-card">
                            <div class="card-image effect-hover-zoom">
                                <a href="javascript:">
                                    <img data-src="img/products/product8.jpg" alt="Product-Image" class="lazyload animate-default">
                                </a>
                            </div>
                            <div class="card-detail">
                                <div class="rating">
                                    <ul class="list-unstyled side-listing">
                                        <li><i class="fas fa-star active"></i></li>
                                        <li><i class="fas fa-star active"></i></li>
                                        <li><i class="fas fa-star active"></i></li>
                                        <li><i class="fas fa-star"></i></li>
                                        <li><i class="fas fa-star"></i></li>
                                    </ul>
                                </div>
                                <p class="product-title"><a href="javascript:">Butterflies Hand-held Bag</a></p>
                                <p class="product-price">RS. 1,10,000</p>
                            </div>
                        </div>

                    </div>
                    <div class="item">
                        <div class="product-card">
                            <div class="card-image effect-hover-zoom">
                                <a href="javascript:">
                                    <img data-src="img/products/product3.jpg" alt="Product-Image" class="lazyload animate-default">
                                </a>
                            </div>
                            <div class="card-detail">
                                <div class="rating">
                                    <ul class="list-unstyled side-listing">
                                        <li><i class="fas fa-star active"></i></li>
                                        <li><i class="fas fa-star active"></i></li>
                                        <li><i class="fas fa-star active"></i></li>
                                        <li><i class="fas fa-star"></i></li>
                                        <li><i class="fas fa-star"></i></li>
                                    </ul>
                                </div>
                                <p class="product-title"><a href="javascript:">Butterflies Hand-held Bag</a></p>
                                <p class="product-price">RS. 1,10,000</p>
                            </div>
                        </div>

                        <div class="product-card">
                            <div class="card-image effect-hover-zoom">
                                <a href="javascript:">
                                    <img data-src="img/products/product9.webp" alt="Product-Image" class="lazyload animate-default">
                                </a>
                            </div>
                            <div class="card-detail">
                                <div class="rating">
                                    <ul class="list-unstyled side-listing">
                                        <li><i class="fas fa-star active"></i></li>
                                        <li><i class="fas fa-star active"></i></li>
                                        <li><i class="fas fa-star active"></i></li>
                                        <li><i class="fas fa-star"></i></li>
                                        <li><i class="fas fa-star"></i></li>
                                    </ul>
                                </div>
                                <p class="product-title"><a href="javascript:">Butterflies Hand-held Bag</a></p>
                                <p class="product-price">RS. 1,10,000</p>
                            </div>
                        </div>

                    </div>
                    <div class="item">
                        <div class="product-card">
                            <div class="card-image effect-hover-zoom">
                                <a href="javascript:">
                                    <img data-src="img/products/product4.jpg" alt="Product-Image" class="lazyload animate-default">
                                </a>
                            </div>
                            <div class="card-detail">
                                <div class="rating">
                                    <ul class="list-unstyled side-listing">
                                        <li><i class="fas fa-star active"></i></li>
                                        <li><i class="fas fa-star active"></i></li>
                                        <li><i class="fas fa-star active"></i></li>
                                        <li><i class="fas fa-star"></i></li>
                                        <li><i class="fas fa-star"></i></li>
                                    </ul>
                                </div>
                                <p class="product-title"><a href="javascript:">Butterflies Hand-held Bag</a></p>
                                <p class="product-price">RS. 1,10,000</p>
                            </div>
                        </div>

                        <div class="product-card">
                            <div class="card-image effect-hover-zoom">
                                <a href="javascript:">
                                    <img data-src="img/products/product1.jpg" alt="Product-Image" class="lazyload animate-default">
                                </a>
                            </div>
                            <div class="card-detail">
                                <div class="rating">
                                    <ul class="list-unstyled side-listing">
                                        <li><i class="fas fa-star active"></i></li>
                                        <li><i class="fas fa-star active"></i></li>
                                        <li><i class="fas fa-star active"></i></li>
                                        <li><i class="fas fa-star"></i></li>
                                        <li><i class="fas fa-star"></i></li>
                                    </ul>
                                </div>
                                <p class="product-title"><a href="javascript:">Butterflies Hand-held Bag</a></p>
                                <p class="product-price">RS. 1,10,000</p>
                            </div>
                        </div>

                    </div>
                    <div class="item">
                        <div class="product-card">
                            <div class="card-image effect-hover-zoom">
                                <a href="javascript:">
                                    <img data-src="img/products/product5.jpg" alt="Product-Image" class="lazyload animate-default">
                                </a>
                            </div>
                            <div class="card-detail">
                                <div class="rating">
                                    <ul class="list-unstyled side-listing">
                                        <li><i class="fas fa-star active"></i></li>
                                        <li><i class="fas fa-star active"></i></li>
                                        <li><i class="fas fa-star active"></i></li>
                                        <li><i class="fas fa-star"></i></li>
                                        <li><i class="fas fa-star"></i></li>
                                    </ul>
                                </div>
                                <p class="product-title"><a href="javascript:">Butterflies Hand-held Bag</a></p>
                                <p class="product-price">RS. 1,10,000</p>
                            </div>
                        </div>

                        <div class="product-card">
                            <div class="card-image effect-hover-zoom">
                                <a href="javascript:">
                                    <img data-src="img/products/product2.jpg" alt="Product-Image" class="lazyload animate-default">
                                </a>
                            </div>
                            <div class="card-detail">
                                <div class="rating">
                                    <ul class="list-unstyled side-listing">
                                        <li><i class="fas fa-star active"></i></li>
                                        <li><i class="fas fa-star active"></i></li>
                                        <li><i class="fas fa-star active"></i></li>
                                        <li><i class="fas fa-star"></i></li>
                                        <li><i class="fas fa-star"></i></li>
                                    </ul>
                                </div>
                                <p class="product-title"><a href="javascript:">Butterflies Hand-held Bag</a></p>
                                <p class="product-price">RS. 1,10,000</p>
                            </div>
                        </div>

                    </div>
                    <div class="item">
                        <div class="product-card">
                            <div class="card-image effect-hover-zoom">
                                <a href="javascript:">
                                    <img data-src="img/products/product3.jpg" alt="Product-Image" class="lazyload animate-default">
                                </a>
                            </div>
                            <div class="card-detail">
                                <div class="rating">
                                    <ul class="list-unstyled side-listing">
                                        <li><i class="fas fa-star active"></i></li>
                                        <li><i class="fas fa-star active"></i></li>
                                        <li><i class="fas fa-star active"></i></li>
                                        <li><i class="fas fa-star"></i></li>
                                        <li><i class="fas fa-star"></i></li>
                                    </ul>
                                </div>
                                <p class="product-title"><a href="javascript:">Butterflies Hand-held Bag</a></p>
                                <p class="product-price">RS. 1,10,000</p>
                            </div>
                        </div>

                        <div class="product-card">
                            <div class="card-image effect-hover-zoom">
                                <a href="javascript:">
                                    <img data-src="img/products/product7.jpg" alt="Product-Image" class="lazyload animate-default">
                                </a>
                            </div>
                            <div class="card-detail">
                                <div class="rating">
                                    <ul class="list-unstyled side-listing">
                                        <li><i class="fas fa-star active"></i></li>
                                        <li><i class="fas fa-star active"></i></li>
                                        <li><i class="fas fa-star active"></i></li>
                                        <li><i class="fas fa-star"></i></li>
                                        <li><i class="fas fa-star"></i></li>
                                    </ul>
                                </div>
                                <p class="product-title"><a href="javascript:">Butterflies Hand-held Bag</a></p>
                                <p class="product-price">RS. 1,10,000</p>
                            </div>
                        </div>

                    </div>
                    <div class="item">
                        <div class="product-card">
                            <div class="card-image effect-hover-zoom">
                                <a href="javascript:">
                                    <img data-src="img/products/product4.jpg" alt="Product-Image" class="lazyload animate-default">
                                </a>
                            </div>
                            <div class="card-detail">
                                <div class="rating">
                                    <ul class="list-unstyled side-listing">
                                        <li><i class="fas fa-star active"></i></li>
                                        <li><i class="fas fa-star active"></i></li>
                                        <li><i class="fas fa-star active"></i></li>
                                        <li><i class="fas fa-star"></i></li>
                                        <li><i class="fas fa-star"></i></li>
                                    </ul>
                                </div>
                                <p class="product-title"><a href="javascript:">Butterflies Hand-held Bag</a></p>
                                <p class="product-price">RS. 1,10,000</p>
                            </div>
                        </div>

                        <div class="product-card">
                            <div class="card-image effect-hover-zoom">
                                <a href="javascript:">
                                    <img data-src="img/products/product1.jpg" alt="Product-Image" class="lazyload animate-default">
                                </a>
                            </div>
                            <div class="card-detail">
                                <div class="rating">
                                    <ul class="list-unstyled side-listing">
                                        <li><i class="fas fa-star active"></i></li>
                                        <li><i class="fas fa-star active"></i></li>
                                        <li><i class="fas fa-star active"></i></li>
                                        <li><i class="fas fa-star"></i></li>
                                        <li><i class="fas fa-star"></i></li>
                                    </ul>
                                </div>
                                <p class="product-title"><a href="javascript:">Butterflies Hand-held Bag</a></p>
                                <p class="product-price">RS. 1,10,000</p>
                            </div>
                        </div>

                    </div>
                    <div class="item">
                        <div class="product-card">
                            <div class="card-image effect-hover-zoom">
                                <a href="javascript:">
                                    <img data-src="img/products/product5.jpg" alt="Product-Image" class="lazyload animate-default">
                                </a>
                            </div>
                            <div class="card-detail">
                                <div class="rating">
                                    <ul class="list-unstyled side-listing">
                                        <li><i class="fas fa-star active"></i></li>
                                        <li><i class="fas fa-star active"></i></li>
                                        <li><i class="fas fa-star active"></i></li>
                                        <li><i class="fas fa-star"></i></li>
                                        <li><i class="fas fa-star"></i></li>
                                    </ul>
                                </div>
                                <p class="product-title"><a href="javascript:">Butterflies Hand-held Bag</a></p>
                                <p class="product-price">RS. 1,10,000</p>
                            </div>
                        </div>

                        <div class="product-card">
                            <div class="card-image effect-hover-zoom">
                                <a href="javascript:">
                                    <img data-src="img/products/product2.jpg" alt="Product-Image" class="lazyload animate-default">
                                </a>
                            </div>
                            <div class="card-detail">
                                <div class="rating">
                                    <ul class="list-unstyled side-listing">
                                        <li><i class="fas fa-star active"></i></li>
                                        <li><i class="fas fa-star active"></i></li>
                                        <li><i class="fas fa-star active"></i></li>
                                        <li><i class="fas fa-star"></i></li>
                                        <li><i class="fas fa-star"></i></li>
                                    </ul>
                                </div>
                                <p class="product-title"><a href="javascript:">Butterflies Hand-held Bag</a></p>
                                <p class="product-price">RS. 1,10,000</p>
                            </div>
                        </div>

                    </div>
                    <div class="item">
                        <div class="product-card">
                            <div class="card-image effect-hover-zoom">
                                <a href="javascript:">
                                    <img data-src="img/products/product3.jpg" alt="Product-Image" class="lazyload animate-default">
                                </a>
                            </div>
                            <div class="card-detail">
                                <div class="rating">
                                    <ul class="list-unstyled side-listing">
                                        <li><i class="fas fa-star active"></i></li>
                                        <li><i class="fas fa-star active"></i></li>
                                        <li><i class="fas fa-star active"></i></li>
                                        <li><i class="fas fa-star"></i></li>
                                        <li><i class="fas fa-star"></i></li>
                                    </ul>
                                </div>
                                <p class="product-title"><a href="javascript:">Butterflies Hand-held Bag</a></p>
                                <p class="product-price">RS. 1,10,000</p>
                            </div>
                        </div>

                        <div class="product-card">
                            <div class="card-image effect-hover-zoom">
                                <a href="javascript:">
                                    <img data-src="img/products/product7.jpg" alt="Product-Image" class="lazyload animate-default">
                                </a>
                            </div>
                            <div class="card-detail">
                                <div class="rating">
                                    <ul class="list-unstyled side-listing">
                                        <li><i class="fas fa-star active"></i></li>
                                        <li><i class="fas fa-star active"></i></li>
                                        <li><i class="fas fa-star active"></i></li>
                                        <li><i class="fas fa-star"></i></li>
                                        <li><i class="fas fa-star"></i></li>
                                    </ul>
                                </div>
                                <p class="product-title"><a href="javascript:">Butterflies Hand-held Bag</a></p>
                                <p class="product-price">RS. 1,10,000</p>
                            </div>
                        </div>

                    </div>
                </div>
            </div> 
        </div>
        </div>

    </div>
</section>

<!-- Fashion-category -->


<!-- Electrical-category -->

<section class="top-category pad-t-20 bg-grey">
    <div class="container-fluid">
        <div class="white-card">
            <div class="section-title-wrapper title-tab clearfix">
                <!-- <h3 class="float-left">Top Category</h3> -->
                <ul class="nav nav-tabs cat-nav" id="myTab" role="tablist">
                    <li class="nav-item">
                        <a class="nav-link active" id="computer-tab" data-toggle="tab" href="#computer" role="tab" aria-controls="computer" aria-selected="true">Computer Accessories</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" id="mobile-tab" data-toggle="tab" href="#mobile" role="tab" aria-controls="mobile" aria-selected="false">Mobile & Accessories</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" id="electrical-tab" data-toggle="tab" href="#electrical" role="tab" aria-controls="electrical" aria-selected="false">Electrical</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" id="home-app-tab" data-toggle="tab" href="#home-app" role="tab" aria-controls="home-app" aria-selected="false">Home Appliance</a>
                    </li>
                    <li class="view-more-btn">
                        <a href="javascript:" class="float-right">View More</a>
                    </li>
                </ul>
            </div>

            <div class="tab-content" id="myTabContent"> 
                <div class="tab-pane fade show active" id="computer" role="tabpanel" aria-labelledby="computer-tab">
                    <div class="owl-carousel owl-theme owl-nav-top">
                        <div class="item">
                            <div class="product-card">
                                <div class="card-image effect-hover-zoom">
                                    <a href="javascript:">
                                        <img data-src="img/products/product1.jpg" alt="Product-Image" class="lazyload animate-default">
                                    </a>
                                </div>
                                <div class="card-detail">
                                    <div class="rating">
                                        <ul class="list-unstyled side-listing">
                                            <li><i class="fas fa-star active"></i></li>
                                            <li><i class="fas fa-star active"></i></li>
                                            <li><i class="fas fa-star active"></i></li>
                                            <li><i class="fas fa-star"></i></li>
                                            <li><i class="fas fa-star"></i></li>
                                        </ul>
                                    </div>
                                    <p class="product-title"><a href="javascript:">Butterflies Hand-held Bag</a></p>
                                    <p class="product-price">RS. 1,10,000</p>
                                </div>
                            </div>
                            <div class="product-card">
                                <div class="card-image effect-hover-zoom">
                                    <a href="javascript:">
                                        <img data-src="img/products/product6.jpg" alt="Product-Image" class="lazyload animate-default">
                                    </a>
                                </div>
                                <div class="card-detail">
                                    <div class="rating">
                                        <ul class="list-unstyled side-listing">
                                            <li><i class="fas fa-star active"></i></li>
                                            <li><i class="fas fa-star active"></i></li>
                                            <li><i class="fas fa-star active"></i></li>
                                            <li><i class="fas fa-star"></i></li>
                                            <li><i class="fas fa-star"></i></li>
                                        </ul>
                                    </div>
                                    <p class="product-title"><a href="javascript:">Butterflies Hand-held Bag</a></p>
                                    <p class="product-price">RS. 1,10,000</p>
                                </div>
                            </div>
                        </div>
                        <div class="item">
                            <div class="product-card">
                                <div class="card-image effect-hover-zoom">
                                    <a href="javascript:">
                                        <img data-src="img/products/product2.jpg" alt="Product-Image" class="lazyload animate-default">
                                    </a>
                                </div>
                                <div class="card-detail">
                                    <div class="rating">
                                        <ul class="list-unstyled side-listing">
                                            <li><i class="fas fa-star active"></i></li>
                                            <li><i class="fas fa-star active"></i></li>
                                            <li><i class="fas fa-star active"></i></li>
                                            <li><i class="fas fa-star"></i></li>
                                            <li><i class="fas fa-star"></i></li>
                                        </ul>
                                    </div>
                                    <p class="product-title"><a href="javascript:">Butterflies Hand-held Bag</a></p>
                                    <p class="product-price">RS. 1,10,000</p>
                                </div>
                            </div>
                            <div class="product-card">
                                <div class="card-image effect-hover-zoom">
                                    <a href="javascript:">
                                        <img data-src="img/products/product8.jpg" alt="Product-Image" class="lazyload animate-default">
                                    </a>
                                </div>
                                <div class="card-detail">
                                    <div class="rating">
                                        <ul class="list-unstyled side-listing">
                                            <li><i class="fas fa-star active"></i></li>
                                            <li><i class="fas fa-star active"></i></li>
                                            <li><i class="fas fa-star active"></i></li>
                                            <li><i class="fas fa-star"></i></li>
                                            <li><i class="fas fa-star"></i></li>
                                        </ul>
                                    </div>
                                    <p class="product-title"><a href="javascript:">Butterflies Hand-held Bag</a></p>
                                    <p class="product-price">RS. 1,10,000</p>
                                </div>
                            </div>
                        </div>
                        <div class="item">
                            <div class="product-card">
                                <div class="card-image effect-hover-zoom">
                                    <a href="javascript:">
                                        <img data-src="img/products/product3.jpg" alt="Product-Image" class="lazyload animate-default">
                                    </a>
                                </div>
                                <div class="card-detail">
                                    <div class="rating">
                                        <ul class="list-unstyled side-listing">
                                            <li><i class="fas fa-star active"></i></li>
                                            <li><i class="fas fa-star active"></i></li>
                                            <li><i class="fas fa-star active"></i></li>
                                            <li><i class="fas fa-star"></i></li>
                                            <li><i class="fas fa-star"></i></li>
                                        </ul>
                                    </div>
                                    <p class="product-title"><a href="javascript:">Butterflies Hand-held Bag</a></p>
                                    <p class="product-price">RS. 1,10,000</p>
                                </div>
                            </div>
                            <div class="product-card">
                                <div class="card-image effect-hover-zoom">
                                    <a href="javascript:">
                                        <img data-src="img/products/product9.webp" alt="Product-Image" class="lazyload animate-default">
                                    </a>
                                </div>
                                <div class="card-detail">
                                    <div class="rating">
                                        <ul class="list-unstyled side-listing">
                                            <li><i class="fas fa-star active"></i></li>
                                            <li><i class="fas fa-star active"></i></li>
                                            <li><i class="fas fa-star active"></i></li>
                                            <li><i class="fas fa-star"></i></li>
                                            <li><i class="fas fa-star"></i></li>
                                        </ul>
                                    </div>
                                    <p class="product-title"><a href="javascript:">Butterflies Hand-held Bag</a></p>
                                    <p class="product-price">RS. 1,10,000</p>
                                </div>
                            </div>
                        </div>
                        <div class="item">
                            <div class="product-card">
                                <div class="card-image effect-hover-zoom">
                                    <a href="javascript:">
                                        <img data-src="img/products/product4.jpg" alt="Product-Image" class="lazyload animate-default">
                                    </a>
                                </div>
                                <div class="card-detail">
                                    <div class="rating">
                                        <ul class="list-unstyled side-listing">
                                            <li><i class="fas fa-star active"></i></li>
                                            <li><i class="fas fa-star active"></i></li>
                                            <li><i class="fas fa-star active"></i></li>
                                            <li><i class="fas fa-star"></i></li>
                                            <li><i class="fas fa-star"></i></li>
                                        </ul>
                                    </div>
                                    <p class="product-title"><a href="javascript:">Butterflies Hand-held Bag</a></p>
                                    <p class="product-price">RS. 1,10,000</p>
                                </div>
                            </div>
                            <div class="product-card">
                                <div class="card-image effect-hover-zoom">
                                    <a href="javascript:">
                                        <img data-src="img/products/product1.jpg" alt="Product-Image" class="lazyload animate-default">
                                    </a>
                                </div>
                                <div class="card-detail">
                                    <div class="rating">
                                        <ul class="list-unstyled side-listing">
                                            <li><i class="fas fa-star active"></i></li>
                                            <li><i class="fas fa-star active"></i></li>
                                            <li><i class="fas fa-star active"></i></li>
                                            <li><i class="fas fa-star"></i></li>
                                            <li><i class="fas fa-star"></i></li>
                                        </ul>
                                    </div>
                                    <p class="product-title"><a href="javascript:">Butterflies Hand-held Bag</a></p>
                                    <p class="product-price">RS. 1,10,000</p>
                                </div>
                            </div>
                        </div>
                        <div class="item">
                            <div class="product-card">
                                <div class="card-image effect-hover-zoom">
                                    <a href="javascript:">
                                        <img data-src="img/products/product5.jpg" alt="Product-Image" class="lazyload animate-default">
                                    </a>
                                </div>
                                <div class="card-detail">
                                    <div class="rating">
                                        <ul class="list-unstyled side-listing">
                                            <li><i class="fas fa-star active"></i></li>
                                            <li><i class="fas fa-star active"></i></li>
                                            <li><i class="fas fa-star active"></i></li>
                                            <li><i class="fas fa-star"></i></li>
                                            <li><i class="fas fa-star"></i></li>
                                        </ul>
                                    </div>
                                    <p class="product-title"><a href="javascript:">Butterflies Hand-held Bag</a></p>
                                    <p class="product-price">RS. 1,10,000</p>
                                </div>
                            </div>
                            <div class="product-card">
                                <div class="card-image effect-hover-zoom">
                                    <a href="javascript:">
                                        <img data-src="img/products/product2.jpg" alt="Product-Image" class="lazyload animate-default">
                                    </a>
                                </div>
                                <div class="card-detail">
                                    <div class="rating">
                                        <ul class="list-unstyled side-listing">
                                            <li><i class="fas fa-star active"></i></li>
                                            <li><i class="fas fa-star active"></i></li>
                                            <li><i class="fas fa-star active"></i></li>
                                            <li><i class="fas fa-star"></i></li>
                                            <li><i class="fas fa-star"></i></li>
                                        </ul>
                                    </div>
                                    <p class="product-title"><a href="javascript:">Butterflies Hand-held Bag</a></p>
                                    <p class="product-price">RS. 1,10,000</p>
                                </div>
                            </div>
                        </div>
                        <div class="item">
                            <div class="product-card">
                                <div class="card-image effect-hover-zoom">
                                    <a href="javascript:">
                                        <img data-src="img/products/product3.jpg" alt="Product-Image" class="lazyload animate-default">
                                    </a>
                                </div>
                                <div class="card-detail">
                                    <div class="rating">
                                        <ul class="list-unstyled side-listing">
                                            <li><i class="fas fa-star active"></i></li>
                                            <li><i class="fas fa-star active"></i></li>
                                            <li><i class="fas fa-star active"></i></li>
                                            <li><i class="fas fa-star"></i></li>
                                            <li><i class="fas fa-star"></i></li>
                                        </ul>
                                    </div>
                                    <p class="product-title"><a href="javascript:">Butterflies Hand-held Bag</a></p>
                                    <p class="product-price">RS. 1,10,000</p>
                                </div>
                            </div>
                            <div class="product-card">
                                <div class="card-image effect-hover-zoom">
                                    <a href="javascript:">
                                        <img data-src="img/products/product7.jpg" alt="Product-Image" class="lazyload animate-default">
                                    </a>
                                </div>
                                <div class="card-detail">
                                    <div class="rating">
                                        <ul class="list-unstyled side-listing">
                                            <li><i class="fas fa-star active"></i></li>
                                            <li><i class="fas fa-star active"></i></li>
                                            <li><i class="fas fa-star active"></i></li>
                                            <li><i class="fas fa-star"></i></li>
                                            <li><i class="fas fa-star"></i></li>
                                        </ul>
                                    </div>
                                    <p class="product-title"><a href="javascript:">Butterflies Hand-held Bag</a></p>
                                    <p class="product-price">RS. 1,10,000</p>
                                </div>
                            </div>
                        </div>
                        <div class="item">
                            <div class="product-card">
                                <div class="card-image effect-hover-zoom">
                                    <a href="javascript:">
                                        <img data-src="img/products/product4.jpg" alt="Product-Image" class="lazyload animate-default">
                                    </a>
                                </div>
                                <div class="card-detail">
                                    <div class="rating">
                                        <ul class="list-unstyled side-listing">
                                            <li><i class="fas fa-star active"></i></li>
                                            <li><i class="fas fa-star active"></i></li>
                                            <li><i class="fas fa-star active"></i></li>
                                            <li><i class="fas fa-star"></i></li>
                                            <li><i class="fas fa-star"></i></li>
                                        </ul>
                                    </div>
                                    <p class="product-title"><a href="javascript:">Butterflies Hand-held Bag</a></p>
                                    <p class="product-price">RS. 1,10,000</p>
                                </div>
                            </div>
                            <div class="product-card">
                                <div class="card-image effect-hover-zoom">
                                    <a href="javascript:">
                                        <img data-src="img/products/product1.jpg" alt="Product-Image" class="lazyload animate-default">
                                    </a>
                                </div>
                                <div class="card-detail">
                                    <div class="rating">
                                        <ul class="list-unstyled side-listing">
                                            <li><i class="fas fa-star active"></i></li>
                                            <li><i class="fas fa-star active"></i></li>
                                            <li><i class="fas fa-star active"></i></li>
                                            <li><i class="fas fa-star"></i></li>
                                            <li><i class="fas fa-star"></i></li>
                                        </ul>
                                    </div>
                                    <p class="product-title"><a href="javascript:">Butterflies Hand-held Bag</a></p>
                                    <p class="product-price">RS. 1,10,000</p>
                                </div>
                            </div>
                        </div>
                        <div class="item">
                            <div class="product-card">
                                <div class="card-image effect-hover-zoom">
                                    <a href="javascript:">
                                        <img data-src="img/products/product5.jpg" alt="Product-Image" class="lazyload animate-default">
                                    </a>
                                </div>
                                <div class="card-detail">
                                    <div class="rating">
                                        <ul class="list-unstyled side-listing">
                                            <li><i class="fas fa-star active"></i></li>
                                            <li><i class="fas fa-star active"></i></li>
                                            <li><i class="fas fa-star active"></i></li>
                                            <li><i class="fas fa-star"></i></li>
                                            <li><i class="fas fa-star"></i></li>
                                        </ul>
                                    </div>
                                    <p class="product-title"><a href="javascript:">Butterflies Hand-held Bag</a></p>
                                    <p class="product-price">RS. 1,10,000</p>
                                </div>
                            </div>
                            <div class="product-card">
                                <div class="card-image effect-hover-zoom">
                                    <a href="javascript:">
                                        <img data-src="img/products/product2.jpg" alt="Product-Image" class="lazyload animate-default">
                                    </a>
                                </div>
                                <div class="card-detail">
                                    <div class="rating">
                                        <ul class="list-unstyled side-listing">
                                            <li><i class="fas fa-star active"></i></li>
                                            <li><i class="fas fa-star active"></i></li>
                                            <li><i class="fas fa-star active"></i></li>
                                            <li><i class="fas fa-star"></i></li>
                                            <li><i class="fas fa-star"></i></li>
                                        </ul>
                                    </div>
                                    <p class="product-title"><a href="javascript:">Butterflies Hand-held Bag</a></p>
                                    <p class="product-price">RS. 1,10,000</p>
                                </div>
                            </div>
                        </div>
                        <div class="item">
                            <div class="product-card">
                                <div class="card-image effect-hover-zoom">
                                    <a href="javascript:">
                                        <img data-src="img/products/product3.jpg" alt="Product-Image" class="lazyload animate-default">
                                    </a>
                                </div>
                                <div class="card-detail">
                                    <div class="rating">
                                        <ul class="list-unstyled side-listing">
                                            <li><i class="fas fa-star active"></i></li>
                                            <li><i class="fas fa-star active"></i></li>
                                            <li><i class="fas fa-star active"></i></li>
                                            <li><i class="fas fa-star"></i></li>
                                            <li><i class="fas fa-star"></i></li>
                                        </ul>
                                    </div>
                                    <p class="product-title"><a href="javascript:">Butterflies Hand-held Bag</a></p>
                                    <p class="product-price">RS. 1,10,000</p>
                                </div>
                            </div>
                            <div class="product-card">
                                <div class="card-image effect-hover-zoom">
                                    <a href="javascript:">
                                        <img data-src="img/products/product7.jpg" alt="Product-Image" class="lazyload animate-default">
                                    </a>
                                </div>
                                <div class="card-detail">
                                    <div class="rating">
                                        <ul class="list-unstyled side-listing">
                                            <li><i class="fas fa-star active"></i></li>
                                            <li><i class="fas fa-star active"></i></li>
                                            <li><i class="fas fa-star active"></i></li>
                                            <li><i class="fas fa-star"></i></li>
                                            <li><i class="fas fa-star"></i></li>
                                        </ul>
                                    </div>
                                    <p class="product-title"><a href="javascript:">Butterflies Hand-held Bag</a></p>
                                    <p class="product-price">RS. 1,10,000</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="tab-pane fade" id="mobile" role="tabpanel" aria-labelledby="mobile-tab">
                    <div class="owl-carousel owl-theme owl-nav-top">
                        <div class="item">
                            <div class="product-card">
                                <div class="card-image effect-hover-zoom">
                                    <a href="javascript:">
                                        <img data-src="img/products/product1.jpg" alt="Product-Image" class="lazyload animate-default">
                                    </a>
                                </div>
                                <div class="card-detail">
                                    <div class="rating">
                                        <ul class="list-unstyled side-listing">
                                            <li><i class="fas fa-star active"></i></li>
                                            <li><i class="fas fa-star active"></i></li>
                                            <li><i class="fas fa-star active"></i></li>
                                            <li><i class="fas fa-star"></i></li>
                                            <li><i class="fas fa-star"></i></li>
                                        </ul>
                                    </div>
                                    <p class="product-title"><a href="javascript:">Butterflies Hand-held Bag</a></p>
                                    <p class="product-price">RS. 1,10,000</p>
                                </div>
                            </div>
                            <div class="product-card">
                                <div class="card-image effect-hover-zoom">
                                    <a href="javascript:">
                                        <img data-src="img/products/product6.jpg" alt="Product-Image" class="lazyload animate-default">
                                    </a>
                                </div>
                                <div class="card-detail">
                                    <div class="rating">
                                        <ul class="list-unstyled side-listing">
                                            <li><i class="fas fa-star active"></i></li>
                                            <li><i class="fas fa-star active"></i></li>
                                            <li><i class="fas fa-star active"></i></li>
                                            <li><i class="fas fa-star"></i></li>
                                            <li><i class="fas fa-star"></i></li>
                                        </ul>
                                    </div>
                                    <p class="product-title"><a href="javascript:">Butterflies Hand-held Bag</a></p>
                                    <p class="product-price">RS. 1,10,000</p>
                                </div>
                            </div>
                        </div>
                        <div class="item">
                            <div class="product-card">
                                <div class="card-image effect-hover-zoom">
                                    <a href="javascript:">
                                        <img data-src="img/products/product2.jpg" alt="Product-Image" class="lazyload animate-default">
                                    </a>
                                </div>
                                <div class="card-detail">
                                    <div class="rating">
                                        <ul class="list-unstyled side-listing">
                                            <li><i class="fas fa-star active"></i></li>
                                            <li><i class="fas fa-star active"></i></li>
                                            <li><i class="fas fa-star active"></i></li>
                                            <li><i class="fas fa-star"></i></li>
                                            <li><i class="fas fa-star"></i></li>
                                        </ul>
                                    </div>
                                    <p class="product-title"><a href="javascript:">Butterflies Hand-held Bag</a></p>
                                    <p class="product-price">RS. 1,10,000</p>
                                </div>
                            </div>
                            <div class="product-card">
                                <div class="card-image effect-hover-zoom">
                                    <a href="javascript:">
                                        <img data-src="img/products/product8.jpg" alt="Product-Image" class="lazyload animate-default">
                                    </a>
                                </div>
                                <div class="card-detail">
                                    <div class="rating">
                                        <ul class="list-unstyled side-listing">
                                            <li><i class="fas fa-star active"></i></li>
                                            <li><i class="fas fa-star active"></i></li>
                                            <li><i class="fas fa-star active"></i></li>
                                            <li><i class="fas fa-star"></i></li>
                                            <li><i class="fas fa-star"></i></li>
                                        </ul>
                                    </div>
                                    <p class="product-title"><a href="javascript:">Butterflies Hand-held Bag</a></p>
                                    <p class="product-price">RS. 1,10,000</p>
                                </div>
                            </div>
                        </div>
                        <div class="item">
                            <div class="product-card">
                                <div class="card-image effect-hover-zoom">
                                    <a href="javascript:">
                                        <img data-src="img/products/product3.jpg" alt="Product-Image" class="lazyload animate-default">
                                    </a>
                                </div>
                                <div class="card-detail">
                                    <div class="rating">
                                        <ul class="list-unstyled side-listing">
                                            <li><i class="fas fa-star active"></i></li>
                                            <li><i class="fas fa-star active"></i></li>
                                            <li><i class="fas fa-star active"></i></li>
                                            <li><i class="fas fa-star"></i></li>
                                            <li><i class="fas fa-star"></i></li>
                                        </ul>
                                    </div>
                                    <p class="product-title"><a href="javascript:">Butterflies Hand-held Bag</a></p>
                                    <p class="product-price">RS. 1,10,000</p>
                                </div>
                            </div>
                            <div class="product-card">
                                <div class="card-image effect-hover-zoom">
                                    <a href="javascript:">
                                        <img data-src="img/products/product9.webp" alt="Product-Image" class="lazyload animate-default">
                                    </a>
                                </div>
                                <div class="card-detail">
                                    <div class="rating">
                                        <ul class="list-unstyled side-listing">
                                            <li><i class="fas fa-star active"></i></li>
                                            <li><i class="fas fa-star active"></i></li>
                                            <li><i class="fas fa-star active"></i></li>
                                            <li><i class="fas fa-star"></i></li>
                                            <li><i class="fas fa-star"></i></li>
                                        </ul>
                                    </div>
                                    <p class="product-title"><a href="javascript:">Butterflies Hand-held Bag</a></p>
                                    <p class="product-price">RS. 1,10,000</p>
                                </div>
                            </div>
                        </div>
                        <div class="item">
                            <div class="product-card">
                                <div class="card-image effect-hover-zoom">
                                    <a href="javascript:">
                                        <img data-src="img/products/product4.jpg" alt="Product-Image" class="lazyload animate-default">
                                    </a>
                                </div>
                                <div class="card-detail">
                                    <div class="rating">
                                        <ul class="list-unstyled side-listing">
                                            <li><i class="fas fa-star active"></i></li>
                                            <li><i class="fas fa-star active"></i></li>
                                            <li><i class="fas fa-star active"></i></li>
                                            <li><i class="fas fa-star"></i></li>
                                            <li><i class="fas fa-star"></i></li>
                                        </ul>
                                    </div>
                                    <p class="product-title"><a href="javascript:">Butterflies Hand-held Bag</a></p>
                                    <p class="product-price">RS. 1,10,000</p>
                                </div>
                            </div>
                            <div class="product-card">
                                <div class="card-image effect-hover-zoom">
                                    <a href="javascript:">
                                        <img data-src="img/products/product1.jpg" alt="Product-Image" class="lazyload animate-default">
                                    </a>
                                </div>
                                <div class="card-detail">
                                    <div class="rating">
                                        <ul class="list-unstyled side-listing">
                                            <li><i class="fas fa-star active"></i></li>
                                            <li><i class="fas fa-star active"></i></li>
                                            <li><i class="fas fa-star active"></i></li>
                                            <li><i class="fas fa-star"></i></li>
                                            <li><i class="fas fa-star"></i></li>
                                        </ul>
                                    </div>
                                    <p class="product-title"><a href="javascript:">Butterflies Hand-held Bag</a></p>
                                    <p class="product-price">RS. 1,10,000</p>
                                </div>
                            </div>
                        </div>
                        <div class="item">
                            <div class="product-card">
                                <div class="card-image effect-hover-zoom">
                                    <a href="javascript:">
                                        <img data-src="img/products/product5.jpg" alt="Product-Image" class="lazyload animate-default">
                                    </a>
                                </div>
                                <div class="card-detail">
                                    <div class="rating">
                                        <ul class="list-unstyled side-listing">
                                            <li><i class="fas fa-star active"></i></li>
                                            <li><i class="fas fa-star active"></i></li>
                                            <li><i class="fas fa-star active"></i></li>
                                            <li><i class="fas fa-star"></i></li>
                                            <li><i class="fas fa-star"></i></li>
                                        </ul>
                                    </div>
                                    <p class="product-title"><a href="javascript:">Butterflies Hand-held Bag</a></p>
                                    <p class="product-price">RS. 1,10,000</p>
                                </div>
                            </div>
                            <div class="product-card">
                                <div class="card-image effect-hover-zoom">
                                    <a href="javascript:">
                                        <img data-src="img/products/product2.jpg" alt="Product-Image" class="lazyload animate-default">
                                    </a>
                                </div>
                                <div class="card-detail">
                                    <div class="rating">
                                        <ul class="list-unstyled side-listing">
                                            <li><i class="fas fa-star active"></i></li>
                                            <li><i class="fas fa-star active"></i></li>
                                            <li><i class="fas fa-star active"></i></li>
                                            <li><i class="fas fa-star"></i></li>
                                            <li><i class="fas fa-star"></i></li>
                                        </ul>
                                    </div>
                                    <p class="product-title"><a href="javascript:">Butterflies Hand-held Bag</a></p>
                                    <p class="product-price">RS. 1,10,000</p>
                                </div>
                            </div>
                        </div>
                        <div class="item">
                            <div class="product-card">
                                <div class="card-image effect-hover-zoom">
                                    <a href="javascript:">
                                        <img data-src="img/products/product3.jpg" alt="Product-Image" class="lazyload animate-default">
                                    </a>
                                </div>
                                <div class="card-detail">
                                    <div class="rating">
                                        <ul class="list-unstyled side-listing">
                                            <li><i class="fas fa-star active"></i></li>
                                            <li><i class="fas fa-star active"></i></li>
                                            <li><i class="fas fa-star active"></i></li>
                                            <li><i class="fas fa-star"></i></li>
                                            <li><i class="fas fa-star"></i></li>
                                        </ul>
                                    </div>
                                    <p class="product-title"><a href="javascript:">Butterflies Hand-held Bag</a></p>
                                    <p class="product-price">RS. 1,10,000</p>
                                </div>
                            </div>
                            <div class="product-card">
                                <div class="card-image effect-hover-zoom">
                                    <a href="javascript:">
                                        <img data-src="img/products/product7.jpg" alt="Product-Image" class="lazyload animate-default">
                                    </a>
                                </div>
                                <div class="card-detail">
                                    <div class="rating">
                                        <ul class="list-unstyled side-listing">
                                            <li><i class="fas fa-star active"></i></li>
                                            <li><i class="fas fa-star active"></i></li>
                                            <li><i class="fas fa-star active"></i></li>
                                            <li><i class="fas fa-star"></i></li>
                                            <li><i class="fas fa-star"></i></li>
                                        </ul>
                                    </div>
                                    <p class="product-title"><a href="javascript:">Butterflies Hand-held Bag</a></p>
                                    <p class="product-price">RS. 1,10,000</p>
                                </div>
                            </div>
                        </div>
                        <div class="item">
                            <div class="product-card">
                                <div class="card-image effect-hover-zoom">
                                    <a href="javascript:">
                                        <img data-src="img/products/product4.jpg" alt="Product-Image" class="lazyload animate-default">
                                    </a>
                                </div>
                                <div class="card-detail">
                                    <div class="rating">
                                        <ul class="list-unstyled side-listing">
                                            <li><i class="fas fa-star active"></i></li>
                                            <li><i class="fas fa-star active"></i></li>
                                            <li><i class="fas fa-star active"></i></li>
                                            <li><i class="fas fa-star"></i></li>
                                            <li><i class="fas fa-star"></i></li>
                                        </ul>
                                    </div>
                                    <p class="product-title"><a href="javascript:">Butterflies Hand-held Bag</a></p>
                                    <p class="product-price">RS. 1,10,000</p>
                                </div>
                            </div>
                            <div class="product-card">
                                <div class="card-image effect-hover-zoom">
                                    <a href="javascript:">
                                        <img data-src="img/products/product1.jpg" alt="Product-Image" class="lazyload animate-default">
                                    </a>
                                </div>
                                <div class="card-detail">
                                    <div class="rating">
                                        <ul class="list-unstyled side-listing">
                                            <li><i class="fas fa-star active"></i></li>
                                            <li><i class="fas fa-star active"></i></li>
                                            <li><i class="fas fa-star active"></i></li>
                                            <li><i class="fas fa-star"></i></li>
                                            <li><i class="fas fa-star"></i></li>
                                        </ul>
                                    </div>
                                    <p class="product-title"><a href="javascript:">Butterflies Hand-held Bag</a></p>
                                    <p class="product-price">RS. 1,10,000</p>
                                </div>
                            </div>
                        </div>
                        <div class="item">
                            <div class="product-card">
                                <div class="card-image effect-hover-zoom">
                                    <a href="javascript:">
                                        <img data-src="img/products/product5.jpg" alt="Product-Image" class="lazyload animate-default">
                                    </a>
                                </div>
                                <div class="card-detail">
                                    <div class="rating">
                                        <ul class="list-unstyled side-listing">
                                            <li><i class="fas fa-star active"></i></li>
                                            <li><i class="fas fa-star active"></i></li>
                                            <li><i class="fas fa-star active"></i></li>
                                            <li><i class="fas fa-star"></i></li>
                                            <li><i class="fas fa-star"></i></li>
                                        </ul>
                                    </div>
                                    <p class="product-title"><a href="javascript:">Butterflies Hand-held Bag</a></p>
                                    <p class="product-price">RS. 1,10,000</p>
                                </div>
                            </div>
                            <div class="product-card">
                                <div class="card-image effect-hover-zoom">
                                    <a href="javascript:">
                                        <img data-src="img/products/product2.jpg" alt="Product-Image" class="lazyload animate-default">
                                    </a>
                                </div>
                                <div class="card-detail">
                                    <div class="rating">
                                        <ul class="list-unstyled side-listing">
                                            <li><i class="fas fa-star active"></i></li>
                                            <li><i class="fas fa-star active"></i></li>
                                            <li><i class="fas fa-star active"></i></li>
                                            <li><i class="fas fa-star"></i></li>
                                            <li><i class="fas fa-star"></i></li>
                                        </ul>
                                    </div>
                                    <p class="product-title"><a href="javascript:">Butterflies Hand-held Bag</a></p>
                                    <p class="product-price">RS. 1,10,000</p>
                                </div>
                            </div>
                        </div>
                        <div class="item">
                            <div class="product-card">
                                <div class="card-image effect-hover-zoom">
                                    <a href="javascript:">
                                        <img data-src="img/products/product3.jpg" alt="Product-Image" class="lazyload animate-default">
                                    </a>
                                </div>
                                <div class="card-detail">
                                    <div class="rating">
                                        <ul class="list-unstyled side-listing">
                                            <li><i class="fas fa-star active"></i></li>
                                            <li><i class="fas fa-star active"></i></li>
                                            <li><i class="fas fa-star active"></i></li>
                                            <li><i class="fas fa-star"></i></li>
                                            <li><i class="fas fa-star"></i></li>
                                        </ul>
                                    </div>
                                    <p class="product-title"><a href="javascript:">Butterflies Hand-held Bag</a></p>
                                    <p class="product-price">RS. 1,10,000</p>
                                </div>
                            </div>
                            <div class="product-card">
                                <div class="card-image effect-hover-zoom">
                                    <a href="javascript:">
                                        <img data-src="img/products/product7.jpg" alt="Product-Image" class="lazyload animate-default">
                                    </a>
                                </div>
                                <div class="card-detail">
                                    <div class="rating">
                                        <ul class="list-unstyled side-listing">
                                            <li><i class="fas fa-star active"></i></li>
                                            <li><i class="fas fa-star active"></i></li>
                                            <li><i class="fas fa-star active"></i></li>
                                            <li><i class="fas fa-star"></i></li>
                                            <li><i class="fas fa-star"></i></li>
                                        </ul>
                                    </div>
                                    <p class="product-title"><a href="javascript:">Butterflies Hand-held Bag</a></p>
                                    <p class="product-price">RS. 1,10,000</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="tab-pane fade" id="electrical" role="tabpanel" aria-labelledby="electrical-tab">
                    <div class="owl-carousel owl-theme owl-nav-top">
                        <div class="item">
                            <div class="product-card">
                                <div class="card-image effect-hover-zoom">
                                    <a href="javascript:">
                                        <img data-src="img/products/product1.jpg" alt="Product-Image" class="lazyload animate-default">
                                    </a>
                                </div>
                                <div class="card-detail">
                                    <div class="rating">
                                        <ul class="list-unstyled side-listing">
                                            <li><i class="fas fa-star active"></i></li>
                                            <li><i class="fas fa-star active"></i></li>
                                            <li><i class="fas fa-star active"></i></li>
                                            <li><i class="fas fa-star"></i></li>
                                            <li><i class="fas fa-star"></i></li>
                                        </ul>
                                    </div>
                                    <p class="product-title"><a href="javascript:">Butterflies Hand-held Bag</a></p>
                                    <p class="product-price">RS. 1,10,000</p>
                                </div>
                            </div>
                            <div class="product-card">
                                <div class="card-image effect-hover-zoom">
                                    <a href="javascript:">
                                        <img data-src="img/products/product6.jpg" alt="Product-Image" class="lazyload animate-default">
                                    </a>
                                </div>
                                <div class="card-detail">
                                    <div class="rating">
                                        <ul class="list-unstyled side-listing">
                                            <li><i class="fas fa-star active"></i></li>
                                            <li><i class="fas fa-star active"></i></li>
                                            <li><i class="fas fa-star active"></i></li>
                                            <li><i class="fas fa-star"></i></li>
                                            <li><i class="fas fa-star"></i></li>
                                        </ul>
                                    </div>
                                    <p class="product-title"><a href="javascript:">Butterflies Hand-held Bag</a></p>
                                    <p class="product-price">RS. 1,10,000</p>
                                </div>
                            </div>
                        </div>
                        <div class="item">
                            <div class="product-card">
                                <div class="card-image effect-hover-zoom">
                                    <a href="javascript:">
                                        <img data-src="img/products/product2.jpg" alt="Product-Image" class="lazyload animate-default">
                                    </a>
                                </div>
                                <div class="card-detail">
                                    <div class="rating">
                                        <ul class="list-unstyled side-listing">
                                            <li><i class="fas fa-star active"></i></li>
                                            <li><i class="fas fa-star active"></i></li>
                                            <li><i class="fas fa-star active"></i></li>
                                            <li><i class="fas fa-star"></i></li>
                                            <li><i class="fas fa-star"></i></li>
                                        </ul>
                                    </div>
                                    <p class="product-title"><a href="javascript:">Butterflies Hand-held Bag</a></p>
                                    <p class="product-price">RS. 1,10,000</p>
                                </div>
                            </div>
                            <div class="product-card">
                                <div class="card-image effect-hover-zoom">
                                    <a href="javascript:">
                                        <img data-src="img/products/product8.jpg" alt="Product-Image" class="lazyload animate-default">
                                    </a>
                                </div>
                                <div class="card-detail">
                                    <div class="rating">
                                        <ul class="list-unstyled side-listing">
                                            <li><i class="fas fa-star active"></i></li>
                                            <li><i class="fas fa-star active"></i></li>
                                            <li><i class="fas fa-star active"></i></li>
                                            <li><i class="fas fa-star"></i></li>
                                            <li><i class="fas fa-star"></i></li>
                                        </ul>
                                    </div>
                                    <p class="product-title"><a href="javascript:">Butterflies Hand-held Bag</a></p>
                                    <p class="product-price">RS. 1,10,000</p>
                                </div>
                            </div>
                        </div>
                        <div class="item">
                            <div class="product-card">
                                <div class="card-image effect-hover-zoom">
                                    <a href="javascript:">
                                        <img data-src="img/products/product3.jpg" alt="Product-Image" class="lazyload animate-default">
                                    </a>
                                </div>
                                <div class="card-detail">
                                    <div class="rating">
                                        <ul class="list-unstyled side-listing">
                                            <li><i class="fas fa-star active"></i></li>
                                            <li><i class="fas fa-star active"></i></li>
                                            <li><i class="fas fa-star active"></i></li>
                                            <li><i class="fas fa-star"></i></li>
                                            <li><i class="fas fa-star"></i></li>
                                        </ul>
                                    </div>
                                    <p class="product-title"><a href="javascript:">Butterflies Hand-held Bag</a></p>
                                    <p class="product-price">RS. 1,10,000</p>
                                </div>
                            </div>
                            <div class="product-card">
                                <div class="card-image effect-hover-zoom">
                                    <a href="javascript:">
                                        <img data-src="img/products/product9.webp" alt="Product-Image" class="lazyload animate-default">
                                    </a>
                                </div>
                                <div class="card-detail">
                                    <div class="rating">
                                        <ul class="list-unstyled side-listing">
                                            <li><i class="fas fa-star active"></i></li>
                                            <li><i class="fas fa-star active"></i></li>
                                            <li><i class="fas fa-star active"></i></li>
                                            <li><i class="fas fa-star"></i></li>
                                            <li><i class="fas fa-star"></i></li>
                                        </ul>
                                    </div>
                                    <p class="product-title"><a href="javascript:">Butterflies Hand-held Bag</a></p>
                                    <p class="product-price">RS. 1,10,000</p>
                                </div>
                            </div>
                        </div>
                        <div class="item">
                            <div class="product-card">
                                <div class="card-image effect-hover-zoom">
                                    <a href="javascript:">
                                        <img data-src="img/products/product4.jpg" alt="Product-Image" class="lazyload animate-default">
                                    </a>
                                </div>
                                <div class="card-detail">
                                    <div class="rating">
                                        <ul class="list-unstyled side-listing">
                                            <li><i class="fas fa-star active"></i></li>
                                            <li><i class="fas fa-star active"></i></li>
                                            <li><i class="fas fa-star active"></i></li>
                                            <li><i class="fas fa-star"></i></li>
                                            <li><i class="fas fa-star"></i></li>
                                        </ul>
                                    </div>
                                    <p class="product-title"><a href="javascript:">Butterflies Hand-held Bag</a></p>
                                    <p class="product-price">RS. 1,10,000</p>
                                </div>
                            </div>
                            <div class="product-card">
                                <div class="card-image effect-hover-zoom">
                                    <a href="javascript:">
                                        <img data-src="img/products/product1.jpg" alt="Product-Image" class="lazyload animate-default">
                                    </a>
                                </div>
                                <div class="card-detail">
                                    <div class="rating">
                                        <ul class="list-unstyled side-listing">
                                            <li><i class="fas fa-star active"></i></li>
                                            <li><i class="fas fa-star active"></i></li>
                                            <li><i class="fas fa-star active"></i></li>
                                            <li><i class="fas fa-star"></i></li>
                                            <li><i class="fas fa-star"></i></li>
                                        </ul>
                                    </div>
                                    <p class="product-title"><a href="javascript:">Butterflies Hand-held Bag</a></p>
                                    <p class="product-price">RS. 1,10,000</p>
                                </div>
                            </div>
                        </div>
                        <div class="item">
                            <div class="product-card">
                                <div class="card-image effect-hover-zoom">
                                    <a href="javascript:">
                                        <img data-src="img/products/product5.jpg" alt="Product-Image" class="lazyload animate-default">
                                    </a>
                                </div>
                                <div class="card-detail">
                                    <div class="rating">
                                        <ul class="list-unstyled side-listing">
                                            <li><i class="fas fa-star active"></i></li>
                                            <li><i class="fas fa-star active"></i></li>
                                            <li><i class="fas fa-star active"></i></li>
                                            <li><i class="fas fa-star"></i></li>
                                            <li><i class="fas fa-star"></i></li>
                                        </ul>
                                    </div>
                                    <p class="product-title"><a href="javascript:">Butterflies Hand-held Bag</a></p>
                                    <p class="product-price">RS. 1,10,000</p>
                                </div>
                            </div>
                            <div class="product-card">
                                <div class="card-image effect-hover-zoom">
                                    <a href="javascript:">
                                        <img data-src="img/products/product2.jpg" alt="Product-Image" class="lazyload animate-default">
                                    </a>
                                </div>
                                <div class="card-detail">
                                    <div class="rating">
                                        <ul class="list-unstyled side-listing">
                                            <li><i class="fas fa-star active"></i></li>
                                            <li><i class="fas fa-star active"></i></li>
                                            <li><i class="fas fa-star active"></i></li>
                                            <li><i class="fas fa-star"></i></li>
                                            <li><i class="fas fa-star"></i></li>
                                        </ul>
                                    </div>
                                    <p class="product-title"><a href="javascript:">Butterflies Hand-held Bag</a></p>
                                    <p class="product-price">RS. 1,10,000</p>
                                </div>
                            </div>
                        </div>
                        <div class="item">
                            <div class="product-card">
                                <div class="card-image effect-hover-zoom">
                                    <a href="javascript:">
                                        <img data-src="img/products/product3.jpg" alt="Product-Image" class="lazyload animate-default">
                                    </a>
                                </div>
                                <div class="card-detail">
                                    <div class="rating">
                                        <ul class="list-unstyled side-listing">
                                            <li><i class="fas fa-star active"></i></li>
                                            <li><i class="fas fa-star active"></i></li>
                                            <li><i class="fas fa-star active"></i></li>
                                            <li><i class="fas fa-star"></i></li>
                                            <li><i class="fas fa-star"></i></li>
                                        </ul>
                                    </div>
                                    <p class="product-title"><a href="javascript:">Butterflies Hand-held Bag</a></p>
                                    <p class="product-price">RS. 1,10,000</p>
                                </div>
                            </div>
                            <div class="product-card">
                                <div class="card-image effect-hover-zoom">
                                    <a href="javascript:">
                                        <img data-src="img/products/product7.jpg" alt="Product-Image" class="lazyload animate-default">
                                    </a>
                                </div>
                                <div class="card-detail">
                                    <div class="rating">
                                        <ul class="list-unstyled side-listing">
                                            <li><i class="fas fa-star active"></i></li>
                                            <li><i class="fas fa-star active"></i></li>
                                            <li><i class="fas fa-star active"></i></li>
                                            <li><i class="fas fa-star"></i></li>
                                            <li><i class="fas fa-star"></i></li>
                                        </ul>
                                    </div>
                                    <p class="product-title"><a href="javascript:">Butterflies Hand-held Bag</a></p>
                                    <p class="product-price">RS. 1,10,000</p>
                                </div>
                            </div>
                        </div>
                        <div class="item">
                            <div class="product-card">
                                <div class="card-image effect-hover-zoom">
                                    <a href="javascript:">
                                        <img data-src="img/products/product4.jpg" alt="Product-Image" class="lazyload animate-default">
                                    </a>
                                </div>
                                <div class="card-detail">
                                    <div class="rating">
                                        <ul class="list-unstyled side-listing">
                                            <li><i class="fas fa-star active"></i></li>
                                            <li><i class="fas fa-star active"></i></li>
                                            <li><i class="fas fa-star active"></i></li>
                                            <li><i class="fas fa-star"></i></li>
                                            <li><i class="fas fa-star"></i></li>
                                        </ul>
                                    </div>
                                    <p class="product-title"><a href="javascript:">Butterflies Hand-held Bag</a></p>
                                    <p class="product-price">RS. 1,10,000</p>
                                </div>
                            </div>
                            <div class="product-card">
                                <div class="card-image effect-hover-zoom">
                                    <a href="javascript:">
                                        <img data-src="img/products/product1.jpg" alt="Product-Image" class="lazyload animate-default">
                                    </a>
                                </div>
                                <div class="card-detail">
                                    <div class="rating">
                                        <ul class="list-unstyled side-listing">
                                            <li><i class="fas fa-star active"></i></li>
                                            <li><i class="fas fa-star active"></i></li>
                                            <li><i class="fas fa-star active"></i></li>
                                            <li><i class="fas fa-star"></i></li>
                                            <li><i class="fas fa-star"></i></li>
                                        </ul>
                                    </div>
                                    <p class="product-title"><a href="javascript:">Butterflies Hand-held Bag</a></p>
                                    <p class="product-price">RS. 1,10,000</p>
                                </div>
                            </div>
                        </div>
                        <div class="item">
                            <div class="product-card">
                                <div class="card-image effect-hover-zoom">
                                    <a href="javascript:">
                                        <img data-src="img/products/product5.jpg" alt="Product-Image" class="lazyload animate-default">
                                    </a>
                                </div>
                                <div class="card-detail">
                                    <div class="rating">
                                        <ul class="list-unstyled side-listing">
                                            <li><i class="fas fa-star active"></i></li>
                                            <li><i class="fas fa-star active"></i></li>
                                            <li><i class="fas fa-star active"></i></li>
                                            <li><i class="fas fa-star"></i></li>
                                            <li><i class="fas fa-star"></i></li>
                                        </ul>
                                    </div>
                                    <p class="product-title"><a href="javascript:">Butterflies Hand-held Bag</a></p>
                                    <p class="product-price">RS. 1,10,000</p>
                                </div>
                            </div>
                            <div class="product-card">
                                <div class="card-image effect-hover-zoom">
                                    <a href="javascript:">
                                        <img data-src="img/products/product2.jpg" alt="Product-Image" class="lazyload animate-default">
                                    </a>
                                </div>
                                <div class="card-detail">
                                    <div class="rating">
                                        <ul class="list-unstyled side-listing">
                                            <li><i class="fas fa-star active"></i></li>
                                            <li><i class="fas fa-star active"></i></li>
                                            <li><i class="fas fa-star active"></i></li>
                                            <li><i class="fas fa-star"></i></li>
                                            <li><i class="fas fa-star"></i></li>
                                        </ul>
                                    </div>
                                    <p class="product-title"><a href="javascript:">Butterflies Hand-held Bag</a></p>
                                    <p class="product-price">RS. 1,10,000</p>
                                </div>
                            </div>
                        </div>
                        <div class="item">
                            <div class="product-card">
                                <div class="card-image effect-hover-zoom">
                                    <a href="javascript:">
                                        <img data-src="img/products/product3.jpg" alt="Product-Image" class="lazyload animate-default">
                                    </a>
                                </div>
                                <div class="card-detail">
                                    <div class="rating">
                                        <ul class="list-unstyled side-listing">
                                            <li><i class="fas fa-star active"></i></li>
                                            <li><i class="fas fa-star active"></i></li>
                                            <li><i class="fas fa-star active"></i></li>
                                            <li><i class="fas fa-star"></i></li>
                                            <li><i class="fas fa-star"></i></li>
                                        </ul>
                                    </div>
                                    <p class="product-title"><a href="javascript:">Butterflies Hand-held Bag</a></p>
                                    <p class="product-price">RS. 1,10,000</p>
                                </div>
                            </div>
                            <div class="product-card">
                                <div class="card-image effect-hover-zoom">
                                    <a href="javascript:">
                                        <img data-src="img/products/product7.jpg" alt="Product-Image" class="lazyload animate-default">
                                    </a>
                                </div>
                                <div class="card-detail">
                                    <div class="rating">
                                        <ul class="list-unstyled side-listing">
                                            <li><i class="fas fa-star active"></i></li>
                                            <li><i class="fas fa-star active"></i></li>
                                            <li><i class="fas fa-star active"></i></li>
                                            <li><i class="fas fa-star"></i></li>
                                            <li><i class="fas fa-star"></i></li>
                                        </ul>
                                    </div>
                                    <p class="product-title"><a href="javascript:">Butterflies Hand-held Bag</a></p>
                                    <p class="product-price">RS. 1,10,000</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="tab-pane fade" id="home-app" role="tabpanel" aria-labelledby="home-app-tab">
                    <div class="owl-carousel owl-theme owl-nav-top">
                        <div class="item">
                            <div class="product-card">
                                <div class="card-image effect-hover-zoom">
                                    <a href="javascript:">
                                        <img data-src="img/products/product1.jpg" alt="Product-Image" class="lazyload animate-default">
                                    </a>
                                </div>
                                <div class="card-detail">
                                    <div class="rating">
                                        <ul class="list-unstyled side-listing">
                                            <li><i class="fas fa-star active"></i></li>
                                            <li><i class="fas fa-star active"></i></li>
                                            <li><i class="fas fa-star active"></i></li>
                                            <li><i class="fas fa-star"></i></li>
                                            <li><i class="fas fa-star"></i></li>
                                        </ul>
                                    </div>
                                    <p class="product-title"><a href="javascript:">Butterflies Hand-held Bag</a></p>
                                    <p class="product-price">RS. 1,10,000</p>
                                </div>
                            </div>
                            <div class="product-card">
                                <div class="card-image effect-hover-zoom">
                                    <a href="javascript:">
                                        <img data-src="img/products/product6.jpg" alt="Product-Image" class="lazyload animate-default">
                                    </a>
                                </div>
                                <div class="card-detail">
                                    <div class="rating">
                                        <ul class="list-unstyled side-listing">
                                            <li><i class="fas fa-star active"></i></li>
                                            <li><i class="fas fa-star active"></i></li>
                                            <li><i class="fas fa-star active"></i></li>
                                            <li><i class="fas fa-star"></i></li>
                                            <li><i class="fas fa-star"></i></li>
                                        </ul>
                                    </div>
                                    <p class="product-title"><a href="javascript:">Butterflies Hand-held Bag</a></p>
                                    <p class="product-price">RS. 1,10,000</p>
                                </div>
                            </div>
                        </div>
                        <div class="item">
                            <div class="product-card">
                                <div class="card-image effect-hover-zoom">
                                    <a href="javascript:">
                                        <img data-src="img/products/product2.jpg" alt="Product-Image" class="lazyload animate-default">
                                    </a>
                                </div>
                                <div class="card-detail">
                                    <div class="rating">
                                        <ul class="list-unstyled side-listing">
                                            <li><i class="fas fa-star active"></i></li>
                                            <li><i class="fas fa-star active"></i></li>
                                            <li><i class="fas fa-star active"></i></li>
                                            <li><i class="fas fa-star"></i></li>
                                            <li><i class="fas fa-star"></i></li>
                                        </ul>
                                    </div>
                                    <p class="product-title"><a href="javascript:">Butterflies Hand-held Bag</a></p>
                                    <p class="product-price">RS. 1,10,000</p>
                                </div>
                            </div>
                            <div class="product-card">
                                <div class="card-image effect-hover-zoom">
                                    <a href="javascript:">
                                        <img data-src="img/products/product8.jpg" alt="Product-Image" class="lazyload animate-default">
                                    </a>
                                </div>
                                <div class="card-detail">
                                    <div class="rating">
                                        <ul class="list-unstyled side-listing">
                                            <li><i class="fas fa-star active"></i></li>
                                            <li><i class="fas fa-star active"></i></li>
                                            <li><i class="fas fa-star active"></i></li>
                                            <li><i class="fas fa-star"></i></li>
                                            <li><i class="fas fa-star"></i></li>
                                        </ul>
                                    </div>
                                    <p class="product-title"><a href="javascript:">Butterflies Hand-held Bag</a></p>
                                    <p class="product-price">RS. 1,10,000</p>
                                </div>
                            </div>
                        </div>
                        <div class="item">
                            <div class="product-card">
                                <div class="card-image effect-hover-zoom">
                                    <a href="javascript:">
                                        <img data-src="img/products/product3.jpg" alt="Product-Image" class="lazyload animate-default">
                                    </a>
                                </div>
                                <div class="card-detail">
                                    <div class="rating">
                                        <ul class="list-unstyled side-listing">
                                            <li><i class="fas fa-star active"></i></li>
                                            <li><i class="fas fa-star active"></i></li>
                                            <li><i class="fas fa-star active"></i></li>
                                            <li><i class="fas fa-star"></i></li>
                                            <li><i class="fas fa-star"></i></li>
                                        </ul>
                                    </div>
                                    <p class="product-title"><a href="javascript:">Butterflies Hand-held Bag</a></p>
                                    <p class="product-price">RS. 1,10,000</p>
                                </div>
                            </div>
                            <div class="product-card">
                                <div class="card-image effect-hover-zoom">
                                    <a href="javascript:">
                                        <img data-src="img/products/product9.webp" alt="Product-Image" class="lazyload animate-default">
                                    </a>
                                </div>
                                <div class="card-detail">
                                    <div class="rating">
                                        <ul class="list-unstyled side-listing">
                                            <li><i class="fas fa-star active"></i></li>
                                            <li><i class="fas fa-star active"></i></li>
                                            <li><i class="fas fa-star active"></i></li>
                                            <li><i class="fas fa-star"></i></li>
                                            <li><i class="fas fa-star"></i></li>
                                        </ul>
                                    </div>
                                    <p class="product-title"><a href="javascript:">Butterflies Hand-held Bag</a></p>
                                    <p class="product-price">RS. 1,10,000</p>
                                </div>
                            </div>
                        </div>
                        <div class="item">
                            <div class="product-card">
                                <div class="card-image effect-hover-zoom">
                                    <a href="javascript:">
                                        <img data-src="img/products/product4.jpg" alt="Product-Image" class="lazyload animate-default">
                                    </a>
                                </div>
                                <div class="card-detail">
                                    <div class="rating">
                                        <ul class="list-unstyled side-listing">
                                            <li><i class="fas fa-star active"></i></li>
                                            <li><i class="fas fa-star active"></i></li>
                                            <li><i class="fas fa-star active"></i></li>
                                            <li><i class="fas fa-star"></i></li>
                                            <li><i class="fas fa-star"></i></li>
                                        </ul>
                                    </div>
                                    <p class="product-title"><a href="javascript:">Butterflies Hand-held Bag</a></p>
                                    <p class="product-price">RS. 1,10,000</p>
                                </div>
                            </div>
                            <div class="product-card">
                                <div class="card-image effect-hover-zoom">
                                    <a href="javascript:">
                                        <img data-src="img/products/product1.jpg" alt="Product-Image" class="lazyload animate-default">
                                    </a>
                                </div>
                                <div class="card-detail">
                                    <div class="rating">
                                        <ul class="list-unstyled side-listing">
                                            <li><i class="fas fa-star active"></i></li>
                                            <li><i class="fas fa-star active"></i></li>
                                            <li><i class="fas fa-star active"></i></li>
                                            <li><i class="fas fa-star"></i></li>
                                            <li><i class="fas fa-star"></i></li>
                                        </ul>
                                    </div>
                                    <p class="product-title"><a href="javascript:">Butterflies Hand-held Bag</a></p>
                                    <p class="product-price">RS. 1,10,000</p>
                                </div>
                            </div>
                        </div>
                        <div class="item">
                            <div class="product-card">
                                <div class="card-image effect-hover-zoom">
                                    <a href="javascript:">
                                        <img data-src="img/products/product5.jpg" alt="Product-Image" class="lazyload animate-default">
                                    </a>
                                </div>
                                <div class="card-detail">
                                    <div class="rating">
                                        <ul class="list-unstyled side-listing">
                                            <li><i class="fas fa-star active"></i></li>
                                            <li><i class="fas fa-star active"></i></li>
                                            <li><i class="fas fa-star active"></i></li>
                                            <li><i class="fas fa-star"></i></li>
                                            <li><i class="fas fa-star"></i></li>
                                        </ul>
                                    </div>
                                    <p class="product-title"><a href="javascript:">Butterflies Hand-held Bag</a></p>
                                    <p class="product-price">RS. 1,10,000</p>
                                </div>
                            </div>
                            <div class="product-card">
                                <div class="card-image effect-hover-zoom">
                                    <a href="javascript:">
                                        <img data-src="img/products/product2.jpg" alt="Product-Image" class="lazyload animate-default">
                                    </a>
                                </div>
                                <div class="card-detail">
                                    <div class="rating">
                                        <ul class="list-unstyled side-listing">
                                            <li><i class="fas fa-star active"></i></li>
                                            <li><i class="fas fa-star active"></i></li>
                                            <li><i class="fas fa-star active"></i></li>
                                            <li><i class="fas fa-star"></i></li>
                                            <li><i class="fas fa-star"></i></li>
                                        </ul>
                                    </div>
                                    <p class="product-title"><a href="javascript:">Butterflies Hand-held Bag</a></p>
                                    <p class="product-price">RS. 1,10,000</p>
                                </div>
                            </div>
                        </div>
                        <div class="item">
                            <div class="product-card">
                                <div class="card-image effect-hover-zoom">
                                    <a href="javascript:">
                                        <img data-src="img/products/product3.jpg" alt="Product-Image" class="lazyload animate-default">
                                    </a>
                                </div>
                                <div class="card-detail">
                                    <div class="rating">
                                        <ul class="list-unstyled side-listing">
                                            <li><i class="fas fa-star active"></i></li>
                                            <li><i class="fas fa-star active"></i></li>
                                            <li><i class="fas fa-star active"></i></li>
                                            <li><i class="fas fa-star"></i></li>
                                            <li><i class="fas fa-star"></i></li>
                                        </ul>
                                    </div>
                                    <p class="product-title"><a href="javascript:">Butterflies Hand-held Bag</a></p>
                                    <p class="product-price">RS. 1,10,000</p>
                                </div>
                            </div>
                            <div class="product-card">
                                <div class="card-image effect-hover-zoom">
                                    <a href="javascript:">
                                        <img data-src="img/products/product7.jpg" alt="Product-Image" class="lazyload animate-default">
                                    </a>
                                </div>
                                <div class="card-detail">
                                    <div class="rating">
                                        <ul class="list-unstyled side-listing">
                                            <li><i class="fas fa-star active"></i></li>
                                            <li><i class="fas fa-star active"></i></li>
                                            <li><i class="fas fa-star active"></i></li>
                                            <li><i class="fas fa-star"></i></li>
                                            <li><i class="fas fa-star"></i></li>
                                        </ul>
                                    </div>
                                    <p class="product-title"><a href="javascript:">Butterflies Hand-held Bag</a></p>
                                    <p class="product-price">RS. 1,10,000</p>
                                </div>
                            </div>
                        </div>
                        <div class="item">
                            <div class="product-card">
                                <div class="card-image effect-hover-zoom">
                                    <a href="javascript:">
                                        <img data-src="img/products/product4.jpg" alt="Product-Image" class="lazyload animate-default">
                                    </a>
                                </div>
                                <div class="card-detail">
                                    <div class="rating">
                                        <ul class="list-unstyled side-listing">
                                            <li><i class="fas fa-star active"></i></li>
                                            <li><i class="fas fa-star active"></i></li>
                                            <li><i class="fas fa-star active"></i></li>
                                            <li><i class="fas fa-star"></i></li>
                                            <li><i class="fas fa-star"></i></li>
                                        </ul>
                                    </div>
                                    <p class="product-title"><a href="javascript:">Butterflies Hand-held Bag</a></p>
                                    <p class="product-price">RS. 1,10,000</p>
                                </div>
                            </div>
                            <div class="product-card">
                                <div class="card-image effect-hover-zoom">
                                    <a href="javascript:">
                                        <img data-src="img/products/product1.jpg" alt="Product-Image" class="lazyload animate-default">
                                    </a>
                                </div>
                                <div class="card-detail">
                                    <div class="rating">
                                        <ul class="list-unstyled side-listing">
                                            <li><i class="fas fa-star active"></i></li>
                                            <li><i class="fas fa-star active"></i></li>
                                            <li><i class="fas fa-star active"></i></li>
                                            <li><i class="fas fa-star"></i></li>
                                            <li><i class="fas fa-star"></i></li>
                                        </ul>
                                    </div>
                                    <p class="product-title"><a href="javascript:">Butterflies Hand-held Bag</a></p>
                                    <p class="product-price">RS. 1,10,000</p>
                                </div>
                            </div>
                        </div>
                        <div class="item">
                            <div class="product-card">
                                <div class="card-image effect-hover-zoom">
                                    <a href="javascript:">
                                        <img data-src="img/products/product5.jpg" alt="Product-Image" class="lazyload animate-default">
                                    </a>
                                </div>
                                <div class="card-detail">
                                    <div class="rating">
                                        <ul class="list-unstyled side-listing">
                                            <li><i class="fas fa-star active"></i></li>
                                            <li><i class="fas fa-star active"></i></li>
                                            <li><i class="fas fa-star active"></i></li>
                                            <li><i class="fas fa-star"></i></li>
                                            <li><i class="fas fa-star"></i></li>
                                        </ul>
                                    </div>
                                    <p class="product-title"><a href="javascript:">Butterflies Hand-held Bag</a></p>
                                    <p class="product-price">RS. 1,10,000</p>
                                </div>
                            </div>
                            <div class="product-card">
                                <div class="card-image effect-hover-zoom">
                                    <a href="javascript:">
                                        <img data-src="img/products/product2.jpg" alt="Product-Image" class="lazyload animate-default">
                                    </a>
                                </div>
                                <div class="card-detail">
                                    <div class="rating">
                                        <ul class="list-unstyled side-listing">
                                            <li><i class="fas fa-star active"></i></li>
                                            <li><i class="fas fa-star active"></i></li>
                                            <li><i class="fas fa-star active"></i></li>
                                            <li><i class="fas fa-star"></i></li>
                                            <li><i class="fas fa-star"></i></li>
                                        </ul>
                                    </div>
                                    <p class="product-title"><a href="javascript:">Butterflies Hand-held Bag</a></p>
                                    <p class="product-price">RS. 1,10,000</p>
                                </div>
                            </div>
                        </div>
                        <div class="item">
                            <div class="product-card">
                                <div class="card-image effect-hover-zoom">
                                    <a href="javascript:">
                                        <img data-src="img/products/product3.jpg" alt="Product-Image" class="lazyload animate-default">
                                    </a>
                                </div>
                                <div class="card-detail">
                                    <div class="rating">
                                        <ul class="list-unstyled side-listing">
                                            <li><i class="fas fa-star active"></i></li>
                                            <li><i class="fas fa-star active"></i></li>
                                            <li><i class="fas fa-star active"></i></li>
                                            <li><i class="fas fa-star"></i></li>
                                            <li><i class="fas fa-star"></i></li>
                                        </ul>
                                    </div>
                                    <p class="product-title"><a href="javascript:">Butterflies Hand-held Bag</a></p>
                                    <p class="product-price">RS. 1,10,000</p>
                                </div>
                            </div>
                            <div class="product-card">
                                <div class="card-image effect-hover-zoom">
                                    <a href="javascript:">
                                        <img data-src="img/products/product7.jpg" alt="Product-Image" class="lazyload animate-default">
                                    </a>
                                </div>
                                <div class="card-detail">
                                    <div class="rating">
                                        <ul class="list-unstyled side-listing">
                                            <li><i class="fas fa-star active"></i></li>
                                            <li><i class="fas fa-star active"></i></li>
                                            <li><i class="fas fa-star active"></i></li>
                                            <li><i class="fas fa-star"></i></li>
                                            <li><i class="fas fa-star"></i></li>
                                        </ul>
                                    </div>
                                    <p class="product-title"><a href="javascript:">Butterflies Hand-held Bag</a></p>
                                    <p class="product-price">RS. 1,10,000</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<!-- Fashion-category -->

<!-- Electrical-category -->

<section class="top-category pad-t-20 bg-grey">
    <div class="container-fluid"> 
        <div class="white-card">
            <div class="section-title-wrapper title-tab clearfix"> 
                <ul class="nav nav-tabs cat-nav" id="myTab" role="tablist">
                    <li class="nav-item">
                        <a class="nav-link active" id="agro-tab" data-toggle="tab" href="#agro" role="tab" aria-controls="agro" aria-selected="true">Agro Product</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" id="general-kirana-tab" data-toggle="tab" href="#general-kirana" role="tab" aria-controls="general-kirana" aria-selected="false">general-kirana</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" id="hardware-tab" data-toggle="tab" href="#hardware" role="tab" aria-controls="hardware" aria-selected="false">hardware</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" id="makeup-tab" data-toggle="tab" href="#makeup" role="tab" aria-controls="makeup" aria-selected="false">Make Up</a>
                    </li>
                    <li class="view-more-btn">
                        <a href="javascript:" class="float-right">View More</a>
                    </li>
                </ul>
            </div> 
            <div class="tab-content" id="myTabContent"> 
                <div class="tab-pane fade show active" id="agro" role="tabpanel" aria-labelledby="agro-tab">
                    <div class="owl-carousel owl-theme owl-nav-top"> 
                        <div class="item">
                            <div class="product-card">
                                <div class="card-image effect-hover-zoom">
                                    <a href="javascript:">
                                        <img data-src="img/products/product1.jpg" alt="Product-Image" class="lazyload animate-default">
                                    </a>
                                </div>
                                <div class="card-detail">
                                    <div class="rating">
                                        <ul class="list-unstyled side-listing">
                                            <li><i class="fas fa-star active"></i></li>
                                            <li><i class="fas fa-star active"></i></li>
                                            <li><i class="fas fa-star active"></i></li>
                                            <li><i class="fas fa-star"></i></li>
                                            <li><i class="fas fa-star"></i></li>
                                        </ul>
                                    </div>
                                    <p class="product-title"><a href="javascript:">Butterflies Hand-held Bag</a></p>
                                    <p class="product-price">RS. 1,10,000</p>
                                </div>
                            </div> 
                            <div class="product-card">
                                <div class="card-image effect-hover-zoom">
                                    <a href="javascript:">
                                        <img data-src="img/products/product6.jpg" alt="Product-Image" class="lazyload animate-default">
                                    </a>
                                </div>
                                <div class="card-detail">
                                    <div class="rating">
                                        <ul class="list-unstyled side-listing">
                                            <li><i class="fas fa-star active"></i></li>
                                            <li><i class="fas fa-star active"></i></li>
                                            <li><i class="fas fa-star active"></i></li>
                                            <li><i class="fas fa-star"></i></li>
                                            <li><i class="fas fa-star"></i></li>
                                        </ul>
                                    </div>
                                    <p class="product-title"><a href="javascript:">Butterflies Hand-held Bag</a></p>
                                    <p class="product-price">RS. 1,10,000</p>
                                </div>
                            </div> 
                        </div>
                        <div class="item">
                            <div class="product-card">
                                <div class="card-image effect-hover-zoom">
                                    <a href="javascript:">
                                        <img data-src="img/products/product2.jpg" alt="Product-Image" class="lazyload animate-default">
                                    </a>
                                </div>
                                <div class="card-detail">
                                    <div class="rating">
                                        <ul class="list-unstyled side-listing">
                                            <li><i class="fas fa-star active"></i></li>
                                            <li><i class="fas fa-star active"></i></li>
                                            <li><i class="fas fa-star active"></i></li>
                                            <li><i class="fas fa-star"></i></li>
                                            <li><i class="fas fa-star"></i></li>
                                        </ul>
                                    </div>
                                    <p class="product-title"><a href="javascript:">Butterflies Hand-held Bag</a></p>
                                    <p class="product-price">RS. 1,10,000</p>
                                </div>
                            </div> 
                            <div class="product-card">
                                <div class="card-image effect-hover-zoom">
                                    <a href="javascript:">
                                        <img data-src="img/products/product8.jpg" alt="Product-Image" class="lazyload animate-default">
                                    </a>
                                </div>
                                <div class="card-detail">
                                    <div class="rating">
                                        <ul class="list-unstyled side-listing">
                                            <li><i class="fas fa-star active"></i></li>
                                            <li><i class="fas fa-star active"></i></li>
                                            <li><i class="fas fa-star active"></i></li>
                                            <li><i class="fas fa-star"></i></li>
                                            <li><i class="fas fa-star"></i></li>
                                        </ul>
                                    </div>
                                    <p class="product-title"><a href="javascript:">Butterflies Hand-held Bag</a></p>
                                    <p class="product-price">RS. 1,10,000</p>
                                </div>
                            </div> 
                        </div>
                        <div class="item">
                            <div class="product-card">
                                <div class="card-image effect-hover-zoom">
                                    <a href="javascript:">
                                        <img data-src="img/products/product3.jpg" alt="Product-Image" class="lazyload animate-default">
                                    </a>
                                </div>
                                <div class="card-detail">
                                    <div class="rating">
                                        <ul class="list-unstyled side-listing">
                                            <li><i class="fas fa-star active"></i></li>
                                            <li><i class="fas fa-star active"></i></li>
                                            <li><i class="fas fa-star active"></i></li>
                                            <li><i class="fas fa-star"></i></li>
                                            <li><i class="fas fa-star"></i></li>
                                        </ul>
                                    </div>
                                    <p class="product-title"><a href="javascript:">Butterflies Hand-held Bag</a></p>
                                    <p class="product-price">RS. 1,10,000</p>
                                </div>
                            </div> 
                            <div class="product-card">
                                <div class="card-image effect-hover-zoom">
                                    <a href="javascript:">
                                        <img data-src="img/products/product9.webp" alt="Product-Image" class="lazyload animate-default">
                                    </a>
                                </div>
                                <div class="card-detail">
                                    <div class="rating">
                                        <ul class="list-unstyled side-listing">
                                            <li><i class="fas fa-star active"></i></li>
                                            <li><i class="fas fa-star active"></i></li>
                                            <li><i class="fas fa-star active"></i></li>
                                            <li><i class="fas fa-star"></i></li>
                                            <li><i class="fas fa-star"></i></li>
                                        </ul>
                                    </div>
                                    <p class="product-title"><a href="javascript:">Butterflies Hand-held Bag</a></p>
                                    <p class="product-price">RS. 1,10,000</p>
                                </div>
                            </div> 
                        </div>
                        <div class="item">
                            <div class="product-card">
                                <div class="card-image effect-hover-zoom">
                                    <a href="javascript:">
                                        <img data-src="img/products/product4.jpg" alt="Product-Image" class="lazyload animate-default">
                                    </a>
                                </div>
                                <div class="card-detail">
                                    <div class="rating">
                                        <ul class="list-unstyled side-listing">
                                            <li><i class="fas fa-star active"></i></li>
                                            <li><i class="fas fa-star active"></i></li>
                                            <li><i class="fas fa-star active"></i></li>
                                            <li><i class="fas fa-star"></i></li>
                                            <li><i class="fas fa-star"></i></li>
                                        </ul>
                                    </div>
                                    <p class="product-title"><a href="javascript:">Butterflies Hand-held Bag</a></p>
                                    <p class="product-price">RS. 1,10,000</p>
                                </div>
                            </div> 
                            <div class="product-card">
                                <div class="card-image effect-hover-zoom">
                                    <a href="javascript:">
                                        <img data-src="img/products/product1.jpg" alt="Product-Image" class="lazyload animate-default">
                                    </a>
                                </div>
                                <div class="card-detail">
                                    <div class="rating">
                                        <ul class="list-unstyled side-listing">
                                            <li><i class="fas fa-star active"></i></li>
                                            <li><i class="fas fa-star active"></i></li>
                                            <li><i class="fas fa-star active"></i></li>
                                            <li><i class="fas fa-star"></i></li>
                                            <li><i class="fas fa-star"></i></li>
                                        </ul>
                                    </div>
                                    <p class="product-title"><a href="javascript:">Butterflies Hand-held Bag</a></p>
                                    <p class="product-price">RS. 1,10,000</p>
                                </div>
                            </div> 
                        </div>
                        <div class="item">
                            <div class="product-card">
                                <div class="card-image effect-hover-zoom">
                                    <a href="javascript:">
                                        <img data-src="img/products/product5.jpg" alt="Product-Image" class="lazyload animate-default">
                                    </a>
                                </div>
                                <div class="card-detail">
                                    <div class="rating">
                                        <ul class="list-unstyled side-listing">
                                            <li><i class="fas fa-star active"></i></li>
                                            <li><i class="fas fa-star active"></i></li>
                                            <li><i class="fas fa-star active"></i></li>
                                            <li><i class="fas fa-star"></i></li>
                                            <li><i class="fas fa-star"></i></li>
                                        </ul>
                                    </div>
                                    <p class="product-title"><a href="javascript:">Butterflies Hand-held Bag</a></p>
                                    <p class="product-price">RS. 1,10,000</p>
                                </div>
                            </div> 
                            <div class="product-card">
                                <div class="card-image effect-hover-zoom">
                                    <a href="javascript:">
                                        <img data-src="img/products/product2.jpg" alt="Product-Image" class="lazyload animate-default">
                                    </a>
                                </div>
                                <div class="card-detail">
                                    <div class="rating">
                                        <ul class="list-unstyled side-listing">
                                            <li><i class="fas fa-star active"></i></li>
                                            <li><i class="fas fa-star active"></i></li>
                                            <li><i class="fas fa-star active"></i></li>
                                            <li><i class="fas fa-star"></i></li>
                                            <li><i class="fas fa-star"></i></li>
                                        </ul>
                                    </div>
                                    <p class="product-title"><a href="javascript:">Butterflies Hand-held Bag</a></p>
                                    <p class="product-price">RS. 1,10,000</p>
                                </div>
                            </div> 
                        </div>
                        <div class="item">
                            <div class="product-card">
                                <div class="card-image effect-hover-zoom">
                                    <a href="javascript:">
                                        <img data-src="img/products/product3.jpg" alt="Product-Image" class="lazyload animate-default">
                                    </a>
                                </div>
                                <div class="card-detail">
                                    <div class="rating">
                                        <ul class="list-unstyled side-listing">
                                            <li><i class="fas fa-star active"></i></li>
                                            <li><i class="fas fa-star active"></i></li>
                                            <li><i class="fas fa-star active"></i></li>
                                            <li><i class="fas fa-star"></i></li>
                                            <li><i class="fas fa-star"></i></li>
                                        </ul>
                                    </div>
                                    <p class="product-title"><a href="javascript:">Butterflies Hand-held Bag</a></p>
                                    <p class="product-price">RS. 1,10,000</p>
                                </div>
                            </div> 
                            <div class="product-card">
                                <div class="card-image effect-hover-zoom">
                                    <a href="javascript:">
                                        <img data-src="img/products/product7.jpg" alt="Product-Image" class="lazyload animate-default">
                                    </a>
                                </div>
                                <div class="card-detail">
                                    <div class="rating">
                                        <ul class="list-unstyled side-listing">
                                            <li><i class="fas fa-star active"></i></li>
                                            <li><i class="fas fa-star active"></i></li>
                                            <li><i class="fas fa-star active"></i></li>
                                            <li><i class="fas fa-star"></i></li>
                                            <li><i class="fas fa-star"></i></li>
                                        </ul>
                                    </div>
                                    <p class="product-title"><a href="javascript:">Butterflies Hand-held Bag</a></p>
                                    <p class="product-price">RS. 1,10,000</p>
                                </div>
                            </div> 
                        </div>
                        <div class="item">
                            <div class="product-card">
                                <div class="card-image effect-hover-zoom">
                                    <a href="javascript:">
                                        <img data-src="img/products/product4.jpg" alt="Product-Image" class="lazyload animate-default">
                                    </a>
                                </div>
                                <div class="card-detail">
                                    <div class="rating">
                                        <ul class="list-unstyled side-listing">
                                            <li><i class="fas fa-star active"></i></li>
                                            <li><i class="fas fa-star active"></i></li>
                                            <li><i class="fas fa-star active"></i></li>
                                            <li><i class="fas fa-star"></i></li>
                                            <li><i class="fas fa-star"></i></li>
                                        </ul>
                                    </div>
                                    <p class="product-title"><a href="javascript:">Butterflies Hand-held Bag</a></p>
                                    <p class="product-price">RS. 1,10,000</p>
                                </div>
                            </div> 
                            <div class="product-card">
                                <div class="card-image effect-hover-zoom">
                                    <a href="javascript:">
                                        <img data-src="img/products/product1.jpg" alt="Product-Image" class="lazyload animate-default">
                                    </a>
                                </div>
                                <div class="card-detail">
                                    <div class="rating">
                                        <ul class="list-unstyled side-listing">
                                            <li><i class="fas fa-star active"></i></li>
                                            <li><i class="fas fa-star active"></i></li>
                                            <li><i class="fas fa-star active"></i></li>
                                            <li><i class="fas fa-star"></i></li>
                                            <li><i class="fas fa-star"></i></li>
                                        </ul>
                                    </div>
                                    <p class="product-title"><a href="javascript:">Butterflies Hand-held Bag</a></p>
                                    <p class="product-price">RS. 1,10,000</p>
                                </div>
                            </div> 
                        </div>
                        <div class="item">
                            <div class="product-card">
                                <div class="card-image effect-hover-zoom">
                                    <a href="javascript:">
                                        <img data-src="img/products/product5.jpg" alt="Product-Image" class="lazyload animate-default">
                                    </a>
                                </div>
                                <div class="card-detail">
                                    <div class="rating">
                                        <ul class="list-unstyled side-listing">
                                            <li><i class="fas fa-star active"></i></li>
                                            <li><i class="fas fa-star active"></i></li>
                                            <li><i class="fas fa-star active"></i></li>
                                            <li><i class="fas fa-star"></i></li>
                                            <li><i class="fas fa-star"></i></li>
                                        </ul>
                                    </div>
                                    <p class="product-title"><a href="javascript:">Butterflies Hand-held Bag</a></p>
                                    <p class="product-price">RS. 1,10,000</p>
                                </div>
                            </div> 
                            <div class="product-card">
                                <div class="card-image effect-hover-zoom">
                                    <a href="javascript:">
                                        <img data-src="img/products/product2.jpg" alt="Product-Image" class="lazyload animate-default">
                                    </a>
                                </div>
                                <div class="card-detail">
                                    <div class="rating">
                                        <ul class="list-unstyled side-listing">
                                            <li><i class="fas fa-star active"></i></li>
                                            <li><i class="fas fa-star active"></i></li>
                                            <li><i class="fas fa-star active"></i></li>
                                            <li><i class="fas fa-star"></i></li>
                                            <li><i class="fas fa-star"></i></li>
                                        </ul>
                                    </div>
                                    <p class="product-title"><a href="javascript:">Butterflies Hand-held Bag</a></p>
                                    <p class="product-price">RS. 1,10,000</p>
                                </div>
                            </div> 
                        </div>
                        <div class="item">
                            <div class="product-card">
                                <div class="card-image effect-hover-zoom">
                                    <a href="javascript:">
                                        <img data-src="img/products/product3.jpg" alt="Product-Image" class="lazyload animate-default">
                                    </a>
                                </div>
                                <div class="card-detail">
                                    <div class="rating">
                                        <ul class="list-unstyled side-listing">
                                            <li><i class="fas fa-star active"></i></li>
                                            <li><i class="fas fa-star active"></i></li>
                                            <li><i class="fas fa-star active"></i></li>
                                            <li><i class="fas fa-star"></i></li>
                                            <li><i class="fas fa-star"></i></li>
                                        </ul>
                                    </div>
                                    <p class="product-title"><a href="javascript:">Butterflies Hand-held Bag</a></p>
                                    <p class="product-price">RS. 1,10,000</p>
                                </div>
                            </div> 
                            <div class="product-card">
                                <div class="card-image effect-hover-zoom">
                                    <a href="javascript:">
                                        <img data-src="img/products/product7.jpg" alt="Product-Image" class="lazyload animate-default">
                                    </a>
                                </div>
                                <div class="card-detail">
                                    <div class="rating">
                                        <ul class="list-unstyled side-listing">
                                            <li><i class="fas fa-star active"></i></li>
                                            <li><i class="fas fa-star active"></i></li>
                                            <li><i class="fas fa-star active"></i></li>
                                            <li><i class="fas fa-star"></i></li>
                                            <li><i class="fas fa-star"></i></li>
                                        </ul>
                                    </div>
                                    <p class="product-title"><a href="javascript:">Butterflies Hand-held Bag</a></p>
                                    <p class="product-price">RS. 1,10,000</p>
                                </div>
                            </div> 
                        </div>
                    </div>
                </div> 
                <div class="tab-pane fade" id="general-kirana" role="tabpanel" aria-labelledby="general-kirana-tab"> 
                    <div class="owl-carousel owl-theme owl-nav-top"> 
                        <div class="item">
                            <div class="product-card">
                                <div class="card-image effect-hover-zoom">
                                    <a href="javascript:">
                                        <img data-src="img/products/product1.jpg" alt="Product-Image" class="lazyload animate-default">
                                    </a>
                                </div>
                                <div class="card-detail">
                                    <div class="rating">
                                        <ul class="list-unstyled side-listing">
                                            <li><i class="fas fa-star active"></i></li>
                                            <li><i class="fas fa-star active"></i></li>
                                            <li><i class="fas fa-star active"></i></li>
                                            <li><i class="fas fa-star"></i></li>
                                            <li><i class="fas fa-star"></i></li>
                                        </ul>
                                    </div>
                                    <p class="product-title"><a href="javascript:">Butterflies Hand-held Bag</a></p>
                                    <p class="product-price">RS. 1,10,000</p>
                                </div>
                            </div> 
                            <div class="product-card">
                                <div class="card-image effect-hover-zoom">
                                    <a href="javascript:">
                                        <img data-src="img/products/product6.jpg" alt="Product-Image" class="lazyload animate-default">
                                    </a>
                                </div>
                                <div class="card-detail">
                                    <div class="rating">
                                        <ul class="list-unstyled side-listing">
                                            <li><i class="fas fa-star active"></i></li>
                                            <li><i class="fas fa-star active"></i></li>
                                            <li><i class="fas fa-star active"></i></li>
                                            <li><i class="fas fa-star"></i></li>
                                            <li><i class="fas fa-star"></i></li>
                                        </ul>
                                    </div>
                                    <p class="product-title"><a href="javascript:">Butterflies Hand-held Bag</a></p>
                                    <p class="product-price">RS. 1,10,000</p>
                                </div>
                            </div> 
                        </div>
                        <div class="item">
                            <div class="product-card">
                                <div class="card-image effect-hover-zoom">
                                    <a href="javascript:">
                                        <img data-src="img/products/product2.jpg" alt="Product-Image" class="lazyload animate-default">
                                    </a>
                                </div>
                                <div class="card-detail">
                                    <div class="rating">
                                        <ul class="list-unstyled side-listing">
                                            <li><i class="fas fa-star active"></i></li>
                                            <li><i class="fas fa-star active"></i></li>
                                            <li><i class="fas fa-star active"></i></li>
                                            <li><i class="fas fa-star"></i></li>
                                            <li><i class="fas fa-star"></i></li>
                                        </ul>
                                    </div>
                                    <p class="product-title"><a href="javascript:">Butterflies Hand-held Bag</a></p>
                                    <p class="product-price">RS. 1,10,000</p>
                                </div>
                            </div> 
                            <div class="product-card">
                                <div class="card-image effect-hover-zoom">
                                    <a href="javascript:">
                                        <img data-src="img/products/product8.jpg" alt="Product-Image" class="lazyload animate-default">
                                    </a>
                                </div>
                                <div class="card-detail">
                                    <div class="rating">
                                        <ul class="list-unstyled side-listing">
                                            <li><i class="fas fa-star active"></i></li>
                                            <li><i class="fas fa-star active"></i></li>
                                            <li><i class="fas fa-star active"></i></li>
                                            <li><i class="fas fa-star"></i></li>
                                            <li><i class="fas fa-star"></i></li>
                                        </ul>
                                    </div>
                                    <p class="product-title"><a href="javascript:">Butterflies Hand-held Bag</a></p>
                                    <p class="product-price">RS. 1,10,000</p>
                                </div>
                            </div> 
                        </div>
                        <div class="item">
                            <div class="product-card">
                                <div class="card-image effect-hover-zoom">
                                    <a href="javascript:">
                                        <img data-src="img/products/product3.jpg" alt="Product-Image" class="lazyload animate-default">
                                    </a>
                                </div>
                                <div class="card-detail">
                                    <div class="rating">
                                        <ul class="list-unstyled side-listing">
                                            <li><i class="fas fa-star active"></i></li>
                                            <li><i class="fas fa-star active"></i></li>
                                            <li><i class="fas fa-star active"></i></li>
                                            <li><i class="fas fa-star"></i></li>
                                            <li><i class="fas fa-star"></i></li>
                                        </ul>
                                    </div>
                                    <p class="product-title"><a href="javascript:">Butterflies Hand-held Bag</a></p>
                                    <p class="product-price">RS. 1,10,000</p>
                                </div>
                            </div> 
                            <div class="product-card">
                                <div class="card-image effect-hover-zoom">
                                    <a href="javascript:">
                                        <img data-src="img/products/product9.webp" alt="Product-Image" class="lazyload animate-default">
                                    </a>
                                </div>
                                <div class="card-detail">
                                    <div class="rating">
                                        <ul class="list-unstyled side-listing">
                                            <li><i class="fas fa-star active"></i></li>
                                            <li><i class="fas fa-star active"></i></li>
                                            <li><i class="fas fa-star active"></i></li>
                                            <li><i class="fas fa-star"></i></li>
                                            <li><i class="fas fa-star"></i></li>
                                        </ul>
                                    </div>
                                    <p class="product-title"><a href="javascript:">Butterflies Hand-held Bag</a></p>
                                    <p class="product-price">RS. 1,10,000</p>
                                </div>
                            </div> 
                        </div>
                        <div class="item">
                            <div class="product-card">
                                <div class="card-image effect-hover-zoom">
                                    <a href="javascript:">
                                        <img data-src="img/products/product4.jpg" alt="Product-Image" class="lazyload animate-default">
                                    </a>
                                </div>
                                <div class="card-detail">
                                    <div class="rating">
                                        <ul class="list-unstyled side-listing">
                                            <li><i class="fas fa-star active"></i></li>
                                            <li><i class="fas fa-star active"></i></li>
                                            <li><i class="fas fa-star active"></i></li>
                                            <li><i class="fas fa-star"></i></li>
                                            <li><i class="fas fa-star"></i></li>
                                        </ul>
                                    </div>
                                    <p class="product-title"><a href="javascript:">Butterflies Hand-held Bag</a></p>
                                    <p class="product-price">RS. 1,10,000</p>
                                </div>
                            </div> 
                            <div class="product-card">
                                <div class="card-image effect-hover-zoom">
                                    <a href="javascript:">
                                        <img data-src="img/products/product1.jpg" alt="Product-Image" class="lazyload animate-default">
                                    </a>
                                </div>
                                <div class="card-detail">
                                    <div class="rating">
                                        <ul class="list-unstyled side-listing">
                                            <li><i class="fas fa-star active"></i></li>
                                            <li><i class="fas fa-star active"></i></li>
                                            <li><i class="fas fa-star active"></i></li>
                                            <li><i class="fas fa-star"></i></li>
                                            <li><i class="fas fa-star"></i></li>
                                        </ul>
                                    </div>
                                    <p class="product-title"><a href="javascript:">Butterflies Hand-held Bag</a></p>
                                    <p class="product-price">RS. 1,10,000</p>
                                </div>
                            </div> 
                        </div>
                        <div class="item">
                            <div class="product-card">
                                <div class="card-image effect-hover-zoom">
                                    <a href="javascript:">
                                        <img data-src="img/products/product5.jpg" alt="Product-Image" class="lazyload animate-default">
                                    </a>
                                </div>
                                <div class="card-detail">
                                    <div class="rating">
                                        <ul class="list-unstyled side-listing">
                                            <li><i class="fas fa-star active"></i></li>
                                            <li><i class="fas fa-star active"></i></li>
                                            <li><i class="fas fa-star active"></i></li>
                                            <li><i class="fas fa-star"></i></li>
                                            <li><i class="fas fa-star"></i></li>
                                        </ul>
                                    </div>
                                    <p class="product-title"><a href="javascript:">Butterflies Hand-held Bag</a></p>
                                    <p class="product-price">RS. 1,10,000</p>
                                </div>
                            </div> 
                            <div class="product-card">
                                <div class="card-image effect-hover-zoom">
                                    <a href="javascript:">
                                        <img data-src="img/products/product2.jpg" alt="Product-Image" class="lazyload animate-default">
                                    </a>
                                </div>
                                <div class="card-detail">
                                    <div class="rating">
                                        <ul class="list-unstyled side-listing">
                                            <li><i class="fas fa-star active"></i></li>
                                            <li><i class="fas fa-star active"></i></li>
                                            <li><i class="fas fa-star active"></i></li>
                                            <li><i class="fas fa-star"></i></li>
                                            <li><i class="fas fa-star"></i></li>
                                        </ul>
                                    </div>
                                    <p class="product-title"><a href="javascript:">Butterflies Hand-held Bag</a></p>
                                    <p class="product-price">RS. 1,10,000</p>
                                </div>
                            </div> 
                        </div>
                        <div class="item">
                            <div class="product-card">
                                <div class="card-image effect-hover-zoom">
                                    <a href="javascript:">
                                        <img data-src="img/products/product3.jpg" alt="Product-Image" class="lazyload animate-default">
                                    </a>
                                </div>
                                <div class="card-detail">
                                    <div class="rating">
                                        <ul class="list-unstyled side-listing">
                                            <li><i class="fas fa-star active"></i></li>
                                            <li><i class="fas fa-star active"></i></li>
                                            <li><i class="fas fa-star active"></i></li>
                                            <li><i class="fas fa-star"></i></li>
                                            <li><i class="fas fa-star"></i></li>
                                        </ul>
                                    </div>
                                    <p class="product-title"><a href="javascript:">Butterflies Hand-held Bag</a></p>
                                    <p class="product-price">RS. 1,10,000</p>
                                </div>
                            </div> 
                            <div class="product-card">
                                <div class="card-image effect-hover-zoom">
                                    <a href="javascript:">
                                        <img data-src="img/products/product7.jpg" alt="Product-Image" class="lazyload animate-default">
                                    </a>
                                </div>
                                <div class="card-detail">
                                    <div class="rating">
                                        <ul class="list-unstyled side-listing">
                                            <li><i class="fas fa-star active"></i></li>
                                            <li><i class="fas fa-star active"></i></li>
                                            <li><i class="fas fa-star active"></i></li>
                                            <li><i class="fas fa-star"></i></li>
                                            <li><i class="fas fa-star"></i></li>
                                        </ul>
                                    </div>
                                    <p class="product-title"><a href="javascript:">Butterflies Hand-held Bag</a></p>
                                    <p class="product-price">RS. 1,10,000</p>
                                </div>
                            </div> 
                        </div>
                        <div class="item">
                            <div class="product-card">
                                <div class="card-image effect-hover-zoom">
                                    <a href="javascript:">
                                        <img data-src="img/products/product4.jpg" alt="Product-Image" class="lazyload animate-default">
                                    </a>
                                </div>
                                <div class="card-detail">
                                    <div class="rating">
                                        <ul class="list-unstyled side-listing">
                                            <li><i class="fas fa-star active"></i></li>
                                            <li><i class="fas fa-star active"></i></li>
                                            <li><i class="fas fa-star active"></i></li>
                                            <li><i class="fas fa-star"></i></li>
                                            <li><i class="fas fa-star"></i></li>
                                        </ul>
                                    </div>
                                    <p class="product-title"><a href="javascript:">Butterflies Hand-held Bag</a></p>
                                    <p class="product-price">RS. 1,10,000</p>
                                </div>
                            </div> 
                            <div class="product-card">
                                <div class="card-image effect-hover-zoom">
                                    <a href="javascript:">
                                        <img data-src="img/products/product1.jpg" alt="Product-Image" class="lazyload animate-default">
                                    </a>
                                </div>
                                <div class="card-detail">
                                    <div class="rating">
                                        <ul class="list-unstyled side-listing">
                                            <li><i class="fas fa-star active"></i></li>
                                            <li><i class="fas fa-star active"></i></li>
                                            <li><i class="fas fa-star active"></i></li>
                                            <li><i class="fas fa-star"></i></li>
                                            <li><i class="fas fa-star"></i></li>
                                        </ul>
                                    </div>
                                    <p class="product-title"><a href="javascript:">Butterflies Hand-held Bag</a></p>
                                    <p class="product-price">RS. 1,10,000</p>
                                </div>
                            </div> 
                        </div>
                        <div class="item">
                            <div class="product-card">
                                <div class="card-image effect-hover-zoom">
                                    <a href="javascript:">
                                        <img data-src="img/products/product5.jpg" alt="Product-Image" class="lazyload animate-default">
                                    </a>
                                </div>
                                <div class="card-detail">
                                    <div class="rating">
                                        <ul class="list-unstyled side-listing">
                                            <li><i class="fas fa-star active"></i></li>
                                            <li><i class="fas fa-star active"></i></li>
                                            <li><i class="fas fa-star active"></i></li>
                                            <li><i class="fas fa-star"></i></li>
                                            <li><i class="fas fa-star"></i></li>
                                        </ul>
                                    </div>
                                    <p class="product-title"><a href="javascript:">Butterflies Hand-held Bag</a></p>
                                    <p class="product-price">RS. 1,10,000</p>
                                </div>
                            </div> 
                            <div class="product-card">
                                <div class="card-image effect-hover-zoom">
                                    <a href="javascript:">
                                        <img data-src="img/products/product2.jpg" alt="Product-Image" class="lazyload animate-default">
                                    </a>
                                </div>
                                <div class="card-detail">
                                    <div class="rating">
                                        <ul class="list-unstyled side-listing">
                                            <li><i class="fas fa-star active"></i></li>
                                            <li><i class="fas fa-star active"></i></li>
                                            <li><i class="fas fa-star active"></i></li>
                                            <li><i class="fas fa-star"></i></li>
                                            <li><i class="fas fa-star"></i></li>
                                        </ul>
                                    </div>
                                    <p class="product-title"><a href="javascript:">Butterflies Hand-held Bag</a></p>
                                    <p class="product-price">RS. 1,10,000</p>
                                </div>
                            </div> 
                        </div>
                        <div class="item">
                            <div class="product-card">
                                <div class="card-image effect-hover-zoom">
                                    <a href="javascript:">
                                        <img data-src="img/products/product3.jpg" alt="Product-Image" class="lazyload animate-default">
                                    </a>
                                </div>
                                <div class="card-detail">
                                    <div class="rating">
                                        <ul class="list-unstyled side-listing">
                                            <li><i class="fas fa-star active"></i></li>
                                            <li><i class="fas fa-star active"></i></li>
                                            <li><i class="fas fa-star active"></i></li>
                                            <li><i class="fas fa-star"></i></li>
                                            <li><i class="fas fa-star"></i></li>
                                        </ul>
                                    </div>
                                    <p class="product-title"><a href="javascript:">Butterflies Hand-held Bag</a></p>
                                    <p class="product-price">RS. 1,10,000</p>
                                </div>
                            </div>

                            <div class="product-card">
                                <div class="card-image effect-hover-zoom">
                                    <a href="javascript:">
                                        <img data-src="img/products/product7.jpg" alt="Product-Image" class="lazyload animate-default">
                                    </a>
                                </div>
                                <div class="card-detail">
                                    <div class="rating">
                                        <ul class="list-unstyled side-listing">
                                            <li><i class="fas fa-star active"></i></li>
                                            <li><i class="fas fa-star active"></i></li>
                                            <li><i class="fas fa-star active"></i></li>
                                            <li><i class="fas fa-star"></i></li>
                                            <li><i class="fas fa-star"></i></li>
                                        </ul>
                                    </div>
                                    <p class="product-title"><a href="javascript:">Butterflies Hand-held Bag</a></p>
                                    <p class="product-price">RS. 1,10,000</p>
                                </div>
                            </div>

                        </div>
                    </div>
                </div> 
                <div class="tab-pane fade" id="hardware" role="tabpanel" aria-labelledby="hardware-tab"> 
                    <div class="owl-carousel owl-theme owl-nav-top"> 
                        <div class="item">
                            <div class="product-card">
                                <div class="card-image effect-hover-zoom">
                                    <a href="javascript:">
                                        <img data-src="img/products/product1.jpg" alt="Product-Image" class="lazyload animate-default">
                                    </a>
                                </div>
                                <div class="card-detail">
                                    <div class="rating">
                                        <ul class="list-unstyled side-listing">
                                            <li><i class="fas fa-star active"></i></li>
                                            <li><i class="fas fa-star active"></i></li>
                                            <li><i class="fas fa-star active"></i></li>
                                            <li><i class="fas fa-star"></i></li>
                                            <li><i class="fas fa-star"></i></li>
                                        </ul>
                                    </div>
                                    <p class="product-title"><a href="javascript:">Butterflies Hand-held Bag</a></p>
                                    <p class="product-price">RS. 1,10,000</p>
                                </div>
                            </div> 
                            <div class="product-card">
                                <div class="card-image effect-hover-zoom">
                                    <a href="javascript:">
                                        <img data-src="img/products/product6.jpg" alt="Product-Image" class="lazyload animate-default">
                                    </a>
                                </div>
                                <div class="card-detail">
                                    <div class="rating">
                                        <ul class="list-unstyled side-listing">
                                            <li><i class="fas fa-star active"></i></li>
                                            <li><i class="fas fa-star active"></i></li>
                                            <li><i class="fas fa-star active"></i></li>
                                            <li><i class="fas fa-star"></i></li>
                                            <li><i class="fas fa-star"></i></li>
                                        </ul>
                                    </div>
                                    <p class="product-title"><a href="javascript:">Butterflies Hand-held Bag</a></p>
                                    <p class="product-price">RS. 1,10,000</p>
                                </div>
                            </div> 
                        </div>
                        <div class="item">
                            <div class="product-card">
                                <div class="card-image effect-hover-zoom">
                                    <a href="javascript:">
                                        <img data-src="img/products/product2.jpg" alt="Product-Image" class="lazyload animate-default">
                                    </a>
                                </div>
                                <div class="card-detail">
                                    <div class="rating">
                                        <ul class="list-unstyled side-listing">
                                            <li><i class="fas fa-star active"></i></li>
                                            <li><i class="fas fa-star active"></i></li>
                                            <li><i class="fas fa-star active"></i></li>
                                            <li><i class="fas fa-star"></i></li>
                                            <li><i class="fas fa-star"></i></li>
                                        </ul>
                                    </div>
                                    <p class="product-title"><a href="javascript:">Butterflies Hand-held Bag</a></p>
                                    <p class="product-price">RS. 1,10,000</p>
                                </div>
                            </div> 
                            <div class="product-card">
                                <div class="card-image effect-hover-zoom">
                                    <a href="javascript:">
                                        <img data-src="img/products/product8.jpg" alt="Product-Image" class="lazyload animate-default">
                                    </a>
                                </div>
                                <div class="card-detail">
                                    <div class="rating">
                                        <ul class="list-unstyled side-listing">
                                            <li><i class="fas fa-star active"></i></li>
                                            <li><i class="fas fa-star active"></i></li>
                                            <li><i class="fas fa-star active"></i></li>
                                            <li><i class="fas fa-star"></i></li>
                                            <li><i class="fas fa-star"></i></li>
                                        </ul>
                                    </div>
                                    <p class="product-title"><a href="javascript:">Butterflies Hand-held Bag</a></p>
                                    <p class="product-price">RS. 1,10,000</p>
                                </div>
                            </div> 
                        </div>
                        <div class="item">
                            <div class="product-card">
                                <div class="card-image effect-hover-zoom">
                                    <a href="javascript:">
                                        <img data-src="img/products/product3.jpg" alt="Product-Image" class="lazyload animate-default">
                                    </a>
                                </div>
                                <div class="card-detail">
                                    <div class="rating">
                                        <ul class="list-unstyled side-listing">
                                            <li><i class="fas fa-star active"></i></li>
                                            <li><i class="fas fa-star active"></i></li>
                                            <li><i class="fas fa-star active"></i></li>
                                            <li><i class="fas fa-star"></i></li>
                                            <li><i class="fas fa-star"></i></li>
                                        </ul>
                                    </div>
                                    <p class="product-title"><a href="javascript:">Butterflies Hand-held Bag</a></p>
                                    <p class="product-price">RS. 1,10,000</p>
                                </div>
                            </div> 
                            <div class="product-card">
                                <div class="card-image effect-hover-zoom">
                                    <a href="javascript:">
                                        <img data-src="img/products/product9.webp" alt="Product-Image" class="lazyload animate-default">
                                    </a>
                                </div>
                                <div class="card-detail">
                                    <div class="rating">
                                        <ul class="list-unstyled side-listing">
                                            <li><i class="fas fa-star active"></i></li>
                                            <li><i class="fas fa-star active"></i></li>
                                            <li><i class="fas fa-star active"></i></li>
                                            <li><i class="fas fa-star"></i></li>
                                            <li><i class="fas fa-star"></i></li>
                                        </ul>
                                    </div>
                                    <p class="product-title"><a href="javascript:">Butterflies Hand-held Bag</a></p>
                                    <p class="product-price">RS. 1,10,000</p>
                                </div>
                            </div> 
                        </div>
                        <div class="item">
                            <div class="product-card">
                                <div class="card-image effect-hover-zoom">
                                    <a href="javascript:">
                                        <img data-src="img/products/product4.jpg" alt="Product-Image" class="lazyload animate-default">
                                    </a>
                                </div>
                                <div class="card-detail">
                                    <div class="rating">
                                        <ul class="list-unstyled side-listing">
                                            <li><i class="fas fa-star active"></i></li>
                                            <li><i class="fas fa-star active"></i></li>
                                            <li><i class="fas fa-star active"></i></li>
                                            <li><i class="fas fa-star"></i></li>
                                            <li><i class="fas fa-star"></i></li>
                                        </ul>
                                    </div>
                                    <p class="product-title"><a href="javascript:">Butterflies Hand-held Bag</a></p>
                                    <p class="product-price">RS. 1,10,000</p>
                                </div>
                            </div> 
                            <div class="product-card">
                                <div class="card-image effect-hover-zoom">
                                    <a href="javascript:">
                                        <img data-src="img/products/product1.jpg" alt="Product-Image" class="lazyload animate-default">
                                    </a>
                                </div>
                                <div class="card-detail">
                                    <div class="rating">
                                        <ul class="list-unstyled side-listing">
                                            <li><i class="fas fa-star active"></i></li>
                                            <li><i class="fas fa-star active"></i></li>
                                            <li><i class="fas fa-star active"></i></li>
                                            <li><i class="fas fa-star"></i></li>
                                            <li><i class="fas fa-star"></i></li>
                                        </ul>
                                    </div>
                                    <p class="product-title"><a href="javascript:">Butterflies Hand-held Bag</a></p>
                                    <p class="product-price">RS. 1,10,000</p>
                                </div>
                            </div> 
                        </div>
                        <div class="item">
                            <div class="product-card">
                                <div class="card-image effect-hover-zoom">
                                    <a href="javascript:">
                                        <img data-src="img/products/product5.jpg" alt="Product-Image" class="lazyload animate-default">
                                    </a>
                                </div>
                                <div class="card-detail">
                                    <div class="rating">
                                        <ul class="list-unstyled side-listing">
                                            <li><i class="fas fa-star active"></i></li>
                                            <li><i class="fas fa-star active"></i></li>
                                            <li><i class="fas fa-star active"></i></li>
                                            <li><i class="fas fa-star"></i></li>
                                            <li><i class="fas fa-star"></i></li>
                                        </ul>
                                    </div>
                                    <p class="product-title"><a href="javascript:">Butterflies Hand-held Bag</a></p>
                                    <p class="product-price">RS. 1,10,000</p>
                                </div>
                            </div> 
                            <div class="product-card">
                                <div class="card-image effect-hover-zoom">
                                    <a href="javascript:">
                                        <img data-src="img/products/product2.jpg" alt="Product-Image" class="lazyload animate-default">
                                    </a>
                                </div>
                                <div class="card-detail">
                                    <div class="rating">
                                        <ul class="list-unstyled side-listing">
                                            <li><i class="fas fa-star active"></i></li>
                                            <li><i class="fas fa-star active"></i></li>
                                            <li><i class="fas fa-star active"></i></li>
                                            <li><i class="fas fa-star"></i></li>
                                            <li><i class="fas fa-star"></i></li>
                                        </ul>
                                    </div>
                                    <p class="product-title"><a href="javascript:">Butterflies Hand-held Bag</a></p>
                                    <p class="product-price">RS. 1,10,000</p>
                                </div>
                            </div> 
                        </div>
                        <div class="item">
                            <div class="product-card">
                                <div class="card-image effect-hover-zoom">
                                    <a href="javascript:">
                                        <img data-src="img/products/product3.jpg" alt="Product-Image" class="lazyload animate-default">
                                    </a>
                                </div>
                                <div class="card-detail">
                                    <div class="rating">
                                        <ul class="list-unstyled side-listing">
                                            <li><i class="fas fa-star active"></i></li>
                                            <li><i class="fas fa-star active"></i></li>
                                            <li><i class="fas fa-star active"></i></li>
                                            <li><i class="fas fa-star"></i></li>
                                            <li><i class="fas fa-star"></i></li>
                                        </ul>
                                    </div>
                                    <p class="product-title"><a href="javascript:">Butterflies Hand-held Bag</a></p>
                                    <p class="product-price">RS. 1,10,000</p>
                                </div>
                            </div> 
                            <div class="product-card">
                                <div class="card-image effect-hover-zoom">
                                    <a href="javascript:">
                                        <img data-src="img/products/product7.jpg" alt="Product-Image" class="lazyload animate-default">
                                    </a>
                                </div>
                                <div class="card-detail">
                                    <div class="rating">
                                        <ul class="list-unstyled side-listing">
                                            <li><i class="fas fa-star active"></i></li>
                                            <li><i class="fas fa-star active"></i></li>
                                            <li><i class="fas fa-star active"></i></li>
                                            <li><i class="fas fa-star"></i></li>
                                            <li><i class="fas fa-star"></i></li>
                                        </ul>
                                    </div>
                                    <p class="product-title"><a href="javascript:">Butterflies Hand-held Bag</a></p>
                                    <p class="product-price">RS. 1,10,000</p>
                                </div>
                            </div> 
                        </div>
                        <div class="item">
                            <div class="product-card">
                                <div class="card-image effect-hover-zoom">
                                    <a href="javascript:">
                                        <img data-src="img/products/product4.jpg" alt="Product-Image" class="lazyload animate-default">
                                    </a>
                                </div>
                                <div class="card-detail">
                                    <div class="rating">
                                        <ul class="list-unstyled side-listing">
                                            <li><i class="fas fa-star active"></i></li>
                                            <li><i class="fas fa-star active"></i></li>
                                            <li><i class="fas fa-star active"></i></li>
                                            <li><i class="fas fa-star"></i></li>
                                            <li><i class="fas fa-star"></i></li>
                                        </ul>
                                    </div>
                                    <p class="product-title"><a href="javascript:">Butterflies Hand-held Bag</a></p>
                                    <p class="product-price">RS. 1,10,000</p>
                                </div>
                            </div> 
                            <div class="product-card">
                                <div class="card-image effect-hover-zoom">
                                    <a href="javascript:">
                                        <img data-src="img/products/product1.jpg" alt="Product-Image" class="lazyload animate-default">
                                    </a>
                                </div>
                                <div class="card-detail">
                                    <div class="rating">
                                        <ul class="list-unstyled side-listing">
                                            <li><i class="fas fa-star active"></i></li>
                                            <li><i class="fas fa-star active"></i></li>
                                            <li><i class="fas fa-star active"></i></li>
                                            <li><i class="fas fa-star"></i></li>
                                            <li><i class="fas fa-star"></i></li>
                                        </ul>
                                    </div>
                                    <p class="product-title"><a href="javascript:">Butterflies Hand-held Bag</a></p>
                                    <p class="product-price">RS. 1,10,000</p>
                                </div>
                            </div> 
                        </div>
                        <div class="item">
                            <div class="product-card">
                                <div class="card-image effect-hover-zoom">
                                    <a href="javascript:">
                                        <img data-src="img/products/product5.jpg" alt="Product-Image" class="lazyload animate-default">
                                    </a>
                                </div>
                                <div class="card-detail">
                                    <div class="rating">
                                        <ul class="list-unstyled side-listing">
                                            <li><i class="fas fa-star active"></i></li>
                                            <li><i class="fas fa-star active"></i></li>
                                            <li><i class="fas fa-star active"></i></li>
                                            <li><i class="fas fa-star"></i></li>
                                            <li><i class="fas fa-star"></i></li>
                                        </ul>
                                    </div>
                                    <p class="product-title"><a href="javascript:">Butterflies Hand-held Bag</a></p>
                                    <p class="product-price">RS. 1,10,000</p>
                                </div>
                            </div> 
                            <div class="product-card">
                                <div class="card-image effect-hover-zoom">
                                    <a href="javascript:">
                                        <img data-src="img/products/product2.jpg" alt="Product-Image" class="lazyload animate-default">
                                    </a>
                                </div>
                                <div class="card-detail">
                                    <div class="rating">
                                        <ul class="list-unstyled side-listing">
                                            <li><i class="fas fa-star active"></i></li>
                                            <li><i class="fas fa-star active"></i></li>
                                            <li><i class="fas fa-star active"></i></li>
                                            <li><i class="fas fa-star"></i></li>
                                            <li><i class="fas fa-star"></i></li>
                                        </ul>
                                    </div>
                                    <p class="product-title"><a href="javascript:">Butterflies Hand-held Bag</a></p>
                                    <p class="product-price">RS. 1,10,000</p>
                                </div>
                            </div> 
                        </div>
                        <div class="item">
                            <div class="product-card">
                                <div class="card-image effect-hover-zoom">
                                    <a href="javascript:">
                                        <img data-src="img/products/product3.jpg" alt="Product-Image" class="lazyload animate-default">
                                    </a>
                                </div>
                                <div class="card-detail">
                                    <div class="rating">
                                        <ul class="list-unstyled side-listing">
                                            <li><i class="fas fa-star active"></i></li>
                                            <li><i class="fas fa-star active"></i></li>
                                            <li><i class="fas fa-star active"></i></li>
                                            <li><i class="fas fa-star"></i></li>
                                            <li><i class="fas fa-star"></i></li>
                                        </ul>
                                    </div>
                                    <p class="product-title"><a href="javascript:">Butterflies Hand-held Bag</a></p>
                                    <p class="product-price">RS. 1,10,000</p>
                                </div>
                            </div> 
                            <div class="product-card">
                                <div class="card-image effect-hover-zoom">
                                    <a href="javascript:">
                                        <img data-src="img/products/product7.jpg" alt="Product-Image" class="lazyload animate-default">
                                    </a>
                                </div>
                                <div class="card-detail">
                                    <div class="rating">
                                        <ul class="list-unstyled side-listing">
                                            <li><i class="fas fa-star active"></i></li>
                                            <li><i class="fas fa-star active"></i></li>
                                            <li><i class="fas fa-star active"></i></li>
                                            <li><i class="fas fa-star"></i></li>
                                            <li><i class="fas fa-star"></i></li>
                                        </ul>
                                    </div>
                                    <p class="product-title"><a href="javascript:">Butterflies Hand-held Bag</a></p>
                                    <p class="product-price">RS. 1,10,000</p>
                                </div>
                            </div> 
                        </div>
                    </div>
                </div> 
                <div class="tab-pane fade" id="makeup-app" role="tabpanel" aria-labelledby="makeup-app-tab"> 
                    <div class="owl-carousel owl-theme owl-nav-top"> 
                        <div class="item">
                            <div class="product-card">
                                <div class="card-image effect-hover-zoom">
                                    <a href="javascript:">
                                        <img data-src="img/products/product1.jpg" alt="Product-Image" class="lazyload animate-default">
                                    </a>
                                </div>
                                <div class="card-detail">
                                    <div class="rating">
                                        <ul class="list-unstyled side-listing">
                                            <li><i class="fas fa-star active"></i></li>
                                            <li><i class="fas fa-star active"></i></li>
                                            <li><i class="fas fa-star active"></i></li>
                                            <li><i class="fas fa-star"></i></li>
                                            <li><i class="fas fa-star"></i></li>
                                        </ul>
                                    </div>
                                    <p class="product-title"><a href="javascript:">Butterflies Hand-held Bag</a></p>
                                    <p class="product-price">RS. 1,10,000</p>
                                </div>
                            </div> 
                            <div class="product-card">
                                <div class="card-image effect-hover-zoom">
                                    <a href="javascript:">
                                        <img data-src="img/products/product6.jpg" alt="Product-Image" class="lazyload animate-default">
                                    </a>
                                </div>
                                <div class="card-detail">
                                    <div class="rating">
                                        <ul class="list-unstyled side-listing">
                                            <li><i class="fas fa-star active"></i></li>
                                            <li><i class="fas fa-star active"></i></li>
                                            <li><i class="fas fa-star active"></i></li>
                                            <li><i class="fas fa-star"></i></li>
                                            <li><i class="fas fa-star"></i></li>
                                        </ul>
                                    </div>
                                    <p class="product-title"><a href="javascript:">Butterflies Hand-held Bag</a></p>
                                    <p class="product-price">RS. 1,10,000</p>
                                </div>
                            </div> 
                        </div>
                        <div class="item">
                            <div class="product-card">
                                <div class="card-image effect-hover-zoom">
                                    <a href="javascript:">
                                        <img data-src="img/products/product2.jpg" alt="Product-Image" class="lazyload animate-default">
                                    </a>
                                </div>
                                <div class="card-detail">
                                    <div class="rating">
                                        <ul class="list-unstyled side-listing">
                                            <li><i class="fas fa-star active"></i></li>
                                            <li><i class="fas fa-star active"></i></li>
                                            <li><i class="fas fa-star active"></i></li>
                                            <li><i class="fas fa-star"></i></li>
                                            <li><i class="fas fa-star"></i></li>
                                        </ul>
                                    </div>
                                    <p class="product-title"><a href="javascript:">Butterflies Hand-held Bag</a></p>
                                    <p class="product-price">RS. 1,10,000</p>
                                </div>
                            </div> 
                            <div class="product-card">
                                <div class="card-image effect-hover-zoom">
                                    <a href="javascript:">
                                        <img data-src="img/products/product8.jpg" alt="Product-Image" class="lazyload animate-default">
                                    </a>
                                </div>
                                <div class="card-detail">
                                    <div class="rating">
                                        <ul class="list-unstyled side-listing">
                                            <li><i class="fas fa-star active"></i></li>
                                            <li><i class="fas fa-star active"></i></li>
                                            <li><i class="fas fa-star active"></i></li>
                                            <li><i class="fas fa-star"></i></li>
                                            <li><i class="fas fa-star"></i></li>
                                        </ul>
                                    </div>
                                    <p class="product-title"><a href="javascript:">Butterflies Hand-held Bag</a></p>
                                    <p class="product-price">RS. 1,10,000</p>
                                </div>
                            </div> 
                        </div>
                        <div class="item">
                            <div class="product-card">
                                <div class="card-image effect-hover-zoom">
                                    <a href="javascript:">
                                        <img data-src="img/products/product3.jpg" alt="Product-Image" class="lazyload animate-default">
                                    </a>
                                </div>
                                <div class="card-detail">
                                    <div class="rating">
                                        <ul class="list-unstyled side-listing">
                                            <li><i class="fas fa-star active"></i></li>
                                            <li><i class="fas fa-star active"></i></li>
                                            <li><i class="fas fa-star active"></i></li>
                                            <li><i class="fas fa-star"></i></li>
                                            <li><i class="fas fa-star"></i></li>
                                        </ul>
                                    </div>
                                    <p class="product-title"><a href="javascript:">Butterflies Hand-held Bag</a></p>
                                    <p class="product-price">RS. 1,10,000</p>
                                </div>
                            </div> 
                            <div class="product-card">
                                <div class="card-image effect-hover-zoom">
                                    <a href="javascript:">
                                        <img data-src="img/products/product9.webp" alt="Product-Image" class="lazyload animate-default">
                                    </a>
                                </div>
                                <div class="card-detail">
                                    <div class="rating">
                                        <ul class="list-unstyled side-listing">
                                            <li><i class="fas fa-star active"></i></li>
                                            <li><i class="fas fa-star active"></i></li>
                                            <li><i class="fas fa-star active"></i></li>
                                            <li><i class="fas fa-star"></i></li>
                                            <li><i class="fas fa-star"></i></li>
                                        </ul>
                                    </div>
                                    <p class="product-title"><a href="javascript:">Butterflies Hand-held Bag</a></p>
                                    <p class="product-price">RS. 1,10,000</p>
                                </div>
                            </div> 
                        </div>
                        <div class="item">
                            <div class="product-card">
                                <div class="card-image effect-hover-zoom">
                                    <a href="javascript:">
                                        <img data-src="img/products/product4.jpg" alt="Product-Image" class="lazyload animate-default">
                                    </a>
                                </div>
                                <div class="card-detail">
                                    <div class="rating">
                                        <ul class="list-unstyled side-listing">
                                            <li><i class="fas fa-star active"></i></li>
                                            <li><i class="fas fa-star active"></i></li>
                                            <li><i class="fas fa-star active"></i></li>
                                            <li><i class="fas fa-star"></i></li>
                                            <li><i class="fas fa-star"></i></li>
                                        </ul>
                                    </div>
                                    <p class="product-title"><a href="javascript:">Butterflies Hand-held Bag</a></p>
                                    <p class="product-price">RS. 1,10,000</p>
                                </div>
                            </div> 
                            <div class="product-card">
                                <div class="card-image effect-hover-zoom">
                                    <a href="javascript:">
                                        <img data-src="img/products/product1.jpg" alt="Product-Image" class="lazyload animate-default">
                                    </a>
                                </div>
                                <div class="card-detail">
                                    <div class="rating">
                                        <ul class="list-unstyled side-listing">
                                            <li><i class="fas fa-star active"></i></li>
                                            <li><i class="fas fa-star active"></i></li>
                                            <li><i class="fas fa-star active"></i></li>
                                            <li><i class="fas fa-star"></i></li>
                                            <li><i class="fas fa-star"></i></li>
                                        </ul>
                                    </div>
                                    <p class="product-title"><a href="javascript:">Butterflies Hand-held Bag</a></p>
                                    <p class="product-price">RS. 1,10,000</p>
                                </div>
                            </div> 
                        </div>
                        <div class="item">
                            <div class="product-card">
                                <div class="card-image effect-hover-zoom">
                                    <a href="javascript:">
                                        <img data-src="img/products/product5.jpg" alt="Product-Image" class="lazyload animate-default">
                                    </a>
                                </div>
                                <div class="card-detail">
                                    <div class="rating">
                                        <ul class="list-unstyled side-listing">
                                            <li><i class="fas fa-star active"></i></li>
                                            <li><i class="fas fa-star active"></i></li>
                                            <li><i class="fas fa-star active"></i></li>
                                            <li><i class="fas fa-star"></i></li>
                                            <li><i class="fas fa-star"></i></li>
                                        </ul>
                                    </div>
                                    <p class="product-title"><a href="javascript:">Butterflies Hand-held Bag</a></p>
                                    <p class="product-price">RS. 1,10,000</p>
                                </div>
                            </div> 
                            <div class="product-card">
                                <div class="card-image effect-hover-zoom">
                                    <a href="javascript:">
                                        <img data-src="img/products/product2.jpg" alt="Product-Image" class="lazyload animate-default">
                                    </a>
                                </div>
                                <div class="card-detail">
                                    <div class="rating">
                                        <ul class="list-unstyled side-listing">
                                            <li><i class="fas fa-star active"></i></li>
                                            <li><i class="fas fa-star active"></i></li>
                                            <li><i class="fas fa-star active"></i></li>
                                            <li><i class="fas fa-star"></i></li>
                                            <li><i class="fas fa-star"></i></li>
                                        </ul>
                                    </div>
                                    <p class="product-title"><a href="javascript:">Butterflies Hand-held Bag</a></p>
                                    <p class="product-price">RS. 1,10,000</p>
                                </div>
                            </div> 
                        </div>
                        <div class="item">
                            <div class="product-card">
                                <div class="card-image effect-hover-zoom">
                                    <a href="javascript:">
                                        <img data-src="img/products/product3.jpg" alt="Product-Image" class="lazyload animate-default">
                                    </a>
                                </div>
                                <div class="card-detail">
                                    <div class="rating">
                                        <ul class="list-unstyled side-listing">
                                            <li><i class="fas fa-star active"></i></li>
                                            <li><i class="fas fa-star active"></i></li>
                                            <li><i class="fas fa-star active"></i></li>
                                            <li><i class="fas fa-star"></i></li>
                                            <li><i class="fas fa-star"></i></li>
                                        </ul>
                                    </div>
                                    <p class="product-title"><a href="javascript:">Butterflies Hand-held Bag</a></p>
                                    <p class="product-price">RS. 1,10,000</p>
                                </div>
                            </div> 
                            <div class="product-card">
                                <div class="card-image effect-hover-zoom">
                                    <a href="javascript:">
                                        <img data-src="img/products/product7.jpg" alt="Product-Image" class="lazyload animate-default">
                                    </a>
                                </div>
                                <div class="card-detail">
                                    <div class="rating">
                                        <ul class="list-unstyled side-listing">
                                            <li><i class="fas fa-star active"></i></li>
                                            <li><i class="fas fa-star active"></i></li>
                                            <li><i class="fas fa-star active"></i></li>
                                            <li><i class="fas fa-star"></i></li>
                                            <li><i class="fas fa-star"></i></li>
                                        </ul>
                                    </div>
                                    <p class="product-title"><a href="javascript:">Butterflies Hand-held Bag</a></p>
                                    <p class="product-price">RS. 1,10,000</p>
                                </div>
                            </div> 
                        </div>
                        <div class="item">
                            <div class="product-card">
                                <div class="card-image effect-hover-zoom">
                                    <a href="javascript:">
                                        <img data-src="img/products/product4.jpg" alt="Product-Image" class="lazyload animate-default">
                                    </a>
                                </div>
                                <div class="card-detail">
                                    <div class="rating">
                                        <ul class="list-unstyled side-listing">
                                            <li><i class="fas fa-star active"></i></li>
                                            <li><i class="fas fa-star active"></i></li>
                                            <li><i class="fas fa-star active"></i></li>
                                            <li><i class="fas fa-star"></i></li>
                                            <li><i class="fas fa-star"></i></li>
                                        </ul>
                                    </div>
                                    <p class="product-title"><a href="javascript:">Butterflies Hand-held Bag</a></p>
                                    <p class="product-price">RS. 1,10,000</p>
                                </div>
                            </div> 
                            <div class="product-card">
                                <div class="card-image effect-hover-zoom">
                                    <a href="javascript:">
                                        <img data-src="img/products/product1.jpg" alt="Product-Image" class="lazyload animate-default">
                                    </a>
                                </div>
                                <div class="card-detail">
                                    <div class="rating">
                                        <ul class="list-unstyled side-listing">
                                            <li><i class="fas fa-star active"></i></li>
                                            <li><i class="fas fa-star active"></i></li>
                                            <li><i class="fas fa-star active"></i></li>
                                            <li><i class="fas fa-star"></i></li>
                                            <li><i class="fas fa-star"></i></li>
                                        </ul>
                                    </div>
                                    <p class="product-title"><a href="javascript:">Butterflies Hand-held Bag</a></p>
                                    <p class="product-price">RS. 1,10,000</p>
                                </div>
                            </div> 
                        </div>
                        <div class="item">
                            <div class="product-card">
                                <div class="card-image effect-hover-zoom">
                                    <a href="javascript:">
                                        <img data-src="img/products/product5.jpg" alt="Product-Image" class="lazyload animate-default">
                                    </a>
                                </div>
                                <div class="card-detail">
                                    <div class="rating">
                                        <ul class="list-unstyled side-listing">
                                            <li><i class="fas fa-star active"></i></li>
                                            <li><i class="fas fa-star active"></i></li>
                                            <li><i class="fas fa-star active"></i></li>
                                            <li><i class="fas fa-star"></i></li>
                                            <li><i class="fas fa-star"></i></li>
                                        </ul>
                                    </div>
                                    <p class="product-title"><a href="javascript:">Butterflies Hand-held Bag</a></p>
                                    <p class="product-price">RS. 1,10,000</p>
                                </div>
                            </div> 
                            <div class="product-card">
                                <div class="card-image effect-hover-zoom">
                                    <a href="javascript:">
                                        <img data-src="img/products/product2.jpg" alt="Product-Image" class="lazyload animate-default">
                                    </a>
                                </div>
                                <div class="card-detail">
                                    <div class="rating">
                                        <ul class="list-unstyled side-listing">
                                            <li><i class="fas fa-star active"></i></li>
                                            <li><i class="fas fa-star active"></i></li>
                                            <li><i class="fas fa-star active"></i></li>
                                            <li><i class="fas fa-star"></i></li>
                                            <li><i class="fas fa-star"></i></li>
                                        </ul>
                                    </div>
                                    <p class="product-title"><a href="javascript:">Butterflies Hand-held Bag</a></p>
                                    <p class="product-price">RS. 1,10,000</p>
                                </div>
                            </div> 
                        </div>
                        <div class="item">
                            <div class="product-card">
                                <div class="card-image effect-hover-zoom">
                                    <a href="javascript:">
                                        <img data-src="img/products/product3.jpg" alt="Product-Image" class="lazyload animate-default">
                                    </a>
                                </div>
                                <div class="card-detail">
                                    <div class="rating">
                                        <ul class="list-unstyled side-listing">
                                            <li><i class="fas fa-star active"></i></li>
                                            <li><i class="fas fa-star active"></i></li>
                                            <li><i class="fas fa-star active"></i></li>
                                            <li><i class="fas fa-star"></i></li>
                                            <li><i class="fas fa-star"></i></li>
                                        </ul>
                                    </div>
                                    <p class="product-title"><a href="javascript:">Butterflies Hand-held Bag</a></p>
                                    <p class="product-price">RS. 1,10,000</p>
                                </div>
                            </div> 
                            <div class="product-card">
                                <div class="card-image effect-hover-zoom">
                                    <a href="javascript:">
                                        <img data-src="img/products/product7.jpg" alt="Product-Image" class="lazyload animate-default">
                                    </a>
                                </div>
                                <div class="card-detail">
                                    <div class="rating">
                                        <ul class="list-unstyled side-listing">
                                            <li><i class="fas fa-star active"></i></li>
                                            <li><i class="fas fa-star active"></i></li>
                                            <li><i class="fas fa-star active"></i></li>
                                            <li><i class="fas fa-star"></i></li>
                                            <li><i class="fas fa-star"></i></li>
                                        </ul>
                                    </div>
                                    <p class="product-title"><a href="javascript:">Butterflies Hand-held Bag</a></p>
                                    <p class="product-price">RS. 1,10,000</p>
                                </div>
                            </div> 
                        </div>
                    </div>
                </div> 
            </div>
        </div> 
    </div>
</section>

<!-- Fashion-category -->


<?php include('include/index-main.php'); ?>

<?php include('include/footer.php'); ?>
