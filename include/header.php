<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Alpasal | Best Online Experice</title>
    <link rel="stylesheet" href="css/bootstrap.min.css" type="text/css">
    <link rel="stylesheet" href="css/jquery-ui.css" type="text/css">
    <link rel="stylesheet" href="include/font-awesome/css/all.css" type="text/css">
    <link rel="stylesheet" href="css/normalize.css" type="text/css">
    <link rel="stylesheet" href="css/v4-shims.css" type="text/css">
    <link rel="stylesheet" href="css/owl-carousel.min.css" type="text/css">
    <link rel="stylesheet" href="css/xzoom.css" type="text/css">
    <link rel="stylesheet" href="include/select2/css/select2.min.css">
    <link rel="stylesheet" type="text/css" href="css/datatables.min.css"/>
    <link rel="stylesheet" href="css/effect.css" type="text/css">
    <link rel="stylesheet" href="css/main.css" type="text/css">
</head>
<body>

<div class="header-wrapper sticky-top">
    <div class="top-header">
        <div class="container-fluid clearfix">
            <div class="float-right">
                <ul class="top-links list-unstyled side-listing">
                    <li><a href="javascript:">Get the App</a></li>
                    <li><a href="javascript:">Feature your Business</a></li>
                    <li class="dropdown">
                        <a href="javascript:" class="dropdown-toggle">Customer Care</a>
                        <div class="dropdown-pannel">
                            <div class="dropdown-content">
                                <a href="javascript:" class="dropdown-item"><i class="fas fa-user-tie"></i> Help Center</a>
                                <a href="javascript:" class="dropdown-item"><i class="fas fa-box-open"></i> Order</a>
                                <a href="javascript:" class="dropdown-item"><i class="fas fa-shipping-fast"></i> Shipping & Delivery</a>
                                <a href="javascript:" class="dropdown-item"><i class="fas fa-credit-card"></i> Payment</a>
                                <a href="javascript:" class="dropdown-item"><i class="fas fa-hand-holding-usd"></i> Return & Refund</a>
                            </div>
                        </div>
                    </li>
                    <li class="dropdown">
                        <a href="javascript:" class="dropdown-toggle">Track my Order</a>
                        <div class="dropdown-pannel dropdown-large">
                            <div class="dropdown-content p-4">
                                <p class="drop-title">Track My Order</p>
                                <form action="track-order">
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <label for="orderEmail">Pleaes confirm your email:</label>
                                                <input type="email" class="form-control" id="orderEmail" placeholder="abc@happylife.com">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <label for="orderNumber">Your Order Number:</label>
                                                <input type="text" class="form-control" id="orderNumber" placeholder="000-000-000">
                                            </div>
                                        </div>
                                    </div>
                                    <label>For any other inquiries, <a href="javascript:">Click Here</a></label>
                                    <button type="submit" class="btn-main w-100">Submit</button>
                                </form>
                            </div>
                        </div>
                    </li>
                    <li>
                        <span> <i class="fas fa-user"></i> <a href="login.php">Login</a> </span> /
                        <span> <a href="register.php">Register</a> </span>
                    </li>
                </ul>
            </div>
        </div>
    </div>

    <header class="header-main sticky-top">
        <div class="main-header">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-2">
                        <a class="navbar-brand" href="index.php">
                            <img src="img/logo.png" alt="LOGO">
                        </a>
                    </div>
                    <div class="col-lg-7 col-sm-8">
                        <form action="search">
                            <div class="search-bar">
                                <div class="search-addon"><i class="fas fa-search"></i></div>
                                <input type="text" placeholder="Are you looking for something?">
                                <button type="submit">Search</button>
                            </div>
                        </form>
                    </div>
                    <div class="col-lg-3 col-sm-4">
                        <div class="d-flex justify-content-end align-items-center h-100 notifi-prom"> 
                            <div class="head-notification hover-li-noti dropdown">
                                <a href="#" class="notification-a-header dropdown-toggle" id="dropdownMenuButton" data-toggle="dropdown">
                                    <i class="far fa-bell"></i>
                                </a>
                                <span class="cart-length">0</span>
                                <div class="notification-dropdown hover-dis-a-noti dropdown-menu">
                                    <div class="arrow-up"></div>
                                    <ul class="noti-hover-display-ul">
                                        <li class="noti-title-div">
                                            <h5>Notifications</h5>
                                            <a href="">Mark All As Read</a>
                                        </li>
                                        <ul class="noti-hover-display-ul noti-height-dis">
                                            <!-- <li class="unread-noti no-noti"> 
                                                <p>
                                                    No Notifications
                                                </p>  
                                            </li> -->
                                            <li class="unread-noti">
                                                <a href="#">
                                                    <img src="img/user/user-profile.jpg" class="img-fluid noti-side-img" alt="">
                                                    <p> 
                                                        Lorem ipsum elit dolor sit amet, cons ectetur dolor sit amet.
                                                        <span><i class="far fa-clock"></i> 1 min ago</span>
                                                    </p>
                                                     
                                                </a>
                                            </li>
                                            <li class="read-noti">
                                                <a href="#">
                                                    <img src="img/user/user-profile.jpg" class="img-fluid noti-side-img" alt="">
                                                    <p>
                                                        Lorem ipsum dolor sit amet, cons ectetur adipisicing elit dolor sit amet, cons ectetur conse ctetur adipisicing elit.
                                                        <span><i class="far fa-clock"></i> 1 min ago</span>
                                                    </p>  
                                                </a>
                                            </li>
                                            <li class="unread-noti">
                                                <a href="#">
                                                    <img src="img/user/user-profile.jpg" class="img-fluid noti-side-img" alt="">
                                                    <p>
                                                        Lorem ipsum dolor sit amet, cons ectetur adipisicing elit dolor sit amet, cons ecteturcons ectetur.
                                                        <span><i class="far fa-clock"></i> 1 min ago</span>
                                                    </p> 
                                                </a>
                                            </li>
                                            <li class="read-noti">
                                                <a href="#">
                                                    <img src="img/user/user-profile.jpg" class="img-fluid noti-side-img" alt="">
                                                    <p>
                                                        Lorem ipsum dolor sit amet, cons ectetur adipisicing elit dolor sit amet, cons ectetur adipisicing elit.
                                                        <span><i class="far fa-clock"></i> 1 min ago</span>
                                                    </p>
                                                    <span><i class="far fa-clock"></i> 1 min ago</span> 
                                                </a>
                                            </li>
                                            <li class="unread-noti">
                                                <a href="#">
                                                    <img src="img/user/user-profile.jpg" class="img-fluid noti-side-img" alt="">
                                                    <p>
                                                        Lorem ipsum dolor sit amet, consec tetur adipisicing elit.
                                                        <span><i class="far fa-clock"></i> 1 min ago</span>
                                                    </p>  
                                                </a>
                                            </li>
                                            <li class="unread-noti">
                                                <a href="#">
                                                    <img src="img/user/user-profile.jpg" class="img-fluid noti-side-img" alt="">
                                                    <p>
                                                        Lorem ipsum dolor sit amet, consec tetur adipisicing elit.
                                                        <span><i class="far fa-clock"></i> 1 min ago</span>
                                                    </p>  
                                                </a>
                                            </li>
                                            <li class="unread-noti">
                                                <a href="#">
                                                    <img src="img/user/user-profile.jpg" class="img-fluid noti-side-img" alt="">
                                                    <p>
                                                        Lorem ipsum dolor sit amet, consec tetur adipisicing elit.
                                                        <span><i class="far fa-clock"></i> 1 min ago</span>
                                                    </p> 
                                                </a>
                                            </li>
                                            <li class="unread-noti">
                                                <a href="#">
                                                    <img src="img/user/user-profile.jpg" class="img-fluid noti-side-img" alt="">
                                                    <p>
                                                        Lorem ipsum dolor sit amet, consec tetur adipisicing elit.
                                                        <span><i class="far fa-clock"></i> 1 min ago</span>
                                                    </p>   
                                                </a>
                                            </li>
                                        </ul>
                                        <li class="noti-title-div-see"> 
                                            <a href="">See All <i class="fas fa-angle-right"></i></a>
                                        </li>
                                    </ul>
                                </div>
                            </div> 
                            <div class="promoter-div-btn">
                                <a href="promoter-register.php" class="btn promoter-div-a">
                                    BECOME A PROMOTER
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <nav class="navbar navbar-expand-lg navbar-dark">
            <div class="container-fluid">
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                </button>
                <div class="collapse navbar-collapse" id="navbarNav">
                    <ul class="navbar-nav top-nav-menu">
                        <li class="nav-item nav-item-main dropdown dropdown-li hover-li-nav">
                            <a class="nav-link nav-first dropdown-toggle" role="button" aria-haspopup="true" aria-expanded="false" id="dLabel" data-toggle="dropdown">
                                <i class="fas fa-list"></i> Categories
                            </a>
                            <?php include('head-megamenu.php'); ?>
                        </li>
                        <li class="nav-item nav-item-main">
                            <a class="nav-link" href="#">Home</a>
                        </li>
                        <li class="nav-item nav-item-main">
                            <a class="nav-link" href="sell.php">Sell on Alpasal</a>
                        </li>
                        <li class="nav-item nav-item-main">
                            <a class="nav-link" href="#">Track Order</a>
                        </li>
                        <li class="nav-item nav-item-main">
                            <a class="nav-link" href="about.php">About Alpasal</a>
                        </li>
                        <li class="nav-item nav-item-main">
                            <a class="nav-link" href="contact.php">Contact Us</a>
                        </li>
                    </ul>
                </div>
            </div>
        </nav>
    </header>
</div>