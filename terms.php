<?php include('include/header.php'); ?>

<!----------------------------
-------Breadcrumb-------
----------------------------->

<nav aria-label="breadcrumb" class="breadcrumb-main bg-para" style="background: linear-gradient(rgba(0,0,0,0.4),rgba(0,0,0,0.4)), url('img/bread.jpg');">
    <div class="container clearfix"> <!-- Container .// -->
        <h3 class="float-left">Terms & Conditions</h3>
        <ol class="breadcrumb float-right">
            <li class="breadcrumb-item"><a href="index.php">Home</a></li>
            <!-- <li class="breadcrumb-item"><a href="#">Library</a></li> -->
            <li class="breadcrumb-item active" aria-current="page">Terms</li>
        </ol>
    </div> <!-- Container .// -->
</nav>

<!----------------------------
-------Breadcrumb-------
----------------------------->

<!----------------------------
------Terms-&-Conditions------
----------------------------->

<section class="terms common-padding">
    <div class="container"> <!-- Container .// -->
        <h3 class="section-title">Terms & Conditions</h3>

        <div class="box-modal"> <!-- Box-Modal .// -->

            <div class="term-group"> <!-- Term-Group .// -->
                <h4 class="page-title">WHAT INFORMATION DO WE COLLECT?</h4>
                <p class="normal-content">We collect your information and details through online registration form on the site. During registration we collect your name, e-mail id, mailing address, phone number, and other information. If you are owner, then we collect your property details for rent or sale purpose.</p>
                <p class="normal-content">We collect user experience information of registered as well as anonymous use. Like many websites, we use cookies to enhance your user experience and gather information about visitors who visits to hamrogharbhada portal. Please refer to section: “3. Do we use cookies?” for further details about cookies and how do we use cookies details.</p>
            </div> <!-- Term-Group .// -->

            <div class="term-group"> <!-- Term-Group .// -->
                <h4 class="page-title">1. HOW DO WE PROTECT VISITOR'S INFORMATION?</h4>
                <p class="normal-content">We protect visitor's information by using variety of security measures. We assure your personal information will travel behind secured communication channel. Only limited, experienced authorized staff is working to maintain the safety of your personal confidential information.</p>
            </div> <!-- Term-Group .// -->

            <div class="term-group"> <!-- Term-Group .// -->
                <h4 class="page-title">2. DO WE DISCLOSE THE INFORMATION WE COLLECT TO OUTSIDE PARTIES?</h4>
                <p class="normal-content">We do not sell, trade, or otherwise transfer to outside parties your personally identifiable information unless we provide you with advance notice, except as described below. It also does not include website hosting partners and other parties who assist us in operating our website, conducting our business, or servicing you, so long as those parties agree to keep this information confidential. However, non-personally identifiable visitor information may be provided to other parties for marketing, advertising, or other uses.</p>
            </div> <!-- Term-Group .// -->

            <div class="term-group"> <!-- Term-Group .// -->
                <h4 class="page-title">3. DO WE USE COOKIES?</h4>
                <p class="normal-content">Yes, we use cookies, the cookies are small files that a site or its service provider transfers to your computer's hard drive through your web browser (if you allow) that enables the site's or service provider's systems to recognize our browser and capture and remember certain information. For instance, we use cookies to help us understand your preferences based on previous or current site activity, which enables us to provide you with improved services. We also use cookies so that we can offer better site experiences and tools in the future. You can choose to turn off all cookies. If you turn cookies off, you won't have access to many features that make your site experience more efficient.</p>
            </div> <!-- Term-Group .// -->

        </div> <!-- Box-Modal .// -->
    </div> <!-- Container .// -->
</section>

<!----------------------------
------Terms-&-Conditions------
----------------------------->

<?php include('include/footer.php'); ?>