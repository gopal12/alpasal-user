<?php include('include/header.php'); ?>

<nav aria-label="breadcrumb" class="breadcrumb-main bg-para" style="background: linear-gradient(rgba(0,0,0,0.4),rgba(0,0,0,0.4)), url('img/bread.jpg');">
    <div class="container clearfix">
        <h3 class="float-left">About Us</h3>
        <ol class="breadcrumb float-right">
            <li class="breadcrumb-item"><a href="index.php">Home</a></li>
            <!-- <li class="breadcrumb-item"><a href="#">Library</a></li> -->
            <li class="breadcrumb-item active" aria-current="page">About</li>
        </ol>
    </div>
</nav>

<section class="message-ceo common-padding">
    <div class="container">
        <div class="row no-gutters">
            <div class="col-lg-7"> 
                <div class="message-image-wrap">
                    <img src="img/ceo.jpg" alt="CEO">
                </div> 
            </div>
            <div class="col-lg-5"> 
                <div class="h-100 d-flex align-items-center">
                    <div class="message-content-wrap">
                        <h3 class="section-title">Message From CEO</h3>
                        <p class="normal-content">In any organization that you choose to work with, we always believe that the key to success is looking for an organization that strives for a mutually beneficial long term partnership. AlPasal has been providing services to our clients for more than 4 years, and, our key to success is to focus on continuous improvement, process streamlining, and, a consultative approach to market solutions where the main objective is to help exceed the expectation of our clients for them to grow with us. </p>
                        <p class="ceo-name text-center">- Jabindra Panthi</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="about-mission"> 
    <div class="container">
        <div class="row no-gutters">
            <div class="col-lg-4 col-sm-6 col-xs-12">
                <div class="mission-vision-wrap" data-mh="mission">
                    <div class="image-overlay">
                        <img src="img/what-we-do-01.png" alt="Image">
                    </div>
                    <div class="mission-img">
                        <img src="img/what-we-do-01.png" alt="Image">
                    </div>
                    <h4>What we Really do</h4>
                    <p class="normal-content">Lorem ipsum dolor sit amet consectetur adipisicing elit. Vel modi tempore temporibus recusandae molestiae aut quas enim quae asperiores labore, nisi suscipit quisquam. Eaque quas eveniet labore impedit maxime quo ex dicta fugiat eos pariatur.</p>
                </div>
            </div>
            <div class="col-lg-4 col-sm-6 col-xs-12">
                <div class="mission-vision-wrap" data-mh="mission">
                    <div class="image-overlay">
                        <img src="img/our-vision-03.png" alt="Image">
                    </div>
                    <div class="mission-img">
                        <img src="img/our-vision-03.png" alt="Image">
                    </div>
                    <h4>Our Mission</h4>
                    <p class="normal-content">Lorem ipsum dolor sit amet consectetur adipisicing elit. Vel modi tempore temporibus recusandae molestiae aut quas enim quae asperiores labore, nisi suscipit quisquam. Eaque quas eveniet labore impedit maxime quo ex dicta fugiat eos pariatur.</p>
                </div>
            </div>
            <div class="col-lg-4 col-sm-6 col-xs-12">
                <div class="mission-vision-wrap" data-mh="mission">
                    <div class="image-overlay">
                        <img src="img/history-begining-02.png" alt="Image">
                    </div>
                    <div class="mission-img">
                        <img src="img/history-begining-02.png" alt="Image">
                    </div>
                    <h4>History of begining</h4>
                    <p class="normal-content">Lorem ipsum dolor sit amet consectetur adipisicing elit. Vel modi tempore temporibus recusandae molestiae aut quas enim quae asperiores labore, nisi suscipit quisquam. Eaque quas eveniet labore impedit maxime quo ex dicta fugiat eos pariatur.</p>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="our-team common-padding">
    <div class="container">
        <h3 class="section-title">Board Members</h3>
        <div class="owl-carousel side-arr">
            <div class="item">
                <div class="team-wrap" data-mh="team">
                    <div class="team-img">
                        <img src="img/team/team1.jpg" alt="Team">
                        <div class="team-social-wrap">
                            <ul class="mini-comp-logo list-unstyled text-center">
                                <li><a href="#" class="facebook"><i class="fab fa-facebook-f"></i></a></li>
                                <li><a href="#" class="twitter"><i class="fab fa-twitter"></i></a></li>
                                <li><a href="#" class="instagram"><i class="fab fa-instagram"></i></a></li>
                                <li><a href="#" class="linkedin"><i class="fab fa-linkedin-in"></i></a></li>
                            </ul>
                        </div>
                    </div>
                    <div class="team-content text-center">
                        <h5>Jabindra Panthi</h5>
                        <p>Director</p>
                    </div>
                </div>
            </div>
            <div class="item">
                <div class="team-wrap" data-mh="team">
                    <div class="team-img">
                        <img src="img/team/team3.jpg" alt="Team">
                        <div class="team-social-wrap">
                            <ul class="mini-comp-logo list-unstyled text-center">
                                <li><a href="#" class="facebook"><i class="fab fa-facebook-f"></i></a></li>
                                <li><a href="#" class="twitter"><i class="fab fa-twitter"></i></a></li>
                                <li><a href="#" class="instagram"><i class="fab fa-instagram"></i></a></li>
                                <li><a href="#" class="linkedin"><i class="fab fa-linkedin-in"></i></a></li>
                            </ul>
                        </div>
                    </div>
                    <div class="team-content text-center">
                        <h5>Karan Khanal</h5>
                        <p>CEO</p>
                    </div>
                </div>
            </div>
            <div class="item">
                <div class="team-wrap" data-mh="team">
                    <div class="team-img">
                        <img src="img/team/team2.jpg" alt="Team">
                        <div class="team-social-wrap">
                            <ul class="mini-comp-logo list-unstyled text-center">
                                <li><a href="#" class="facebook"><i class="fab fa-facebook-f"></i></a></li>
                                <li><a href="#" class="twitter"><i class="fab fa-twitter"></i></a></li>
                                <li><a href="#" class="instagram"><i class="fab fa-instagram"></i></a></li>
                                <li><a href="#" class="linkedin"><i class="fab fa-linkedin-in"></i></a></li>
                            </ul>
                        </div>
                    </div>
                    <div class="team-content text-center">
                        <h5>Saugat Jonchhen</h5>
                        <p>Project Manager</p>
                    </div>
                </div>
            </div>
            <div class="item">
                <div class="team-wrap" data-mh="team">
                    <div class="team-img">
                        <img src="img/team/team4.jpg" alt="Team">
                        <div class="team-social-wrap">
                            <ul class="mini-comp-logo list-unstyled text-center">
                                <li><a href="#" class="facebook"><i class="fab fa-facebook-f"></i></a></li>
                                <li><a href="#" class="twitter"><i class="fab fa-twitter"></i></a></li>
                                <li><a href="#" class="instagram"><i class="fab fa-instagram"></i></a></li>
                                <li><a href="#" class="linkedin"><i class="fab fa-linkedin-in"></i></a></li>
                            </ul>
                        </div>
                    </div>
                    <div class="team-content text-center">
                        <h5>Gopal Basnet</h5>
                        <p>Frontend Developer</p>
                    </div>
                </div>
            </div>
            <div class="item">
                <div class="team-wrap" data-mh="team">
                    <div class="team-img">
                        <img src="img/team/team5.jpg" alt="Team">
                        <div class="team-social-wrap">
                            <ul class="mini-comp-logo list-unstyled text-center">
                                <li><a href="#" class="facebook"><i class="fab fa-facebook-f"></i></a></li>
                                <li><a href="#" class="twitter"><i class="fab fa-twitter"></i></a></li>
                                <li><a href="#" class="instagram"><i class="fab fa-instagram"></i></a></li>
                                <li><a href="#" class="linkedin"><i class="fab fa-linkedin-in"></i></a></li>
                            </ul>
                        </div>
                    </div>
                    <div class="team-content text-center">
                        <h5>Asmin Ghale</h5>
                        <p>Actor</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="our-team common-padding no-top">
    <div class="container">
        <h3 class="section-title">Development Team</h3> 
        <div class="owl-carousel side-arr">
            <div class="item"> 
                <div class="team-wrap" data-mh="team">
                    <div class="team-img">
                        <img src="img/team/team1.jpg" alt="Team">
                        <div class="team-social-wrap">
                            <ul class="mini-comp-logo list-unstyled text-center">
                                <li><a href="#" class="facebook"><i class="fab fa-facebook-f"></i></a></li>
                                <li><a href="#" class="twitter"><i class="fab fa-twitter"></i></a></li>
                                <li><a href="#" class="instagram"><i class="fab fa-instagram"></i></a></li>
                                <li><a href="#" class="linkedin"><i class="fab fa-linkedin-in"></i></a></li>
                            </ul>
                        </div>
                    </div>
                    <div class="team-content text-center">
                        <h5>Jabindra Panthi</h5>
                        <p>Director</p>
                    </div>
                </div> 
            </div>
            <div class="item"> 
                <div class="team-wrap" data-mh="team">
                    <div class="team-img">
                        <img src="img/team/team3.jpg" alt="Team">
                        <div class="team-social-wrap">
                            <ul class="mini-comp-logo list-unstyled text-center">
                                <li><a href="#" class="facebook"><i class="fab fa-facebook-f"></i></a></li>
                                <li><a href="#" class="twitter"><i class="fab fa-twitter"></i></a></li>
                                <li><a href="#" class="instagram"><i class="fab fa-instagram"></i></a></li>
                                <li><a href="#" class="linkedin"><i class="fab fa-linkedin-in"></i></a></li>
                            </ul>
                        </div>
                    </div>
                    <div class="team-content text-center">
                        <h5>Karan Khanal</h5>
                        <p>CEO</p>
                    </div>
                </div>
            </div>
            <div class="item">
                <div class="team-wrap" data-mh="team">
                    <div class="team-img">
                        <img src="img/team/team2.jpg" alt="Team">
                        <div class="team-social-wrap">
                            <ul class="mini-comp-logo list-unstyled text-center">
                                <li><a href="#" class="facebook"><i class="fab fa-facebook-f"></i></a></li>
                                <li><a href="#" class="twitter"><i class="fab fa-twitter"></i></a></li>
                                <li><a href="#" class="instagram"><i class="fab fa-instagram"></i></a></li>
                                <li><a href="#" class="linkedin"><i class="fab fa-linkedin-in"></i></a></li>
                            </ul>
                        </div>
                    </div>
                    <div class="team-content text-center">
                        <h5>Saugat Jonchhen</h5>
                        <p>Project Manager</p>
                    </div>
                </div> 
            </div>
            <div class="item">
                <div class="team-wrap" data-mh="team">
                    <div class="team-img">
                        <img src="img/team/team4.jpg" alt="Team">
                        <div class="team-social-wrap">
                            <ul class="mini-comp-logo list-unstyled text-center">
                                <li><a href="#" class="facebook"><i class="fab fa-facebook-f"></i></a></li>
                                <li><a href="#" class="twitter"><i class="fab fa-twitter"></i></a></li>
                                <li><a href="#" class="instagram"><i class="fab fa-instagram"></i></a></li>
                                <li><a href="#" class="linkedin"><i class="fab fa-linkedin-in"></i></a></li>
                            </ul>
                        </div>
                    </div>
                    <div class="team-content text-center">
                        <h5>Gopal Basnet</h5>
                        <p>Frontend Developer</p>
                    </div>
                </div>
            </div>
            <div class="item">
                <div class="team-wrap" data-mh="team">
                    <div class="team-img">
                        <img src="img/team/team14.jpg" alt="Team">
                        <div class="team-social-wrap">
                            <ul class="mini-comp-logo list-unstyled text-center">
                                <li><a href="#" class="facebook"><i class="fab fa-facebook-f"></i></a></li>
                                <li><a href="#" class="twitter"><i class="fab fa-twitter"></i></a></li>
                                <li><a href="#" class="instagram"><i class="fab fa-instagram"></i></a></li>
                                <li><a href="#" class="linkedin"><i class="fab fa-linkedin-in"></i></a></li>
                            </ul>
                        </div>
                    </div>
                    <div class="team-content text-center">
                        <h5>Sunny Deol</h5>
                        <p>Actor</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<?php include('include/footer.php'); ?>