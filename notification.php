<?php include('include/header.php'); ?>

 <nav aria-label="breadcrumb" class="breadcrumb-main bg-para" style="background: linear-gradient(rgba(0,0,0,0.4),rgba(0,0,0,0.4)), url('img/bread.jpg');">
    <div class="container-fluid clearfix"> <!-- Container .// -->
        <h3 class="float-left">Notifications</h3>
        <ol class="breadcrumb float-right">
            <li class="breadcrumb-item"><a href="index.php">Home</a></li>
            <li class="breadcrumb-item active" aria-current="page">Notifications</li>
        </ol>
    </div> <!-- Container .// -->
</nav>


<section class="user-dashboard">
    <div class="container-fluid"> 
        <div class="row"> 
            <div class="col-lg-3">
                <div class="left-side-user-dashboard"> 
                    <div class="header">
                        <div class="prof_who">
                            <div class="img_holder">
                                <img src="img/user/user-profile.jpg" class="img-fluid" alt="">
                            </div>
                            <div class="title_holder"> 
                                <span>Welcome</span>
                                <h3>Gopal Basnet</h3> 
                            </div>
                        </div>
                    </div>
                    <div class="content nav flex-column nav-pills" role="tablist" aria-orientation="vertical">
                        <ul class="nav"> 
                            <li class="label">Update User Profile</li>
                            <li class="left-nav"><a class="nav-link " href="update-info.php">Update Info</a></li>
                            <li class="left-nav"><a class="nav-link" href="request-area.php">Request Area</a></li>
                            <li class="left-nav"><a class= "nav-link" href="change-password.php">Change Password</a></li>
                            <li class="left-nav"><a class= "nav-link" href="shipping-address.php">Shipping Address</a></li>
                            <li class="left-nav"><a class= "nav-link " href="wishlist.php">WishList</a></li>
                            <li class="left-nav"><a class= "nav-link active" href="notification.php">Notifications</a></li>
                            <li class="left-nav"><a class= "nav-link" href="#">Log Out</a></li>
                        </ul>
                    </div>
                </div> 
            </div>
            <div class="col-lg-9">
                <div class="right-user-dashboard">
                    <div class="tab-pane fade fade show active">
                        <div class="add-product-right-side">  
                            <h3 class="add-product-title">Notification List</h3> 
                            <a href="" class="notification-items read">
                                <div class="row d-row-noti">
                                    <div class="col-lg-1">
                                        <figure class="notification-img">
                                            <img src="img/comment-2.png" class="img_fluid" alt="">
                                        </figure> 
                                    </div>
                                    <div class="col-lg-11 pad-l-0">
                                        <div class="notification-info">
                                            <p>
                                                Lorem ipsum dolor sit amet, consectetur adipisicing elit. Nihil nam fuga architecto.
                                            </p>
                                            <span><i class="far fa-clock"></i>1 min ago</span>
                                        </div> 
                                    </div>
                                </div> 
                            </a>
                            <a href="" class="notification-items read">
                                <div class="row d-row-noti">
                                    <div class="col-lg-1">
                                        <figure class="notification-img">
                                            <img src="img/comment-1.png" class="img_fluid" alt="">
                                        </figure> 
                                    </div>
                                    <div class="col-lg-11 pad-l-0">
                                        <div class="notification-info">
                                            <p>
                                                Lorem ipsum dolor sit amet, consectetur adipisicing elit. Nihil nam fuga architecto.
                                            </p>
                                            <span><i class="far fa-clock"></i>1 min ago</span>
                                        </div> 
                                    </div>
                                </div> 
                            </a>  
                            <a href="" class="notification-items unread">
                                <div class="row d-row-noti">
                                    <div class="col-lg-1">
                                        <figure class="notification-img">
                                            <img src="img/comment-2.png" class="img_fluid" alt="">
                                        </figure> 
                                    </div>
                                    <div class="col-lg-11 pad-l-0">
                                        <div class="notification-info">
                                            <p>
                                                Lorem ipsum dolor sit amet, consectetur adipisicing elit. Nihil nam fuga architecto.
                                            </p>
                                            <span><i class="far fa-clock"></i>1 min ago</span>
                                        </div> 
                                    </div>
                                </div>
                            </a> 
                            <a href="" class="notification-items unread">
                                <div class="row d-row-noti">
                                    <div class="col-lg-1">
                                        <figure class="notification-img">
                                            <img src="img/comment-1.png" class="img_fluid" alt="">
                                        </figure> 
                                    </div>
                                    <div class="col-lg-11 pad-l-0">
                                        <div class="notification-info">
                                            <p>
                                                Lorem ipsum dolor sit amet, consectetur adipisicing elit. Nihil nam fuga architecto.
                                            </p>
                                            <span><i class="far fa-clock"></i>1 min ago</span>
                                        </div> 
                                    </div>
                                </div> 
                            </a>        
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div> 
</section>





<?php include('include/footer.php'); ?>